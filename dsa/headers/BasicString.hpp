#ifndef __STRING_HPP__
#define __STRING_HPP__

#include <iostream>

namespace dsa {

template <typename CharT> class BasicString; /// forwar declaration

/// BasicString concatenation. A global function, not a member function.
template <typename CharT>
BasicString<CharT> operator+(const BasicString<CharT>& s1, const BasicString<CharT>& s2);

template <typename CharT>
BasicString<CharT> operator+(const CharT* s1, const BasicString<CharT>& s2);

template <typename CharT>
BasicString<CharT> operator+(const BasicString<CharT>& s1, const CharT* s2);

template <typename CharT>
BasicString<CharT> operator+(CharT c, const BasicString<CharT>& s2);

template <typename CharT>
BasicString<CharT> operator+(const BasicString<CharT>& s1, CharT c);

/// BasicString equality. A global function, not a member function.
template <typename CharT>
bool operator==(const BasicString<CharT>& s1, const BasicString<CharT>& s2);

template <typename CharT>
bool operator==(const CharT* s1, const BasicString<CharT>& s2);

template <typename CharT>
bool operator==(const BasicString<CharT>& s1, const CharT* s2);

/// BasicString inequality. A global function, not a member function.
template <typename CharT>
bool operator!=(const BasicString<CharT>& s1, const BasicString<CharT>& s2);

template <typename CharT>
bool operator!=(const CharT* s1, const BasicString<CharT>& s2);

template <typename CharT>
bool operator!=(const BasicString<CharT>& s1, const CharT* s2);

template <typename CharT>
bool operator<(const BasicString<CharT>& s1, const BasicString<CharT>& s2);

/// BasicString comparison. A global function, not a member function.
template <typename CharT>
bool operator<(const CharT* s1, const BasicString<CharT>& s2);

template <typename CharT>
bool operator<(const BasicString<CharT>& s1, const CharT* s2);

/// Swaps the contents of two strings.
template <typename CharT>
void swap(BasicString<CharT>& s1, BasicString<CharT>& s2);

/// Reads s from the input stream is
template <typename CharT>
std::basic_istream<CharT>& operator>>(std::basic_istream<CharT>& is, BasicString<CharT>& s);

/// Writes s to the output stream os
template <typename CharT>
std::basic_ostream<CharT>& operator<<(std::basic_ostream<CharT>& os, const BasicString<CharT>& s);

/// Reads a string from the input stream is, stopping when it reaches delim
template <typename CharT>
std::basic_istream<CharT>& getline(std::basic_istream<CharT>& is, BasicString<CharT>& str, CharT delim);

template <typename CharT>
std::basic_istream<CharT>& getline(std::basic_istream<CharT>& is, BasicString<CharT>& str);

template <typename CharT>
class BasicString
{
    template <typename T>
    friend std::basic_istream<T>& operator>>(std::basic_istream<T>& is, BasicString<T>& s);
    template <typename T> 
    friend std::basic_ostream<T>& operator<<(std::basic_ostream<T>& os, const BasicString<T>& s);
public:
    typedef CharT value_type; /// the type of object stored in BasicString (char, wchar_t etc.)
    typedef value_type* pointer; /// pointer to CharT
    typedef value_type& reference; /// reference to CharT
    typedef const value_type& const_reference; /// const reference to CharT
    typedef size_t size_type; /// unsigned integral type
    typedef std::ptrdiff_t difference_type; /// a signed integral type

    static const size_type npos = -1; /// the possible largest value of type size_type, (size_type(-1)), spcific to BasicString

private:
    static const size_type init_cap = 16;

public:
    /// const iterator used to iterate through a BasicString
    class const_iterator 
    {
        friend class BasicString<value_type>;
    public:
        const_iterator(); /// default constructor;
        const_iterator(const const_iterator& rhv); /// copy constructor
        ~const_iterator(); /// destructor;
    };

    /// iterator used to itarate through a BasicString.
    /// iterator can be const_iterator, but not vice versa.
    class iterator : public const_iterator 
    {
        friend class BasicString<value_type>;
    public:
        iterator(); /// default constructor
        iterator(const iterator& rhv); /// copy constructor
        iterator(const const_iterator& rhv); /// cast constructor
        ~iterator(); /// destructor;
    };

    /// const reverse iterator used to iterate backwards through a BasicString
    class const_reverse_iterator
    {
        friend class BasicString<value_type>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();
    };

    /// reverse iterator used to itarate backwards through a BasicString.
    /// reverse iterator can be const_reverse_iterator, but not vice versa.
    class reverse_iterator : public const_reverse_iterator
    {
        friend class BasicString<value_type>;
    public:
        reverse_iterator();
        reverse_iterator(const reverse_iterator& rhv);
        reverse_iterator(const const_reverse_iterator& rhv);
        ~reverse_iterator();
    };

public:
    BasicString(); /// Default constructor
    BasicString(const BasicString& rhv, size_type pos = 0, size_type n = npos); /// Generalization of the copy constructor
    BasicString(const value_type* rhv); /// Construct a BasicString from a null-terminated character array.
    BasicString(const value_type* rhv, size_type num); /// Construct a string from a character array and a length.
    BasicString(size_type num, value_type c); /// Create a string with n copies of c.
    template <typename InputIterator>
    BasicString(InputIterator first, InputIterator last); /// Create a string from a range.
    ~BasicString(); /// The destructor.

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;

    size_type size() const; /// returns the size of the string
    size_type length() const; /// Synonym for size()
    size_type max_size() const; /// returns the largest possible size of string
    size_type capacity() const; /// Number of elements for which memory has been allocated.
    /// That is, the size to which the string can grow before memory must be reallocated. capacity() is always greater than or equal to size().
    bool empty() const; /// true if the string's size is 0

    reference operator[](size_type n); /// returns the n'th character
    const_reference operator[](size_type n) const; /// returns the n'th character

    const value_type* c_str() const; /// Returns a pointer to a null-terminated array of characters representing the string's contents.
    const value_type* data() const; /// Returns a pointer to an array of characters (not necessarily null-terminated) representing the string's contents.

    BasicString& operator=(const BasicString& rhv); /// Assingnment operator
    BasicString& operator=(const value_type* rhv); /// Assign a null-terminated character array to a BasicString. (specific to BasicString)
    BasicString& operator=(value_type c); /// Assign a single character to a BasicString. (specific to BasicString)

    void reserve(size_type size);
    void swap(BasicString& rhv);

    iterator insert(iterator pos, const value_type& c); /// inserts c before pos
    template <typename InputIterator>
    void         insert(iterator  pos, InputIterator first, InputIterator last); /// inserts the range before pos
    void         insert(iterator  pos, size_type num, const value_type& c); /// inserts n copies of c before pos
    BasicString& insert(size_type pos, const BasicString& str); /// insterts str before pos
    BasicString& insert(size_type pos, const BasicString& str, size_type pos1, size_type num); /// inserts a substring of str befor pos
    BasicString& insert(size_type pos, const value_type* str); /// Inserts str before pos.
    BasicString& insert(size_type pos, const value_type* str, size_type num); /// Inserts the first num characters of str before pos.
    BasicString& insert(size_type pos, size_type num, value_type c); /// 	Inserts num copies of c before pos.

    BasicString& append(const BasicString& str); /// append str to *this
    BasicString& append(const BasicString& str, size_type pos, size_type num); /// append a substring of str to *this
    BasicString& append(const value_type* str); /// append str to *this
    BasicString& append(const value_type* str, size_type num); /// append first num character of str to *this
    BasicString& append(size_type num, value_type c); /// append num copies of c tho *this
    template <typename InputIterator>
    BasicString& append(InputIterator first, InputIterator last); /// append a range to *this

    void push_back(value_type c); /// append a single character to *this

    BasicString& operator+=(const BasicString& str); /// Equivalent to append(s)
    BasicString& operator+=(const value_type* str); /// Equivalent to append(s)
    BasicString& operator+=(value_type c); /// Equivalent to push_back(c)

    iterator erase(iterator pos); /// erases the character at position pos
    iterator erase(iterator first, iterator last); /// erases the range
    BasicString&  erase(size_type pos = 0, size_type n = npos); /// erases a range

    void clear(); /// erase the entire container

    void resize(size_type num, value_type c = value_type()); /// Appends characters, or erases characters from the end,
    /// as necessary to make the string's length exactly n characters.

    BasicString& assign(const BasicString& str); /// Synonym for operator=
    BasicString& assign(const BasicString& str, size_type pos, size_type num); ///Assigns a substring of s to *this
    BasicString& assign(const value_type* str, size_type num); /// Assigns the first n characters of s to *this.
    BasicString& assign(const value_type* str); /// Assigns a null-terminated array of characters to *this.
    BasicString& assign(size_type num, value_type c); /// Erases the existing characters and replaces them by n copies of c.
    template <typename InputIterator>
    BasicString& assign(InputIterator first, InputIterator last); /// Erases the existing characters and replaces them by [first, last)

    BasicString& replace(size_type pos, size_type num, const BasicString& str); /// Replaces a substring of *this with the string s.
    BasicString& replace(size_type pos, size_type num, const BasicString& str, size_type pos1, size_type num1); /// Replaces a substring of *this with a substring of s.
    BasicString& replace(size_type pos, size_type num, const value_type* str, size_type num1); /// Replaces a substring of *this with the first n1 characters of s.
    BasicString& replace(size_type pos, size_type num, const value_type* str); /// Replaces a substring of *this with a null-terminated character array.
    BasicString& replace(size_type pos, size_type num, size_type num1, value_type c); /// Replaces a substring of *this with n1 copies of c.
    BasicString& replace(iterator first, iterator last, const BasicString& str); /// Replaces a substring of *this with the string s.
    BasicString& replace(iterator first, iterator last, const value_type* str, size_type num); /// Replaces a substring of *this with the first n characters of s.
    BasicString& replace(iterator first, iterator last, const value_type* str); /// Replaces a substring of *this with a null-terminated character array.
    BasicString& replace(iterator first, iterator last, size_type num, value_type c); /// Replaces a substring of *this with n copies of c.
    template <typename InputIterator>
    BasicString& replace(iterator first, iterator last, InputIterator f, InputIterator l); /// Replaces a substring of *this with the range [f, l)

    /// Copies a substring of *this to a buffer.
    size_type copy(value_type* buf, size_type num, size_type pos = 0) const;
    /// Searches for s as a substring of *this, beginning at character pos of *this.
    size_type find(const BasicString& str, size_type pos = 0) const;
    /// Searches for the first n characters of s as a substring of *this, beginning at character pos of *this.
    size_type find(const value_type* str, size_type pos, size_type num) const;
    /// Searches for a null-terminated character array as a substring of *this, beginning at character pos of *this.
    size_type find(const value_type* str, size_type pos = 0) const;
    /// Searches for the character c, beginning at character position pos.
    size_type find(value_type c, size_type pos = 0) const;

    /// Searches backward for s as a substring of *this, beginning at character position min(pos, size())
    size_type rfind(const BasicString& str, size_type pos = npos) const;
    /// Searches backward for the first n characters of s as a substring of *this, beginning at character position min(pos, size())
    size_type rfind(const value_type* str, size_type pos, size_type num) const;
    /// Searches backward for a null-terminated character array as a substring of *this, beginning at character min(pos, size())
    size_type rfind(const value_type* str, size_type pos = npos) const;
    /// Searches backward for the character c, beginning at character position min(pos, size().
    size_type rfind(value_type c, size_type pos = npos) const;

    /// Searches within *this, beginning at pos, for the first character that is equal to any character within s.
    size_type find_first_of(const BasicString& str, size_type pos = 0) const;
    /// Searches within *this, beginning at pos, for the first character that is equal to any character within the first n characters of s.
    size_type find_first_of(const value_type* str, size_type pos, size_type n) const;
    /// Searches within *this, beginning at pos, for the first character that is equal to any character within s.
    size_type find_first_of(const value_type* str, size_type pos = 0) const;
    /// Searches within *this, beginning at pos, for the first character that is equal to c.
    size_type find_first_of(value_type c, size_type pos = 0) const;

    /// Searches within *this, beginning at pos, for the first character that is not equal to any character within s.
    size_type find_first_not_of(const BasicString& str, size_type pos = 0) const;
    /// Searches within *this, beginning at pos, for the first character that is not equal to any character within the first n characters of s.
    size_type find_first_not_of(const value_type* str, size_type pos, size_type n) const;
    /// Searches within *this, beginning at pos, for the first character that is not equal to any character within s.
    size_type find_first_not_of(const value_type* str, size_type pos = 0) const;
    /// Searches within *this, beginning at pos, for the first character that is not equal to c.
    size_type find_first_not_of(value_type c, size_type pos = 0) const;

    /// Searches backward within *this, beginning at min(pos, size()), for the first character that is equal to any character within s.
    size_type find_last_of(const BasicString& str, size_type pos = npos) const;
    /// Searches backward within *this, beginning at min(pos, size()), for the first character that is equal to any character within the first n characters of s.
    size_type find_last_of(const value_type* str, size_type pos, size_type num) const;
    /// Searches backward *this, beginning at min(pos, size()), for the first character that is equal to any character within s.
    size_type find_last_of(const value_type* str, size_type pos = npos) const;
    /// Searches backward *this, beginning at min(pos, size()), for the first character that is equal to c.
    size_type find_last_of(value_type c, size_type pos = npos) const;

    /// Searches backward within *this, beginning at min(pos, size()), for the first character that is not equal to any character within s.
    size_type find_last_not_of(const BasicString& str, size_type pos = npos) const;
    /// Searches backward within *this, beginning at min(pos, size()), for the first character that is not equal to any character within the first n characters of s.
    size_type find_last_not_of(const value_type* str, size_type pos, size_type num) const;
    /// Searches backward *this, beginning at min(pos, size()), for the first character that is not equal to any character within s.
    size_type find_last_not_of(const value_type* str, size_type pos = npos) const;
    /// Searches backward *this, beginning at min(pos, size()), for the first character that is not equal to c.
    size_type find_last_not_of(value_type c, size_type pos = npos) const;

    BasicString substr(size_type pos = 0, size_type n = npos) const; /// returns a substring of *this
    int compare(const BasicString& str) const; /// Three-way lexicographical comparison of str and *this.
    int compare(const BasicString& str, size_type pos = 0, size_type n = npos) const; /// Three-way lexicographical comparison of s and a substring of *this.
    ///Three-way lexicographical comparison of a substring of s and a substring of *this.
    int compare(size_type pos, size_type num, const BasicString& str, size_type pos1, size_type num1) const;
    int compare(const value_type* str) const; /// Three-way lexicographical comparison of s and *this.
    /// Three-way lexicographical comparison of the first min(len::length(s) characters of s and a substring of *this.
    int compare(size_type pos, size_type num, const value_type* str, size_type len = npos) const;

private:
    void allocateMemory(const size_type size = 0, const size_type cap = init_cap);
    void copyData(const BasicString& rhv, const size_type pos, const size_type size);
    void copyData(const value_type* rhv, const size_type num);
    void copyData(const value_type c, const size_type num);

private:
    value_type* begin_;
    value_type* end_;
    value_type* bufferEnd_;

}; /// class BasicString

typedef BasicString<char> String;
typedef BasicString<wchar_t> WString;

} /// namespace dsa

#include "sources/BasicString.cpp"

#endif /// __STRING_HPP__


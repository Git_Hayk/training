#ifndef __SINGLE_LIST_HPP__
#define __SINGLE_LIST_HPP__

#include <iostream>
#include <limits.h>

namespace cd06 {

template <typename T> class SingleList; /// forward declaration

template <typename T>
bool operator==(const SingleList<T>& lhv, const SingleList<T>& rhv);
template <typename T>
bool operator!=(const SingleList<T>& lhv, const SingleList<T>& rhv);
template <typename T>
bool operator< (const SingleList<T>& lhv, const SingleList<T>& rhv);
template <typename T>
bool operator> (const SingleList<T>& lhv, const SingleList<T>& rhv);
template <typename T>
bool operator<=(const SingleList<T>& lhv, const SingleList<T>& rhv);
template <typename T>
bool operator>=(const SingleList<T>& lhv, const SingleList<T>& rhv);

template <typename T>
class SingleList
{
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

private:
    struct Node {
        Node() {}
        Node(const_reference d, Node* n = NULL)
            : data_(d), next_(n) {}
        value_type data_;
        Node* next_;
    };

public:
    class const_iterator {
        friend class SingleList<value_type>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();

        const const_iterator& operator=(const const_iterator& rhv);

        const_reference operator*() const;
        const Node* operator->() const;

        const_iterator operator++();
        const_iterator operator++(int);

        bool operator==(const_iterator rhv) const;
        bool operator!=(const_iterator rhv) const;
    protected:
        Node* get() const;
    private:
        explicit const_iterator(Node* p);
    private:
        Node* ptr_;
    };

    class iterator : public const_iterator {
        friend class SingleList<value_type>;
    public:
        iterator();
        iterator(const const_iterator& rhv);
        iterator(const iterator& rhv);
        ~iterator();

        reference operator*();
        Node* operator->();
    private:
        explicit iterator(Node* p);
    };

public:
    SingleList();
    SingleList(const size_type size);
    SingleList(const size_type size, const_reference init);
    SingleList(const SingleList<T>& rhv);
    template <typename InputIterator> SingleList(InputIterator first, InputIterator last);
    ~SingleList();

    const SingleList<T>& operator=(const SingleList<T>& rhv);

    void swap(SingleList<T>& rhv);
    size_type size() const;
    size_type max_size() const;

    bool empty() const;
    void clear();
    void resize(const size_type newSize, const_reference init = value_type());

    reference front();
    const_reference front() const;

    void push_front(const_reference value);
    void pop_front();

    const_iterator previous(const_iterator pos) const;
    iterator previous(iterator pos) const;

    const_iterator begin() const;
    const_iterator end() const;
    iterator begin();
    iterator end();

    iterator insert(iterator pos, const_reference value);
    void insert(iterator pos, const int n, const_reference value);
    template <typename InputIterator> void insert(iterator pos, InputIterator first, InputIterator last);

    iterator insert_after(iterator pos);
    iterator insert_after(iterator pos, const_reference value);
    void insert_after(iterator pos, const int n, const_reference value);
    template <typename InputIterator> void insert_after(iterator pos, InputIterator first, InputIterator last);

    iterator erase(iterator pos);
    iterator erase(iterator first, iterator last);
    iterator erase_after(iterator pos);

    void splice(iterator position, SingleList<T>& other);
    void splice(iterator position, SingleList<T>& other, iterator it);
    void splice(iterator position, SingleList<T>& other, iterator first, iterator last);

private:
    void create(const size_type size = 0, const_reference init = value_type());
    template <typename InputIterator> void copy(InputIterator rhv);
    bool is_begin(const iterator position) const;
    bool is_end(const iterator position) const;
    template <typename Integer> void initializeRange(const Integer size, const_reference init, std::true_type);
    template <typename InputIterator> void initializeRange(InputIterator first, InputIterator last, std::false_type);
private:
    Node* begin_;
    Node* end_;
};

} /// namespace cd06

#include "sources/SingleList.cpp"
#include "sources/SingleListIterator.cpp"

#endif ///__SINGLE_LIST_HPP__


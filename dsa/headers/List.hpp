#ifndef __LIST_HPP__
#define __LIST_HPP__

#include <iostream>
#include <limits.h>

namespace cd06 {

template <typename T> class List; /// forward declaration

template <typename T>
bool operator==(const List<T>& lhv, const List<T>& rhv);
template <typename T>
bool operator!=(const List<T>& lhv, const List<T>& rhv);
template <typename T>
bool operator< (const List<T>& lhv, const List<T>& rhv);
template <typename T>
bool operator> (const List<T>& lhv, const List<T>& rhv);
template <typename T>
bool operator<=(const List<T>& lhv, const List<T>& rhv);
template <typename T>
bool operator>=(const List<T>& lhv, const List<T>& rhv);

template <typename T>
class List
{
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

private:
    struct Node {
        Node() {}
        Node(const_reference d, Node* n = NULL, Node* p = NULL)
            : data_(d), prev_(p), next_(n) {}
        value_type data_;
        Node* prev_;
        Node* next_;
    };

public:
    class const_iterator {
        friend class List<value_type>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();

        const const_iterator& operator=(const const_iterator& rhv);

        const_reference operator*() const;
        const Node* operator->() const;

        const_iterator operator++();
        const_iterator operator++(int);
        const_iterator operator--();
        const_iterator operator--(int);

        bool operator==(const_iterator rhv) const;
        bool operator!=(const_iterator rhv) const;
    protected:
        Node* get() const;
    private:
        explicit const_iterator(Node* p);
    private:
        Node* ptr_;
    };

    class iterator : public const_iterator {
        friend class List<value_type>;
    public:
        iterator();
        iterator(const const_iterator& rhv);
        iterator(const iterator& rhv);
        ~iterator();

        reference operator*();
        Node* operator->();
    private:
        explicit iterator(Node* p);
    };

    class const_reverse_iterator {
        friend class List<value_type>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();

        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);

        const_reference operator*() const;
        const Node* operator->() const;

        const_reverse_iterator operator++();
        const_reverse_iterator operator++(int);
        const_reverse_iterator operator--();
        const_reverse_iterator operator--(int);

        bool operator==(const_reverse_iterator rhv) const;
        bool operator!=(const_reverse_iterator rhv) const;
    protected:
        Node* get() const;
    private:
        explicit const_reverse_iterator(Node* p);
    private:
        Node* ptr_;
    };

    class reverse_iterator : public const_reverse_iterator {
        friend class List<value_type>;
    public:
        reverse_iterator();
        reverse_iterator(const const_reverse_iterator& rhv);
        reverse_iterator(const reverse_iterator& rhv);
        ~reverse_iterator();

        reference operator*();
        Node* operator->();
    private:
        explicit reverse_iterator(Node* p);
    };

public:
    List();
    List(const size_type size);
    List(const size_type size, const_reference init);
    List(const List<T>& rhv);
    template <typename InputIterator> List(InputIterator first, InputIterator last);
    ~List();

    const List<T>& operator=(const List<T>& rhv);

    void swap(List<T>& rhv);
    size_type size() const;
    size_type max_size() const;

    bool empty() const;
    void clear();
    void resize(const size_type newSize, const_reference init = value_type());

    reference front();
    const_reference front() const;
    reference back();
    const_reference back() const;

    void push_front(const_reference value);
    void push_back(const_reference value);
    void pop_front();
    void pop_back();

    const_iterator begin() const;
    const_iterator end() const;
    iterator begin();
    iterator end();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    reverse_iterator rbegin();
    reverse_iterator rend();

    iterator insert(iterator pos, const_reference value);
    void insert(iterator pos, const int n, const_reference value);
    template <typename InputIterator> void insert(iterator pos, InputIterator first, InputIterator last);

    iterator insert_after(iterator pos);
    iterator insert_after(iterator pos, const_reference value);
    void insert_after(iterator pos, const int n, const_reference value);
    template <typename InputIterator> void insert_after(iterator pos, InputIterator first, InputIterator last);

    iterator erase(iterator pos);
    iterator erase(iterator first, iterator last);
    iterator erase_after(iterator pos);

    void splice(iterator position, List<T>& other);
    void splice(iterator position, List<T>& other, iterator it);
    void splice(iterator position, List<T>& other, iterator first, iterator last);

private:
    void create(const size_type size, const_reference init = value_type());
    template <typename InputIterator> void copy(InputIterator rhv);
    bool is_begin(const iterator position) const;
    bool is_end(const iterator position) const;
    template <typename Integer> void initializeRange(const Integer size, const_reference init, std::true_type);
    template <typename InputIterator> void initializeRange(InputIterator first, InputIterator last, std::false_type);
private:
    Node* begin_;
    Node* end_;
};

} /// namespace cd06

#include "sources/List.cpp"
#include "sources/ListIterator.cpp"
#include "sources/ListReverseIterator.cpp"

#endif ///__LIST_HPP__


#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__

#include <iostream>

namespace cd06 {

template <typename T, typename Sequence = cd06::List<T> >
class Queue : private Sequence
{
    template <typename T1, typename Sequence1>
    friend bool operator==(const Queue<T1, Sequence1>& lhv, const Queue<T1, Sequence1>& rhv);
    
    template <typename T1, typename Sequence1>
    friend bool operator==(const Queue<T1, Sequence1>& lhv, const Queue<T1, Sequence1>& rhv);

    template <typename T1, typename Sequence1>
    friend bool operator!=(const Queue<T1, Sequence1>& lhv, const Queue<T1, Sequence1>& rhv);

    template <typename T1, typename Sequence1>
    friend bool operator<(const Queue<T1, Sequence1>& lhv, const Queue<T1, Sequence1>& rhv);

    template <typename T1, typename Sequence1>
    friend bool operator>(const Queue<T1, Sequence1>& lhv, const Queue<T1, Sequence1>& rhv);

    template <typename T1, typename Sequence1>
    friend bool operator<=(const Queue<T1, Sequence1>& lhv, const Queue<T1, Sequence1>& rhv);

    template <typename T1, typename Sequence1>
    friend bool operator>=(const Queue<T1, Sequence1>& lhv, const Queue<T1, Sequence1>& rhv);
    
public:
    typedef typename Sequence::value_type value_type;
    typedef typename Sequence::size_type size_type;
public:
    Queue();
    Queue(const Queue& rhv);

    Queue& operator=(const Queue& rhv);
    
    bool empty() const;
    size_type size() const;

    value_type& front();
    const value_type& front() const;
    value_type& back();
    const value_type& back() const;

    void push(const value_type& d);
    void pop();
};

} /// namespace cd06

#include "sources/Queue.cpp"

#endif ///__QUEUE_HPP__


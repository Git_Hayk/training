#ifndef __STACK_HPP__
#define __STACK_HPP__

#include <iostream>

namespace cd06 {

template <typename T, typename Sequence = cd06::Vector<T> >
class Stack : private Sequence
{
    template <typename T1, typename Sequence1>
    friend bool operator==(const Stack<T1, Sequence1>& lhv, const Stack<T1, Sequence1>& rhv);
    
    template <typename T1, typename Sequence1>
    friend bool operator==(const Stack<T1, Sequence1>& lhv, const Stack<T1, Sequence1>& rhv);

    template <typename T1, typename Sequence1>
    friend bool operator!=(const Stack<T1, Sequence1>& lhv, const Stack<T1, Sequence1>& rhv);

    template <typename T1, typename Sequence1>
    friend bool operator<(const Stack<T1, Sequence1>& lhv, const Stack<T1, Sequence1>& rhv);

    template <typename T1, typename Sequence1>
    friend bool operator>(const Stack<T1, Sequence1>& lhv, const Stack<T1, Sequence1>& rhv);

    template <typename T1, typename Sequence1>
    friend bool operator<=(const Stack<T1, Sequence1>& lhv, const Stack<T1, Sequence1>& rhv);

    template <typename T1, typename Sequence1>
    friend bool operator>=(const Stack<T1, Sequence1>& lhv, const Stack<T1, Sequence1>& rhv);
    
public:
    typedef typename Sequence::value_type value_type;
    typedef typename Sequence::size_type size_type;
public:
    Stack();
    Stack(const Stack& rhv);

    Stack& operator=(const Stack& rhv);
    
    bool empty() const;
    size_type size() const;

    value_type& top();
    const value_type& top() const;

    void push(const value_type& d);
    void pop();
};

} /// namespace cd06

#include "sources/Stack.cpp"

#endif ///__STACK_HPP__


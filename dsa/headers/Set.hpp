#ifndef __SET_HPP__
#define __SET_HPP__

#include <iostream>
#include <utility>
#include <limits.h>
#include <iomanip>
#include <stack>
#include <queue>

namespace cd06 {

template <typename Data> class Set;

template <typename Data>
bool operator==(const Set<Data>& lhv, const Set<Data>& rhv);
template <typename Data>
bool operator!=(const Set<Data>& lhv, const Set<Data>& rhv);
template <typename Data>
bool operator< (const Set<Data>& lhv, const Set<Data>& rhv);
template <typename Data>
bool operator> (const Set<Data>& lhv, const Set<Data>& rhv);
template <typename Data>
bool operator<=(const Set<Data>& lhv, const Set<Data>& rhv);
template <typename Data>
bool operator>=(const Set<Data>& lhv, const Set<Data>& rhv);

template <typename Data>
class Set
{
public:
    typedef Data value_type;
    typedef Data key_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;
private:
    struct Node {
        Node(Data d = value_type(), Node* p = NULL, Node* l = NULL, Node* r = NULL)
            : data_(d), parent_(p), left_(l), right_(r) { }

        Data data_;
        Node* parent_;
        Node* left_;
        Node* right_;
    };
public:
    class const_iterator
    {
        friend class Set<value_type>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();

        const const_iterator& operator=(const const_iterator& rhv);
        const_reference operator*() const;
        pointer operator->() const;

        const_reference operator++();
        const_reference operator++(int);
        const_reference operator--();
        const_reference operator--(int);

        bool operator==(const_iterator rhv) const;
        bool operator!=(const_iterator rhv) const;
    private:
        explicit const_iterator(Node* p);
        Node* getPtr() const;
        const_iterator setPtr(Node* p);
        const_iterator setParent(const_iterator it);
        const_iterator setRight(const_iterator it);
        const_iterator setLeft(const_iterator it);
        const_iterator& goParent();
        const_iterator& goLeft();
        const_iterator& goRight();
        const_iterator parent() const;
        const_iterator left() const;
        const_iterator right() const;
        const_iterator firstLeftParent() const;
        const_iterator firstRightParent() const;
        void createLeft(const_reference value);
        void createRight(const_reference value);
        bool isNull() const;
        bool isLeftParent() const;
        bool isRightParent() const;
        int balanceDiff() const;
        int childCount(const_iterator parent) const;
    private:
        Node* ptr_;
    };

    class iterator : public const_iterator
    {
        friend class Set<value_type>;
    public:
        iterator() : const_iterator() {}
    private:
        explicit iterator(const_iterator rhv) : const_iterator(rhv) {}
        explicit iterator(Node* p) : const_iterator(p) {}
    };

    class const_reverse_iterator
    {
        friend class Set<value_type>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();

        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);
        const_reference operator*() const;
        pointer operator->() const;

        const_reference operator++();
        const_reference operator++(int);
        const_reference operator--();
        const_reference operator--(int);

        bool operator==(const_reverse_iterator rhv) const;
        bool operator!=(const_reverse_iterator rhv) const;
    private:
        explicit const_reverse_iterator(Node* p);
        Node* getPtr() const;
        const_reverse_iterator setPtr(Node* p);
        const_reverse_iterator setParent(const_reverse_iterator it);
        const_reverse_iterator setRight(const_reverse_iterator it);
        const_reverse_iterator setLeft(const_reverse_iterator it);
        const_reverse_iterator& goParent();
        const_reverse_iterator& goLeft();
        const_reverse_iterator& goRight();
        const_reverse_iterator parent() const;
        const_reverse_iterator left() const;
        const_reverse_iterator right() const;
        const_reverse_iterator firstLeftParent() const;
        const_reverse_iterator firstRightParent() const;
        void createLeft(const_reference value);
        void createRight(const_reference value);
        bool isNull() const;
        bool isLeftParent() const;
        bool isRightParent() const;
    private:
        Node* ptr_;
    };

    class reverse_iterator : public const_reverse_iterator
    {
        friend class Set<value_type>;
    public:
        reverse_iterator() : const_reverse_iterator() {}
    private:
        explicit reverse_iterator(Node* p) : const_reverse_iterator(p) {}
    };

public:
    Set();
    Set(const Set<Data>& rhv);
    template <typename InputIterator> Set(InputIterator first, InputIterator last);
    ~Set();

    const Set<value_type>& operator=(const Set<value_type>& rhv);
    void swap(Set<value_type>& rhv);
    size_type size() const;
    size_type max_size() const;
    bool empty() const;
    void clear();

    const_iterator root() const;
    const_iterator begin() const;
    const_iterator end() const;
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;

    std::pair<iterator, bool> insert(const_reference value);
    std::pair<iterator, bool> insert(iterator position, const_reference value);
    template <typename InputIterator>
    void insert(InputIterator first, InputIterator last);

    void erase(iterator position);
    void erase(iterator first, iterator last);

    iterator find(const key_type& key) const;
    iterator count(const key_type& key) const;

    iterator lower_bound(const key_type& key) const;
    iterator upper_bound(const key_type& key) const;

    std::pair<iterator, iterator> equal_range(const key_type& key) const;

    template <typename Functor>
    void preOrder(Functor visit);
    template <typename Functor>
    void inOrder(Functor visit);
    template <typename Functor>
    void postOrder(Functor visit);
    template <typename Functor>
    void levelOrder(Functor visit);

    template <typename Functor>
    void recursivePreOrder(Functor visit);
    template <typename Functor>
    void recursiveInOrder(Functor visit);
    template <typename Functor>
    void recursivePostOrder(Functor visit);
    template <typename Functor>
    void recursiveLevelOrder(Functor visit);

    void output() const;

private:
    std::pair<iterator, bool> inserter(iterator ptr, const_reference value = value_type());
    template <typename InputIterator>
    void range_inserter(InputIterator first, InputIterator last);

    void goUp(iterator& it, const_reference value);
    bool goDown(iterator& it, const_reference value);
    void balance(iterator& it);
    void rotateLeft(iterator& it);
    void rotateRight(iterator& it);
    void checkBeginEnd();

    template <typename Functor>
    void recursivePreOrderUtility(Functor visit, const_iterator it);
    template <typename Functor>
    void recursiveInOrderUtility(Functor visit, const_iterator it);
    template <typename Functor>
    void recursivePostOrderUtility(Functor visit, const_iterator it);
    template <typename Functor>
    void recursiveLevelOrderUtility(Functor visit, const_iterator it);

    void nextPreOrder(iterator& ptr);
    static void nextInOrder(iterator& ptr);
    static void nextPostOrder(iterator& ptr);
    static void prevPreOrder(iterator& ptr);
    static void prevInOrder(iterator& ptr);
    static void prevPostOrder(iterator& ptr);

    void outputSetUtility(const_iterator it, const size_t totalSpaces = 0) const;
private:
    Node* root_;
    Node* begin_;
    Node* end_;
    size_type size_;
};

} /// namespace cd06

#include "sources/Set.cpp"
#include "sources/ConstIterator.cpp"
#include "sources/ConstReverseIterator.cpp"

#endif /// __SET_HPP__


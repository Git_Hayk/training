#ifndef __T_VECTOR_HPP__
#define __T_VECTOR_HPP__

#include <iostream>
#include <limits.h>
#include <algorithm>
#include <cmath>

namespace cd06 {

template <typename T> class Vector;

template <typename T>
std::istream& operator>>(std::istream& in, typename Vector<T>::iterator it);

template <typename T>
std::ostream& operator<<(std::ostream& out, const typename Vector<T>::iterator it);

template <typename T>
std::istream& operator>>(std::istream& in, typename Vector<T>::reverse_iterator it);

template <typename T>
std::ostream& operator<<(std::ostream& out, const typename Vector<T>::reverse_iterator it);

template <typename T>
class Vector
{
public:
    static double RESERVE_COEFF;
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;
public:
    class const_iterator 
    {
        friend class Vector<value_type>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        ~const_iterator();

        const const_iterator& operator=(const const_iterator& rhv);

        const_reference operator*() const;
        const pointer operator->() const;

        const const_iterator operator++();    /// pre-incr
        const const_iterator operator++(int); /// post-incr
        const const_iterator operator--();    /// pre-decr
        const const_iterator operator--(int); /// post-decr
        
        const const_iterator operator+=(const size_type count);
        const const_iterator operator-=(const size_type count);
        const const_iterator operator+ (const size_type count);
        const const_iterator operator- (const size_type count);

        const_reference operator[](const size_type subscript) const;

        bool operator==(const_iterator rhv) const;
        bool operator!=(const_iterator rhv) const;
        bool operator< (const_iterator rhv) const;
        bool operator> (const_iterator rhv) const;
        bool operator<=(const_iterator rhv) const;
        bool operator>=(const_iterator rhv) const;
    protected:
        void setPtr(pointer p);
        pointer getPtr() const;
    private:
        const_iterator(pointer p);
    private:
        pointer ptr_;
    };
    
    class iterator : public const_iterator
    {
        friend class Vector<value_type>;
    public:
        iterator();
        iterator(const const_iterator& rhv); /// converter constructor
        iterator(const iterator& rhv);
        ~iterator();

        iterator& operator=(const iterator& rhv);
        iterator operator+(const size_type count);
        iterator operator-(const size_type count);
        reference operator*();
        pointer operator->();
        reference operator[](const size_type subscript);
    private:
        iterator(pointer p);
    };

    class const_reverse_iterator
    {
        friend class Vector<value_type>;
    public:
        const_reverse_iterator();
        const_reverse_iterator(const const_reverse_iterator& rhv);
        ~const_reverse_iterator();
    public:
        const const_reverse_iterator& operator=(const const_reverse_iterator& rhv);

        const_reference operator*() const;
        const pointer operator->() const;

        const const_reverse_iterator operator++();    /// pre-incr
        const const_reverse_iterator operator++(int); /// post-incr
        const const_reverse_iterator operator--();    /// pre-decr
        const const_reverse_iterator operator--(int); /// post-decr

        const const_reverse_iterator operator+=(const size_type count);
        const const_reverse_iterator operator-=(const size_type count);
        const const_reverse_iterator operator+ (const size_type count);
        const const_reverse_iterator operator- (const size_type count);

        const_reference operator[](const size_type subscript) const;

        bool operator==(const_reverse_iterator rhv) const;
        bool operator!=(const_reverse_iterator rhv) const;
        bool operator< (const_reverse_iterator rhv) const;
        bool operator> (const_reverse_iterator rhv) const;
        bool operator<=(const_reverse_iterator rhv) const;
        bool operator>=(const_reverse_iterator rhv) const;
    protected:
        void setPtr(pointer p);
        pointer getPtr() const;
    private:
        const_reverse_iterator(pointer p);
    private:
        pointer ptr_;
    };

    class reverse_iterator : public const_reverse_iterator
    {
        friend class Vector<value_type>;
    public:
        reverse_iterator();
        reverse_iterator(const const_reverse_iterator& rhv); /// converter constructor
        reverse_iterator(const reverse_iterator& rhv);
        ~reverse_iterator();

        reverse_iterator& operator=(const reverse_iterator& rhv);
        reverse_iterator operator+(const size_type count);
        reverse_iterator operator-(const size_type count);
        reference operator*();
        pointer operator->();
        reference operator[](const size_type subscript);
    private:
        reverse_iterator(pointer p);
    };

public:
    Vector();
    explicit Vector(const size_type size);
    explicit Vector(const size_type size, const_reference init);
    Vector(const Vector<T>& rhv);
    template <class InputIterator> Vector(InputIterator first, InputIterator last);
    ~Vector();

    size_type size() const;
    size_type max_size() const;
    size_type capacity() const;
    bool empty() const;
    void clear();

    void resize(const size_type size, const_reference init = T());
    void reserve(const size_type n);

    void push_back(const_reference element);
    void pop_back();

    iterator insert(iterator position, const_reference init);
    void insert(iterator posisiton, int n, const_reference init);
    template <class InputIterator> void insert(iterator posisiton, InputIterator first, InputIterator last);

    iterator erase(iterator position);
    iterator erase(iterator first, iterator last);

    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
    const_reverse_iterator rbegin() const;
    reverse_iterator rbegin();
    const_reverse_iterator rend() const;
    reverse_iterator rend();
    
    const_reference front() const; 
    reference front();
    const_reference back() const;
    reference back();

    void set(const size_t index, const_reference value);
    const_reference get(const size_t index) const;

    const Vector<T>& operator=(const Vector<T>& rhv);
    reference operator[](const size_t index);
    const_reference operator[](const size_t index) const;

    bool operator==(const Vector<T>& rhv) const;
    bool operator!=(const Vector<T>& rhv) const;
    bool operator<(const Vector<T>& rhv) const;
    bool operator>(const Vector<T>& rhv) const;
    bool operator<=(const Vector<T>& rhv) const;
    bool operator>=(const Vector<T>& rhv) const;

private:
    void allocateMemory(const size_type size, const size_type cap = 0);
    void validateIndex(const size_t index) const;
    void insure(iterator& position); /// when new memory was allocated, using insert
    void shift_forward(iterator position, const int step = 1);
    void shift_backward(iterator position, const int step = 1);

    template <typename Integer> void initializeRange(const Integer size, const_reference init, std::true_type);
    template <typename InputIterator> void initializeRange(InputIterator first, InputIterator last, std::false_type);
private:
    pointer begin_;
    pointer end_;
    pointer bufferEnd_;
}; /// end class Vector

} /// namespace cd06

#include "sources/Vector.cpp"
#include "sources/VectorIterator.cpp"
#include "sources/VectorReverseIterator.cpp"

#endif /// __T_VECTOR_HPP__


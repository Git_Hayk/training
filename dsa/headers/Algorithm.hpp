#ifndef __ALGORITHM_HPP__
#define __ALGORITHM_HPP__

#include "Vector.hpp"
#include "SingleList.hpp"
#include "List.hpp"
#include "Set.hpp"

#include <cstddef>
#include <vector>
#include <forward_list>
#include <list>
#include <set>

namespace cd06 {

template <typename InputIterator, typename T>
void fill(InputIterator first, InputIterator last, const T& value);

template <typename OutputIterator, typename Size, typename T>
OutputIterator fill_n(OutputIterator first, Size n, const T& value);

template <typename InputIterator, typename T>
void iota(InputIterator first, InputIterator last, T value);

template <typename InputIterator, typename Generator>
void generate(InputIterator first, InputIterator last, Generator gen);

template <typename OutputIterator, typename Size, typename Generator>
OutputIterator generate_n(OutputIterator first, Size n, Generator gen);

template <typename InputIterator, typename T>
T accumulate(InputIterator first, InputIterator last, T init);

template <typename InputIterator, typename EqualityComparable, typename Size>
void count(InputIterator first, InputIterator last, const EqualityComparable& value, Size& n);

template <typename InputIterator, typename OutputIterator>
OutputIterator copy(InputIterator first, InputIterator last, OutputIterator result);

template <typename InputIterator, typename Size, typename OutputIterator>
OutputIterator copy_n(InputIterator first, Size count, OutputIterator result);


/// here is an iterative solutions of searching algorithms on
/// data structures such as c-array, single-list, list, set and so on.
namespace iterative {
    
template <typename EqualityComparable, std::size_t size>
long int linearSearch(const EqualityComparable (&array)[size], const EqualityComparable& value);

template <typename EqualityComparable>
typename std::forward_list<EqualityComparable>::const_iterator
linearSearch(const std::forward_list<EqualityComparable>& container, const EqualityComparable& value);

template <typename EqualityComparable>
typename std::list<EqualityComparable>::const_iterator
linearSearch(const std::list<EqualityComparable>& container, const EqualityComparable& value);

template <typename EqualityComparable>
typename std::set<EqualityComparable>::const_iterator
linearSearch(const std::set<EqualityComparable>& container, const EqualityComparable& value);

template <typename EqualityComparable>
typename cd06::SingleList<EqualityComparable>::const_iterator
linearSearch(const cd06::SingleList<EqualityComparable>& container, const EqualityComparable& value);

template <typename EqualityComparable>
typename cd06::List<EqualityComparable>::const_iterator
linearSearch(const cd06::List<EqualityComparable>& container, const EqualityComparable& value);

template <typename EqualityComparable>
typename cd06::Set<EqualityComparable>::const_iterator
linearSearch(const cd06::Set<EqualityComparable>& container, const EqualityComparable& value);

template <typename ForwardIterator, typename EqualityComparable>
ForwardIterator linearSearch(ForwardIterator first, ForwardIterator last, const EqualityComparable& value);

template <typename LessThanComparable, std::size_t size>
long int binarySearch(const LessThanComparable (&array)[size], const LessThanComparable& value);

template <typename LessThanComparable>
long int binarySearch(const std::vector<LessThanComparable>& containder, const LessThanComparable& value);

template <typename LessThanComparable>
long int binarySearch(const cd06::Vector<LessThanComparable>& containder, const LessThanComparable& value);

} /// namespace iterative

/// here is an recursive solutions of searching algorithms on
/// data structures such as c-array, single-list, list, set and so on.
namespace recursive {
    
template <typename EqualityComparable, std::size_t size>
long int linearSearch(const EqualityComparable (&array)[size], const EqualityComparable& value, long int index = 0);

template <typename EqualityComparable>
typename std::forward_list<EqualityComparable>::const_iterator
linearSearch(const std::forward_list<EqualityComparable>& container, const EqualityComparable& value);

template <typename EqualityComparable>
typename std::list<EqualityComparable>::const_iterator
linearSearch(const std::list<EqualityComparable>& container, const EqualityComparable& value);

template <typename EqualityComparable>
typename std::set<EqualityComparable>::const_iterator
linearSearch(const std::set<EqualityComparable>& container, const EqualityComparable& value);

template <typename EqualityComparable>
typename cd06::Vector<EqualityComparable>::const_iterator
linearSearch(const cd06::Vector<EqualityComparable>& container, const EqualityComparable& value);

template <typename EqualityComparable>
typename cd06::SingleList<EqualityComparable>::const_iterator
linearSearch(const cd06::SingleList<EqualityComparable>& container, const EqualityComparable& value);

template <typename EqualityComparable>
typename cd06::List<EqualityComparable>::const_iterator
linearSearch(const cd06::List<EqualityComparable>& container, const EqualityComparable& value);

template <typename EqualityComparable>
typename cd06::Set<EqualityComparable>::const_iterator
linearSearch(const cd06::Set<EqualityComparable>& container, const EqualityComparable& value);

template <typename ForwardIterator, typename EqualityComparable>
ForwardIterator linearSearch(ForwardIterator first, ForwardIterator last, const EqualityComparable& value);

template <typename LessThanComparable, std::size_t size>
long int binarySearch(const LessThanComparable (&array)[size], const LessThanComparable& value);

template <typename LessThanComparable>
long int binarySearch(const std::vector<LessThanComparable>& containder, const LessThanComparable& value);

template <typename LessThanComparable>
long int binarySearch(const cd06::Vector<LessThanComparable>& containder, const LessThanComparable& value);

} /// namespace recursive


} /// namespace cd06

#include "sources/Algorithm.cpp"

#endif ///__ALGORITHM_HPP__


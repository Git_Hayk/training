#include "headers/Algorithm.hpp"
#include "headers/Vector.hpp"
#include "headers/SingleList.hpp"
#include "headers/List.hpp"
#include "headers/Set.hpp"
#include "headers/BasicString.hpp"

#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>
#include <cmath>
#include <numeric>
#include <iterator> /// ostream_iterator
#include <iomanip>
#include <vector>
#include <list>
#include <forward_list>
#include <set>

const bool isInteractive = ::isatty(STDIN_FILENO);
const int ITER_COUNT = 1024; /// suppose that push_back function was called ITER_COUNT times

typedef cd06::Vector<int> int_v;
typedef cd06::List<int> l_int;

typedef cd06::List<int>::const_iterator const_iterator;
typedef cd06::List<int>::iterator iterator;
typedef cd06::List<int>::const_reverse_iterator const_reverse_iterator;
typedef cd06::List<int>::reverse_iterator reverse_iterator;

struct Printf
{
    template <typename T>
    void operator()(const T& value) const
    {
        std::cout << value << ' ';
    }
};

unsigned short randomise();
template <typename InputIterator>
void print(InputIterator begin, InputIterator end);
template <typename T>
void print(const cd06::List<T>& l);
template <typename T>
void print_reverse(const cd06::List<T>& l);

double
complexity(int_v& v, int& allocCount, int& sumOfActions)
{
    for (int i = 0; i < ITER_COUNT; ++i) {
        v.push_back(1);
        ++sumOfActions;
        size_t s = v.size();
        if (s + 1 == v.capacity()) {
            sumOfActions += s;
            ++allocCount;
        }
    }
    return static_cast<double>(sumOfActions) / static_cast<double>(ITER_COUNT);
}

void
print(const double mediumComplexity, const int allocCount, const int sumOfActions)
{
    std::cout << std::setw(9) << int_v::RESERVE_COEFF <<     std::setw(7) << "| " <<
                 std::setw(11) << ITER_COUNT <<              std::setw(8) << "| " <<
                 std::setw(6)  << sumOfActions <<            std::setw(4) << "| " <<
                 std::setw(10) << allocCount <<              std::setw(8) << "| " <<
                 std::setw(12) << ITER_COUNT - allocCount << std::setw(9) << "| " <<
                 std::setw(11) << mediumComplexity <<        std::setw(9) << "| " <<
                 std::setw(3)  << static_cast<int>(std::round(mediumComplexity)) << std::endl;
}

void
calculate()
{
    int_v v;
    int allocCount = 0;
    int sumOfActions = 0;
    double mediumComplexity = complexity(v, allocCount, sumOfActions);
    print(mediumComplexity, allocCount, sumOfActions);
}

int
main()
{
    if (isInteractive) std::srand(::time(0));

    std::cout << std::fixed << std::setprecision(2);
    std::cout << "Reserve coeff | Iterations count | Actions | With allocation | Whitout allocation | Medium complexity | O(x)" << std::endl;

    int_v::RESERVE_COEFF = 3.0;
    calculate();
    int_v::RESERVE_COEFF = 1.6;
    calculate();
    int_v::RESERVE_COEFF = 1.4;
    calculate();
    int_v::RESERVE_COEFF = 1.2;
    calculate();

    cd06::Vector<int> v1;
    std::cout << "v1.size() = " << v1.size() << std::endl;
    std::cout << "v1.capacity() = " << v1.capacity() << '\n' << std::endl;

    v1.push_back(2);
    std::cout << "v1.size() = " << v1.size() << std::endl;
    std::cout << "v1.capacity() = " << v1.capacity() << '\n' << std::endl;

    v1.reserve(10);
    std::cout << "v1.size() = " << v1.size() << std::endl;
    std::cout << "v1.capacity() = " << v1.capacity() << '\n' << std::endl;

    v1.resize(5, 1);
    std::cout << "v1.size() = " << v1.size() << std::endl;
    std::cout << "v1.capacity() = " << v1.capacity() << '\n' << std::endl;

    v1.resize(10, 0);
    std::cout << "v1.size() = " << v1.size() << std::endl;
    std::cout << "v1.capacity() = " << v1.capacity() << '\n' << std::endl;

    v1.resize(15, 2);
    std::cout << "v1.size() = " << v1.size() << std::endl;
    std::cout << "v1.capacity() = " << v1.capacity() << '\n' << std::endl;

    v1.reserve(1);
    std::cout << "v1.size() = " << v1.size() << std::endl;
    std::cout << "v1.capacity() = " << v1.capacity() << '\n' << std::endl;

    v1.resize(17, 10);
    std::cout << "v1.size() = " << v1.size() << std::endl;
    std::cout << "v1.capacity() = " << v1.capacity() << '\n' << std::endl;

    cd06::Vector<int> v2(3);
    std::cout << "v2.size() = " << v2.size() << std::endl;
    std::cout << "v2.capacity() = " << v2.capacity() << '\n' << std::endl;

    v2.reserve(9);
    std::cout << "v2.size() = " << v2.size() << std::endl;
    std::cout << "v2.capacity() = " << v2.capacity() << '\n' << std::endl;

    v2.resize(1);
    std::cout << "v2.size() = " << v2.size() << std::endl;
    std::cout << "v2.capacity() = " << v2.capacity() << '\n' << std::endl;


    cd06::Vector<int> v3;
    v3.resize(5, 7);
    std::cout << "v3.size() = " << v3.size() << std::endl;
    std::cout << "v3.capacity() = " << v3.capacity() << '\n' << std::endl;


    const size_t SIZE_1 = 24;
    const size_t SIZE_2 = 33;

    cd06::Vector<int> intVector(SIZE_1);
    for (size_t i = 0; i < SIZE_1; ++i) {
        intVector[i] = i * i;
    }
    cd06::Vector<short> shortVector(SIZE_2);
    for (size_t i = 0; i < SIZE_2; ++i) {
        shortVector[i] = i;
    }

    std::cout << "\tintVector.capacity() = " << intVector.capacity() << std::endl;
    std::cout << "\tintVector.size() = "     << intVector.size() << std::endl;
    std::cout << "\tintVector.max_size() = " << intVector.max_size() << std::endl;
    std::cout << "\tintVector.front() = " << intVector.front() << std::endl;
    std::cout << "\tintVector.back() = " << intVector.back() << std::endl;

    std::cout << "\tshortVector.capacity() = " << shortVector.capacity() << std::endl;
    std::cout << "\tshortVector.size() = "     << shortVector.size() << std::endl;
    std::cout << "\tshortVector.max_size() = " << shortVector.max_size() << std::endl;
    std::cout << "\tshortVector.front() = " << shortVector.front() << std::endl;
    std::cout << "\tshortVector.back() = " << shortVector.back() << std::endl;

    intVector.resize(SIZE_1 + 8, 5);
    std::cout << "\tintVector.capacity() = " << intVector.capacity() << std::endl;
    std::cout << "\tintVector.size() = "     << intVector.size() << std::endl;
    std::cout << "\tintVector.max_size() = " << intVector.max_size() << std::endl;
    std::cout << "\tintVector.front() = " << intVector.front() << std::endl;
    std::cout << "\tintVector.back() = " << intVector.back() << std::endl;

    intVector.push_back(12);
    std::cout << "\tintVector.capacity() = " << intVector.capacity() << std::endl;
    std::cout << "\tintVector.size() = "     << intVector.size() << std::endl;
    std::cout << "\tintVector.max_size() = " << intVector.max_size() << std::endl;
    std::cout << "\tintVector.front() = " << intVector.front() << std::endl;
    std::cout << "\tintVector.back() = " << intVector.back() << std::endl;

    shortVector.pop_back();
    std::cout << "\tshortVector.capacity() = " << shortVector.capacity() << std::endl;
    std::cout << "\tshortVector.size() = "     << shortVector.size() << std::endl;
    std::cout << "\tshortVector.max_size() = " << shortVector.max_size() << std::endl;
    std::cout << "\tshortVector.front() = " << shortVector.front() << std::endl;
    std::cout << "\tshortVector.back() = " << shortVector.back() << std::endl;


    cd06::Vector<int> v(20, 1);
    std::cout << &v << std::endl;
    cd06::Vector<int>::iterator it = v.begin();

    for (int i = 0; it != v.end(); ++it, ++i) {
        *it = i;
    }

    it = v.begin();
    for ( ; it != v.end(); ++it) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;

    it = v.begin();
    for ( ; it != v.end(); it++) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;

    it = v.begin();
    for ( ; it != v.end(); it += 2) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;

    it = v.begin();
    for ( ; it != v.end(); it = it + 2) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;

    it = v.begin();
    v[0] = 15;
    std::cout << v[0] << std::endl;
    std::cout << it[0] << std::endl;

    v.insert(it + 10, 100);
    it = v.begin();
    for ( ; it != v.end(); it++) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;


    cd06::Vector<int>::const_reverse_iterator rit = v.rbegin();
    
    for ( ; rit != v.rend(); ++rit) {
        std::cout << *rit << ' ';
    }
    std::cout << std::endl;
    std::cout << rit[-10] << std::endl;


    cd06::Vector<int> range_v(v.begin() + 1, v.end() - 1);
    for (size_t i = 0; i < range_v.size(); ++i) {
        std::cout << range_v[i] << ' ';
    }
    std::cout << std::endl;


    range_v.insert(range_v.begin() + 5, 200);
    for (size_t i = 0; i < range_v.size(); ++i) {
        std::cout << range_v[i] << ' ';
    }
    std::cout << std::endl;

    range_v.insert(range_v.begin() + 10, 4, -8);
    for (size_t i = 0; i < range_v.size(); ++i) {
        std::cout << range_v[i] << ' ';
    }
    std::cout << std::endl;

    range_v.insert(range_v.begin(), 4, -8);
    for (size_t i = 0; i < range_v.size(); ++i) {
        std::cout << range_v[i] << ' ';
    }
    std::cout << std::endl;

    range_v.insert(range_v.end(), 10, -8);
    for (size_t i = 0; i < range_v.size(); ++i) {
        std::cout << range_v[i] << ' ';
    }
    std::cout << std::endl;

    range_v.insert(range_v.begin() + 2, v.begin() + 5, v.end() - 5);
    for (size_t i = 0; i < range_v.size(); ++i) {
        std::cout << range_v[i] << ' ';
    }
    std::cout << std::endl;

    range_v.erase(range_v.begin());
    range_v.erase(range_v.begin() + 1);
    range_v.erase(range_v.end());
    range_v.erase(range_v.end());
    range_v.erase(range_v.end());
    range_v.erase(range_v.end());
    range_v.erase(range_v.end() - 20);
    for (size_t i = 0; i < range_v.size(); ++i) {
        std::cout << range_v[i] << ' ';
    }
    std::cout << std::endl;

    range_v.erase(range_v.end() - 6, range_v.end());
    for (size_t i = 0; i < range_v.size(); ++i) {
        std::cout << range_v[i] << ' ';
    }
    std::cout << std::endl;

    range_v.erase(range_v.begin(), range_v.begin() + 10);
    for (size_t i = 0; i < range_v.size(); ++i) {
        std::cout << range_v[i] << ' ';
    }
    std::cout << std::endl;

    range_v.erase(range_v.begin(), range_v.end());
    for (size_t i = 0; i < range_v.size(); ++i) {
        std::cout << range_v[i] << ' ';
    }
    std::cout << std::endl;

    if (range_v.empty()) {
        std::cout << "empty" << std::endl;
    }
    std::cout << "cap = " << range_v.capacity() << std::endl;
    std::cout << "size = " << range_v.size() << std::endl;

    range_v.resize(10);
    cd06::Vector<int>::const_iterator c_it = range_v.begin();
    for ( ; c_it != range_v.end(); ++c_it) {
        std::cout << *c_it << ' ';
    }
    std::cout << std::endl;

    cd06::Vector<int> first;
    first.reserve(5);
    first.resize(3, 3);

    cd06::Vector<int> last;
    last.reserve(5);
    last.resize(4, 4);
    first = last;

    if (first == last) {
        std::cout << "DONE" << std::endl;
    }
    cd06::Vector<int> v10;
    v10.push_back(5);
    v10.push_back(5);
    v10.push_back(5);
    v10.push_back(5);
    v10.push_back(5);
    std::cout << "v10.cap = " << v10.capacity() << std::endl;
    std::cout << "v10.size = " << v10.size() << std::endl;

    cd06::Vector<int>::iterator one = v10.begin();
    cd06::Vector<int>::iterator two = ++one;

    cd06::Vector<int>::reverse_iterator three = v10.rbegin();
    cd06::Vector<int>::reverse_iterator four = ++three;

    l_int testList(10, 5);
    std::cout << "Test List size() = " << testList.size() << std::endl;
    std::cout << "Test List: ";
    print(testList);

    cd06::iota(testList.begin(), testList.end(), 0);

    std::cout << "Test List: ";
    print(testList);
    std::cout << "Test List\nprint reversed: ";
    print_reverse(testList);

    std::cout << "Chalange: " << *--testList.rend() << std::endl;
    std::cout << "Chalange: " << *--testList.end() << std::endl;

    std::cout << "back() = " << testList.back() << std::endl;
    std::cout << "front() = " << testList.front() << std::endl;


    testList.push_front(100);
    std::cout << "Test List front() = " << testList.front() << std::endl;
    std::cout << "Test List size() = " << testList.size() << std::endl;
    testList.push_back(99);
    std::cout << "Test List back() = " << testList.back() << std::endl;
    std::cout << "Test List size() = " << testList.size() << std::endl;

    std::cout << "Test List: ";
    print(testList);

    l_int newList(testList);
    if (newList == testList) {
        std::cout << "newList is equal to testList" << std::endl;
    }

    std::cout << "New List: ";
    print(newList);

    testList.pop_front();
    std::cout << "Test List front() = " << testList.front() << std::endl;
    std::cout << "Test List size() = " << testList.size() << std::endl;
    testList.pop_back();
    std::cout << "Test List back() = " << testList.back() << std::endl;
    std::cout << "Test List size() = " << testList.size() << std::endl;

    std::cout << "Test List: ";
    print(testList);

    std::cout << "Test List size() = " << testList.size() << std::endl;
    testList = newList;

    std::cout << "Test List: ";
    print(testList);

    std::cout << "New List: ";
    print(newList);

    newList.clear();
    std::cout << "New List: ";
    print(newList);

    l_int list_negative(10, -5);
    l_int list_positive(15, 5);
    std::cout << "Negative: ";
    print(list_negative);
    std::cout << "Positive: ";
    print(list_positive);

    list_negative.swap(list_positive);
    std::cout << "After swap" << std::endl;

    std::cout << "Negative: ";
    print(list_negative);
    std::cout << "Positive: ";
    print(list_positive);

    list_negative.resize(5);
    list_positive.resize(20, 10);

    std::cout << "After resize" << std::endl;

    std::cout << "Negative: ";
    print(list_negative);
    std::cout << "Positive: ";
    print(list_positive);

    l_int empty_list;
    std::cout << "Empty List: ";
    print(empty_list);
    empty_list.push_front(0);
    std::cout << "Empty List after push_front: ";
    print(empty_list);
    empty_list.pop_front();
    std::cout << "Empty List after pop_front: ";
    print(empty_list);

    std::cout << "Test List: ";
    for (const_iterator it = testList.begin(); it != testList.end(); ++it) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;

    std::cout << "Test List: ";
    for (const_iterator it = testList.end(); it != testList.begin(); --it) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;

    cd06::List<int> l(10);
    std::iota(l.begin(), l.end(), 0);
    cd06::List<int>::const_iterator l_it = l.begin();

    for ( ; l_it != l.end(); ++l_it) {
        std::cout << *l_it << ' ';
    }
    std::cout << std::endl;

    l_it = l.end();
    for ( ; l_it != l.begin(); --l_it) {
        std::cout << *l_it << ' ';
    }
    std::cout << std::endl;

    cd06::List<int>::const_reverse_iterator l_rit = l.rbegin();
    for ( ; l_rit != l.rend(); ++l_rit) {
        std::cout << *l_rit << ' ';
    }
    std::cout << std::endl;

    l_rit = l.rend();
    for ( ; l_rit != l.rbegin(); --l_rit) {
        std::cout << *l_rit << ' ';
    }
    std::cout << std::endl;

    l_int final_list(5, 10);
    print(final_list);

    final_list.pop_front();
    print(final_list);

    final_list.push_front(20);
    print(final_list);

    final_list.pop_back();
    print(final_list);

    final_list.push_back(20);
    print(final_list);

    while (!final_list.empty()) {
        final_list.pop_front();
    }

    print(final_list);

    l_int copy_list(final_list);
    print(final_list);
    print(copy_list);
    ///copy_list.pop_front();
    ///final_list.pop_back();

    final_list.resize(5, 10);
    copy_list.resize(10,5);
    print(final_list);
    print(copy_list);

    final_list.swap(copy_list);
    print(final_list);
    print(copy_list);
    
    std::cout << "Copy list: ";

    final_list.clear();
    print(final_list);

    l_int range_const_list(copy_list.begin(), copy_list.end());
    print(range_const_list);

    range_const_list.insert_after(range_const_list.begin());
    print(range_const_list);
    range_const_list.insert_after(range_const_list.end());
    print(range_const_list);
    range_const_list.insert_after(range_const_list.end(), 15);
    print(range_const_list);
    range_const_list.insert_after(range_const_list.begin(), 5, 8);
    print(range_const_list);
    range_const_list.insert_after(range_const_list.end(), 5, 6);
    print(range_const_list);
    range_const_list.insert_after(range_const_list.end(), range_const_list.begin(), range_const_list.end());
    print(range_const_list);
    
    range_const_list = copy_list;
    range_const_list.insert(range_const_list.end(), 15);
    print(range_const_list);
    range_const_list.insert(range_const_list.begin(), 5, 8);
    print(range_const_list);
    range_const_list.insert(range_const_list.end(), 5, 6);
    print(range_const_list);

    std::iota(range_const_list.begin(), range_const_list.end(), 0);
    print(range_const_list);

    l_int::iterator ax = range_const_list.begin();
    ++ax;
    ++ax;
    range_const_list.insert(ax, range_const_list.begin(), range_const_list.end());
    print(range_const_list);
    l_int::const_iterator b = range_const_list.begin();
    l_int::const_iterator e = range_const_list.end();

    std::cout << *b << std::endl;
    std::cout << *e << std::endl;
    ++ax;
    ++ax;
    range_const_list.insert_after(ax, b, e);
    print(range_const_list);

    l_int datark;
    datark.insert(datark.begin(), 5);
    print(datark);

    std::cout << "Max size is " << datark.max_size() << std::endl;
    cd06::List<double> double_list;
    std::cout << "Max size is " << double_list.max_size() << std::endl;

    std::list<double> double_std;
    std::cout << "std Max size is " << double_std.max_size() << std::endl;

    for (int i = 1; i < 11; ++i) {
        datark.push_back(i);
    }
    print(datark);

    l_int::iterator d_it = datark.begin();

    datark.erase(d_it);
    std::cout << *datark.begin() << std::endl;
    std::cout << *datark.end() << std::endl;
    std::cout << *datark.rbegin() << std::endl;
    std::cout << *datark.rend() << std::endl;
    
    print(datark);

    d_it = datark.begin();
    ++d_it;
    ++d_it;

    d_it = datark.erase(d_it);
    print(datark);

    l_int::iterator pos = datark.erase(datark.begin(), datark.end());
    print(datark);
    if (datark.empty()) {
        std::cout << "pos " << *pos << std::endl;
    }

    l_int lst1;
    lst1.push_back(1);
    std::cout << "something went wrong" << std::endl;
    lst1.push_back(2);
    lst1.push_back(3);
    lst1.push_back(4);
    lst1.push_back(5);
    iterator x = lst1.begin();
    ++x;
    ++x;
    iterator y = lst1.begin();
    ++y;
    ++y;
    ++y;
    ++y;
    std::cout << " x = " << *x << " y = " << *y << " lst1: ";
    print(lst1);
    iterator a = lst1.erase(x, y);
    std::cout << " x = " << *a << " y = " << *y << " lst1: ";
    print(lst1);

    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };
    std::forward_list<int> test_slist(array, array + sizeof(array) / sizeof(array[0]));
    cd06::recursive::linearSearch(test_slist, 15);

    cd06::SingleList<int> int_slist;
    std::cout << "\nint_slist.size() = " << int_slist.size() << std::endl;
    std::cout << "int_slist.max_size() = " << int_slist.max_size() << std::endl;

    std::forward_list<int> int_forward_list;
    std::cout << "int_forward_list.max_size() = " << int_forward_list.max_size() << std::endl;

    int_slist.push_front(0);
    int_slist.push_front(1);
    int_slist.insert_after(int_slist.begin(), 2);

    std::cout << "\nint_slist: ";
    cd06::copy(int_slist.begin(), int_slist.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    cd06::SingleList<int>::iterator back = int_slist.previous(int_slist.end());
    back = int_slist.insert_after(back, 3);
    back = int_slist.insert_after(back, 4);
    back = int_slist.insert_after(back, 5);

    std::cout << "int_slist: ";
    cd06::copy(int_slist.begin(), int_slist.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    if (isInteractive) std::srand(::time(0));

    const size_t SIZE = 10;

    std::vector<int> vs(SIZE);
    std::generate(vs.begin(), vs.end(), randomise);

    std::cout << "std::vector<int> v = ";
    print(vs.begin(), vs.end());

    std::set<int> s1(vs.begin(), vs.end());
    std::cout << "\nstd::set<int> s1 = ";
    print(s1.begin(), s1.end());

    std::cout << "\ns1.size() = " << s1.size() << std::endl;
    std::cout << "sizeof(s1) = " << sizeof(s1) << std::endl;

    std::cout << "\nreversed std::set<int> s1 = ";
    print(s1.rbegin(), s1.rend());

    std::cout << "*s1.begin() = "   << *s1.begin() << std::endl;
    std::cout << "*s1.end() = "     << *s1.end() << std::endl;
    std::cout << "\ns1.max_size() = " << s1.max_size() << std::endl;

    cd06::Set<int> mySet(vs.begin(), vs.end());
    std::cout << "\nmySet.size() = " << mySet.size() << std::endl;
    std::cout << "sizeof(mySet) = " << sizeof(mySet) << std::endl;
    
    std::cout << "\nPre order traversal: ";
    mySet.preOrder(Printf());
    std::cout << std::endl;

    std::cout << "\nIn order traversal: ";
    mySet.inOrder(Printf());
    std::cout << std::endl;

    std::cout << "\nPost order traversal: ";
    mySet.postOrder(Printf());
    std::cout << std::endl;

    std::cout << "\nLevel order traversal: ";
    mySet.levelOrder(Printf());
    std::cout << std::endl;

    mySet.insert(16);
    std::cout << "\nAfter mySet.insert(16);" << std::endl;
    std::cout << "\nmySet.size() = " << mySet.size() << std::endl;

    std::cout << "\nIn order traversal: ";
    mySet.inOrder(Printf());
    std::cout << std::endl;

    std::cout << "\n*mySet.begin() = " << *mySet.begin() << std::endl;
    std::cout << "*mySet.end() = " << *mySet.end() << std::endl;
    std::cout << "\nmySet.max_size() = " << mySet.max_size() << std::endl;

    std::cout << "\nPre order recursive traversal: ";
    mySet.recursivePreOrder(Printf());
    std::cout << std::endl;

    std::cout << "\nIn order recursive traversal: ";
    mySet.recursiveInOrder(Printf());
    std::cout << std::endl;

    std::cout << "\nPost order recursive traversal: ";
    mySet.recursivePostOrder(Printf());
    std::cout << std::endl;

    std::cout << "\nLevel order recursive traversal: ";
    mySet.recursiveLevelOrder(Printf());
    std::cout << std::endl;
    
    std::cout << "\nmySet.outputSet();\n" << std::endl;
    mySet.output();

    std::string std_s = "Hayk";
    std_s += "Harutyunyan !!! lastname";
    std::cout << "std_s.capacity() = " << std_s.capacity() << std::endl;
    std::cout << "std_s.size() = " << std_s.size() << std::endl;
    std::cout << "std_s.max_size() = " << std_s.max_size() << std::endl;
    std::cout << "sizeof(std_s) = " << sizeof(std_s) << std::endl;
    std::cout << std::endl;

    dsa::String dsa_s1 = "Hello World!";
    std::cout << "dsa_s1 string`\n";
    std::cout << dsa_s1 << std::endl;

    std::cin >> dsa_s1;
    std::cout << dsa_s1 << std::endl;
    std::cout << "dsa_s1.capacity() = " << dsa_s1.capacity() << std::endl;
    std::cout << "dsa_s1.size() = " << dsa_s1.size() << std::endl;

    std::cin >> std_s;
    std::cout << std_s << std::endl;

    std::stringstream ss;
    ss << std_s;

    ss >> dsa_s1;
    std::cout << "dsa_s1 after ss >> dsa_s1 - " << dsa_s1 << std::endl;

    return 0;
}

unsigned short
randomise()
{
    return std::rand() % 100;
}

template <typename InputIterator>
void
print(InputIterator b, InputIterator e)
{
    std::cout << "{ ";
    for ( ; b != e; ++b) {
        std::cout << *b << ' ';
    }
    std::cout << "}\n";
}

template <typename T>
void
print(const cd06::List<T>& l)
{
    if (l.empty()) {
        std::cout << std::endl;
        return;
    }
    typename cd06::List<T>::const_iterator c_it = l.begin();
    for ( ; c_it != l.end(); ++c_it) {
        std::cout << *c_it << ' ';
    }
    std::cout << std::endl;
}

template <typename T>
void
print_reverse(const cd06::List<T>& l)
{
    if (l.empty()) {
        std::cout << std::endl;
        return;
    }
    typename cd06::List<T>::const_reverse_iterator cr_it = l.rbegin();
    for ( ; cr_it != l.rend(); ++cr_it) {
        std::cout << *cr_it << ' ';
    }
    std::cout << std::endl;
}


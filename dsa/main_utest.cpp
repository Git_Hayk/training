#include <gtest/gtest.h>
#include "headers/Vector.hpp"
#include "headers/SingleList.hpp"
#include "headers/List.hpp"
#include "headers/Stack.hpp"
#include "headers/Queue.hpp"
#include "headers/Algorithm.hpp"
#include "headers/Set.hpp"
#include "headers/BasicString.hpp"
#include <iostream>
#include <iterator>
#include <string>
#include <vector>
#include <list>
#include <unistd.h>
#include <iomanip>
#include <numeric>
#include <cmath>


TEST(VectorTest, DefaultConstSize)
{
    cd06::Vector<int> v;
    EXPECT_EQ(static_cast<int>(v.size()), 0);
}

TEST(VectorTest, ConstArgumentSize)
{
    cd06::Vector<int> v1(10);
    EXPECT_EQ(static_cast<int>(v1.size()), 10);
    EXPECT_EQ(static_cast<int>(v1.capacity()), 10);
    cd06::Vector<int> v2(0);
    EXPECT_EQ(static_cast<int>(v2.size()), 0);
    EXPECT_EQ(static_cast<int>(v2.capacity()), 0);
    cd06::Vector<int> v3(3);
    EXPECT_EQ(static_cast<int>(v3.size()), 3);
    EXPECT_EQ(static_cast<int>(v3.capacity()), 3);
    cd06::Vector<int> v4(8);
    EXPECT_EQ(static_cast<int>(v4.size()), 8);
    EXPECT_EQ(static_cast<int>(v4.capacity()), 8);
}

TEST(VectorTest, max_size_test)
{
    cd06::Vector<int> v1;
    cd06::Vector<double> v2;
    cd06::Vector<char> v3;
    cd06::Vector<std::string> v4; /// std::string occubies 24 bytes of memory on macOS
    cd06::Vector<short> v5;

    EXPECT_EQ(v1.max_size(), size_t(2305843009213693951));
    EXPECT_EQ(v2.max_size(), size_t(1152921504606846975));
    EXPECT_EQ(v3.max_size(), size_t(9223372036854775807));
    EXPECT_EQ(v4.max_size(), size_t(384307168202282325));
    EXPECT_EQ(v5.max_size(), size_t(4611686018427387903));
}

TEST(VectorTest, DefaultConstEmpty)
{
    cd06::Vector<int> v;
    EXPECT_TRUE(v.empty());
}

TEST(VectorTest, ConstNotEmpty)
{
    cd06::Vector<int> v(10);
    EXPECT_FALSE(v.empty());
}
/*
TEST(VectorTest, Stream)
{
    cd06::Vector<int> v(10);

    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v;

    std::ostringstream out;
    out << v;

    EXPECT_EQ(out.str(), str);
}

TEST(VectorTest, Copy)
{
    cd06::Vector<int> v(10);

    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v;

    const cd06::Vector<int> cv = v;
    std::ostringstream out;
    out << cv;

    EXPECT_EQ(out.str(), str);
}

TEST(VectorTest, Assignment1)
{
    cd06::Vector<int> v(10);

    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v;
    /// v = v;
    
    cd06::Vector<int> v1, v2;
    v1 = v2 = v;
    std::ostringstream out;
    out << v1;

    EXPECT_EQ(out.str(), str);
}
*/
TEST(VectorTest, Assignment2)
{
    cd06::Vector<int> v1;
    v1.reserve(5);
    v1.resize(3, 3);

    cd06::Vector<int> v2;
    v2.reserve(5);
    v2.resize(4, 4);
    v1 = v2;
    EXPECT_EQ(v1, v2);
}

TEST(VectorTest, Capacity)
{
    cd06::Vector<int> v;
    EXPECT_EQ(v.capacity(), size_t(0));
    cd06::Vector<int> v2(1);
    EXPECT_EQ(v2.capacity(), size_t(1));
    cd06::Vector<int> v3(3);
    EXPECT_EQ(v3.capacity(), size_t(3));
    cd06::Vector<int> v4 = v3;
    EXPECT_EQ(v4.capacity(), size_t(3));
}

TEST(VectorTest, Reserve1)
{
    cd06::Vector<int> v(3);
    EXPECT_EQ(v.capacity(), size_t(3));
    v.reserve(9);
    EXPECT_EQ(v.capacity(), size_t(9));
    EXPECT_EQ(v.size(), size_t(3));
}

TEST(VectorTest, Reserve2)
{
    cd06::Vector<int> v;
    v.reserve(9);
    EXPECT_EQ(v.capacity(), size_t(9));
    EXPECT_EQ(v.size(), size_t(0));
}

TEST(VectorTest, frontTest)
{
    cd06::Vector<int> v(5);
    EXPECT_EQ(v.front(), v[0]);
    v.front() = 15;
    EXPECT_EQ(v.front(), 15);
}

TEST(VectorTest, backTest)
{
    cd06::Vector<int> v(5);
    EXPECT_EQ(v.back(), v[4]);
    v.back() = 15;
    EXPECT_EQ(v.back(), 15);
}

TEST(VectorTest, Resize1)
{
    cd06::Vector<int> v(3);
    v.resize(4, 5);
    EXPECT_EQ(v.capacity(), size_t(4));
    EXPECT_EQ(v.size(), size_t(4));
}

TEST(VectorTest, Resize2)
{
    cd06::Vector<int> v(3);
    v.resize(7);
    EXPECT_EQ(v.capacity(), size_t(7));
    EXPECT_EQ(v.size(), size_t(7));
}

TEST(VectorTest, Resize3)
{
    cd06::Vector<int> v(5);
    v.resize(2);
    EXPECT_EQ(v.size(), size_t(2));
    EXPECT_EQ(v.capacity(), size_t(5));
}

TEST(VectorTest, Resize4)
{
    cd06::Vector<int> v;
    v.resize(5);
    EXPECT_EQ(v.size(), size_t(5));
    EXPECT_EQ(v.capacity(), size_t(5));
}

TEST(VectorTest, PushBack)
{
    cd06::Vector<int> v(3);
    EXPECT_EQ(v.capacity(), size_t(3));
    EXPECT_EQ(v.size(), size_t(3));

    v.push_back(5);
    EXPECT_EQ(v.capacity(), size_t(6));
    EXPECT_EQ(v.size(), size_t(4));

    v.push_back(5);
    EXPECT_EQ(v.capacity(), size_t(6));
    EXPECT_EQ(v.size(), size_t(5));

    v.push_back(5);
    EXPECT_EQ(v.capacity(), size_t(6));
    EXPECT_EQ(v.size(), size_t(6));
}

TEST(IntVectorTest, PushBack1)
{
    cd06::Vector<int> v10(3);
    v10.push_back(5);
    v10.push_back(5);
    v10.push_back(5);
    v10.push_back(5);
    v10.push_back(5);
    EXPECT_EQ(v10.capacity(), size_t(12));
    EXPECT_EQ(v10.size(), size_t(8));
}

TEST(VectorTest, PopBack)
{
    cd06::Vector<int> v(5);
    v.pop_back();
    v.pop_back();
    EXPECT_EQ(v.size(), size_t(3));
    EXPECT_EQ(v.capacity(), size_t(5));
}
/*
TEST(VectorTest, GeneralTest)
{
    cd06::Vector<int> v(10);
    EXPECT_EQ(v.capacity(), size_t(10));

    v.push_back(1);
    EXPECT_EQ(v.capacity(), size_t(20));
    
    v.resize(21);
    EXPECT_EQ(v.capacity(), size_t(22));

    v.resize(41);
    EXPECT_EQ(v.capacity(), size_t(42));
}
*/
TEST(VectorTest, Comparison)
{
    cd06::Vector<short> v1(10, 3);
    cd06::Vector<short> v2(10, 3);
    EXPECT_TRUE(v1 == v2);
    EXPECT_FALSE(v1 != v2);
    EXPECT_TRUE(v1 <= v2);
    EXPECT_FALSE(v1 < v2);
    EXPECT_TRUE(v1 >= v2);
    EXPECT_FALSE(v1 > v2);
    
    v1.resize(5);
    EXPECT_TRUE(v1 != v2);
    EXPECT_FALSE(v1 == v2);
    EXPECT_TRUE(v1 < v2);
    EXPECT_FALSE(v1 > v2);
    EXPECT_TRUE(v1 <= v2);
    EXPECT_FALSE(v1 >= v2);

    v1.resize(10, 5);
    EXPECT_TRUE(v1 != v2);
    EXPECT_FALSE(v1 == v2);
    EXPECT_TRUE(v1 > v2);
    EXPECT_FALSE(v1 < v2);
    EXPECT_TRUE(v1 >= v2);
    EXPECT_FALSE(v1 <= v2);
}

TEST(const_iterator_test, begin_end)
{
    cd06::Vector<int> v(10);
    cd06::Vector<int>::const_iterator first = v.begin();
    cd06::Vector<int>::const_iterator last = v.end();
    EXPECT_EQ(*first, v[0]);
    EXPECT_EQ(*(last - 1),  v[9]);

    ++first;
    EXPECT_EQ(*first, v[1]);
    first++;
    EXPECT_EQ(*first, v[2]);
    first += 2;
    EXPECT_EQ(*first, v[4]);
    first = first + 2;
    EXPECT_EQ(*first, v[6]);
    --last;
    EXPECT_EQ(*last, v[9]);
    last--;
    EXPECT_EQ(*last, v[8]);
    last -= 2;
    EXPECT_EQ(*last, v[6]);
    last = last - 2;
    EXPECT_EQ(*last, v[4]);
}

TEST(iterator_test, begin_end)
{
    cd06::Vector<int> v(10);
    cd06::Vector<int>::iterator first = v.begin();
    cd06::Vector<int>::iterator last = v.end();
    EXPECT_EQ(*(first + 1), v[1]);
    EXPECT_EQ(*(last - 1),  v[9]);

    ++first;
    EXPECT_EQ(*first, v[1]);
    first++;
    EXPECT_EQ(*first, v[2]);
    first += 2;
    EXPECT_EQ(*first, v[4]);
    first = first + 2;
    EXPECT_EQ(*first, v[6]);
    --last;
    EXPECT_EQ(*last, v[9]);
    last--;
    EXPECT_EQ(*last, v[8]);
    last -= 2;
    EXPECT_EQ(*last, v[6]);
    last = last - 2;
    EXPECT_EQ(*last, v[4]);
}

TEST(const_reverse_iterator_test, rbegin_rend)
{
    cd06::Vector<int> v(10);
    cd06::Vector<int>::const_reverse_iterator first = v.rbegin();
    cd06::Vector<int>::const_reverse_iterator last = v.rend();

    EXPECT_EQ(*first, v[9]);
    EXPECT_EQ(*(last - 1), v[0]);

    ++first;
    EXPECT_EQ(*first, v[8]);
    first++;
    EXPECT_EQ(*first, v[7]);
    first += 2;
    EXPECT_EQ(*first, v[5]);
    first = first + 2;
    EXPECT_EQ(*first, v[3]);
    --last;
    EXPECT_EQ(*last, v[0]);
    last--;
    EXPECT_EQ(*last, v[1]);
    last -= 2;
    EXPECT_EQ(*last, v[3]);
    last = last - 2;
    EXPECT_EQ(*last, v[5]);
}

TEST(reverse_iterator_test, rbegin_rend)
{
    cd06::Vector<int> v(10);
    cd06::Vector<int>::reverse_iterator first = v.rbegin();
    cd06::Vector<int>::reverse_iterator last = v.rend();

    EXPECT_EQ(*first, v[9]);
    EXPECT_EQ(*(last - 1), v[0]);

    ++first;
    EXPECT_EQ(*first, v[8]);
    first++;
    EXPECT_EQ(*first, v[7]);
    first += 2;
    EXPECT_EQ(*first, v[5]);
    first = first + 2;
    EXPECT_EQ(*first, v[3]);
    --last;
    EXPECT_EQ(*last, v[0]);
    last--;
    EXPECT_EQ(*last, v[1]);
    last -= 2;
    EXPECT_EQ(*last, v[3]);
    last = last - 2;
    EXPECT_EQ(*last, v[5]);
}

TEST(IntVectorTest, OperatorIsEqual)
{
    cd06::Vector<int> v1(10);
    cd06::Vector<int> v2(10);
    EXPECT_TRUE(v1 == v2);
}

TEST(IntVectorTest, OperatorIsNotEqual)
{
    cd06::Vector<int> v1(10);
    cd06::Vector<int> v2(5);
    EXPECT_TRUE(v1 != v2);
}

TEST(IntVectorTest, OperatorLess)
{
    cd06::Vector<int> v1(10);
    cd06::Vector<int> v2(12);
    EXPECT_TRUE(v1 < v2);
}

TEST(IntVectorTest, OperatorLesOrEqual)
{
    cd06::Vector<int> v1(5);
    cd06::Vector<int> v2(15);
    EXPECT_TRUE(v1 <= v2);

    cd06::Vector<int> v3(5);
    cd06::Vector<int> v4(5);
    EXPECT_TRUE(v3 <= v4);
}

TEST(IntVectorTest, OperatorGreater)
{
    cd06::Vector<int> v1(10);
    cd06::Vector<int> v2(8);
    EXPECT_TRUE(v1 > v2);
}

TEST(IntVectorTest, OperatorGreaterOrEqual)
{
    cd06::Vector<int> v1(10, 5);
    cd06::Vector<int> v2(10, 5);
    EXPECT_TRUE(v1 >= v2);

    cd06::Vector<int> v3(15, 5);
    cd06::Vector<int> v4(10, 6);
    EXPECT_TRUE(v3 >= v4);
}

TEST(IntVectorTest, PushBack2)
{
    cd06::Vector<int> v(3);
    v.push_back(9);
    EXPECT_EQ(v.capacity(), size_t(6));
}

TEST(IntVectorTest, PushBack3)
{
    cd06::Vector<int> v;
    v.push_back(9);
    EXPECT_EQ(v.capacity(), size_t(1));
    EXPECT_EQ(v.size(), size_t(1));
}

TEST(IntVectorTest, PopBack)
{
    cd06::Vector<int> v(5);
    v.pop_back();
    v.pop_back();
    EXPECT_EQ(v.size(), size_t(3));
}

TEST(IntVectorTest, IteratorOperators)
{
    cd06::Vector<int> v1(6, 5);
    cd06::Vector<int>::const_iterator it1(v1.begin());

    cd06::Vector<int>::const_iterator it2(v1.begin());
    EXPECT_TRUE(it1 == it2);
    it2 += 1;
    EXPECT_TRUE(it1 != it2);
    EXPECT_TRUE(it1 < it2);
    it2 -= 1;
    EXPECT_TRUE(it1 <= it2);
    it1 = v1.end();
    EXPECT_TRUE(it1 > it2);
    it1 = it2 - 1;
    it1 = it2 + 1;
    EXPECT_TRUE(it1 >= it2);

}

typedef cd06::List<int> l_int;
typedef cd06::List<int>::const_iterator const_iterator;
typedef cd06::List<int>::iterator iterator;
typedef cd06::List<int>::const_reverse_iterator const_reverse_iterator;
typedef cd06::List<int>::reverse_iterator reverse_iterator;

TEST(ListTest, DefaultConstSize)
{
    cd06::List<int> lst;
    EXPECT_EQ(lst.size(), size_t(0));
    EXPECT_TRUE(lst.empty());
}

TEST(ListTest, CopyConstructor)
{
    l_int lst1(5, 4);
    const_iterator it = lst1.begin();
    l_int lst2(lst1);
    EXPECT_EQ(lst2, lst1);
}

TEST(ListTest, front_back)
{
    l_int lst1;
    lst1.push_front(9);
    EXPECT_EQ(lst1.front(), 9);
    EXPECT_EQ(lst1.back(), 9);
    EXPECT_EQ(lst1.size(), size_t(1));

    lst1.push_front(8);
    EXPECT_EQ(lst1.front(), 8);
    EXPECT_EQ(lst1.back(), 9);
    EXPECT_EQ(lst1.size(), size_t(2));

    lst1.push_back(10);
    EXPECT_EQ(lst1.back(), 10);
    EXPECT_EQ(lst1.size(), size_t(3));

    lst1.push_back(0);
    EXPECT_EQ(lst1.back(), 0);
    EXPECT_EQ(lst1.size(), size_t(4));
    EXPECT_FALSE(lst1.empty());
}

TEST(ListTest, NotEmpty)
{
    cd06::List<int> lst(5);
    EXPECT_EQ(lst.size(), size_t(5));
    EXPECT_FALSE(lst.empty());
}

TEST(ListTest, Swap)
{
    cd06::List<int> lst1(5, 6);
    cd06::List<int> lst2(7, 2);
    EXPECT_EQ(lst1.size(), size_t(5));
    EXPECT_EQ(lst2.size(), size_t(7));
    EXPECT_EQ(lst1.front(), 6);
    EXPECT_EQ(lst2.front(), 2);

    lst1.swap(lst2);
    EXPECT_EQ(lst1.size(), size_t(7));
    EXPECT_EQ(lst2.size(), size_t(5));
    EXPECT_EQ(lst1.front(), 2);
    EXPECT_EQ(lst2.front(), 6);
}


TEST(ListTest, Resize)
{
    cd06::List<int> lst;
    EXPECT_EQ(lst.size(), size_t(0));
    EXPECT_TRUE(lst.empty());
    
    lst.resize(6, 7);
    EXPECT_EQ(lst.size(), size_t(6));
    EXPECT_FALSE(lst.empty());
    
    lst.resize(4);
    EXPECT_EQ(lst.size(), size_t(4));
    EXPECT_FALSE(lst.empty());
    
    lst.resize(9, 2);
    EXPECT_EQ(lst.size(), size_t(9));
    EXPECT_FALSE(lst.empty());
    
    lst.resize(2);
    EXPECT_EQ(lst.size(), size_t(2));
    EXPECT_FALSE(lst.empty());
    
    lst.resize(1);
    EXPECT_EQ(lst.size(), size_t(1));
    EXPECT_FALSE(lst.empty());
    
    lst.resize(0);
    EXPECT_EQ(lst.size(), size_t(0));
    EXPECT_TRUE(lst.empty());
}

TEST(ListTest, Clear)
{
    cd06::List<int> lst(5, 4);
    lst.clear();
    EXPECT_TRUE(lst.empty());
}

TEST(ListTest, PushFront1)
{
    cd06::List<int> lst;
    lst.push_front(5);
    lst.push_front(6);
    lst.push_front(7);
    EXPECT_EQ(lst.size(), size_t(3));
}

TEST(ListTest, PopFront1)
{
    cd06::List<int> lst;
    lst.push_front(5);
    lst.push_front(6);
    lst.push_front(7);
    lst.pop_front();
    lst.pop_front();
    lst.pop_front();
    EXPECT_EQ(lst.size(), size_t(0));
    EXPECT_TRUE(lst.empty());
}

TEST(ListTest, PushBack1)
{
    cd06::List<int> lst;
    lst.push_back(5);
    lst.push_back(6);
    lst.push_back(7);
    EXPECT_EQ(lst.size(), size_t(3));
}

TEST(ListTest, Operators)
{
    l_int lst1(5, 4);
    l_int lst2(3, 7);
    EXPECT_FALSE(lst1 == lst2);
    EXPECT_TRUE(lst1 != lst2);
    EXPECT_TRUE(lst1 >= lst2);
    l_int lst3(4, 4);
    lst3.push_back(3);
    EXPECT_FALSE(lst1 < lst3);
    EXPECT_TRUE(lst1 > lst3);
    EXPECT_FALSE(lst1 <= lst3);
}



TEST(ListTest, Insert1)
{
    l_int lst1;
    lst1.push_back(5);
    lst1.push_back(4);
    lst1.push_back(3);
    lst1.push_back(2);
    lst1.push_back(1);
    iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ++it;
    lst1.insert(it, 9999);
}

TEST(ListTest, Insert2)
{
    l_int lst1;
    lst1.push_back(5);
    lst1.push_back(4);
    lst1.push_back(3);
    lst1.push_back(2);
    lst1.push_back(1);
    iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ++it;
    lst1.insert(it, 4, 9999);
}

TEST(ListTest, Insert3)
{
    l_int lst1;
    lst1.push_back(7);
    lst1.push_back(8);
    lst1.push_back(9);
    lst1.push_back(10);
    lst1.push_back(11);
    lst1.push_back(12);
    l_int lst2;
    lst2.push_back(1);
    lst2.push_back(2);
    lst2.push_back(3);
    lst2.push_back(4);
    lst2.push_back(5);
    iterator f = lst2.begin();
    ++f;
    iterator l(f);
    ++l;
    ++l;
    iterator it = lst1.begin();
    ++it;
    ++it;
    lst1.insert(it, f, l);
}

TEST(ListTest, Insert4)
{
    cd06::List<int> sl;
    sl.insert(sl.begin(), 0);
}

TEST(ListTest, InsertAfter1)
{
    l_int lst1;
    lst1.push_back(5);
    lst1.push_back(4);
    lst1.push_back(3);
    lst1.push_back(2);
    lst1.push_back(1);
    iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ++it;
    lst1.insert_after(it, 9999);
}

TEST(ListTest, InsertAfter2)
{
    l_int lst1;
    lst1.push_back(5);
    lst1.push_back(4);
    lst1.push_back(3);
    lst1.push_back(2);
    lst1.push_back(1);
    iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ++it;
    lst1.insert_after(it, 4, 9999);
}

TEST(ListTest, InsertAfter3)
{
    l_int lst1;
    lst1.push_back(7);
    lst1.push_back(8);
    lst1.push_back(9);
    lst1.push_back(10);
    lst1.push_back(11);
    lst1.push_back(12);
    l_int lst2;
    lst2.push_back(1);
    lst2.push_back(2);
    lst2.push_back(3);
    lst2.push_back(4);
    lst2.push_back(5);
    iterator f = lst2.begin();
    ++f;
    iterator l(f);
    ++l;
    ++l;
    iterator it = lst1.begin();
    ++it;
    ++it;
    lst1.insert_after(it, f, l);
}

TEST(ListTest, Erase1)
{
    l_int lst1;
    lst1.push_back(5);
    lst1.push_back(4);
    lst1.push_back(3);
    lst1.push_back(2);
    lst1.push_back(1);
    iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ++it;
    it = lst1.erase(it);
}

TEST(ListTest, Erase2)
{
    l_int lst1;
    lst1.push_back(1);
    lst1.push_back(2);
    lst1.push_back(3);
    lst1.push_back(4);
    lst1.push_back(5);
    iterator f = lst1.begin();
    ++f;
    ++f;
    iterator l = lst1.begin();
    ++l;
    ++l;
    ++l;
    ++l;
    iterator a = lst1.erase(f, l);
}

TEST(ListTest, EraseAfter1)
{
    l_int lst1;
    lst1.push_back(5);
    lst1.push_back(4);
    lst1.push_back(3);
    lst1.push_back(2);
    lst1.push_back(1);
    iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    lst1.erase_after(it);
}

TEST(ListTest, IteratorTest1)
{
    l_int l(10, 100);
    std::iota(l.begin(), l.end(), 1);

    EXPECT_EQ(*l.end(), 100);
    EXPECT_EQ(*l.rend(), 100);
    EXPECT_EQ(*l.begin(), 1);
    EXPECT_EQ(*l.rbegin(), 10);

    EXPECT_EQ(*--l.end(), 10);
    EXPECT_EQ(*--l.rend(), 1);
    EXPECT_EQ(*--l.begin(), 100);
    EXPECT_EQ(*--l.rbegin(), 100);

    EXPECT_EQ(*--(--l.end()), 9);
    EXPECT_EQ(*--(--l.rend()), 2);
    EXPECT_EQ(*--(--l.begin()), 10);
    EXPECT_EQ(*--(--l.rbegin()), 1);

    EXPECT_EQ(*++l.end(), 1);
    EXPECT_EQ(*++l.rend(), 10);
    EXPECT_EQ(*++l.begin(), 2);
    EXPECT_EQ(*++l.rbegin(), 9);
}

TEST(ListTest, IteratorTest2)
{
    l_int l;

    EXPECT_EQ(*l.end(), 0);
    EXPECT_EQ(*l.rend(), 0);
    EXPECT_EQ(*l.begin(), 0);
    EXPECT_EQ(*l.rbegin(), 0);

    l.push_back(10);

    EXPECT_EQ(*l.end(), 0);
    EXPECT_EQ(*l.rend(), 0);
    EXPECT_EQ(*l.begin(), 10);
    EXPECT_EQ(*l.rbegin(), 10);

    l.pop_back();

    EXPECT_EQ(*l.end(), 0);
    EXPECT_EQ(*l.rend(), 0);
    EXPECT_EQ(*l.begin(), 0);
    EXPECT_EQ(*l.rbegin(), 0);

    l.push_front(20);

    EXPECT_EQ(*l.end(), 0);
    EXPECT_EQ(*l.rend(), 0);
    EXPECT_EQ(*l.begin(), 20);
    EXPECT_EQ(*l.rbegin(), 20);
}

TEST(ListTest, IteratorTest3)
{
    l_int l;

    EXPECT_EQ(*--l.end(), 0);
    EXPECT_EQ(*--l.rend(), 0);
    EXPECT_EQ(*--l.begin(), 0);
    EXPECT_EQ(*--l.rbegin(), 0);

    EXPECT_EQ(*++l.end(), 0);
    EXPECT_EQ(*++l.rend(), 0);
    EXPECT_EQ(*++l.begin(), 0);
    EXPECT_EQ(*++l.rbegin(), 0);
}

/*
TEST(ListTest, Splice1)
{
    l_int lst1;
    lst1.push_back(5);
    lst1.push_back(4);
    lst1.push_back(3);
    lst1.push_back(2);
    lst1.push_back(1);
    l_int lst2;
    lst2.push_back(9);
    lst2.push_back(8);
    lst2.push_back(7);
    lst2.push_back(6);
    iterator it1 = lst1.begin();
   /// std::cout << lst1 << lst2 << std::endl;
    ++it1;
    lst1.splice(it1, lst2);
   /// std::cout << lst1 << lst2 << std::endl;
}

TEST(ListTest, Splice2)
{
    l_int lst1;
    lst1.push_back(5);
    lst1.push_back(4);
    lst1.push_back(3);
    lst1.push_back(2);
    lst1.push_back(1);
    l_int lst2;
    lst2.push_back(9);
    lst2.push_back(8);
    lst2.push_back(7);
    lst2.push_back(6);
 /// std::cout << lst1 << lst2 << std::endl;
    iterator it1 = lst1.begin();
    iterator it2 = lst2.begin();
    ++it1;
    ++it2;
  /// std::cout << *it1 << " | " << *it2 << std::endl;
    lst1.splice(it1, lst2, it2);
  /// std::cout << lst1 << lst2 << std::endl;
}

TEST(ListTest, Splice3)
{
    l_int lst1;
    lst1.push_back(5);
    lst1.push_back(4);
    lst1.push_back(3);
    lst1.push_back(2);
    lst1.push_back(1);
    l_int lst2;
    lst2.push_back(9);
    lst2.push_back(8);
    lst2.push_back(7);
    lst2.push_back(6);
   /// std::cout << lst1 << lst2 << std::endl;
    iterator it1 = lst1.begin();
    ++it1;
    iterator f = lst2.begin();
    ++f;
    iterator l(f);
    ++l;
    ++l;
   /// std::cout << *it1 << " | " << *f << " | " << *l << std::endl;
    lst1.splice(it1, lst2, f, l);
   /// std::cout << lst1 << lst2 << std::endl;
}

TEST(ListTest, Splice4)
{
    cd06::List<int> sl1(1);
    cd06::List<int> sl2;
    sl2.push_back(9);
    sl2.push_back(8);
    sl1.splice(sl1.begin(), sl2);
}
*/

TEST(QueueTest, DefaultConst)
{
    cd06::Queue<int> queue1;
    EXPECT_EQ(queue1.size(), size_t(0));
    EXPECT_TRUE(queue1.empty());
}

TEST(QueueTest, CopyConst)
{
    cd06::Queue<int> queue1;
    queue1.push(5);
    queue1.push(6);
    cd06::Queue<int> queue2(queue1);
    EXPECT_FALSE(queue1.empty());
    EXPECT_FALSE(queue2.empty());

    EXPECT_TRUE(queue1.size() == queue2.size());
    EXPECT_EQ(queue1.front(), queue2.front());
    EXPECT_EQ(queue1.back(), queue2.back());

    queue1.pop();
    queue2.pop();
    EXPECT_EQ(queue1.front(), queue2.front());
    EXPECT_EQ(queue1.back(), queue2.back());

    queue1.pop();
    queue2.pop();
    EXPECT_TRUE(queue1.empty());
    EXPECT_TRUE(queue2.empty());
}

TEST(QueueTest, front_back)
{
    cd06::Queue<short> queue1;
    EXPECT_TRUE(queue1.empty());
    queue1.push(10);
    EXPECT_EQ(queue1.front(), queue1.back());
    EXPECT_FALSE(queue1.empty());
    EXPECT_EQ(queue1.size(), size_t(1));
}

TEST(QueueTest, Size)
{
    cd06::Queue<int> queue1;
    queue1.push(5);
    queue1.push(6);
    queue1.push(7);

    EXPECT_EQ(queue1.front(), 7);
    EXPECT_EQ(queue1.back(), 5);

    queue1.pop();
    EXPECT_EQ(queue1.front(), 7);
    EXPECT_EQ(queue1.back(), 6);

    queue1.push(10);
    EXPECT_EQ(queue1.front(), 10);
    EXPECT_EQ(queue1.back(), 6);
}

TEST(QueueTest, Assignment)
{
    cd06::Queue<int> queue1;
    queue1.push(5);
    queue1.push(6);
    cd06::Queue<int> queue2;
    queue2 = queue1;

    EXPECT_EQ(queue1.front(), queue2.front());
    EXPECT_EQ(queue1.back(), queue2.back());
    EXPECT_TRUE(queue1.size() == queue2.size());

    queue1.pop();
    queue2.pop();
    EXPECT_EQ(queue1.front(), queue2.front());
    EXPECT_EQ(queue1.back(), queue2.back());
    EXPECT_TRUE(queue1.size() == queue2.size());
    EXPECT_FALSE(queue1.size() != queue2.size());
}

TEST(QueueTest, Equality)
{
    cd06::Queue<int> queue1;
    queue1.push(5);
    queue1.push(6);
    cd06::Queue<int> queue2(queue1);

    EXPECT_TRUE(queue1 == queue2);
    EXPECT_FALSE(queue1 != queue2);

    queue2.push(7);

    EXPECT_FALSE(queue1 == queue2);
    EXPECT_TRUE(queue1 != queue2);
}

TEST(QueueTest, Less)
{
    cd06::Queue<int> queue1;
    queue1.push(5);
    queue1.push(6);
    cd06::Queue<int> queue2(queue1);
    queue2.push(7);

    EXPECT_TRUE(queue1 < queue2);
    EXPECT_FALSE(queue1 > queue2);

    queue1 = queue2;

    EXPECT_TRUE(queue1 <= queue2);
    EXPECT_TRUE(queue1 >= queue2);

    queue1.front() = 4;

    EXPECT_TRUE(queue1 <= queue2);
    EXPECT_FALSE(queue1 >= queue2);
}

TEST(AlgorithmIterative, linearSearchArray)
{
    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };
    EXPECT_EQ(cd06::iterative::linearSearch(array, -2), 3);
    EXPECT_EQ(cd06::iterative::linearSearch(array, 15), -1);
    EXPECT_EQ(cd06::iterative::linearSearch(array, 4), 9);
}

TEST(AlgorithmIterative, binarySearchArray)
{
    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };
    EXPECT_EQ(cd06::iterative::binarySearch(array, -2), 3);
    EXPECT_EQ(cd06::iterative::binarySearch(array, 15), -1);
    EXPECT_EQ(cd06::iterative::binarySearch(array, 4), 9);

    std::vector<int> test_vector(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::iterative::binarySearch(test_vector, -2), 3);
    EXPECT_EQ(cd06::iterative::binarySearch(test_vector, 15), -1);
    EXPECT_EQ(cd06::iterative::binarySearch(test_vector, 4), 9);

    cd06::Vector<int> cd06_test_vector(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::iterative::binarySearch(cd06_test_vector, -2), 3);
    EXPECT_EQ(cd06::iterative::binarySearch(cd06_test_vector, 15), -1);
    EXPECT_EQ(cd06::iterative::binarySearch(cd06_test_vector, 4), 9);
}

TEST(AlgorithmIterative, linearSearchSingleList)
{
    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };
    std::forward_list<int> test_slist(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::iterative::linearSearch(test_slist, -5), test_slist.begin());
    EXPECT_EQ(cd06::iterative::linearSearch(test_slist, -4), ++test_slist.begin());
    EXPECT_EQ(cd06::iterative::linearSearch(test_slist, 15), test_slist.end());

    cd06::SingleList<int> cd06_test_list(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::iterative::linearSearch(cd06_test_list, -5), cd06_test_list.begin());
    EXPECT_EQ(cd06::iterative::linearSearch(cd06_test_list, -4), ++cd06_test_list.begin());
    EXPECT_EQ(cd06::iterative::linearSearch(cd06_test_list, 15), cd06_test_list.end());
}

TEST(AlgorithmIterative, linearSearchList)
{
    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };
    std::list<int> test_list(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::iterative::linearSearch(test_list, -5), test_list.begin());
    EXPECT_EQ(cd06::iterative::linearSearch(test_list, -4), ++test_list.begin());
    EXPECT_EQ(cd06::iterative::linearSearch(test_list, 15), test_list.end());

    cd06::List<int> cd06_test_list(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::iterative::linearSearch(cd06_test_list, -5), cd06_test_list.begin());
    EXPECT_EQ(cd06::iterative::linearSearch(cd06_test_list, -4), ++cd06_test_list.begin());
    EXPECT_EQ(cd06::iterative::linearSearch(cd06_test_list, 15), cd06_test_list.end());
}

TEST(AlgorithmIterative, linearSearchSet)
{
    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };
    std::set<int> test_set(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::iterative::linearSearch(test_set, -5), test_set.begin());
    EXPECT_EQ(cd06::iterative::linearSearch(test_set, -4), ++test_set.begin());
    EXPECT_EQ(cd06::iterative::linearSearch(test_set, 15), test_set.end());
}

TEST(AlgorithmIterative, linearSearchIterator)
{
    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };

    std::vector<int> test_vector(array, array + sizeof(array) / sizeof(array[0]));
    std::forward_list<int> test_slist(array, array + sizeof(array) / sizeof(array[0]));
    std::list<int> test_list(array, array + sizeof(array) / sizeof(array[0]));
    std::set<int> test_set(array, array + sizeof(array) / sizeof(array[0]));


    EXPECT_EQ(cd06::iterative::linearSearch(test_vector.begin(), test_vector.end(), -1), test_vector.begin() + 4);
    EXPECT_EQ(cd06::iterative::linearSearch(test_slist.begin(), test_slist.end(), -4), ++test_slist.begin());
    EXPECT_EQ(cd06::iterative::linearSearch(test_list.begin(), test_list.end(), 15), test_list.end());
    EXPECT_EQ(cd06::iterative::linearSearch(test_set.begin(), test_set.end(), 15), test_set.end());
}

TEST(AlgorithmRecursive, linearSearchArray)
{
    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };
    EXPECT_EQ(cd06::recursive::linearSearch(array, -2), 3);
    EXPECT_EQ(cd06::recursive::linearSearch(array, 15), -1);
    EXPECT_EQ(cd06::recursive::linearSearch(array, 4), 9);
}

TEST(AlgorithmRecursive, linearSearchSingleList)
{
    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };
    std::forward_list<int> test_slist(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::recursive::linearSearch(test_slist, -5), test_slist.begin());
    EXPECT_EQ(cd06::recursive::linearSearch(test_slist, -4), ++test_slist.begin());
    EXPECT_EQ(cd06::recursive::linearSearch(test_slist, 15), test_slist.end());

    cd06::SingleList<int> cd06_test_list(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::recursive::linearSearch(cd06_test_list, -5), cd06_test_list.begin());
    EXPECT_EQ(cd06::recursive::linearSearch(cd06_test_list, -4), ++cd06_test_list.begin());
    EXPECT_EQ(cd06::recursive::linearSearch(cd06_test_list, 15), cd06_test_list.end());
}

TEST(AlgorithmRecursive, linearSearchList)
{
    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };
    std::list<int> test_list(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::recursive::linearSearch(test_list, -5), test_list.begin());
    EXPECT_EQ(cd06::recursive::linearSearch(test_list, -4), ++test_list.begin());
    EXPECT_EQ(cd06::recursive::linearSearch(test_list, 15), test_list.end());

    cd06::List<int> cd06_test_list(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::recursive::linearSearch(cd06_test_list, -5), cd06_test_list.begin());
    EXPECT_EQ(cd06::recursive::linearSearch(cd06_test_list, -4), ++cd06_test_list.begin());
    EXPECT_EQ(cd06::recursive::linearSearch(cd06_test_list, 15), cd06_test_list.end());
}

TEST(AlgorithmRecursive, linearSearchSet)
{
    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };
    std::set<int> test_set(array, array + sizeof(array) / sizeof(array[0]));
    EXPECT_EQ(cd06::recursive::linearSearch(test_set, -5), test_set.begin());
    EXPECT_EQ(cd06::recursive::linearSearch(test_set, -4), ++test_set.begin());
    EXPECT_EQ(cd06::recursive::linearSearch(test_set, 15), test_set.end());
}

TEST(AlgorithmRecursive, linearSearchIterator)
{
    int array[] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4 };

    std::vector<int> test_vector(array, array + sizeof(array) / sizeof(array[0]));
    std::forward_list<int> test_slist(array, array + sizeof(array) / sizeof(array[0]));
    std::list<int> test_list(array, array + sizeof(array) / sizeof(array[0]));
    std::set<int> test_set(array, array + sizeof(array) / sizeof(array[0]));


    EXPECT_EQ(cd06::recursive::linearSearch(test_vector.begin(), test_vector.end(), -1), test_vector.begin() + 4);
    EXPECT_EQ(cd06::recursive::linearSearch(test_slist.begin(), test_slist.end(), -4), ++test_slist.begin());
    EXPECT_EQ(cd06::recursive::linearSearch(test_list.begin(), test_list.end(), 15), test_list.end());
    EXPECT_EQ(cd06::recursive::linearSearch(test_set.begin(), test_set.end(), 15), test_set.end());
}

typedef cd06::SingleList<int> int_slist;

TEST(SingleListTest, DefaultConstSize)
{
    int_slist lst;
    EXPECT_EQ(lst.size(), size_t(0));
    EXPECT_TRUE(lst.empty());
}

TEST(SingleListTest, CopyConstructor)
{
    int_slist lst1(5, 4);
    int_slist lst2(lst1);
    EXPECT_EQ(lst2, lst1);
}

TEST(SingleListTest, front_back)
{
    int_slist lst1;
    lst1.push_front(9);
    EXPECT_EQ(lst1.front(), 9);
    EXPECT_EQ(lst1.size(), size_t(1));

    lst1.push_front(8);
    EXPECT_EQ(lst1.front(), 8);
    EXPECT_EQ(lst1.size(), size_t(2));

    EXPECT_FALSE(lst1.empty());
}

TEST(SingleListTest, NotEmpty)
{
    int_slist lst(5);
    EXPECT_EQ(lst.size(), size_t(5));
    EXPECT_FALSE(lst.empty());
}

TEST(SingleListTest, Swap)
{
    int_slist lst1(5, 6);
    int_slist lst2(7, 2);
    EXPECT_EQ(lst1.size(), size_t(5));
    EXPECT_EQ(lst2.size(), size_t(7));
    EXPECT_EQ(lst1.front(), 6);
    EXPECT_EQ(lst2.front(), 2);

    lst1.swap(lst2);
    EXPECT_EQ(lst1.size(), size_t(7));
    EXPECT_EQ(lst2.size(), size_t(5));
    EXPECT_EQ(lst1.front(), 2);
    EXPECT_EQ(lst2.front(), 6);
}


TEST(SingleListTest, Resize)
{
    int_slist lst;
    EXPECT_EQ(lst.size(), size_t(0));
    EXPECT_TRUE(lst.empty());
    
    lst.resize(6, 7);
    EXPECT_EQ(lst.size(), size_t(6));
    EXPECT_FALSE(lst.empty());
    
    lst.resize(4);
    EXPECT_EQ(lst.size(), size_t(4));
    EXPECT_FALSE(lst.empty());
    
    lst.resize(9, 2);
    EXPECT_EQ(lst.size(), size_t(9));
    EXPECT_FALSE(lst.empty());
    
    lst.resize(2);
    EXPECT_EQ(lst.size(), size_t(2));
    EXPECT_FALSE(lst.empty());
    
    lst.resize(1);
    EXPECT_EQ(lst.size(), size_t(1));
    EXPECT_FALSE(lst.empty());
    
    lst.resize(0);
    EXPECT_EQ(lst.size(), size_t(0));
    EXPECT_TRUE(lst.empty());
}

TEST(SingleListTest, Clear)
{
    int_slist lst(5, 4);
    lst.clear();
    EXPECT_TRUE(lst.empty());
}

TEST(SingleListTest, PushFront1)
{
    int_slist lst;
    lst.push_front(5);
    lst.push_front(6);
    lst.push_front(7);
    EXPECT_EQ(lst.size(), size_t(3));
}

TEST(SingleListTest, PopFront1)
{
    int_slist lst;
    lst.push_front(5);
    lst.push_front(6);
    lst.push_front(7);
    lst.pop_front();
    lst.pop_front();
    lst.pop_front();
    EXPECT_EQ(lst.size(), size_t(0));
    EXPECT_TRUE(lst.empty());
}

TEST(SingleListTest, Operators)
{
    int_slist lst1(5, 4);
    int_slist lst2(3, 7);
    EXPECT_FALSE(lst1 == lst2);
    EXPECT_TRUE(lst1 != lst2);
    EXPECT_TRUE(lst1 >= lst2);
    int_slist lst3(4, 4);
    lst3.push_front(3);
    EXPECT_FALSE(lst1 < lst3);
    EXPECT_TRUE(lst1 > lst3);
    EXPECT_FALSE(lst1 <= lst3);
}

TEST(SingleListTest, Insert1)
{
    int_slist lst1;
    lst1.push_front(5);
    lst1.push_front(4);
    lst1.push_front(3);
    lst1.push_front(2);
    lst1.push_front(1);
    int_slist::iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ++it;
    lst1.insert(it, 9999);
}

TEST(SingleListTest, Insert2)
{
    int_slist lst1;
    lst1.push_front(5);
    lst1.push_front(4);
    lst1.push_front(3);
    lst1.push_front(2);
    lst1.push_front(1);
    int_slist::iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ++it;
    lst1.insert(it, 4, 9999);
}

TEST(SingleListTest, Insert3)
{
    int_slist lst1;
    lst1.push_front(7);
    lst1.push_front(8);
    lst1.push_front(9);
    lst1.push_front(10);
    lst1.push_front(11);
    lst1.push_front(12);
    int_slist lst2;
    lst2.push_front(1);
    lst2.push_front(2);
    lst2.push_front(3);
    lst2.push_front(4);
    lst2.push_front(5);
    int_slist::iterator f = lst2.begin();
    ++f;
    int_slist::iterator l(f);
    ++l;
    ++l;
    int_slist::iterator it = lst1.begin();
    ++it;
    ++it;
    lst1.insert(it, f, l);
}

TEST(SingleListTest, Insert4)
{
    cd06::List<int> sl;
    sl.insert(sl.begin(), 0);
}

TEST(SingleListTest, InsertAfter1)
{
    int_slist lst1;
    lst1.push_front(5);
    lst1.push_front(4);
    lst1.push_front(3);
    lst1.push_front(2);
    lst1.push_front(1);
    int_slist::iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ++it;
    lst1.insert_after(it, 9999);
}

TEST(SingleListTest, InsertAfter2)
{
    int_slist lst1;
    lst1.push_front(5);
    lst1.push_front(4);
    lst1.push_front(3);
    lst1.push_front(2);
    lst1.push_front(1);
    int_slist::iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ++it;
    lst1.insert_after(it, 4, 9999);
}

TEST(SingleListTest, InsertAfter3)
{
    int_slist lst1;
    lst1.push_front(7);
    lst1.push_front(8);
    lst1.push_front(9);
    lst1.push_front(10);
    lst1.push_front(11);
    lst1.push_front(12);
    int_slist lst2;
    lst2.push_front(1);
    lst2.push_front(2);
    lst2.push_front(3);
    lst2.push_front(4);
    lst2.push_front(5);
    int_slist::iterator f = lst2.begin();
    ++f;
    int_slist::iterator l(f);
    ++l;
    ++l;
    int_slist::iterator it = lst1.begin();
    ++it;
    ++it;
    lst1.insert_after(it, f, l);
}

TEST(SingleListTest, Erase1)
{
    int_slist lst1;
    lst1.push_front(5);
    lst1.push_front(4);
    lst1.push_front(3);
    lst1.push_front(2);
    lst1.push_front(1);
    int_slist::iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    ++it;
    it = lst1.erase(it);
}

TEST(SingleListTest, Erase2)
{
    int_slist lst1;
    lst1.push_front(1);
    lst1.push_front(2);
    lst1.push_front(3);
    lst1.push_front(4);
    lst1.push_front(5);
    int_slist::iterator f = lst1.begin();
    ++f;
    ++f;
    int_slist::iterator l = lst1.begin();
    ++l;
    ++l;
    ++l;
    ++l;
    int_slist::iterator a = lst1.erase(f, l);
}

TEST(SingleListTest, EraseAfter1)
{
    int_slist lst1;
    lst1.push_front(5);
    lst1.push_front(4);
    lst1.push_front(3);
    lst1.push_front(2);
    lst1.push_front(1);
    int_slist::iterator it = lst1.begin();
    ++it;
    ++it;
    ++it;
    lst1.erase_after(it);
}

TEST(SingleListTest, IteratorTest1)
{
    int_slist l(10, 100);
    std::iota(l.begin(), l.end(), 1);

    EXPECT_EQ(*l.end(), 100);
    EXPECT_EQ(*l.begin(), 1);

    EXPECT_EQ(*++l.begin(), 2);
}

TEST(SingleListTest, IteratorTest2)
{
    int_slist l;

    EXPECT_EQ(*l.end(), 0);
    EXPECT_EQ(*l.begin(), 0);

    l.push_front(20);

    EXPECT_EQ(*l.end(), 20);
    EXPECT_EQ(*l.begin(), 20);

    l.pop_front();

    EXPECT_EQ(*l.end(), 0);
    EXPECT_EQ(*l.begin(), 0);
}

TEST(SingleListTest, IteratorTest3)
{
    int_slist l;

    EXPECT_EQ(*l.begin(), 0);
}

typedef cd06::Set<int> i_set;
typedef cd06::Set<int>::iterator i_set_iterator;
typedef cd06::Set<double> d_set;
typedef cd06::Set<double>::iterator d_set_iterator;

TEST(SetTest, DefaultConst)
{
    cd06::Set<int> s;
}

TEST(SetTest, size)
{
    i_set s;
    EXPECT_EQ(s.size(), size_t(0));
}

TEST(SetTest, max_size)
{
    i_set intSet;
    EXPECT_EQ(intSet.max_size(), size_t(576460752303423487));
    d_set doubleSet;
    EXPECT_EQ(doubleSet.max_size(), size_t(576460752303423487));
}

TEST(SetTest, empty)
{
    i_set intSet;
    EXPECT_TRUE(intSet.empty());
    d_set doubleSet;
    EXPECT_TRUE(doubleSet.empty());
}

TEST(SetTest, insert)
{
    i_set intSet;
    //intSet.insert(1);

    //EXPECT_FALSE(intSet.empty());
    //EXPECT_EQ(*intSet.root(), 1);
}

TEST(String, DefaultConst)
{
    dsa::String s;

    EXPECT_EQ(s.size(), size_t(0));
    EXPECT_EQ(s.length(), size_t(0));
    EXPECT_EQ(s.max_size(), size_t(9223372036854775791));
    EXPECT_EQ(s.capacity(), size_t(16));
    EXPECT_TRUE(s.empty());
}

TEST(String, ConstOtherString1)
{
    dsa::String s1("Hello World!");
    dsa::String s2(s1);

    EXPECT_EQ(s1.size(), s2.size());
    EXPECT_EQ(s1.capacity(), s2.capacity());
    for (size_t i = 0; i < s1.size(); ++i) {
        EXPECT_EQ(s1[i], s2[i]);
    }
}

TEST(String, ConstOtherString2)
{
    dsa::String s1("Hello World!");
    dsa::String s2(s1, 5, 10);

    EXPECT_EQ(s2.size(), size_t(5));

    dsa::String s3(" Worl");
    for (size_t i = 0; i < s3.size(); ++i) {
        EXPECT_EQ(s2[i], s3[i]);
    }
}

TEST(String, ConstNullTerm)
{
    const char* text1 = "Hello World!";
    dsa::String s1(text1);

    EXPECT_EQ(s1.size(), ::strlen(text1));
    EXPECT_EQ(s1.length(), ::strlen(text1));
    EXPECT_EQ(s1.capacity(), size_t(16));
    EXPECT_FALSE(s1.empty());

    const char* text2 = "A big guy jups over the dog.";
    dsa::String s2(text2);

    EXPECT_EQ(s2.size(), ::strlen(text2));
    EXPECT_EQ(s2.length(), ::strlen(text2));
    EXPECT_EQ(s2.capacity(), ::strlen(text2));
    EXPECT_FALSE(s2.empty());

    dsa::String s3(text2, 17);

    EXPECT_EQ(s3.size(), size_t(17));
    EXPECT_EQ(s3.length(), size_t(17));
    EXPECT_EQ(s3.capacity(), size_t(17));
    EXPECT_FALSE(s3.empty());
}

TEST(String, ConstCharArray)
{
    char text1[] = "Hello World!";
    dsa::String s1(text1, 5);

    EXPECT_EQ(s1.size(), size_t(5));
    EXPECT_EQ(s1.length(), size_t(5));
    EXPECT_EQ(s1.capacity(), size_t(16));
    EXPECT_FALSE(s1.empty());
}

TEST(String, ConstNumChar)
{
    const char c = 'b';
    dsa::String s1(10, c);

    EXPECT_EQ(s1.size(), size_t(10));
    EXPECT_EQ(s1.length(), size_t(10));
    EXPECT_EQ(s1.capacity(), size_t(16));
    EXPECT_FALSE(s1.empty());

    const size_t s = s1.size();
    for (size_t i = 0; i < s; ++i) {
        EXPECT_EQ(s1[i], c);
    }
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


#include <benchmark/benchmark.h>
#include "headers/Vector.hpp"
#include "headers/SingleList.hpp"
#include "headers/List.hpp"
#include "headers/Queue.hpp"
#include "headers/Stack.hpp"
#include "headers/Algorithm.hpp"
#include "headers/Set.hpp"
#include "headers/BasicString.hpp"
#include <iostream>
#include <sstream>
#include <numeric>
#include <unistd.h>
#include <vector>
#include <list>

const size_t SIZE = 2<<10;
const int ITER_COUNT = 2<<10;
typedef cd06::List<int> l_int;
typedef cd06::SingleList<int> sl_int;

int arr[SIZE] = { 0 };

struct Find
{
    template <typename T>
    bool operator()(const T& value) const
    {
        return value == 0;
    }
};

struct Random
{
    int operator()()
    {
        return std::rand() % (SIZE * 10);
    }
};

static void
BM_pre_incr(benchmark::State& state)
{
    int x = 1;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(++x);
    }
}
BENCHMARK(BM_pre_incr);

static void
BM_post_incr(benchmark::State& state)
{
    int x = 1;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x++);
    }
}
BENCHMARK(BM_post_incr);

static void
BM_std_string_copy_const(benchmark::State& state)
{
    std::string s("Hello World!");
    while (state.KeepRunning()) {
        std::string str(s);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_std_string_copy_const);

static void
BM_dsa_string_copy_const(benchmark::State& state)
{
    dsa::String s("Hello World!");
    while (state.KeepRunning()) {
        dsa::String str(s);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_dsa_string_copy_const);

static void
BM_std_string_null_term_const(benchmark::State& state)
{
    const char* str = "Hello World!";
    while (state.KeepRunning()) {
        std::string s(str);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_std_string_null_term_const);

static void
BM_dsa_string_null_term_const(benchmark::State& state)
{
    const char* str = "Hello World!";
    while (state.KeepRunning()) {
        dsa::String s(str);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_dsa_string_null_term_const);

static void
BM_std_string_num_char_const(benchmark::State& state)
{
    const char c = 'a';
    while (state.KeepRunning()) {
        std::string s(16, c);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_std_string_num_char_const);

static void
BM_dsa_string_num_char_const(benchmark::State& state)
{
    const char c = 'a';
    while (state.KeepRunning()) {
        dsa::String s(16, c);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_dsa_string_num_char_const);

static void
BM_std_string_size(benchmark::State& state)
{
    std::string str("Hello World!");
    while (state.KeepRunning()) {
        str.size();
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_std_string_size);

static void
BM_dsa_string_size(benchmark::State& state)
{
    dsa::String str("Hello World!");
    while (state.KeepRunning()) {
        str.size();
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_dsa_string_size);

static void
BM_std_string_capacity(benchmark::State& state)
{
    std::string str("Hello World!");
    while (state.KeepRunning()) {
        str.capacity();
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_std_string_capacity);

static void
BM_dsa_string_capacity(benchmark::State& state)
{
    dsa::String str("Hello World!");
    while (state.KeepRunning()) {
        str.capacity();
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_dsa_string_capacity);

static void
BM_std_string_max_size(benchmark::State& state)
{
    std::string str("Hello World!");
    while (state.KeepRunning()) {
        str.max_size();
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_std_string_max_size);

static void
BM_dsa_string_max_size(benchmark::State& state)
{
    dsa::String str("Hello World!");
    while (state.KeepRunning()) {
        str.max_size();
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_dsa_string_max_size);

static void
BM_std_string_output(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    std::string str = "Hello World!";
    while (state.KeepRunning()) {
        std::cout << str << std::endl;
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_std_string_output);

static void
BM_dsa_string_output(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    dsa::String str = "Hello World!";
    while (state.KeepRunning()) {
        std::cout << str << std::endl;
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_dsa_string_output);

static void
BM_std_string_input(benchmark::State& state)
{
    std::string str = "HelloWorld";
    std::stringstream ss;
    ss << str;
    while (state.KeepRunning()) {
        ss >> str;
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_std_string_input);

static void
BM_dsa_string_input(benchmark::State& state)
{
    dsa::String str = "HelloWorld";
    std::stringstream ss;
    ss << str;
    while (state.KeepRunning()) {
        ss >> str;
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_dsa_string_input);

static void
BM_vector_push_back(benchmark::State& state)
{
    while (state.KeepRunning()) {
        cd06::Vector<int> v;
        for (int i = 0; i < ITER_COUNT; ++i) {
            v.push_back(1);
        }
        state.SetBytesProcessed(ITER_COUNT * state.iterations() * sizeof(int));
    }
}
BENCHMARK(BM_vector_push_back);

static void
BM_std_vector_push_back(benchmark::State& state)
{
    while (state.KeepRunning()) {
        std::vector<int> v;
        for (int i = 0; i < ITER_COUNT; ++i) {
            v.push_back(1);
        }
        state.SetBytesProcessed(ITER_COUNT * state.iterations() * sizeof(int));
    }
}
BENCHMARK(BM_std_vector_push_back);

static void
BM_vector_reserve_push_back(benchmark::State& state)
{
    while (state.KeepRunning()) {
        cd06::Vector<int> v;
        v.reserve(ITER_COUNT);
        for (int i = 0; i < ITER_COUNT; ++i) {
            v.push_back(1);
        }
        state.SetBytesProcessed(ITER_COUNT * state.iterations() * sizeof(int));
    }
}
BENCHMARK(BM_vector_reserve_push_back);

static void
BM_std_vector_reserve_push_back(benchmark::State& state)
{
    while (state.KeepRunning()) {
        std::vector<int> v;
        v.reserve(ITER_COUNT);
        for (int i = 0; i < ITER_COUNT; ++i) {
            v.push_back(1);
        }
        state.SetBytesProcessed(ITER_COUNT * state.iterations() * sizeof(int));
    }
}
BENCHMARK(BM_std_vector_reserve_push_back);

static void
BM_List_push_back(benchmark::State& state)
{
    while (state.KeepRunning()) {
        l_int listBench;
        for (int i = 0; i < ITER_COUNT; ++i) {
            listBench.push_back(0);
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_List_push_back);

static void
BM_List_pop_back(benchmark::State& state)
{
    while (state.KeepRunning()) {
        l_int listBench(ITER_COUNT);
        for (int i = 0; i < ITER_COUNT; ++i) {
            listBench.pop_back();
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_List_pop_back);

static void
BM_List_push_front(benchmark::State& state)
{
    while (state.KeepRunning()) {
        l_int listBench;
        for (int i = 0; i < ITER_COUNT; ++i) {
            listBench.push_front(0);
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_List_push_front);

static void
BM_List_pop_front(benchmark::State& state)
{
    while (state.KeepRunning()) {
        l_int listBench(ITER_COUNT);
        for (int i = 0; i < ITER_COUNT; ++i) {
            listBench.pop_front();
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_List_pop_front);

static void
BM_List_resize(benchmark::State& state)
{
    while (state.KeepRunning()) {
        l_int listBench(10);
        listBench.resize(ITER_COUNT, 0);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_List_resize);

static void
BM_List_clear(benchmark::State& state)
{
    while (state.KeepRunning()) {
        l_int listBench(ITER_COUNT);
        listBench.clear();
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_List_clear);

static void
BM_std_list_push_back(benchmark::State& state)
{
    while (state.KeepRunning()) {
        std::list<int> listBench;
        for (int i = 0; i < ITER_COUNT; ++i) {
            listBench.push_back(0);
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_std_list_push_back);

static void
BM_std_list_pop_back(benchmark::State& state)
{
    while (state.KeepRunning()) {
        std::list<int> listBench(ITER_COUNT);
        for (int i = 0; i < ITER_COUNT; ++i) {
            listBench.pop_back();
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_std_list_pop_back);

static void
BM_std_list_push_front(benchmark::State& state)
{
    while (state.KeepRunning()) {
        std::list<int> listBench;
        for (int i = 0; i < ITER_COUNT; ++i) {
            listBench.push_front(0);
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_std_list_push_front);

static void
BM_std_list_pop_front(benchmark::State& state)
{
    while (state.KeepRunning()) {
        std::list<int> listBench(ITER_COUNT);
        for (int i = 0; i < ITER_COUNT; ++i) {
            listBench.pop_front();
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_std_list_pop_front);

static void
BM_std_list_resize(benchmark::State& state)
{
    while (state.KeepRunning()) {
        std::list<int> listBench(10);
        listBench.resize(ITER_COUNT, 0);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_std_list_resize);

static void
BM_std_list_clear(benchmark::State& state)
{
    while (state.KeepRunning()) {
        std::list<int> listBench(ITER_COUNT);
        listBench.clear();
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_std_list_clear);

static void
BM_Single_List_push_front(benchmark::State& state)
{
    while (state.KeepRunning()) {
        sl_int listBench;
        for (int i = 0; i < ITER_COUNT; ++i) {
            listBench.push_front(0);
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_Single_List_push_front);

static void
BM_Single_List_pop_front(benchmark::State& state)
{
    while (state.KeepRunning()) {
        sl_int listBench(ITER_COUNT);
        for (int i = 0; i < ITER_COUNT; ++i) {
            listBench.pop_front();
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_Single_List_pop_front);

static void
BM_Single_List_resize(benchmark::State& state)
{
    while (state.KeepRunning()) {
        sl_int listBench(10);
        listBench.resize(ITER_COUNT, 0);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_Single_List_resize);

static void
BM_Single_List_clear(benchmark::State& state)
{
    while (state.KeepRunning()) {
        l_int listBench(ITER_COUNT);
        listBench.clear();
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_Single_List_clear);

static void
BM_queue_push(benchmark::State& state)
{
    while (state.KeepRunning()) {
        cd06::Queue<int> q1;
        for (int i = 0; i < ITER_COUNT; ++i) {
            q1.push(i);
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_queue_push);

static void
BM_queue_pop(benchmark::State& state)
{
    cd06::Queue<int> q1;
    for (int i = 0; i < ITER_COUNT; ++i) {
        q1.push(i);
    }
    while (state.KeepRunning()) {
        cd06::Queue<int> q2(q1);
        for (int i = 0; i < ITER_COUNT; ++i) {
            q2.pop();
        }
    }
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_queue_pop);

static void
BM_copy_n(benchmark::State& state)
{
    std::vector<int> v(10, 0);
    std::list<int> l(10, 1);
    while (state.KeepRunning()) {
        cd06::copy_n(v.begin(), 5, l.begin());
    }
    state.SetBytesProcessed(5 * state.iterations());
}
BENCHMARK(BM_copy_n);

static void
BM_fill_n(benchmark::State& state)
{
    std::vector<int> v(10);
    while (state.KeepRunning()) {
        cd06::fill_n(v.begin(), 10, 3);
    }
    state.SetBytesProcessed(10 * state.iterations());
}
BENCHMARK(BM_fill_n);

static void
BM_generat_n(benchmark::State& state)
{
    std::vector<int> v(10, 0);
    while (state.KeepRunning()) {
        cd06::generate_n(v.begin(), 10, std::rand);
    }
    std::cout.clear();
}
BENCHMARK(BM_generat_n);

static void
BM_fill(benchmark::State& state)
{
    std::vector<int> v(SIZE);
    while (state.KeepRunning()) {
        cd06::fill(v.begin(), v.end(), 1);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_fill);

static void
BM_iota(benchmark::State& state)
{
    std::vector<int> v(SIZE);
    while (state.KeepRunning()) {
        cd06::iota(v.begin(), v.end(), 1);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_iota);

static void
BM_generate(benchmark::State& state)
{
    std::vector<int> v(SIZE);
    while (state.KeepRunning()) {
        cd06::generate(v.begin(), v.end(), std::rand);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_generate);

static void
BM_accumulate(benchmark::State& state)
{
    std::vector<int> v(SIZE, 5);
    while (state.KeepRunning()) {
        int sum = 0;
        sum = cd06::accumulate(v.begin(), v.end(), sum);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_accumulate);

static void
BM_count(benchmark::State& state)
{
    std::vector<int> v(SIZE);
    while (state.KeepRunning()) {
        size_t n = 0;
        cd06::count(v.begin(), v.end(), 0, n);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_count);

static void
BM_copy(benchmark::State& state)
{
    std::vector<int> v(SIZE);
    std::list<int> l;
    while (state.KeepRunning()) {
        cd06::copy(v.begin(), v.end(), l.begin());
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_copy);

static void
BM_iterative_linear_search_array(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    while (state.KeepRunning()) {
        cd06::iterative::linearSearch(arr, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_linear_search_array);

static void
BM_iterative_linear_search_single_list(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    std::forward_list<int> test_slist(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::iterative::linearSearch(test_slist, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_linear_search_single_list);

static void
BM_iterative_linear_search_cd06_single_list(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    cd06::SingleList<int> test_slist(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::iterative::linearSearch(test_slist, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_linear_search_cd06_single_list);

static void
BM_iterative_linear_search_list(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    std::list<int> test_list(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::iterative::linearSearch(test_list, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_linear_search_list);

static void
BM_iterative_linear_search_cd06_list(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    cd06::List<int> test_list(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::iterative::linearSearch(test_list, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_linear_search_cd06_list);

static void
BM_iterative_linear_search_set(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    std::set<int> test_set(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::iterative::linearSearch(test_set, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_linear_search_set);

static void
BM_iterative_linear_search_set_iterator(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    std::set<int> test_set(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::iterative::linearSearch(test_set.begin(), test_set.end(), 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_linear_search_set_iterator);

static void
BM_iterative_linear_search_vector_iterator(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    std::vector<int> test_vector(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::iterative::linearSearch(test_vector.begin(), test_vector.end(), 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_linear_search_vector_iterator);

static void
BM_iterative_binary_search_array(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    while (state.KeepRunning()) {
        cd06::iterative::binarySearch(arr, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_binary_search_array);

static void
BM_iterative_binary_search_std_vector(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    std::vector<int> test_vector(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::iterative::binarySearch(test_vector, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_binary_search_std_vector);

static void
BM_iterative_binary_search_cd06_vector(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    cd06::Vector<int> test_vector(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::iterative::binarySearch(test_vector, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_binary_search_cd06_vector);

static void
BM_recursive_linear_search_array(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    while (state.KeepRunning()) {
        cd06::recursive::linearSearch(arr, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_linear_search_array);

static void
BM_recursive_linear_search_single_list(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    std::forward_list<int> test_slist(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::recursive::linearSearch(test_slist, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_linear_search_single_list);

static void
BM_recursive_linear_search_cd06_single_list(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    cd06::SingleList<int> test_slist(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::recursive::linearSearch(test_slist, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_linear_search_cd06_single_list);

static void
BM_recursive_linear_search_list(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    std::list<int> test_list(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::recursive::linearSearch(test_list, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_linear_search_list);

static void
BM_recursive_linear_search_cd06_list(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    cd06::List<int> test_list(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::recursive::linearSearch(test_list, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_linear_search_cd06_list);

static void
BM_recursive_linear_search_set(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    std::set<int> test_set(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::recursive::linearSearch(test_set, 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_linear_search_set);

static void
BM_recursive_linear_search_set_iterator(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    std::set<int> test_set(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::recursive::linearSearch(test_set.begin(), test_set.end(), 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_linear_search_set_iterator);

static void
BM_recursive_linear_search_vector_iterator(benchmark::State& state)
{
    cd06::iota(arr, arr + SIZE, 0);
    std::vector<int> test_vector(arr, arr + SIZE);
    while (state.KeepRunning()) {
        cd06::recursive::linearSearch(test_vector.begin(), test_vector.end(), 1000);
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_linear_search_vector_iterator);

static void
BM_iterative_pre_order(benchmark::State& state)
{
    std::generate(arr, arr + SIZE, Random());
    cd06::Set<int> mySet(arr, arr + SIZE);
    while (state.KeepRunning()) {
        mySet.preOrder(Find());
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_pre_order);

static void
BM_iterative_in_order(benchmark::State& state)
{
    std::generate(arr, arr + SIZE, Random());
    cd06::Set<int> mySet(arr, arr + SIZE);
    while (state.KeepRunning()) {
        mySet.inOrder(Find());
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_in_order);

static void
BM_iterative_post_order(benchmark::State& state)
{
    std::generate(arr, arr + SIZE, Random());
    cd06::Set<int> mySet(arr, arr + SIZE);
    while (state.KeepRunning()) {
        mySet.postOrder(Find());
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_post_order);

static void
BM_iterative_level_order(benchmark::State& state)
{
    std::generate(arr, arr + SIZE, Random());
    cd06::Set<int> mySet(arr, arr + SIZE);
    while (state.KeepRunning()) {
        mySet.levelOrder(Find());
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_iterative_level_order);

static void
BM_recursive_pre_order(benchmark::State& state)
{
    std::generate(arr, arr + SIZE, Random());
    cd06::Set<int> mySet(arr, arr + SIZE);
    while (state.KeepRunning()) {
        mySet.recursivePreOrder(Find());
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_pre_order);

static void
BM_recursive_in_order(benchmark::State& state)
{
    std::generate(arr, arr + SIZE, Random());
    cd06::Set<int> mySet(arr, arr + SIZE);
    while (state.KeepRunning()) {
        mySet.recursiveInOrder(Find());
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_in_order);

static void
BM_recursive_post_order(benchmark::State& state)
{
    std::generate(arr, arr + SIZE, Random());
    cd06::Set<int> mySet(arr, arr + SIZE);
    while (state.KeepRunning()) {
        mySet.recursivePostOrder(Find());
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_post_order);

static void
BM_recursive_level_order(benchmark::State& state)
{
    std::generate(arr, arr + SIZE, Random());
    cd06::Set<int> mySet(arr, arr + SIZE);
    while (state.KeepRunning()) {
        mySet.recursiveLevelOrder(Find());
    }
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_level_order);

BENCHMARK_MAIN();


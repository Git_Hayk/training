#include "headers/BasicString.hpp"

namespace dsa {

template <typename CharT>
std::basic_istream<CharT>& operator>>(std::basic_istream<CharT>& is, BasicString<CharT>& str)
{
    if (!str.empty()) {
        str.clear();
    }
    for (; is.get(*(str.end_)) && !std::isspace(*(str.end_)); ++str.end_);
    return is;
}

template <typename CharT>
std::basic_ostream<CharT>& operator<<(std::basic_ostream<CharT>& os, const BasicString<CharT>& str)
{
    if (!str.empty()) {
        os << str.begin_;
    }
    return os;
}

template <typename CharT>
BasicString<CharT>::BasicString()
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    allocateMemory();
}

template <typename CharT>
BasicString<CharT>::BasicString(const BasicString& rhv, size_type pos, size_type n)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    const size_type size = (n < rhv.size() ? n : rhv.size()) ;
    allocateMemory(size - pos, rhv.capacity());
    copyData(rhv, pos, size);
}

template <typename CharT>
BasicString<CharT>::BasicString(const value_type* rhv)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    const size_type size = ::strlen(rhv);
    allocateMemory(size);
    copyData(rhv, size);
}

template <typename CharT>
BasicString<CharT>::BasicString(const value_type* rhv, size_type n)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    assert(n <= ::strlen(rhv));
    allocateMemory(n);
    copyData(rhv, n);
}

template <typename CharT>
BasicString<CharT>::BasicString(size_type num, value_type c)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    assert(num <= npos);
    allocateMemory(num);
    copyData(c, num);
}

template <typename CharT>
inline typename BasicString<CharT>::size_type
BasicString<CharT>::size() const
{
    return end_ - begin_;
}

template <typename CharT>
inline typename BasicString<CharT>::size_type
BasicString<CharT>::length() const
{
    return end_ - begin_;
}

template <typename CharT>
inline typename BasicString<CharT>::size_type
BasicString<CharT>::max_size() const
{
    return npos / 2 * sizeof(value_type) - (sizeof(end_) + sizeof(bufferEnd_));
}

template <typename CharT>
inline typename BasicString<CharT>::size_type
BasicString<CharT>::capacity() const
{
    return bufferEnd_ - begin_;
}

template <typename CharT>
inline bool
BasicString<CharT>::empty() const
{
    return begin_ == end_;
}

template <typename CharT>
typename BasicString<CharT>::reference
BasicString<CharT>::operator[](size_type n)
{
    assert(n >= 0);
    return begin_[n];
}

template <typename CharT>
typename BasicString<CharT>::const_reference
BasicString<CharT>::operator[](size_type n) const
{
    assert(n >= 0);
    return begin_[n];
}

template <typename CharT>
BasicString<CharT>::~BasicString() {}

template <typename CharT>
void
BasicString<CharT>::clear()
{
    for (typename BasicString<CharT>::pointer ptr = begin_; ptr != end_; ++ptr) {
        *ptr = '\0';
    }
    end_ = begin_;
}

template <typename CharT>
void
BasicString<CharT>::allocateMemory(const size_type s, const size_type c)
{
    begin_ = new value_type[(c > s ? c : s)];
    end_ = begin_ + s;
    bufferEnd_ = begin_ + (c > s ? c : s);
}

template <typename CharT>
void
BasicString<CharT>::copyData(const BasicString& rhv, const size_type p, const size_type s)
{
    for (size_type i = 0, j = p; i < s; ++i, ++j) {
        *(begin_ + i) = *(rhv.begin_ + j);
    }
}

template <typename CharT>
void
BasicString<CharT>::copyData(const value_type* rhv, const size_type n)
{
    for (size_type i = 0; i < n; ++i) {
        *(begin_ + i) = *(rhv + i);
    }
}

template <typename CharT>
void
BasicString<CharT>::copyData(const value_type c, const size_type n)
{
    for (size_type i = 0; i < n; ++i) {
        *(begin_ + i) = c;
    }
}

} /// namespace dsa


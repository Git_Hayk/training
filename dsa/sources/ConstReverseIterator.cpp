#include "headers/Set.hpp"

namespace cd06 {

template <typename T>
std::istream& operator>>(std::istream& in, typename Set<T>::const_reverse_iterator it)
{
    in >> *it;
    return in;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const typename Set<T>::const_reverse_iterator it)
{
    out << *it;
    return out;
}

template <typename Data>
Set<Data>::const_reverse_iterator::const_reverse_iterator()
    : ptr_(NULL)
{
}

template <typename Data>
Set<Data>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : ptr_(rhv.getPtr())
{
}

template <typename Data>
Set<Data>::const_reverse_iterator::const_reverse_iterator(Node* p)
    : ptr_(p)
{
}

template <typename Data>
Set<Data>::const_reverse_iterator::~const_reverse_iterator()
{
    setPtr(NULL);
}

template <typename Data>
const typename Set<Data>::const_reverse_iterator&
Set<Data>::const_reverse_iterator::operator=(const const_reverse_iterator& rhv)
{
    setPtr(rhv.getPtr());
    return *this;
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_reverse_iterator::operator*() const
{
    return ptr_->data_;
}

template <typename Data>
typename Set<Data>::pointer
Set<Data>::const_reverse_iterator::operator->() const
{
    return ptr_->data_;
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_reverse_iterator::operator++()
{
    return prevInOrder(this);
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_reverse_iterator::operator++(int)
{
    typename Set<Data>::const_reverse_iterator temp = this;
    prevInOrder(this);
    return temp;
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_reverse_iterator::operator--()
{
    return nextInOrder(this);
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_reverse_iterator::operator--(int)
{
    typename Set<Data>::const_reverse_iterator temp = this;
    nextInOrder(this);
    return temp;
}

template <typename Data>
bool
Set<Data>::const_reverse_iterator::operator==(const_reverse_iterator rhv) const
{
    return this->getPtr() == rhv.getPtr();
}

template <typename Data>
bool
Set<Data>::const_reverse_iterator::operator!=(const_reverse_iterator rhv) const
{
    return this->getPtr() != rhv.getPtr();
}

template <typename Data>
typename Set<Data>::Node*
Set<Data>::const_reverse_iterator::getPtr() const
{
    return ptr_;
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::const_reverse_iterator::setPtr(Node* p)
{
    ptr_ = p;
    return *this;
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::const_reverse_iterator::setParent(const_reverse_iterator it)
{
    this->parent() = it.getPtr();
    return *this;
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::const_reverse_iterator::setLeft(const_reverse_iterator it)
{
    this->left() = it.getPtr();
    return *this;
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::const_reverse_iterator::setRight(const_reverse_iterator it)
{
    this->right() = it.getPtr();
    return *this;
}

template <typename Data>
typename Set<Data>::const_reverse_iterator&
Set<Data>::const_reverse_iterator::goParent()
{
    ptr_ = ptr_ -> parent_;
    return *this;
}

template <typename Data>
typename Set<Data>::const_reverse_iterator&
Set<Data>::const_reverse_iterator::goLeft()
{
    ptr_ = ptr_ -> left_;
    return *this;
}

template <typename Data>
typename Set<Data>::const_reverse_iterator&
Set<Data>::const_reverse_iterator::goRight()
{
    ptr_ = ptr_ -> right_;
    return *this;
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::const_reverse_iterator::parent() const
{
    return ptr_ -> parent_;
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::const_reverse_iterator::left() const
{
    return ptr_ -> left_;
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::const_reverse_iterator::right() const
{
    return ptr_ -> right_;
}

template <typename Data>
bool
Set<Data>::const_reverse_iterator::isNull() const
{
    return NULL == ptr_;
}

template <typename Data>
bool
Set<Data>::const_reverse_iterator::isLeftParent() const
{
    return ptr_ == ptr_->parent_->right_;
}

template <typename Data>
bool
Set<Data>::const_reverse_iterator::isRightParent() const
{
    return ptr_ == ptr_->parent_->left_;
}
} /// namespace cd06


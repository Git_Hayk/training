#include "headers/Vector.hpp"

namespace cd06 {

template <typename T>
std::istream& operator>>(std::istream& in, typename Vector<T>::reverse_iterator it)
{
    in >> *it;
    return in;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const typename Vector<T>::reverse_iterator it)
{
    out << *it;
    return out;
}

/// const_itrator
template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator()
    : ptr_(NULL)
{
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator(pointer p)
    : ptr_(p)
{
}

template <typename T>
Vector<T>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : ptr_(rhv.getPtr())
{
}

template <typename T>
Vector<T>::const_reverse_iterator::~const_reverse_iterator()
{
    setPtr(NULL);
}

template <typename T>
void
Vector<T>::const_reverse_iterator::setPtr(pointer p)
{
    ptr_ = p;
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::const_reverse_iterator::getPtr() const
{
    return ptr_;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator&
Vector<T>::const_reverse_iterator::operator=(const const_reverse_iterator& rhv)
{
    ptr_ = rhv.getPtr();
    return *this;
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_reverse_iterator::operator*() const
{
    assert(ptr_ != NULL);
    return *ptr_;
}

template <typename T>
const typename Vector<T>::pointer
Vector<T>::const_reverse_iterator::operator->() const
{
    assert(ptr_ != NULL);
    return ptr_;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator++()
{
    assert(ptr_ != NULL);
    --ptr_;
    return *this;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator++(int)
{
    assert(ptr_ != NULL);
    const_reverse_iterator temp = *this;
    ++(*this);
    return temp;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator--()
{
    assert(ptr_ != NULL);
    ++ptr_;
    return *this;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator--(int)
{
    assert(ptr_ != NULL);
    const_reverse_iterator temp = *this;
    --(*this);
    return temp;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator+=(const size_type count)
{
    assert(ptr_ != NULL);
    ptr_ -= count;
    return *this;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator-=(const size_type count)
{
    assert(ptr_ != NULL);
    ptr_ += count;
    return *this;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator+(const size_type count)
{
    assert(ptr_ != NULL);
    return ptr_ - count;
}

template <typename T>
const typename Vector<T>::const_reverse_iterator
Vector<T>::const_reverse_iterator::operator-(const size_type count)
{
    assert(ptr_ != NULL);
    return ptr_ + count;
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::const_reverse_iterator::operator[](const size_type subscript) const
{
    assert(ptr_ != NULL);
    return ptr_[-subscript];
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator==(const_reverse_iterator rhv) const
{
    return this->getPtr() == rhv.getPtr();
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator!=(const_reverse_iterator rhv) const
{
    return this->getPtr() != rhv.getPtr();
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator<(const_reverse_iterator rhv) const
{
    return this->getPtr() < rhv.getPtr();
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator>(const_reverse_iterator rhv) const
{
    return this->getPtr() > rhv.getPtr();
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator<=(const_reverse_iterator rhv) const
{
    return this->getPtr() <= rhv.getPtr();
}

template <typename T>
bool
Vector<T>::const_reverse_iterator::operator>=(const_reverse_iterator rhv) const
{
    return this->getPtr() >= rhv.getPtr();
}

/// reverse_iterator

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator()
{
    const_reverse_iterator::setPtr(NULL);
}

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator(pointer p)
{
    const_reverse_iterator::setPtr(p);
}

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator(const const_reverse_iterator& rhv)
    : const_reverse_iterator(rhv) /// converter constructor calls base class copy constructor
{
}

template <typename T>
Vector<T>::reverse_iterator::reverse_iterator(const reverse_iterator& rhv)
{
    const_reverse_iterator::setPtr(rhv.getPtr());
}

template <typename T>
Vector<T>::reverse_iterator::~reverse_iterator()
{
    const_reverse_iterator::setPtr(NULL);
}

template <typename T>
typename Vector<T>::reverse_iterator&
Vector<T>::reverse_iterator::operator=(const reverse_iterator& rhv)
{
    const_reverse_iterator::ptr_ = rhv.getPtr();
    return *this;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator+(const size_type count)
{
    assert(const_reverse_iterator::ptr_ != NULL);
    return const_reverse_iterator::ptr_ - count;
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::reverse_iterator::operator-(const size_type count)
{
    assert(const_reverse_iterator::ptr_ != NULL);
    return const_reverse_iterator::ptr_ + count;
}

template <typename T>
typename Vector<T>::reference
Vector<T>::reverse_iterator::operator*()
{
    assert(const_reverse_iterator::ptr_ != NULL);
    return *const_reverse_iterator::ptr_;
}

template <typename T>
typename Vector<T>::pointer
Vector<T>::reverse_iterator::operator->()
{
    assert(const_reverse_iterator::ptr_ != NULL);
    return const_reverse_iterator::ptr_;
}

template <typename T>
typename Vector<T>::reference
Vector<T>::reverse_iterator::operator[](const size_type subscript)
{
    assert(const_reverse_iterator::ptr_ != NULL);
    return const_reverse_iterator::ptr_[-subscript];
}

} /// namespace cd06


#include "headers/Algorithm.hpp"
#include <iostream>

namespace cd06 {

template <typename InputIterator, typename T>
void
fill(InputIterator first, InputIterator last, const T& value)
{
    for ( ; first != last; ++first) {
        *first = value;
    }
}

template <typename OutputIterator, typename Size, typename T>
OutputIterator
fill_n(OutputIterator first, Size n, const T& value)
{
    assert(n >= 0);
    for ( ; n != 0; ++first, --n) {
        *first = value;
    }
    return first;
}

template <typename InputIterator, typename T>
void
iota(InputIterator first, InputIterator last, T value)
{
    for ( ; first != last; ++first, ++value) {
        *first = value;
    }
}

template <typename InputIterator, typename Generator>
void
generate(InputIterator first, InputIterator last, Generator gen)
{
    for ( ; first != last; ++first) {
        *first = gen();
    }
}

template <typename OutputIterator, typename Size, typename Generator>
OutputIterator
generate_n(OutputIterator first, Size n, Generator gen)
{
    assert(n >= 0);
    for ( ; n != 0; ++first, --n) {
        *first = gen();
    }
    return first;
}

template <typename InputIterator, typename T>
T
accumulate(InputIterator first, InputIterator last, T init)
{
    for ( ; first != last; ++first) {
        init += *first;
    }
    return init;
}

template <typename InputIterator, typename EqualityComparable, typename Size>
void
count(InputIterator first, InputIterator last, const EqualityComparable& value, Size& n)
{
    for ( ; first != last; ++first) {
        if (value == *first) {
            ++n;
        }
    }
}

template <typename InputIterator, typename OutputIterator>
OutputIterator
copy(InputIterator first, InputIterator last, OutputIterator result)
{
    for ( ; first != last; ++first, ++result) {
        *result = *first;
    }
    return result;
}

template <typename InputIterator, typename Size, typename OutputIterator>
OutputIterator
copy_n(InputIterator first, Size count, OutputIterator result)
{
    assert(count >= 0);
    for ( ; count != 0; ++first, ++result, --count) {
        *result = *first;
    }
    return result;
}

/// here is an iterative solutions of searching algorithms on
/// data structures such as c-array, single-list, list, set and so on.
namespace iterative {

template <typename EqualityComparable, std::size_t size>
long int
linearSearch(const EqualityComparable (&array)[size], const EqualityComparable& value)
{
    for (size_t index = 0; index < size; ++index) {
        if (array[index] == value) {
            return index;
        }
    }
    return -1;
}

template <typename EqualityComparable>
typename std::forward_list<EqualityComparable>::const_iterator
linearSearch(const std::forward_list<EqualityComparable>& container, const EqualityComparable& value)
{
    for (typename std::forward_list<EqualityComparable>::const_iterator first = container.begin(); first != container.end(); ++first) {
        if (*first == value) {
            return first;
        }
    }
    return container.end();
}

template <typename EqualityComparable>
typename std::list<EqualityComparable>::const_iterator
linearSearch(const std::list<EqualityComparable>& container, const EqualityComparable& value)
{
    for (typename std::list<EqualityComparable>::const_iterator first = container.begin(); first != container.end(); ++first) {
        if (*first == value) {
            return first;
        }
    }
    return container.end();
}

template <typename EqualityComparable>
typename std::set<EqualityComparable>::const_iterator
linearSearch(const std::set<EqualityComparable>& container, const EqualityComparable& value)
{
    for (typename std::set<EqualityComparable>::const_iterator first = container.begin(); first != container.end(); ++first) {
        if (*first == value) {
            return first;
        }
    }
    return container.end();
}

/// same function for cd06 namespace containers
template <typename EqualityComparable>
typename cd06::SingleList<EqualityComparable>::const_iterator
linearSearch(const cd06::SingleList<EqualityComparable>& container, const EqualityComparable& value)
{
    for (typename cd06::SingleList<EqualityComparable>::const_iterator first = container.begin(); first != container.end(); ++first) {
        if (*first == value) {
            return first;
        }
    }
    return container.end();
}

template <typename EqualityComparable>
typename cd06::List<EqualityComparable>::const_iterator
linearSearch(const cd06::List<EqualityComparable>& container, const EqualityComparable& value)
{
    for (typename cd06::List<EqualityComparable>::const_iterator first = container.begin(); first != container.end(); ++first) {
        if (*first == value) {
            return first;
        }
    }
    return container.end();
}

template <typename EqualityComparable>
typename cd06::Set<EqualityComparable>::const_iterator
linearSearch(const cd06::Set<EqualityComparable>& container, const EqualityComparable& value)
{
    for (typename cd06::Set<EqualityComparable>::const_iterator first = container.begin(); first != container.end(); ++first) {
        if (*first == value) {
            return first;
        }
    }
    return container.end();
}

template <typename ForwardIterator, typename EqualityComparable>
ForwardIterator
linearSearch(ForwardIterator first, ForwardIterator last, const EqualityComparable& value)
{
    for ( ; first != last; ++first) {
        if (*first == value) {
            return first;
        }
    }
    return last;
}

template <typename LessThanComparable, std::size_t size>
long int
binarySearch(const LessThanComparable (&array)[size], const LessThanComparable& value)
{
    size_t begin = 0;
    size_t end = size - 1;
    while (begin <= end) {
        size_t middle = begin + (end - begin) / 2;

        if (value == array[middle]) {
            return middle;
        }
        if (value < array[middle]) {
            end = middle - 1;
        } else if (value > array[middle]) {
            begin = middle + 1;
        }
    }
    return -1;
}

template <typename LessThanComparable>
long int
binarySearch(const std::vector<LessThanComparable>& container, const LessThanComparable& value)
{
    size_t begin = 0;
    size_t end = container.size() - 1;
    while (begin <= end) {
        size_t middle = begin + (end - begin) / 2;

        if (value == container[middle]) {
            return middle;
        }
        if (value < container[middle]) {
            end = middle - 1;
        } else if (value > container[middle]) {
            begin = middle + 1;
        }
    }
    return -1;
}

template <typename LessThanComparable>
long int
binarySearch(const cd06::Vector<LessThanComparable>& container, const LessThanComparable& value)
{
    size_t begin = 0;
    size_t end = container.size() - 1;
    while (begin <= end) {
        size_t middle = begin + (end - begin) / 2;

        if (value == container[middle]) {
            return middle;
        }
        if (value < container[middle]) {
            end = middle - 1;
        } else if (value > container[middle]) {
            begin = middle + 1;
        }
    }
    return -1;
}


} /// namespace iterative


namespace recursive {

template <typename EqualityComparable, std::size_t size>
long int
linearSearch(const EqualityComparable (&array)[size], const EqualityComparable& value, long int index)
{
    if (array[index] == value) {
        return index;
    }
    if (index == size) {
        return -1;
    }
    return linearSearch(array, value, ++index);
}

template <typename EqualityComparable>
typename std::forward_list<EqualityComparable>::const_iterator
linearSearch(const std::forward_list<EqualityComparable>& container, const EqualityComparable& value)
{
    return linearSearch(container.begin(), container.end(), value);
}

template <typename EqualityComparable>
typename std::list<EqualityComparable>::const_iterator
linearSearch(const std::list<EqualityComparable>& container, const EqualityComparable& value)
{
    return linearSearch(container.begin(), container.end(), value);
}

template <typename EqualityComparable>
typename std::set<EqualityComparable>::const_iterator
linearSearch(const std::set<EqualityComparable>& container, const EqualityComparable& value)
{
    return linearSearch(container.begin(), container.end(), value);
}

/// same function for cd06 namespace containders
template <typename EqualityComparable>
typename cd06::Vector<EqualityComparable>::const_iterator
linearSearch(const cd06::Vector<EqualityComparable>& container, const EqualityComparable& value)
{
    return linearSearch(container.begin(), container.end(), value);
}

template <typename EqualityComparable>
typename cd06::SingleList<EqualityComparable>::const_iterator
linearSearch(const cd06::SingleList<EqualityComparable>& container, const EqualityComparable& value)
{
    return linearSearch(container.begin(), container.end(), value);
}

template <typename EqualityComparable>
typename cd06::List<EqualityComparable>::const_iterator
linearSearch(const cd06::List<EqualityComparable>& container, const EqualityComparable& value)
{
    return linearSearch(container.begin(), container.end(), value);
}

template <typename EqualityComparable>
typename cd06::Set<EqualityComparable>::const_iterator
linearSearch(const cd06::Set<EqualityComparable>& container, const EqualityComparable& value)
{
    return linearSearch(container.begin(), container.end(), value);
}

template <typename ForwardIterator, typename EqualityComparable>
ForwardIterator
linearSearch(ForwardIterator first, ForwardIterator last, const EqualityComparable& value)
{
    if (!(first == last || *first == value)) {
        return linearSearch(++first, last, value);
    }
    return first;
}

} /// namespace recursive

} /// namespace cd06


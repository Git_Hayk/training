#include "headers/List.hpp"

namespace cd06 {

template <typename T>
bool
operator==(const List<T>& lhv, const List<T>& rhv)
{
    if (lhv.size() != rhv.size()) {
        return false;
    } 
    typename List<T>::const_iterator it1 = lhv.begin();
    typename List<T>::const_iterator it2 = rhv.begin();
    for ( ; it1 != lhv.end(); ++it1, ++it2) {
        if (*it1 != *it2) return false;
    }
    return true;
}

template <typename T>
bool
operator!=(const List<T>& lhv, const List<T>& rhv)
{
    return !(lhv == rhv);
}

template <typename T>
bool
operator<(const List<T>& lhv, const List<T>& rhv)
{
    if (lhv.size() == rhv.size()) {
        typename List<T>::const_iterator it1 = lhv.begin();
        typename List<T>::const_iterator it2 = rhv.begin();
        for ( ; it1 != lhv.end(); ++it1, ++it2) {
            if (*it1 < *it2) return true;
            if (*it1 > *it2) return false;
        }
    }
    return lhv.size() < rhv.size();
}

template <typename T>
bool
operator>(const List<T>& lhv, const List<T>& rhv)
{
    return rhv < lhv;
}

template <typename T>
bool
operator<=(const List<T>& lhv, const List<T>& rhv)
{
    return (lhv < rhv || lhv == rhv);
}

template <typename T>
bool
operator>=(const List<T>& lhv, const List<T>& rhv)
{
    return (lhv > rhv || lhv == rhv);
}

template <typename T>
List<T>::List()
    : begin_(NULL), end_(NULL)
{
    create(0);
}

template <typename T>
List<T>::List(const size_type size)
    : begin_(NULL), end_(NULL)
{
    create(size);
}

template <typename T>
List<T>::List(const size_type size, const_reference init)
    : begin_(NULL), end_(NULL)
{
    assert(size < max_size());
    create(size, init);
}

template <typename T>
List<T>::List(const List<T>& rhv)
    : begin_(NULL), end_(NULL)
{
    const size_type size = rhv.size();
    create(size);
    copy(rhv.begin());
}

template <typename T>
template <typename InputIterator>
List<T>::List(InputIterator first, InputIterator last)
    : begin_(NULL), end_(NULL)
{
    typedef typename std::is_integral<InputIterator>::type Integral;
    initializeRange(first, last, Integral());
}

template <typename T>
List<T>::~List()
{
    if (begin_ != NULL) {
        clear();
        assert(begin_ != NULL && end_ != NULL && begin_ == end_);
        delete begin_;
        begin_ = end_ = NULL;
    }
}

template <typename T>
const List<T>&
List<T>::operator=(const List<T>& rhv)
{
    if (*this == rhv) return *this;
    const size_type rhvSize = rhv.size();
    if (0 == rhvSize) {
        this->clear();
        return *this;
    }
    resize(rhvSize, value_type());
    copy(rhv.begin());
    return *this;
}

template <typename T>
void
List<T>::swap(List<T>& rhv)
{
    Node* tmp = rhv.begin_;
    rhv.begin_ = this->begin_;
    this->begin_ = tmp;
    tmp = rhv.end_;
    rhv.end_ = this->end_;
    this->end_ = tmp;
}

template <typename T>
typename List<T>::size_type
List<T>::size() const
{
    if (empty()) return 0;
    Node* tmp = begin_;
    size_type size = 0;
    while (tmp != end_) {
        ++size;
        tmp = tmp ->next_;
    }
    return size;
}

template <typename T>
typename List<T>::size_type
List<T>::max_size() const
{
    return static_cast<size_type>(ULLONG_MAX / (sizeof(value_type*) + 2 * sizeof(Node*)));
}


template <typename T>
bool
List<T>::empty() const
{
    return end_ == begin_;
}

template <typename T>
void
List<T>::clear()
{
    while (begin_ != end_) {
        pop_back();
    }
}

template <typename T>
void
List<T>::resize(const size_type newSize, const_reference init)
{
    size_type currentSize = this->size();
    while (currentSize != newSize) {
        currentSize < newSize ? push_back(init) : pop_back();
        currentSize < newSize ? ++currentSize : --currentSize;
    }
}

template <typename T>
typename List<T>::reference
List<T>::front()
{
    assert(!empty());
    return *begin();
}

template <typename T>
typename List<T>::const_reference
List<T>::front() const
{
    assert(!empty());
    return *begin();
}

template <typename T>
typename List<T>::reference
List<T>::back()
{
    assert(!empty());
    return end_->prev_->data_;
}

template <typename T>
typename List<T>::const_reference
List<T>::back() const
{
    assert(end_ != NULL);
    return end_->prev_->data_;
}

template <typename T>
void
List<T>::push_front(const_reference value)
{
    if (empty()) {
        begin_ = new Node(value);
        end_->prev_ = end_->next_ = begin_;
        begin_->next_ = begin_->prev_ = end_;
    } else {
        begin_ = new Node(value, begin_, end_);
        begin_->next_->prev_ = end_->next_ = begin_;
    }
}

template <typename T>
void
List<T>::push_back(const_reference value)
{
    if (empty()) {
        begin_ = new Node(value);
        end_->prev_ = end_->next_ = begin_;
        begin_->next_ = begin_->prev_ = end_;
    } else {
        end_->data_ = value;
        end_ = new Node(value, begin_, end_);
        end_->prev_->next_ = begin_->prev_ = end_;
    }
}

template <typename T>
void
List<T>::pop_front()
{
    assert(!empty());
    Node* tmp = begin_;
    begin_ = begin_->next_;
    begin_->prev_ = end_;
    end_->next_ = begin_;
    if (begin_ == end_) {
        begin_->data_ = value_type();
    }
    delete tmp;
}

template <typename T>
void
List<T>::pop_back()
{
    assert(!empty());
    Node* tmp = end_;
    end_ = end_->prev_;
    begin_->prev_ = end_;
    end_->next_ = begin_;
    end_->data_ = (end_ == begin_ ? value_type() : end_->prev_->data_);
    delete tmp;
}

template <typename T>
typename List<T>::const_iterator
List<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename List<T>::const_iterator
List<T>::end() const
{
    return const_iterator(end_);
}

template <typename T>
typename List<T>::iterator
List<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename List<T>::iterator
List<T>::end()
{
    return iterator(end_);
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rbegin() const
{
    return const_reverse_iterator(end_->prev_);
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::rend() const
{
    return const_reverse_iterator(end_);
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rbegin()
{
    return reverse_iterator(end_->prev_);
}

template <typename T>
typename List<T>::reverse_iterator
List<T>::rend()
{
    return reverse_iterator(end_);
}

template <typename T>
typename List<T>::iterator
List<T>::insert(iterator pos, const_reference value)
{
    if (is_begin(pos)) {
        push_front(value);
        return iterator(begin_);
    }
    assert(pos != iterator(NULL));
    --pos;
    return this->insert_after(pos, value);
}

template <typename T>
void
List<T>::insert(iterator pos, const int n, const_reference value)
{
    if (is_begin(pos)) {
        for (int i = 0; i < n; ++i) {
            push_front(value);
        }
        return;
    }
    --pos;
    insert_after(pos, n, value);
}

template <typename T>
template <typename InputIterator>
void
List<T>::insert(iterator pos, InputIterator f, InputIterator l)
{
    if (is_begin(pos)) {
        --l; /// as the last element is not an inclusive;
        for ( ; l != f; --l) {
            push_front(*l);
        }
        push_front(*f);
        return;
    }
    --pos;
    insert_after(pos, f, l);
}

template <typename T>
typename List<T>::iterator
List<T>::insert_after(iterator pos)
{
    if (is_end(pos)) {
        push_back(value_type());
        return pos;
    }
    Node* newNode = new Node(value_type(), pos->next_, pos->next_->prev_);
    newNode->prev_->next_ = newNode;
    newNode->next_->prev_ = newNode;
    ++pos;
    return pos;
}

template <typename T>
typename List<T>::iterator
List<T>::insert_after(iterator pos, const_reference value)
{
    if (is_end(pos )) {
        push_back(value);
        pos = iterator(end_->prev_);
        return pos;
    }
    Node* newNode = new Node(value, pos->next_, pos->next_->prev_);
    newNode->prev_->next_ = newNode;
    newNode->next_->prev_ = newNode;
    ++pos;
    return pos;
}

template <typename T>
void
List<T>::insert_after(iterator pos, const int n, const_reference value)
{
    if (is_end(pos)) {
        for (int i = 0; i < n; ++i) {
            push_back(value);
        }
        return;
    }
    for (int i = 0; i < n; ++i) {
        pos = insert_after(pos, value);
    }
}

template <typename T>
template <typename InputIterator>
void
List<T>::insert_after(iterator pos, InputIterator f, InputIterator l)
{
    if (is_end(pos)) {
        for ( ; f != l; ++f) {
            push_back(*f);
        }
        return;
    }
    if (f == l) return;

    Node* begin = pos.get();
    Node* end = (++pos).get();
    Node* temp = new Node(*f, end, begin);
    Node* firstNode = temp;
    for (++f; f != l; ++f) {
        temp = new Node(*f, end, temp);
        temp->prev_->next_ = temp;
    }
    begin->next_ = firstNode;
    end->prev_ = temp;
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator pos)
{
    assert(!is_end(pos));
    if (!is_begin(pos)) {
        pos->prev_->next_ = pos->next_;
        pos->next_->prev_ = pos->prev_;
        Node* temp = pos.get();
        ++pos;
        delete temp;
        temp = NULL;
        return pos;
    }
    ++pos;
    pop_front();
    return pos;
}

template <typename T>
typename List<T>::iterator
List<T>::erase(iterator f, iterator l)
{
    if (is_begin(f) && is_end(l)) {
        clear();
        *f = 0;
        return f;
    }
    iterator temp = iterator(NULL);
    while (temp != f) {
        temp = iterator(l->prev_);
        temp->prev_->next_ = temp->next_;
        temp->next_->prev_ = temp->prev_;
        delete temp.get();
    }
    return l;
}

template <typename T>
typename List<T>::iterator
List<T>::erase_after(iterator pos)
{
    assert(!is_end(pos));
    return erase(++pos);
}

template <typename T>
void
List<T>::create(const size_type size, const_reference init)
{
    if (size != 0) {
        begin_ = new Node(init);
        end_ = new Node(init, begin_, begin_);
        begin_->next_ = begin_->prev_ = end_;
    } else {
        begin_ = new Node(value_type(), begin_, begin_);
        begin_->prev_ = begin_->next_ = begin_;
        end_ = begin_; // there is a default constructed one element, but list is empty
    }
    for (size_type i = 1; i < size; ++i) {
        end_ = new Node(init, begin_, end_);
        end_->prev_->next_ = begin_->prev_ = end_;
    }
}

template <typename T>
template <typename InputIterator>
void
List<T>::copy(InputIterator rhv)
{
    List<T>::iterator thisIter = this->begin();
    for ( ; !is_end(thisIter); ++rhv, ++thisIter) {
        *thisIter = *rhv;
    }
}

template <typename T>
bool
List<T>::is_begin(const iterator pos) const
{
    return pos == this->begin();
}

template <typename T>
bool
List<T>::is_end(const iterator pos) const
{
    return pos == this->end();
}

template <typename T>
template <typename Integer>
void
List<T>::initializeRange(Integer size, const_reference init, std::true_type)
{
    assert(static_cast<size_type>(size) < max_size());
    create(static_cast<size_type>(size), init);
}

template <typename T>
template <typename InputIterator>
void
List<T>::initializeRange(InputIterator f, InputIterator l, std::false_type)
{
    size_type size = 0;
    InputIterator it = f;
    for ( ; it != l; ++it, ++size);
    assert(size < max_size());
    create(size);
    copy(f);
}

} /// namespace cd06


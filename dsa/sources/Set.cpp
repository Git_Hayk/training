#include "headers/Set.hpp"
#include <vector>

namespace cd06 {

template <typename Data>
Set<Data>::Set()
    : root_(new Node())
    , begin_(root_)
    , end_(root_)
    , size_(0)
{
}

template <typename Data>
template <typename InputIterator>
Set<Data>::Set(InputIterator f, InputIterator l)
    : root_(new Node(*f))
    , begin_(new Node(root_->data_, root_, NULL, NULL))
    , end_(new Node(root_->data_, root_, NULL, NULL))
    , size_(1)
{
    root_->left_ = begin_;
    root_->right_ = end_;
    range_inserter(++f, l);
}

template <typename Data>
Set<Data>::~Set()
{
    if (root_ != NULL) {
        delete root_;
        root_ = NULL;
    }
}

template <typename Data>
const Set<Data>&
Set<Data>::operator=(const Set<Data>& rhv)
{
    this->root_ = rhv.root_;
    return *this;
}

template <typename Data>
void
Set<Data>::swap(Set<Data>& rhv)
{
    Node* temp = rhv.root_;
    rhv.root_ = this->root_;
    this->root_ = temp;
    temp = rhv.begin_;
    rhv.begin_ = this->begin_;
    this->begin_ = temp;
    temp = rhv.end_;
    rhv.end_ = this->end_;
    this->end_ = temp;
    std::swap(this->size_, rhv.size_);
}

template <typename Data>
typename Set<Data>::size_type
Set<Data>::size() const
{
    return size_;
}

template <typename Data>
typename Set<Data>::size_type
Set<Data>::max_size() const
{
    return ULLONG_MAX / (sizeof(value_type*) + 3 * sizeof(Node*));
}

template <typename Data>
bool
Set<Data>::empty() const
{
    return 0 == size_;
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::root() const
{
    return const_iterator(root_);
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::begin() const
{
    return const_iterator(begin_->parent_);
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::end() const
{
    return const_iterator(end_);
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::rbegin() const
{
    return const_reverse_iterator(end_->parent_);
}

template <typename Data>
typename Set<Data>::const_reverse_iterator
Set<Data>::rend() const
{
    return const_reverse_iterator(begin_);
}

template <typename Data>
std::pair<typename Set<Data>::iterator, bool>
Set<Data>::insert(const_reference value)
{
    std::pair<iterator, bool> result = inserter(iterator(root_), value);
    if (result.second) {
        ++size_;
        checkBeginEnd();
    }
    return result;
}

template <typename Data>
template <typename Functor>
void
Set<Data>::preOrder(Functor visit)
{
    if (this->empty()) return;
    Node* current = root_;
    Node* previous;
    while (current != end_) {
        if (current->left_ == NULL || current->left_ == begin_) {
            visit(current->data_);
            current = current->right_;
        } else {
            previous = current->left_;
            while (!(previous->right_ == NULL || previous->right_ == current)) {
                previous = previous->right_;
            }
            if (previous->right_ == current) {
                previous->right_ = NULL;
                current = current->right_;
            } else {
                visit(current->data_);
                previous->right_ = current;
                current = current->left_;
            }
        }
    }
}

template <typename Data>
template <typename Functor>
void
Set<Data>::inOrder(Functor visit)
{
    if (this->empty()) return;
    Node* current = root_;
    Node* previous;
    while (current != end_) {
        if (current->left_ == NULL || current->left_ == begin_) {
            visit(current->data_);
            current = current->right_;
        } else {
            previous = current->left_;
            while (!(previous->right_ == NULL || previous->right_ == current)) {
                previous = previous->right_;
            }
            if (previous->right_ == NULL) {
                previous->right_ = current;
                current = current->left_;
            } else {
                previous->right_ = NULL;
                visit(current->data_);
                current = current->right_;
            }
        }
    }
}

template <typename Data>
template <typename Functor>
void
Set<Data>::postOrder(Functor visit)
{
    if (root_ == NULL) return;

    std::stack<const_iterator> nodeStack;
    Set<Data>::const_iterator it = const_iterator(root_);

    const_iterator last = const_iterator(NULL);
    while (!nodeStack.empty() || !it.isNull()) {
        if (!it.isNull()) {
            nodeStack.push(it);
            it.goLeft();
        } else {
            const_iterator temp = nodeStack.top();
            if (!(temp.right().isNull()) && last != const_iterator(temp.right())) {
                it = const_iterator(temp.right());
            } else {
                visit(*temp);
                last = nodeStack.top();
                nodeStack.pop();
            }
        }
    }
}

template <typename Data>
template <typename Functor>
void
Set<Data>::levelOrder(Functor visit)
{
    if (root_ == NULL) return;

    std::queue<const_iterator> nodeQueue;
    Set<Data>::const_iterator it = const_iterator(root_);

    nodeQueue.push(it);
    while (!nodeQueue.empty()) {
        Set<Data>::const_iterator current = nodeQueue.front();
        nodeQueue.pop();
        visit(*current);
        if (!(current.left().isNull())) {
            nodeQueue.push(current.left());
        }
        if (!(current.right().isNull())) {
            nodeQueue.push(current.right());
        }
    }
}

template <typename Data>
template <typename Functor>
void
Set<Data>::recursivePreOrder(Functor visit)
{
    Set<Data>::const_iterator it = const_iterator(root_);
    recursivePreOrderUtility(visit, it);
}

template <typename Data>
template <typename Functor>
void
Set<Data>::recursiveInOrder(Functor visit)
{
    Set<Data>::const_iterator it = const_iterator(root_);
    recursiveInOrderUtility(visit, it);
}

template <typename Data>
template <typename Functor>
void
Set<Data>::recursivePostOrder(Functor visit)
{
    Set<Data>::const_iterator it = const_iterator(root_);
    recursivePostOrderUtility(visit, it);
}

template <typename Data>
template <typename Functor>
void
Set<Data>::recursiveLevelOrder(Functor visit)
{
    Set<Data>::const_iterator it = const_iterator(root_);
    recursiveLevelOrderUtility(visit, it);
}

template <typename Data>
void
Set<Data>::output() const
{
    outputSetUtility(const_iterator(root_));
}

/// here is implementation of utility private functions
/// ***************************************************

template <typename Data>
std::pair<typename Set<Data>::iterator, bool>
Set<Data>::inserter(typename Set<Data>::iterator ptr, const_reference value)
{
    std::pair<iterator, bool> result(ptr, false);
    typename Set<Data>::iterator previous;

    while (!(ptr.isNull() || result.second)) {

        if (value < *ptr) {
            previous = ptr;
            ptr.goLeft();
        } else if (value > *ptr) {
            previous = ptr;
            ptr.goRight();
        } else {
            return result;
        }

    }
    value < *previous ? previous.createLeft(value) : previous.createRight(value);
    result.first = ptr;
    result.second = true;
    return result;
}

template <typename Data>
template <typename InputIterator>
void
Set<Data>::range_inserter(InputIterator f, InputIterator l)
{
    for ( ; f != l; ++f) {
        std::pair<iterator, bool> isInserted = inserter(iterator(root_), *f);
        if (isInserted.second) {
            ++size_;
            checkBeginEnd();
        }
    }
}

template <typename Data>
inline void
Set<Data>::checkBeginEnd()
{
    if (begin_->left_ != NULL) { /// if new inserted value is the leftmost
        begin_->data_ = begin_->left_->data_;
        begin_ = begin_->left_;
    } else if (end_->right_ != NULL) { /// if new iserted value is the rightmost
        end_->data_ = end_->right_->data_;
        end_ = end_->right_;
    }
}

template <typename Data>
template <typename Functor>
void
Set<Data>::recursivePreOrderUtility(Functor visit, typename Set<Data>::const_iterator it)
{
    if (!it.isNull() && !(it.getPtr() == begin_) && !(it.getPtr() == end_)) {
        visit(*it);
        recursivePreOrderUtility(visit, it.left());
        recursivePreOrderUtility(visit, it.right());
    }
}

template <typename Data>
template <typename Functor>
void
Set<Data>::recursiveInOrderUtility(Functor visit, typename Set<Data>::const_iterator it)
{
    if (!it.isNull() && !(it.getPtr() == begin_) && !(it.getPtr() == end_)) {
        recursiveInOrderUtility(visit, it.left());
        visit(*it);
        recursiveInOrderUtility(visit, it.right());
    }
}

template <typename Data>
template <typename Functor>
void
Set<Data>::recursivePostOrderUtility(Functor visit, typename Set<Data>::const_iterator it)
{
    if (!it.isNull() && !(it.getPtr() == begin_) && !(it.getPtr() == end_)) {
        recursivePostOrderUtility(visit, it.left());
        recursivePostOrderUtility(visit, it.right());
        visit(*it);
    }
}

template <typename Data>
template <typename Functor>
void
Set<Data>::recursiveLevelOrderUtility(Functor visit, typename Set<Data>::const_iterator it)
{
    static std::queue<const_iterator> nodeQueue;
    visit(*it);
    if (!nodeQueue.empty()) {
        nodeQueue.pop();
    }
    if (!it.left().isNull() && !(it.left().getPtr() == begin_)) {
        nodeQueue.push(it.left());
    }
    if (!it.right().isNull() && !(it.right().getPtr() == end_)) {
        nodeQueue.push(it.right());
    }
    if (!nodeQueue.empty()) {
        recursiveLevelOrderUtility(visit, nodeQueue.front());
    }
}

template <typename Data>
void
Set<Data>::nextPreOrder(iterator& ptr)
{
    if (ptr->left_ != NULL) {
        ptr = const_iterator(ptr->left_);
    } else if (ptr->right_ != NULL) {
        ptr = const_iterator(ptr->right_);
    } else {
        ptr = const_iterator(ptr->parent_);
    }
}

template <typename Data>
void
Set<Data>::nextInOrder(iterator& ptr)
{
    if (ptr->left_ != NULL) {
        ptr = const_iterator(ptr->left_);
    } else if (ptr->right_ != NULL) {
        ptr = const_iterator(ptr->right_);
    } else {
        ptr = const_iterator(ptr->parent_);
    }
}

template <typename Data>
void
Set<Data>::outputSetUtility(Set<Data>::const_iterator it, const size_t totalSpaces) const
{
    if (!it.isNull()) {
        outputSetUtility(it.right(), totalSpaces + 5);
        std::cout << std::setw(totalSpaces) << *it << std::endl;
        outputSetUtility(it.left(), totalSpaces + 5);
    }
}

} /// namespace cd06


#include "headers/List.hpp"
#include <iostream>

namespace cd06 {

template <typename T>
std::istream& operator>>(std::istream& in, typename List<T>::reverse_iterator it)
{
    in >> *it;
    return in;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const typename List<T>::reverse_iterator it)
{
    out << *it;
    return out;
}

/// const_itrator
template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator()
    : ptr_(NULL)
{
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(Node* p)
    : ptr_(p)
{
}

template <typename T>
List<T>::const_reverse_iterator::const_reverse_iterator(const const_reverse_iterator& rhv)
    : ptr_(rhv.ptr_)
{
}

template <typename T>
List<T>::const_reverse_iterator::~const_reverse_iterator()
{
    ptr_ = NULL;
}

template <typename T>
const typename List<T>::const_reverse_iterator&
List<T>::const_reverse_iterator::operator=(const const_reverse_iterator& rhv)
{
    ptr_ = rhv.ptr_;
    return *this;
}

template <typename T>
typename List<T>::const_reference
List<T>::const_reverse_iterator::operator*() const
{
    return ptr_->data_;
}

template <typename T>
const typename List<T>::Node*
List<T>::const_reverse_iterator::operator->() const
{
    return ptr_;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator++()
{
    ptr_ = ptr_->prev_;
    return *this;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator++(int)
{
    const_reverse_iterator temp = *this;
    this->ptr_ = this->ptr_->prev_;
    return temp;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator--()
{
    ptr_ = ptr_->next_;
    return *this;
}

template <typename T>
typename List<T>::const_reverse_iterator
List<T>::const_reverse_iterator::operator--(int)
{
    const_reverse_iterator temp = *this;
    this->ptr_ = this->ptr->next_;
    return temp;
}

template <typename T>
bool
List<T>::const_reverse_iterator::operator==(const_reverse_iterator rhv) const
{
    return this->ptr_ == rhv.ptr_;
}

template <typename T>
bool
List<T>::const_reverse_iterator::operator!=(const_reverse_iterator rhv) const
{
    return this->ptr_ != rhv.ptr_;
}

template <typename T>
typename List<T>::Node*
List<T>::const_reverse_iterator::get() const
{
    return ptr_;
}

/// reverse_iterator

template <typename T>
List<T>::reverse_iterator::reverse_iterator()
{
    const_reverse_iterator::ptr_ = NULL;
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator(Node* p)
{
    const_reverse_iterator::ptr_ = p;
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator(const const_reverse_iterator& rhv)
    : const_reverse_iterator(rhv)
{
}

template <typename T>
List<T>::reverse_iterator::reverse_iterator(const reverse_iterator& rhv)
{
    const_reverse_iterator::ptr_ = rhv.ptr_;
}

template <typename T>
List<T>::reverse_iterator::~reverse_iterator()
{
    const_reverse_iterator::ptr_ = NULL;
}

template <typename T>
typename List<T>::reference
List<T>::reverse_iterator::operator*()
{
    return const_reverse_iterator::ptr_->data_;
}

template <typename T>
typename List<T>::Node*
List<T>::reverse_iterator::operator->()
{
    return const_reverse_iterator::ptr_;
}

} /// namespace cd06


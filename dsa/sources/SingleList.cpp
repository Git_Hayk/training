#include "headers/SingleList.hpp"

namespace cd06 {

template <typename T>
bool
operator==(const SingleList<T>& lhv, const SingleList<T>& rhv)
{
    if (lhv.size() != rhv.size()) {
        return false;
    } 
    typename SingleList<T>::const_iterator it1 = lhv.begin();
    typename SingleList<T>::const_iterator it2 = rhv.begin();
    for ( ; it1 != lhv.end(); ++it1, ++it2) {
        if (*it1 != *it2) return false;
    }
    return true;
}

template <typename T>
bool
operator!=(const SingleList<T>& lhv, const SingleList<T>& rhv)
{
    return !(lhv == rhv);
}

template <typename T>
bool
operator<(const SingleList<T>& lhv, const SingleList<T>& rhv)
{
    if (lhv.size() == rhv.size()) {
        typename SingleList<T>::const_iterator it1 = lhv.begin();
        typename SingleList<T>::const_iterator it2 = rhv.begin();
        for ( ; it1 != lhv.end(); ++it1, ++it2) {
            if (*it1 < *it2) return true;
            if (*it1 > *it2) return false;
        }
    }
    return lhv.size() < rhv.size();
}

template <typename T>
bool
operator>(const SingleList<T>& lhv, const SingleList<T>& rhv)
{
    return rhv < lhv;
}

template <typename T>
bool
operator<=(const SingleList<T>& lhv, const SingleList<T>& rhv)
{
    return (lhv < rhv || lhv == rhv);
}

template <typename T>
bool
operator>=(const SingleList<T>& lhv, const SingleList<T>& rhv)
{
    return (lhv > rhv || lhv == rhv);
}

template <typename T>
SingleList<T>::SingleList()
    : begin_(NULL), end_(NULL)
{
    create();
}

template <typename T>
SingleList<T>::SingleList(const size_type size)
    : begin_(NULL), end_(NULL)
{
    create(size);
}

template <typename T>
SingleList<T>::SingleList(const size_type size, const_reference init)
    : begin_(NULL), end_(NULL)
{
    assert(size < max_size());
    create(size, init);
}

template <typename T>
SingleList<T>::SingleList(const SingleList<T>& rhv)
    : begin_(NULL), end_(NULL)
{
    const size_type size = rhv.size();
    create(size);
    copy(rhv.begin());
}

template <typename T>
template <typename InputIterator>
SingleList<T>::SingleList(InputIterator first, InputIterator last)
    : begin_(NULL), end_(NULL)
{
    typedef typename std::is_integral<InputIterator>::type Integral;
    initializeRange(first, last, Integral());
}

template <typename T>
SingleList<T>::~SingleList()
{
    if (begin_ != NULL) {
        clear();
        assert(begin_ != NULL && end_ != NULL && begin_ == end_);
        delete begin_;
        begin_ = end_ = NULL;
    }
}

template <typename T>
const SingleList<T>&
SingleList<T>::operator=(const SingleList<T>& rhv)
{
    if (*this == rhv) return *this;
    const size_type rhvSize = rhv.size();
    if (0 == rhvSize) {
        this->clear();
        return *this;
    }
    resize(rhvSize, value_type());
    copy(rhv.begin());
    return *this;
}

template <typename T>
void
SingleList<T>::swap(SingleList<T>& rhv)
{
    Node* tmp = rhv.begin_;
    rhv.begin_ = this->begin_;
    this->begin_ = tmp;
    tmp = rhv.end_;
    rhv.end_ = this->end_;
    this->end_ = tmp;
}

template <typename T>
typename SingleList<T>::size_type
SingleList<T>::size() const
{
    if (empty()) return 0;
    Node* tmp = begin_;
    size_type size = 0;
    while (tmp != end_) {
        ++size;
        tmp = tmp ->next_;
    }
    return size;
}

template <typename T>
typename SingleList<T>::size_type
SingleList<T>::max_size() const
{
    return static_cast<size_type>(ULLONG_MAX / (sizeof(value_type*) + sizeof(Node*)));
}

template <typename T>
bool
SingleList<T>::empty() const
{
    return end_ == begin_;
}

template <typename T>
void
SingleList<T>::clear()
{
    while (begin_ != end_) {
        pop_front();
    }
}

template <typename T>
void
SingleList<T>::resize(const size_type newSize, const_reference init)
{
    size_type currentSize = this->size();
    while (currentSize != newSize) {
        currentSize < newSize ? push_front(init) : pop_front();
        currentSize < newSize ? ++currentSize : --currentSize;
    }
}

template <typename T>
typename SingleList<T>::reference
SingleList<T>::front()
{
    assert(!empty());
    return *begin();
}

template <typename T>
typename SingleList<T>::const_reference
SingleList<T>::front() const
{
    assert(!empty());
    return *begin();
}

template <typename T>
void
SingleList<T>::push_front(const_reference value)
{
    if (empty()) {
        begin_ = new Node(value, begin_);
        end_->data_ = value;
    } else {
        begin_ = new Node(value, begin_);
    }
}

template <typename T>
void
SingleList<T>::pop_front()
{
    assert(!empty());
    Node* tmp = begin_;
    begin_ = begin_->next_;
    if (begin_ == end_) {
        begin_->data_ = value_type();
    }
    delete tmp;
    tmp = NULL;
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::previous(const_iterator pos) const
{
    const_iterator prev = const_iterator(begin_);
    for ( ; prev.get()->next_ != pos.get(); ++prev);
    return prev;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::previous(iterator pos) const
{
    iterator prev = iterator(begin_);
    for ( ; prev.get()->next_ != pos.get(); ++prev);
    return prev;
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename SingleList<T>::const_iterator
SingleList<T>::end() const
{
    return const_iterator(end_);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::end()
{
    return iterator(end_);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert(iterator pos, const_reference value)
{
    if (is_begin(pos)) {
        push_front(value);
        return iterator(begin_);
    }
    assert(pos != iterator(NULL));
    pos = previous(pos);
    return this->insert_after(pos, value);
}

template <typename T>
void
SingleList<T>::insert(iterator pos, const int n, const_reference value)
{
    if (is_begin(pos)) {
        for (int i = 0; i < n; ++i) {
            push_front(value);
        }
        return;
    }
    pos = previous(pos);
    insert_after(pos, n, value);
}

template <typename T>
template <typename InputIterator>
void
SingleList<T>::insert(iterator pos, InputIterator f, InputIterator l)
{
    if (is_begin(pos)) {
        begin_ = new Node(*f, begin_);
        pos = iterator(begin_);
        ++f;
        insert_after(pos, f, l);
        return;
    }
    pos = previous(pos);
    insert_after(pos, f, l);
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_after(iterator pos)
{
    if (is_end(pos)) {
        Node* tmp = end_;
        tmp->data_ = value_type();
        end_ = new Node(value_type());
        tmp->next_ = end_;
        pos = iterator(tmp);
        return pos;
    }
    Node* newNode = new Node(value_type(), pos->next_);
    pos->next_ = newNode;
    ++pos;
    return pos;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::insert_after(iterator pos, const_reference value)
{
    if (is_end(pos)) {
        Node* tmp = end_;
        tmp->data_ = value;
        end_ = new Node(value);
        tmp->next_ = end_;
        pos = iterator(tmp);
        return pos;
    }
    Node* newNode = new Node(value, pos->next_);
    pos->next_ = newNode;
    ++pos;
    return pos;
}

template <typename T>
void
SingleList<T>::insert_after(iterator pos, const int n, const_reference value)
{
    if (is_end(pos)) {
        for (int i = 0; i < n; ++i) {
            Node* tmp = end_;
            tmp->data_ = value;
            end_ = new Node(value);
            tmp->next_ = end_;
            pos = iterator(tmp);
        }
        return;
    }
    for (int i = 0; i < n; ++i) {
        pos = insert_after(pos, value);
    }
}

template <typename T>
template <typename InputIterator>
void
SingleList<T>::insert_after(iterator pos, InputIterator f, InputIterator l)
{
    if (is_end(pos)) {
        for ( ; f != l; ++f) {
            Node* tmp = end_;
            tmp->data_ = *f;
            end_ = new Node(*f);
            tmp->next_ = end_;
            pos = iterator(tmp);
        }
        return;
    }
    if (f == l) return;

    Node* begin = pos.get();
    Node* end = (++pos).get();
    Node* temp = new Node(*f, end);
    Node* firstNode = temp;
    for (++f; f != l; ++f) {
        Node* prev = temp;
        temp = new Node(*f, end);
        prev->next_ = temp;
    }
    begin->next_ = firstNode;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator pos)
{
    assert(!is_end(pos));
    if (!is_begin(pos)) {
        iterator prev = previous(pos);
        Node* temp = pos.get();
        prev->next_ = temp->next_;
        ++pos;
        delete temp;
        temp = NULL;
        return pos;
    }
    ++pos;
    pop_front();
    return pos;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase(iterator f, iterator l)
{
    if (is_begin(f) && is_end(l)) {
        clear();
        *f = value_type();
        return f;
    }
    iterator prev = previous(f);
    while (f != l) {
        iterator temp = f;
        ++f;
        delete temp.get();
    }
    prev->next_ = l.get();
    return l;
}

template <typename T>
typename SingleList<T>::iterator
SingleList<T>::erase_after(iterator pos)
{
    assert(!is_end(pos));
    return erase(++pos);
}

template <typename T>
void
SingleList<T>::create(const size_type size, const_reference init)
{
    if (size != 0) {
        begin_ = new Node(init);
        end_ = new Node(init);
        begin_->next_ = end_;
    } else {
        begin_ = new Node(value_type());
        end_ = begin_; // there is a default constructed one element, but list is empty
    }
    for (size_type i = 1; i < size; ++i) {
        begin_ = new Node(init, begin_);
    }
}

template <typename T>
template <typename InputIterator>
void
SingleList<T>::copy(InputIterator rhv)
{
    SingleList<T>::iterator thisIter = this->begin();
    for ( ; !is_end(thisIter); ++rhv, ++thisIter) {
        *thisIter = *rhv;
    }
}

template <typename T>
bool
SingleList<T>::is_begin(const iterator pos) const
{
    return pos == this->begin();
}

template <typename T>
bool
SingleList<T>::is_end(const iterator pos) const
{
    return pos == this->end();
}

template <typename T>
template <typename Integer>
void
SingleList<T>::initializeRange(Integer size, const_reference init, std::true_type)
{
    assert(static_cast<size_type>(size) < max_size());
    create(static_cast<size_type>(size), init);
}

template <typename T>
template <typename InputIterator>
void
SingleList<T>::initializeRange(InputIterator f, InputIterator l, std::false_type)
{
    size_type size = 0;
    InputIterator it = f;
    for ( ; it != l; ++it, ++size);
    assert(size < max_size());
    create(size);
    copy(f);
}

} /// namespace cd06


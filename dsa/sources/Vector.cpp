#include "headers/Vector.hpp"

namespace cd06 {

template <typename T>
double
Vector<T>::RESERVE_COEFF = 2.0;

template <typename T>
Vector<T>::Vector()
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
}

template <typename T>
Vector<T>::Vector(const size_type size)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    assert(size < max_size());
    allocateMemory(size);
    for (size_type i = 0; i < size; ++i) {
        begin_[i] = value_type();
    }
}

template <typename T>
Vector<T>::Vector(const size_type size, const_reference init)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    initializeRange(size, init, std::true_type());
}

template <typename T>
Vector<T>::Vector(const Vector<T>& rhv)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    allocateMemory(rhv.size(), rhv.capacity());
    const size_t s = rhv.size();
    for (size_t i = 0; i < s; ++i) {
        begin_[i] = rhv[i];
    }
}

template <typename T>
template <typename InputIterator>
Vector<T>::Vector(InputIterator f, InputIterator l)
    : begin_(NULL)
    , end_(NULL)
    , bufferEnd_(NULL)
{
    typedef typename std::is_integral<InputIterator>::type Integral;
    initializeRange(f, l, Integral());
}

template <typename T>
Vector<T>::~Vector()
{
    if (begin_ != NULL) {
        ::operator delete[](begin_);
        begin_ = end_ = NULL;
    }
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::size() const
{
    return end_ - begin_;
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::max_size() const
{
    return LLONG_MAX / sizeof(value_type);
}

template <typename T>
bool
Vector<T>::empty() const
{
    return begin_ == end_;
}

template <typename T>
typename Vector<T>::size_type
Vector<T>::capacity() const
{
    return bufferEnd_ - begin_;
}

template <typename T>
inline void
Vector<T>::clear()
{
    while (end_ != begin_) {
        pop_back();
    }
}

template <typename T>
void
Vector<T>::resize(const size_t newSize, const_reference init)
{
    reserve(newSize);
    while (this->size() != newSize) {
        newSize > this->size() ? push_back(init) : pop_back();
    }
}

template <typename T>
void
Vector<T>::reserve(const size_type n)
{
    assert(n < max_size());
    if (n > capacity()) {
        const size_t size = this->size();
        pointer temp = reinterpret_cast<pointer>(::operator new(n * sizeof(value_type)));
        for (size_t i = 0; i < size; ++i) {
            temp[i] = begin_[i];
        }
        if (begin_ != NULL) {
            ::operator delete[](begin_);
        }
        begin_ = temp;
        end_ = begin_ + size;
        bufferEnd_ = begin_ + n;
    }
}

template <typename T>
void
Vector<T>::push_back(const_reference element)
{
    if (size() == capacity()) {
        reserve(size() != 0 ? std::ceil(RESERVE_COEFF * capacity()) : 1 );
    }
    ++end_;
    back() = element;
}

template <typename T>
void
Vector<T>::pop_back()
{
    assert(begin_ != NULL);
    --end_;
    end_->~value_type();
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::insert(iterator pos, const_reference init)
{
    insure(pos);
    shift_forward(pos);
    ++end_;
    *pos = init;
    return pos;
}

template <typename T>
void
Vector<T>::insert(iterator pos, int n, const_reference init)
{
    insure(pos);
    shift_forward(pos, n);
    end_ += n;
    for (int i = 0; i < n; ++i, ++pos) {
        *pos = init;
    }
}

template <typename T>
template <typename InputIterator>
void
Vector<T>::insert(iterator pos, InputIterator first, InputIterator last)
{
    insure(pos);
    size_type size = 0;
    for (InputIterator copy = first; copy != last; ++copy, ++size);
    shift_forward(pos, size);
    for ( ; first != last; ++first, ++pos, ++end_) {
        *pos = *first;
    }
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::erase(iterator pos)
{
    iterator tmp = pos - 1;
    shift_backward(pos);
    --end_;
    end_->~value_type();
    return tmp;
}

template <typename T>
typename Vector<T>::iterator 
Vector<T>::erase(iterator first, iterator last)
{
    size_type size = 0;
    iterator copy = first;
    for ( ; copy != last; ++copy, ++size);
    shift_backward(last, size);
    for (size_type i = size; i > 0; --i, --end_) {
        end_->~value_type();
    }
    return copy;
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::begin()
{
    return iterator(begin_);
}

template <typename T>
typename Vector<T>::const_iterator
Vector<T>::end() const
{
    return const_iterator(end_);
}

template <typename T>
typename Vector<T>::iterator
Vector<T>::end()
{
    return iterator(end_);
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rbegin() const
{
    assert(end_ != NULL);
    return const_reverse_iterator(end_ - 1);
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rbegin()
{
    assert(end_ != NULL);
    return reverse_iterator(end_ - 1);
}

template <typename T>
typename Vector<T>::const_reverse_iterator
Vector<T>::rend() const
{
    assert(begin_ != NULL);
    return const_reverse_iterator(begin_ - 1);
}

template <typename T>
typename Vector<T>::reverse_iterator
Vector<T>::rend()
{
    assert(begin_ != NULL);
    return reverse_iterator(begin_ - 1);
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::front() const
{
    assert(begin_ != end_);
    return *begin_;
}

template <typename T>
typename Vector<T>::reference
Vector<T>::front()
{
    assert(begin_ != end_);
    return *begin_;
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::back() const
{
    assert(begin_ != end_);
    return *(end_ - 1);
}

template <typename T>
typename Vector<T>::reference
Vector<T>::back() 
{
    assert(begin_ != end_);
    return *(end_ - 1);
}

template <typename T>
void
Vector<T>::set(const size_t index, const_reference value)
{
    assert(index < size());
    begin_[index] = value;
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::get(const size_t index) const
{
    assert(index < size());
    return begin_[index];
}

template <typename T>
const Vector<T>&
Vector<T>::operator=(const Vector<T>& rhv)
{
    if (this == &rhv) return *this;

    const size_t c = rhv.capacity();
    const size_t s = rhv.size();
    if (capacity() != c) {
        ::operator delete[](begin_);
        begin_ = end_ = NULL;  /// must be checked
        allocateMemory(s, c);
    }

    end_ = begin_;
    const_iterator rhvIt = rhv.begin();
    iterator thisIt = this->begin();
    for ( ; rhvIt != rhv.end(); ++rhvIt, ++thisIt, ++end_) {
        *thisIt = *rhvIt;
    }
    return *this;
}

template <typename T>
typename Vector<T>::const_reference
Vector<T>::operator[](const size_t index) const
{
    validateIndex(index);
    return begin_[index];
}

template <typename T>
typename Vector<T>::reference
Vector<T>::operator[](const size_t index)
{
    validateIndex(index);
    return begin_[index];
}

template <typename T>
bool
Vector<T>::operator==(const Vector<T>& rhv) const
{
    if (this == &rhv) return true;
    
    if (this->size() == rhv.size()) {
        const size_t size = this->size();
        for (size_t i = 0; i < size; ++i) {
            if (this->begin_[i] != rhv.begin_[i]) return false;
        }
        return true;
    }
    return false;
}

template <typename T>
bool
Vector<T>::operator!=(const Vector<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
bool
Vector<T>::operator<(const Vector<T>& rhv) const
{
    if (this == &rhv) return false;

    const size_t size = this->size();
    if (size == rhv.size()) {
        for (size_t i = 0; i < size; ++i) {
            if (this->get(i) < rhv.get(i)) {
                return true;
            }
        }
    }
    return this->size() < rhv.size();
}

template <typename T>
bool
Vector<T>::operator>(const Vector<T>& rhv) const
{
    return rhv < *this;
}

template <typename T>
bool
Vector<T>::operator<=(const Vector<T>& rhv) const
{
    return (*this < rhv || *this == rhv);
}

template <typename T>
bool
Vector<T>::operator>=(const Vector<T>& rhv) const
{
    return (*this > rhv || *this == rhv);
}

template <typename T>
inline void
Vector<T>::allocateMemory(const size_type size, const size_type cap)
{
    begin_ = reinterpret_cast<pointer>(::operator new(size * sizeof(value_type)));
    end_ = begin_ + size;
    bufferEnd_ = begin_ + (cap == 0 ? size : cap);
}

template <typename T>
inline void
Vector<T>::validateIndex(const size_t index) const
{
    const size_t s = this->size();
    if (index >= s) {
        std::cerr << "Error 1: Index out of range!" << std::endl;
        ::exit(1);
    }
}

template <typename T>
void
Vector<T>::insure(iterator& pos)
{
    if (size() == capacity()) { /// save position for new allocation
        const size_type size = &(*pos) - begin_;
        reserve(this->size() != 0 ? std::ceil(RESERVE_COEFF * capacity()) : 1 );
        pos = begin_ + size;
    }
}

template <typename T>
inline void
Vector<T>::shift_forward(iterator pos, const int step)
{
    for (iterator it = end(); it >= pos; --it) {
        *(it + step) = *it;
    }
}

template <typename T>
inline void
Vector<T>::shift_backward(iterator pos, const int step)
{
    for ( ; pos != end(); ++pos) {
        *(pos - step) = *pos;
    }
}

template <typename T>
template <typename Integer>
void
Vector<T>::initializeRange(Integer size, const_reference init, std::true_type)
{
    assert(static_cast<size_type>(size) < max_size());
    allocateMemory(size);
    for (Integer i = 0; i < size; ++i) {
        begin_[i] = init;
    }
}

template <typename T>
template <typename InputIterator>
void
Vector<T>::initializeRange(InputIterator f, InputIterator l, std::false_type)
{
    size_type range = 0;
    for (InputIterator copy = f; copy != l; ++copy, ++range);
    allocateMemory(range);
    for (size_type i = 0; f != l; ++f, ++i) {
        begin_[i] = *f;
    }
}

} /// namespace cd06


#include "headers/Set.hpp"

namespace cd06 {

template <typename T>
std::istream&
operator>>(std::istream& in, typename Set<T>::const_iterator it)
{
    in >> *it;
    return in;
}

template <typename T>
std::ostream&
operator<<(std::ostream& out, const typename Set<T>::const_iterator it)
{
    out << *it;
    return out;
}

template <typename Data>
Set<Data>::const_iterator::const_iterator()
    : ptr_(NULL)
{
}

template <typename Data>
Set<Data>::const_iterator::const_iterator(const const_iterator& rhv)
    : ptr_(rhv.getPtr())
{
}

template <typename Data>
Set<Data>::const_iterator::const_iterator(Node* p)
    : ptr_(p)
{
}

template <typename Data>
Set<Data>::const_iterator::~const_iterator()
{
    ptr_ = NULL;
}

template <typename Data>
const typename Set<Data>::const_iterator&
Set<Data>::const_iterator::operator=(const const_iterator& rhv)
{
    setPtr(rhv.getPtr());
    return *this;
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_iterator::operator*() const
{
    return ptr_->data_;
}

template <typename Data>
typename Set<Data>::pointer
Set<Data>::const_iterator::operator->() const
{
    return ptr_->data_;
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_iterator::operator++()
{
    return nextInOrder(this);
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_iterator::operator++(int)
{
    typename Set<Data>::const_iterator temp = this;
    nextInOrder(this);
    return temp;
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_iterator::operator--()
{
    return prevInOrder(this);
}

template <typename Data>
typename Set<Data>::const_reference
Set<Data>::const_iterator::operator--(int)
{
    typename Set<Data>::const_iterator temp = this;
    prevInOrder(this);
    return temp;
}

template <typename Data>
bool
Set<Data>::const_iterator::operator==(const_iterator rhv) const
{
    return this->getPtr() == rhv.getPtr();
}

template <typename Data>
bool
Set<Data>::const_iterator::operator!=(const_iterator rhv) const
{
    return this->getPtr() != rhv.getPtr();
}

template <typename Data>
typename Set<Data>::Node*
Set<Data>::const_iterator::getPtr() const
{
    return ptr_;
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::setPtr(Node* p)
{
    ptr_ = p;
    return *this;
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::setParent(const_iterator it)
{
    this->parent() = const_iterator(it.getPtr());
    return *this;
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::setLeft(const_iterator it)
{
    this->left() = const_iterator(it.getPtr());
    return *this;
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::setRight(const_iterator it)
{
    this->right() = const_iterator(it.getPtr());
    return *this;
}

template <typename Data>
typename Set<Data>::const_iterator&
Set<Data>::const_iterator::goParent()
{
    ptr_ = ptr_ -> parent_;
    return *this;
}

template <typename Data>
typename Set<Data>::const_iterator&
Set<Data>::const_iterator::goLeft()
{
    ptr_ = ptr_ -> left_;
    return *this;
}

template <typename Data>
typename Set<Data>::const_iterator&
Set<Data>::const_iterator::goRight()
{
    ptr_ = ptr_ -> right_;
    return *this;
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::parent() const
{
    return const_iterator(ptr_ -> parent_);
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::left() const
{
    return const_iterator(ptr_ -> left_);
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::right() const
{
    return const_iterator(ptr_ -> right_);
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::firstLeftParent() const
{
    Node* firstLeft = this->getPtr();
    while (firstLeft->parent_ != NULL && firstLeft != firstLeft->parent_->right_) {
        firstLeft = firstLeft->parent_;
    }
    return const_iterator(firstLeft->parent_);
}

template <typename Data>
typename Set<Data>::const_iterator
Set<Data>::const_iterator::firstRightParent() const
{
    Node* firstRight = this->getPtr();
    while (firstRight->parent_ != NULL && firstRight != firstRight->parent_->left_) {
        firstRight = firstRight->parent_;
    }
    return const_iterator(firstRight->parent_);
}

template <typename Data>
void
Set<Data>::const_iterator::createLeft(const_reference value)
{
    this->getPtr()->left_ = new Node(value, this->getPtr());
}

template <typename Data>
void
Set<Data>::const_iterator::createRight(const_reference value)
{
    this->getPtr()->right_ = new Node(value, this->getPtr());
}

template <typename Data>
bool
Set<Data>::const_iterator::isNull() const
{
    return NULL == ptr_;
}

template <typename Data>
bool
Set<Data>::const_iterator::isLeftParent() const
{
    return ptr_ == ptr_->parent_->right_;
}

template <typename Data>
bool
Set<Data>::const_iterator::isRightParent() const
{
    return ptr_ == ptr_->parent_->left_;
}

template <typename Data>
int
Set<Data>::const_iterator::balanceDiff() const
{
    int leftSideCount = childCount(this->left());
    int rightSideCount = childCount(this->right());
    return leftSideCount - rightSideCount;
}

template <typename Data>
int
Set<Data>::const_iterator::childCount(const_iterator parent) const
{
    if (parent.isNull()) return 0;

    int count = 0;
    std::stack<const_iterator> nodeStack;

    nodeStack.push(parent);
    while (!nodeStack.empty()) {
        parent = nodeStack.top();
        nodeStack.pop();
        ++count;
        if (!(parent.left().isNull())) {
            nodeStack.push(parent.left());
        }
        if (!(parent.right().isNull())) {
            nodeStack.push(parent.right());
        } 
    }
    return count;
}

} /// namespace cd06


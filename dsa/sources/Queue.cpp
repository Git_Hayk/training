#include "headers/Queue.hpp"

namespace cd06 {

template <typename T, typename Sequence>
bool
operator==(const Queue<T, Sequence>& lhv, const Queue<T, Sequence>& rhv)
{
    if (lhv.size() != rhv.size()) return false;
    typename Sequence::const_iterator lit = lhv.begin();
    typename Sequence::const_iterator rit = rhv.begin();
    for ( ; lit != lhv.end(); ++lit, ++rit) {
        if (*lit != *rit) return false;
    }
    assert(rit == rhv.end());
    return true;
}

template <typename T, typename Sequence>
bool
operator!=(const Queue<T, Sequence>& lhv, const Queue<T, Sequence>& rhv)
{
    return !(lhv == rhv);
}

template <typename T, typename Sequence>
bool
operator<(const Queue<T, Sequence>& lhv, const Queue<T, Sequence>& rhv)
{
    if (lhv.size() == rhv.size()) {
        typename Sequence::const_iterator lit = lhv.begin();
        typename Sequence::const_iterator rit = rhv.begin();
        for ( ; lit != lhv.end(); ++lit, ++rit) {
            if (*lit < *rit) return true;
            if (*lit > *rit) return false;
        }
        assert(rit == rhv.end());
        return false;
    }
    return lhv.size() < rhv.size();
}

template <typename T, typename Sequence>
bool
operator>(const Queue<T, Sequence>& lhv, const Queue<T, Sequence>& rhv)
{
    return !(lhv < rhv || lhv == rhv);
}

template <typename T, typename Sequence>
bool
operator<=(const Queue<T, Sequence>& lhv, const Queue<T, Sequence>& rhv)
{
    return (lhv < rhv || lhv == rhv);
}

template <typename T, typename Sequence>
bool
operator>=(const Queue<T, Sequence>& lhv, const Queue<T, Sequence>& rhv)
{
    return (lhv > rhv || lhv == rhv);
}

template <typename T, typename Sequence>
Queue<T, Sequence>::Queue()
    : Sequence()
{
}

template <typename T, typename Sequence>
Queue<T, Sequence>::Queue(const Queue& rhv)
    : Sequence(rhv)
{
}

template <typename T, typename Sequence>
Queue<T, Sequence>& 
Queue<T, Sequence>::operator=(const Queue& rhv)
{
    Sequence::operator=(rhv);
    return *this;
}

template <typename T, typename Sequence>
bool
Queue<T, Sequence>::empty() const
{
    return Sequence::empty();
}

template <typename T, typename Sequence>
typename Queue<T, Sequence>::size_type
Queue<T, Sequence>::size() const
{
    return Sequence::size();
}

template <typename T, typename Sequence>
void
Queue<T, Sequence>::push(const value_type& d)
{
    Sequence::push_front(d);
}

template <typename T, typename Sequence>
void
Queue<T, Sequence>::pop()
{
    Sequence::pop_back();
}

template <typename T, typename Sequence>
typename Queue<T, Sequence>::value_type&
Queue<T, Sequence>::front()
{
    assert(!empty());
    return Sequence::front();
}

template <typename T, typename Sequence>
const typename Queue<T, Sequence>::value_type&
Queue<T, Sequence>::front() const
{
    assert(!empty());
    return Sequence::front();
}
template <typename T, typename Sequence>
typename Queue<T, Sequence>::value_type&
Queue<T, Sequence>::back()
{
    assert(!empty());
    return Sequence::back();
}

template <typename T, typename Sequence>
const typename Queue<T, Sequence>::value_type&
Queue<T, Sequence>::back() const
{
    assert(!empty());
    return Sequence::back();
}

} /// namespace cd06


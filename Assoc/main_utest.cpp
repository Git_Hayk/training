#include "headers/Assoc.hpp"
#include <gtest/gtest.h>

TEST(AssocConst, Num2Str)
{
    for (int i = 0; i < NUMBER_COUNT; ++i) {
        EXPECT_EQ(values[i], getSpelledOutValue(i));
    }
}

TEST(AssocLinear, Str2Num)
{
    for (int i = 0; i < NUMBER_COUNT; ++i) {
        EXPECT_EQ(i, linear::getNumber(values[i]));
    }
}

TEST(AssocLogarithmic, Str2Num)
{
    for (int i = 0; i < NUMBER_COUNT; ++i) {
        EXPECT_EQ(i, logarithmic::getNumber(values[i]));
    }
}

TEST(AssocLogarithmic2, Str2Num)
{
    for (int i = 0; i < NUMBER_COUNT; ++i) {
        EXPECT_EQ(i, logarithmic2::getNumber(values[i]));
    }
}

TEST(AssocLogarithmic3, Str2Num)
{
    for (int i = 0; i < NUMBER_COUNT; ++i) {
        EXPECT_EQ(i, logarithmic3::getNumber(values[i]));
    }
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


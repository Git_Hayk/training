#include "headers/Assoc.hpp"
#include <benchmark/benchmark.h>

static void
BM_const_int_to_str(benchmark::State& state)
{
    while (state.KeepRunning()) {
        for (int i = 0; i < NUMBER_COUNT; ++i) {
            benchmark::DoNotOptimize(getSpelledOutValue(i));
        }
    }
    state.SetBytesProcessed(NUMBER_COUNT * state.iterations());
}
BENCHMARK(BM_const_int_to_str);

static void
BM_linear_str_to_int(benchmark::State& state)
{
    while (state.KeepRunning()) {
        for (int i = 0; i < NUMBER_COUNT; ++i) {
            benchmark::DoNotOptimize(linear::getNumber(values[i]));
        }
    }
    state.SetBytesProcessed(NUMBER_COUNT * state.iterations());
}
BENCHMARK(BM_linear_str_to_int);

static void
BM_logarithmic_str_to_int(benchmark::State& state)
{
    while (state.KeepRunning()) {
        for (int i = 0; i < NUMBER_COUNT; ++i) {
            benchmark::DoNotOptimize(logarithmic::getNumber(values[i]));
        }
    }
    state.SetBytesProcessed(NUMBER_COUNT * state.iterations());
}
BENCHMARK(BM_logarithmic_str_to_int);

static void
BM_logarithmic2_str_to_int(benchmark::State& state)
{
    while (state.KeepRunning()) {
        for (int i = 0; i < NUMBER_COUNT; ++i) {
            benchmark::DoNotOptimize(logarithmic2::getNumber(values[i]));
        }
    }
    state.SetBytesProcessed(NUMBER_COUNT * state.iterations());
}
BENCHMARK(BM_logarithmic2_str_to_int);

static void
BM_logarithmic3_str_to_int(benchmark::State& state)
{
    while (state.KeepRunning()) {
        for (int i = 0; i < NUMBER_COUNT; ++i) {
            benchmark::DoNotOptimize(logarithmic3::getNumber(values[i]));
        }
    }
    state.SetBytesProcessed(NUMBER_COUNT * state.iterations());
}
BENCHMARK(BM_logarithmic3_str_to_int);

BENCHMARK_MAIN();


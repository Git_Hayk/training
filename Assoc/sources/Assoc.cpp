#include "headers/Assoc.hpp"
#include <cassert>

const std::string&
getSpelledOutValue(const int number)
{
    assert(number >= 0 && number <= 9);
    return values[number];
}

namespace linear {

    int
    getNumber(const std::string& str)
    {
        for (int i = 0; i < NUMBER_COUNT; ++i) {
            if (values[i] == str) {
                return i;
            }
        }
        assert(0);
        return -1;
    }

} /// namespace linear

namespace logarithmic {
    namespace recursive {

        int
        binary_search(const std::string* arr, const int& begin, const int& end, const std::string& value)
        {
            if (begin >= end) {
                return - 1;
            }
            const int middle = (begin + end) / 2;
            if (arr[middle] > value) {
                return binary_search(arr, begin, middle, value);
            }
            if (arr[middle] < value) {
                return binary_search(arr, middle + 1, end, value);
            }
            return middle;
        }

    } // namespace recursive

    int
    getNumber(const std::string& str)
    {
        const int index = recursive::binary_search(sorted_values, 0, NUMBER_COUNT, str);
        assert(index >= 0 && index <= 9);
        return sorted_indices[index];
    }

} /// namespace logarithmic

namespace logarithmic2 {
    namespace iterative {

        int
        binary_search(const std::string* arr, int begin, int end, const std::string& value)
        {
            while (begin < end) {
                const int middle = (begin + end) / 2;
                if (arr[middle] > value) {
                    end = middle;
                    continue;
                }
                if (arr[middle] < value) {
                    begin = middle + 1;
                    continue;
                }
                return middle;
            }
            return - 1;
        }

    } // namespace iterative

    int
    getNumber(const std::string& str)
    {
        const int index = iterative::binary_search(sorted_values, 0, NUMBER_COUNT, str);
        assert(index >= 0 && index <= 9);
        return sorted_indices[index];
    }

} /// namespace logarithmic2

namespace logarithmic3 {
    namespace iterative {

        int
        binary_search(const cd06::Pair<std::string, int>* arr, int begin, int end, const std::string& value)
        {
            while (begin < end) {
                const int middle = (begin + end) / 2;
                if (arr[middle].first > value) {
                    end = middle;
                    continue;
                }
                if (arr[middle].first < value) {
                    begin = middle + 1;
                    continue;
                }
                return middle;
            }
            return - 1;
        }

    } // namespace iterative

    int
    getNumber(const std::string& str)
    {
        const int index = iterative::binary_search(sorted_pairs, 0, NUMBER_COUNT, str);
        assert(index >= 0 && index <= 9);
        return sorted_pairs[index].second;
    }

} /// namespace logarithmic3


#ifndef __PAIR_HPP__
#define __PAIR_HPP__

namespace cd06 {

template <typename T1, typename T2>
struct Pair
{
    Pair() {}
    Pair(const T1& first, const T2& second) : first(first), second(second) {}

    T1 first;
    T2 second;
};

template <typename T1, typename T2>
Pair<T1, T2>
make_pair(const T1& first, const T2& second)
{
    return Pair<T1, T2>(first, second);
}

} /// namespace cd06

#endif ///__PAIR_HPP__


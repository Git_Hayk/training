#ifndef __ASSOC_HPP__
#define __ASSOC_HPP__

#include "headers/Pair.hpp"
#include <string>

const int NUMBER_COUNT = 10;
const std::string values[NUMBER_COUNT] = { "zero", "one", "two",   "three", "four",
                                           "five", "six", "seven", "eight", "nine" };

const std::string sorted_values[NUMBER_COUNT] = { "eight", "five", "four",   "nine", "one",
                                                 "seven", "six",  "three",  "two",  "zero" };

const int sorted_indices[NUMBER_COUNT] = { 8, 5, 4, 9, 1, 7, 6, 3, 2, 0 };

const cd06::Pair<std::string, int> sorted_pairs[NUMBER_COUNT] = {
    cd06::make_pair<std::string, int>("eight", 8),
    cd06::make_pair<std::string, int>("five",  5),
    cd06::make_pair<std::string, int>("four",  4),
    cd06::make_pair<std::string, int>("nine",  9),
    cd06::make_pair<std::string, int>("one",   1),
    cd06::make_pair<std::string, int>("seven", 7),
    cd06::make_pair<std::string, int>("six",   6),
    cd06::make_pair<std::string, int>("three", 3),
    cd06::make_pair<std::string, int>("two",   2),
    cd06::make_pair<std::string, int>("zero",  0),
};

const std::string& getSpelledOutValue(const int number);

namespace linear       { int getNumber(const std::string& str); }
namespace logarithmic  { int getNumber(const std::string& str); }
namespace logarithmic2 { int getNumber(const std::string& str); }
namespace logarithmic3 { int getNumber(const std::string& str); }

#endif ///__ASSOC_HPP__


#include "headers/Assoc.hpp"
#include <iostream>
#include <unistd.h>
#include <cstring>

int
main(int argc, char** argv)
{
    if (argc != 2) {
        std::cout << "Usage:\n"
                  << "\t/Assoc_v_dbg [-num2str | -str2num]\n" << std::endl; 
        return 0;
    }

    if (::strcmp(argv[1], "-num2str") == 0) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input number: ";
        }
        int number1;
        std::cin >> number1;
        if (number1 < 0 || number1 > 9) {
            std::cerr << "Error 1: Invalid digit!" << std::endl;
            return 1;
        }
        std::cout << number1 << " is " << getSpelledOutValue(number1) << std::endl;
        return 0;
    }

    if (::strcmp(argv[1], "-str2num") == 0) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input string: ";
        }
        std::string str;
        std::cin >> str;
        ///if (number1 < 0 || number1 > 9) {
        ///    std::cerr << "Error 2: Invalid string!" << std::endl;
        ///    return 2;
        ///}
        std::cout << str << " is " << linear::getNumber(str) << std::endl;
        return 0;
    }

    std::cerr << "Error 3: Invalid argumen!" << std::endl;
    return 3;
}


#include "headers/IntVector.hpp"

#include <cassert>
#include <algorithm>

namespace co06 {

std::istream&
operator>>(std::istream& in, IntVector& v)
{
    char bracket;
    in >> bracket;

    if (bracket != '{') {
        std::cerr << "Error 1: Invalid vector!" << std::endl;
        ::exit(1);
    }
    
    const size_t s = v.size();
    for (size_t i = 0; i < s; ++i) {
        in >> v[i];
    }
    in >> bracket;
    if (bracket != '}') {
        std::cerr << "Error 1: Invalid vector!" << std::endl;
        ::exit(1);
    }
    return in;
}

std::ostream&
operator<<(std::ostream& out, const IntVector& v)
{
    out << "{ ";

    const size_t s = v.size();
    for (size_t i = 0; i < s; ++i) {
        out << v[i] << ' ';
    }

    out << '}';

    return out;
}

IntVector::IntVector(const size_t size)
    : begin_(new int[size])
    , end_(begin_ + size)
{
}

IntVector::IntVector(const IntVector& rhv)
    : begin_(new int[rhv.size()])
    , end_(begin_ + rhv.size())
{
    const size_t s = rhv.size();
    for (size_t i = 0; i < s; ++i) {
        begin_[i] = rhv[i];
    }
}

IntVector::~IntVector()
{
    if (begin_ != NULL) {
        delete [] begin_;
        begin_ = end_ = NULL;
    }
}

const IntVector&
IntVector::operator=(const IntVector& rhv)
{
    if (this == &rhv) {
        return *this;
    }

    const size_t s = rhv.size();
    if (size() != s) {
        if (begin_ != NULL) {
            delete [] begin_;
            begin_ = end_ = NULL;
        }

        begin_ = new int[s];
        end_ = begin_ + s;
    }

    for (size_t i = 0; i < s; ++i) {
        begin_[i] = rhv[i];
    }
    return *this;
}

void
IntVector::resize(const size_t size)
{
    const size_t s = this->size();

    if (s == size) {
        return;
    }

    int* temp = new int[size];
    const size_t minSize = std::min(s, size);

    for (size_t i = 0; i < minSize; ++i) {
        temp[i] = begin_[i];
    }
         
    if (begin_ != NULL) {
        delete [] begin_;
        begin_ = end_ = NULL;
    }
    
    begin_ = temp;
    end_ = begin_ + size;
}

bool
IntVector::empty() const
{
    return begin_ == end_;
}

size_t
IntVector::size() const
{
    return end_ - begin_;
}

void
IntVector::set(const size_t index, const int value)
{
    assert(index < size());
    begin_[index] = value;
}

size_t
IntVector::get(const size_t index) const
{
    assert(index < size());
    return begin_[index];
}

int&
IntVector::operator[](const size_t index)
{
    assert(index < size());
    return begin_[index];
}

int
IntVector::operator[](const size_t index) const
{
    assert(index < size());
    return begin_[index];
}

}


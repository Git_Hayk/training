#include "headers/MiniComputer.hpp"

#include <iostream>
#include <unistd.h>
#include <cstdlib>
#include <cassert>

const int REGISTER_COUNT = 4;

MiniComputer::MiniComputer()
{
    computerName_ = "Mini";
    computerVersion_ = "1.0";
    register_.resize(REGISTER_COUNT);
    isInteractive_ = ::isatty(STDIN_FILENO);
}

MiniComputer::MiniComputer(const std::string name)
{
    setComputerName(name);
}

void
MiniComputer::setComputerName(const std::string name)
{
    if (name.length() > 25) {
        computerName_ = name.substr(0, 25);
        return;
    }

    computerName_ = name;
}

std::string
MiniComputer::getComputerName()
{
    return computerName_;
}

std::string
MiniComputer::getComputerVersion()
{
    return computerVersion_;
}

int
MiniComputer::run()
{
    /// Print program name and version
    printProgramNameAndVersion();

    while (true) {
        /// Print main menu
        printMenu(MAIN_MENU);

        /// Get command from user
        const int command = getCommandFromUser();
        validateCommand(MAIN_MENU, command);

        /// Execute the command
        executeCommand(command);
    } /// Start again from main menu
    return 0;
}

void
MiniComputer::printProgramNameAndVersion()
{
    std::cout << getComputerName() << getComputerVersion() << std::endl;
}

void
MiniComputer::printMenu(const Menu menu)
{
    if (isInteractive_) {
        static const std::string header[MENU_SIZE] = {
            "Main menu", "Load (into register)", "Store menu", "Print (Register or string)", "Add Menu (Register 1 + Register 2 = Register 3)", "Registers", "", "" /// No Menu
        };
        static const int menuSize[MENU_SIZE] = { 5, 4, 2, 2, 4, 4, 0, 0 };
        static const std::string menuItem[MENU_SIZE][5] = {
            { "exit", "laod", "store", "print", "add" },
            { "a", "b", "c", "d" },
            { "store", "more" },
            { "register", "string" },
            { "a", "b", "c", "d" },
            { "a", "b", "c", "d" },
        };
        static const std::string promt[MENU_SIZE] = {
            "Command", "Register", "Store", "Choose", "", "Register", "String Length", "String"
        };
        
        if (menu < PRINT_STRING_LENGTH) {
            std::cout << header[menu] << ":\n";
            for (int i = 0; i < menuSize[menu]; ++i) {
                printMenuItem(menuItem[menu], i);
            }
        }
        
        const std::string& currentPromt = promt[menu];
        printPromt(currentPromt);
    }
}

inline void
MiniComputer::printPromt(const std::string& promt)
{
    if (!promt.empty()) {
        std::cout << "> " << promt << ": ";
    }
}

inline void
MiniComputer::printMenuItem(const std::string menu[], const int index)
{
    std::cout << '\t' << index << " - " << menu[index] << std::endl;
}

void
MiniComputer::executeCommand(const int command)
{
    switch (command) {
    case 0: executeExitCommand();  break;
    case 1: executeLoadCommand();  break;
    case 2: executeStoreCommand(); break;
    case 3: executePrintCommand(); break;
    case 4: executeAddCommand();   break;
    default:
        assert(0 && "There are only five commands");
    }
}

void
MiniComputer::executeExitCommand()
{
    std::cout << "Exiting..." << std::endl;
    ::exit(0);
}

void
MiniComputer::printLoadIntoMenu(const int load)
{
    if (isInteractive_) {
        const char name = getRegisterName(load);
        std::cout << "Load into " << name << std::endl;
        std::cout << "\tInput the value to load into register " << name << '.' << std::endl;
        std::cout << "> "<< name << ": ";
    }
}

char
MiniComputer::getRegisterName(const int load)
{
    const static int STARTING_CHAR = 97; /// characters for register names
    return static_cast<char>(load + STARTING_CHAR);
}

int
MiniComputer::getCommandFromUser()
{
    int command;
    std::cin >> command;
    return command;
}

void
MiniComputer::validateCommand(const Menu menu, const int command)
{
    static const int menuItemCount[MENU_SIZE] = { 5, 4, 2, 2, 4, 4 };
    static const std::string errorMessage[MENU_SIZE] = { 
        "main menu command", "load menu command", "store menu command", "print menu command", "add menu command" , "register"
    };
    
    if (command >= menuItemCount[menu] || command < 0) {
        std::cerr << "Error " << menu + 1 << ": Invalid " << errorMessage[menu] << '!' << std::endl;
        ::exit(menu + 1);
    }
}

void
MiniComputer::setRegisterValue(const int load, const int number)
{
    assert(load >= 0 && load < REGISTER_COUNT && "Invalid register number!");
    register_.set(load, number);
}

int
MiniComputer::getRegisterValue(const int load)
{
    assert(load >= 0 && load < REGISTER_COUNT && "Invalid register number!");
    return register_.get(load);
}

void
MiniComputer::executeLoadCommand()
{
    printMenu(LOAD_MENU);
    const int load = getCommandFromUser();
    validateCommand(REGISTER_MENU, load);
    executeLoadIntoRegister(load);
}

void
MiniComputer::executeLoadIntoRegister(const int load)
{
    printLoadIntoMenu(load);
    const int number = getCommandFromUser();
    setRegisterValue(load, number);
    std::cout << getRegisterName(load) << " = " << getRegisterValue(load) << std::endl;
}

void
MiniComputer::executeStoreCommand()
{
    /// Not implemented yet
    ::abort();
}

void
MiniComputer::executePrintCommand()
{
    printMenu(PRINT_MENU);
    const int print = getCommandFromUser();
    validateCommand(PRINT_MENU, print);
    (0 == print) ? executePrintRegister() : executePrintString();
}

void
MiniComputer::executePrintRegister()
{
    printMenu(REGISTER_MENU);
    const int printRegister = getCommandFromUser();
    validateCommand(REGISTER_MENU, printRegister);
    std::cout << getRegisterValue(printRegister) << std::endl;
}

void
MiniComputer::executePrintString()
{
    printMenu(PRINT_STRING_LENGTH);
    const unsigned length = getCommandFromUser();
    std::cin.ignore(256, '\n');

    printMenu(PRINT_STRING);
    std::string text;
    std::getline(std::cin, text);
    
    printString(length, text);
}

void
MiniComputer::printString(const unsigned length, const std::string& text)
{
    if (text.length() > length) {
        std::cout << text.substr(0, length) << std::endl;
        return;
    }
    std::cout << text << std::endl;
}

void
MiniComputer::executeAddCommand()
{
    printMenu(ADD_MENU);

    const int register1Number = getRegister(1);
    const int register2Number = getRegister(2);
    const int register3Number = getRegister(3);

    std::cout << getRegisterName(register3Number) << " = "
              << getRegisterName(register1Number) << " + "
              << getRegisterName(register2Number) << " = "
              << getRegisterValue(register1Number) << " + "
              << getRegisterValue(register2Number) << " = "
              << calculateAdd(register1Number, register2Number, register3Number) << std::endl;
}

inline int
MiniComputer::getRegister(const int registerMenuNumber)
{
    if (isInteractive_) {
        std::cout << "> Register " << registerMenuNumber << ": ";
    }

    int registerNumber;
    std::cin >> registerNumber;
    validateCommand(REGISTER_MENU, registerNumber);

    return registerNumber;
}

int
MiniComputer::calculateAdd(const int number1, const int number2, const int number3)
{
    const int result = getRegisterValue(number1) + getRegisterValue(number2);
    setRegisterValue(number3, result);

    return result;
}


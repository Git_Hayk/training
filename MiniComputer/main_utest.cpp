#include "headers/IntVector.hpp"
#include <gtest/gtest.h>

TEST(IntVectorTest, DefaultConstSize)
{
    co06::IntVector v;
    const size_t x = 10;
    EXPECT_EQ(v.size(), x);

    const size_t SIZE = 24;
    co06::IntVector v1(SIZE);
    EXPECT_EQ(v1.size(), SIZE);
}

TEST(IntVectorTest, DefaultConstEmpty)
{
    co06::IntVector v;
    EXPECT_FALSE(v.empty());

    co06::IntVector v1(0);
    EXPECT_TRUE(v1.empty());
}

TEST(IntVectorTest, Stream)
{
    co06::IntVector v;
    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v;

    std::ostringstream out;
    out << v;

    EXPECT_EQ(out.str(), str);
}

TEST(IntVectorTest, Copy)
{
    co06::IntVector v;
    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v;

    const co06::IntVector cv(v);
    std::ostringstream out;
    out << cv;

    EXPECT_EQ(out.str(), str);
}

TEST(IntVectorTest, Assignment)
{
    co06::IntVector v;
    const std::string str = "{ 1 2 3 4 5 6 7 8 9 10 }";
    std::istringstream in(str);
    in >> v;

    co06::IntVector v1, v2;
    v1 = v2 = v;
    std::ostringstream out;
    out << v1;

    EXPECT_EQ(out.str(), str);
}

TEST(IntVectorTest, IndexAssignmentTest)
{
    co06::IntVector v;
    v[4] = 30;
    EXPECT_EQ(static_cast<int>(v.get(4)), 30);

    for (int i = 0; i < 10; ++i) {
        EXPECT_EQ(static_cast<int>(v.get(i)), v[i]);
    }
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


#ifndef __MINI_COMPUTER_HPP__
#define __MINI_COMPUTER_HPP__

#include "IntVector.hpp"
#include <string>

class MiniComputer
{
public:
    enum Menu {
        MAIN_MENU,
        LOAD_MENU,
        STORE_MENU,
        PRINT_MENU,
        ADD_MENU,
        REGISTER_MENU,
        PRINT_STRING_LENGTH,
        PRINT_STRING,
        MENU_SIZE
    };

public:
    MiniComputer();
    MiniComputer(std::string name);
    void setComputerName(std::string name);
    std::string getComputerName();
    std::string getComputerVersion();
    int run();

private:
    void printProgramNameAndVersion();
    void printMenu(const Menu menu);
    void printPromt(const std::string& promt);
    void printLoadIntoMenu(int load);

    void printMenuItem(const std::string mainMenu[], const int index);
    char getRegisterName(int load);

    int  getCommandFromUser();
    void validateCommand(const Menu menu, const int command);
    void executeCommand(int command);

    void executeExitCommand();
    void executeLoadCommand();
    void executeStoreCommand();
    void executePrintCommand();
    void executeAddCommand();

    void executeLoadIntoRegister(int load);
    void setRegisterValue(int load, int number);
    int  getRegisterValue(int load);
    int  calculateAdd(int number1, int number2, int number3);

    int  getRegister(const int number);

    void executePrintRegister();
    void executePrintString();

    void printString(const unsigned lenght, const std::string& text);
    
private:
    std::string computerName_;
    std::string computerVersion_;
    bool isInteractive_;
    co06::IntVector register_;
};

#endif /// __MINI_COMPUTER_HPP__

#ifndef __INT_VECTOR_HPP__
#define __INT_VECTOR_HPP__

#include <iostream>

namespace co06 {

const size_t DEFAULT_SIZE = 10;

class IntVector;
std::istream& operator>>(std::istream& in, IntVector& v);
std::ostream& operator<<(std::ostream& out, const IntVector& v);

class IntVector
{
///friend std::istream& operator>>(std::istream& in, IntVector& v);
///friend std::ostream& operator<<(std::ostream& out, const IntVector& v);

public:
    IntVector(const size_t size = DEFAULT_SIZE);
    IntVector(const IntVector& rhv);
    ~IntVector();
    const IntVector& operator=(const IntVector& rhv);
    void resize(const size_t size);
    bool empty() const;
    size_t size() const;
    void set(const size_t index, const int value);
    size_t get(const size_t index) const;
    int& operator[](const size_t index);
    int operator[](const size_t index) const;

private:
    int* begin_;
    int* end_;
};

}

#endif /// __INT_VECTOR_HPP__


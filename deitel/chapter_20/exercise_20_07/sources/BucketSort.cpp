#include "headers/BucketSort.hpp"

BucketSort::BucketSort()
{
    data_.resize(20);
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(NULL));
    }
    std::generate(data_.begin(), data_.end(), std::rand);
}

BucketSort::BucketSort(const size_t* begin, const size_t* end)
    : data_(begin, end)
{
}

void
BucketSort::displayData() const
{
    std::cout << "{ ";
    const int size = static_cast<int>(data_.size());
    for (int i = 0; i < size; ++i) {
        std::cout << data_[i] << (i == size - 1 ? " " : ", ");
    }
    std::cout << "}" << std::endl;
}

void
BucketSort::sort()
{
    const size_t bucketSize = 10;
    /// creatin a vector of vectors with 10 rows and data_.size() columns
    std::vector<std::vector<size_t> > bucket(bucketSize);
    for (size_t i = 0; i < bucketSize; ++i) {
        bucket[i].reserve(data_.size());
    }
    
    int devider = 10;
    while (true) {
        for (size_t i = 0; i < data_.size(); ++i) {
            const int remainder = data_[i] % devider / (devider / 10);
            bucket[remainder].push_back(data_[i]);
        }

        /// if all element were placed in 0th row of bucket vector, it means that
        /// there are no element with higher rank, and data_ is already sorted
        if (bucket[0].size() == data_.size()) break;

        int index = 0;
        for (size_t i = 0; i < bucketSize; ++i) {
            for (size_t j = 0; j < bucket[i].size(); ++j, ++index) {
                data_[index] = bucket[i][j];
            }
            bucket[i].clear();
        }
        devider *= 10;
    }
}

void
BucketSort::print(const std::vector<std::vector<size_t> >& mat)
{
    for (size_t row = 0; row < mat.size(); ++row) {
        for (size_t column = 0; column < mat[row].capacity(); ++column) {
            if (0 == mat[row][column]) {
                std::cout << "- ";
            } else {
                std::cout << mat[row][column] << " ";
            }
        }
        std::cout << std::endl;
    }
}


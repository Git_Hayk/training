#include "headers/BucketSort.hpp"
#include <unistd.h>

bool isInteractive = ::isatty(STDIN_FILENO);

void fill(size_t* begin, const size_t* end);

int
main()
{
    if (isInteractive) {
        std::cout << "Input vector size: ";
    }
    size_t size;
    std::cin >> size;

    if (size < 0) {
        std::cerr << "Error 1: Invalid size (less than zero)!" << std::endl;
        return 1;
    }

    size_t* arr = new size_t[size];

    if (isInteractive) {
        std::cout << "Input " << size << " size_teger values to store in vector : ";
    }
    fill(arr, arr + size);

    BucketSort vect(arr, arr + size);

    std::cout << "Unsorted vector\n";
    vect.displayData();

    vect.sort();

    std::cout << "Sorted vector\n";
    vect.displayData();
    return 0;
}

void
fill(size_t* begin, const size_t* end)
{
    for ( ; begin != end; ++begin) {
        std::cin >> *begin;
    }
}


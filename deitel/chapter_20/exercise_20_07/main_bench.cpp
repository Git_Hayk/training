#include "headers/BucketSort.hpp"
#include <benchmark/benchmark.h>

static void
BM_example(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    while (state.KeepRunning()) {
        BucketSort vect;
        vect.sort();
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_example);

BENCHMARK_MAIN();


#ifndef __BUCKET_SORT_HPP__
#define __BUCKET_SORT_HPP__

#include <iostream>
#include <algorithm>
#include <unistd.h>
#include <vector>

class BucketSort
{
public:
    BucketSort();
    BucketSort(const size_t* begin, const size_t* end);

    void sort();
    void displayData() const;
private:
    void print(const std::vector<std::vector<size_t> >& mat);
private:
    std::vector<size_t> data_;
};

#endif ///__BUCKET_SORT_HPP__


#include "headers/bubbleSort.hpp"

template <typename T, typename Generator>
void
fill(std::vector<T>& v, const int size, Generator gen)
{
    v.reserve(size);

    for (int i = 0; i < size; ++i) {
        v.push_back(10 + gen() % 90);
    }
}

template <typename T>
void
print(const std::vector<T>& v)
{
    const int size = v.size();
    for (int i = 0; i < size; ++i) {
        std::cout << v[i] << ' ';
    }
    std::cout << std::endl;
}

template <typename LessThanComparable>
void
bubbleSort(std::vector<LessThanComparable>& v)
{
    const int size = v.size();
    for (int i = 0; i < size - 1; ++i) {
        for (int j = 1; j < size; ++j) {
            if (v[j] < v[j - 1]) {
                std::swap(v[j], v[j - 1]);
            }
        }
    }
}


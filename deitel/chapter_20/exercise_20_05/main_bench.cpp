#include "headers/bubbleSort.hpp"
#include <benchmark/benchmark.h>
#include <vector>

const int SIZE = 2<<10;

static void
BM_bubble_sort(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    while (state.KeepRunning()) {
        std::vector<int> intVector;
        fill(intVector, SIZE, std::rand);
        bubbleSort(intVector);
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_bubble_sort);

BENCHMARK_MAIN();


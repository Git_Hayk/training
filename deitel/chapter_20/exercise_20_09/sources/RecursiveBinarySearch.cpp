#include "headers/RecursiveBinarySearch.hpp"

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <ctime>
#include <algorithm> // prototype for sort function

BinarySearch::BinarySearch(int vectorSize)
{
    size_ = (vectorSize > 0 ? vectorSize : 10);
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    }

    for (int i = 0; i < size_; ++i) {
        data_.push_back(10 + std::rand() % 90);
    }

    std::sort(data_.begin(), data_.end());
}

int
BinarySearch::recursiveBinarySearch(const int searchElement, int low, int high) const
{
    const int middle = (low + high) / 2;
    displaySubElements(low, high);
    for (int i = 0; i < middle; ++i) {
        std::cout << "   ";             
    }     

    std::cout << " * " << std::endl; // indicate current middle

    if (searchElement != data_[middle] && low == high) {
        return -1;
    } else if (searchElement < data_[middle]) {
        return recursiveBinarySearch(searchElement, low, middle - 1);
    } else if (searchElement > data_[middle]) {
        return recursiveBinarySearch(searchElement, middle + 1, high);
    }
    return middle;
}

void
BinarySearch::displayElements() const
{
    displaySubElements(0, size_ - 1);
}

void
BinarySearch::displaySubElements(int low, int high) const
{
    for ( int i = 0; i < low; ++i) {
        std::cout << "  ";
    }
    for (int i = low; i <= high; ++i) {
        std::cout << data_[i] << " ";
    }
    std::cout << std::endl;
}


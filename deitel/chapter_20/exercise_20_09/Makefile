progname=$(shell basename $(CURDIR))
utest=utest_$(progname)
bench=bench_$(progname)
CXX=g++
CXXFLAGS=-Wall -Wextra -Werror -std=c++03
INCLUDEFLAG=-I.

PASSED=$@\033[0;32m PASSED \033[0m        #green
FAILED=$@\033[0;31m FAILED \033[0m        #red
NEGATIVE=\033[0;33m Negative $@...\033[0m #yellow

BUILDS=builds
HEADER_DIR=headers
SOURCE_DIR=sources
TEST_DIR=tests

ifeq ($(MAKECMDGOALS), )
	BUILD_DIR=$(BUILDS)/debug
        VERSION=_v_dbg
else
	BUILD_DIR=$(BUILDS)/$(MAKECMDGOALS)
        VERSION=_v_rls
endif

SOURCES:=main.cpp $(wildcard $(SOURCE_DIR)/*.cpp)
DEPENDS:=$(patsubst %.cpp, $(BUILD_DIR)/%.d, $(SOURCES))
PREPROCS:=$(patsubst %.cpp, $(BUILD_DIR)/%.ii, $(SOURCES))
ASSEMBLES:=$(patsubst %.cpp, $(BUILD_DIR)/%.s, $(SOURCES))
OBJS:=$(patsubst %.cpp, $(BUILD_DIR)/%.o, $(SOURCES))

UTEST_SOURCES:=main_utest.cpp $(wildcard $(SOURCE_DIR)/*.cpp)
UTEST_DEPENDS:=$(patsubst %.cpp, $(BUILD_DIR)/%.d, $(UTEST_SOURCES))
UTEST_PREPROCS:=$(patsubst %.cpp, $(BUILD_DIR)/%.ii, $(UTEST_SOURCES))
UTEST_ASSEMBLES:=$(patsubst %.cpp, $(BUILD_DIR)/%.s, $(UTEST_SOURCES))
UTEST_OBJS:=$(patsubst %.cpp, $(BUILD_DIR)/%.o, $(UTEST_SOURCES))

BENCH_SOURCES:=main_bench.cpp $(wildcard $(SOURCE_DIR)/*.cpp)
BENCH_DEPENDS:=$(patsubst %.cpp, $(BUILD_DIR)/%.d, $(BENCH_SOURCES))
BENCH_PREPROCS:=$(patsubst %.cpp, $(BUILD_DIR)/%.ii, $(BENCH_SOURCES))
BENCH_ASSEMBLES:=$(patsubst %.cpp, $(BUILD_DIR)/%.s, $(BENCH_SOURCES))
BENCH_OBJS:=$(patsubst %.cpp, $(BUILD_DIR)/%.o, $(BENCH_SOURCES))

TEST_INPUTS:=$(wildcard $(TEST_DIR)/test*.input)
TESTS:=$(patsubst $(TEST_DIR)/%.input, %, $(TEST_INPUTS))

debug: $(BUILD_DIR) test1
release: $(BUILD_DIR) $(BUILD_DIR)/$(progname)

debug: CXXFLAGS+=-g3
release: CXXFLAGS+=-g0 -DNDEBUG

debug: qa utest bench
release: bench

qa: $(TESTS)

utest: $(BUILD_DIR)/$(utest)
	./$<

bench: $(BUILD_DIR)/$(bench)
	./$<

test%: $(BUILD_DIR)/$(progname)
	@./$< < $(TEST_DIR)/$@.input > $(BUILD_DIR)/$(TEST_DIR)/$@.output || echo "$(NEGATIVE)"
	@diff $(BUILD_DIR)/$(TEST_DIR)/$@.output $(TEST_DIR)/$@.expected > /dev/null && echo "$(PASSED)" || echo "$(FAILED)"

$(BUILD_DIR)/$(utest): $(UTEST_OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) -lgtest -lpthread $^ -o $@
	rm -rf ${utest}
	ln $(BUILD_DIR)/$(utest) ${utest}

$(BUILD_DIR)/$(bench): $(BENCH_OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) -lbenchmark -lpthread $^ -o $@
	rm -rf ${bench}${VERSION}
	ln $(BUILD_DIR)/$(bench) ${bench}${VERSION}

$(BUILD_DIR)/$(progname): $(OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) $^ -o $@
	rm -rf ${progname}${VERSION}
	ln $(BUILD_DIR)/$(progname) ${progname}${VERSION}

$(BUILD_DIR)/%.ii: %.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDEFLAG) -E $< -o $@
	$(CXX) $(CXXFLAGS) $(INCLUDEFLAG) -MT $@ -MM $< > $(patsubst %.cpp, $(BUILD_DIR)/%.d, $<)

$(BUILD_DIR)/%.s: $(BUILD_DIR)/%.ii
	$(CXX) $(CXXFLAGS) -S $< -o $@

$(BUILD_DIR)/%.o: $(BUILD_DIR)/%.s
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(BUILD_DIR):
	@mkdir -p $(BUILD_DIR)
	@mkdir -p $(BUILD_DIR)/$(SOURCE_DIR) $(BUILD_DIR)/$(TEST_DIR)

.gitignore:
	@echo $(BUILDS)                > .gitignore
	@echo $(utest)                >> .gitignore
	@echo $(bench)*               >> .gitignore
	@echo $(progname)_v*          >> .gitignore
	@echo .vscode/                >> .gitignore

clean:
	@rm -rfv $(BUILDS) ${progname}* $(utest) $(bench)* .gitignore > /dev/null
	ls -ali

.PRECIOUS: $(PREPROCS) $(ASSEMBLES) $(UTEST_PREPROCS) $(UTEST_ASSEMBLES) $(BENCH_PREPROCS) $(BENCH_ASSEMBLES) 
.SECONDARY: $(PREPROCS) $(ASSEMBLES) $(UTEST_PREPROCS) $(UTEST_ASSEMBLES) $(BENCH_PREPROCS) $(BENCH_ASSEMBLES)

sinclude $(DEPENDS) ($UTEST_DEPENDS) ($BENCH_DEPENDS)

#include "headers/RecursiveBinarySearch.hpp"
#include <iostream>

int
main()
{

    BinarySearch searchVector(15);
    searchVector.displayElements();

    int searchInt;
    std::cout << "\nPlease enter an integer value (-1 to quit): ";
    std::cin >> searchInt; // read an int from user
    std::cout << std::endl;

    int position;
    while (searchInt != -1) {
       position = searchVector.recursiveBinarySearch(searchInt, 0, searchVector.size() - 1);

       if (position == -1) { 
           std::cout << "The integer " << searchInt << " was not found.\n";
       } else { 
           std::cout << "The integer " << searchInt
                << " was found in position " << position << ".\n";
       }

       std::cout << "\n\nPlease enter an integer value (-1 to quit): ";
       std::cin >> searchInt;
       std::cout << std::endl;
    }
    return 0;
}


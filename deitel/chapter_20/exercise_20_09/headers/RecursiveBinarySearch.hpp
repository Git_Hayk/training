#ifndef __RECURSIVE_BINARY_SEARCH_HPP__
#define __RECURSIVE_BINARY_SEARCH_HPP__

#include <vector>

class BinarySearch
{
public:
    BinarySearch(int vectorSize);
    int recursiveBinarySearch(int searchElement, int low, int high) const;
    void displayElements() const;
    int size() const { return size_; }
private:
    void displaySubElements(int low, int high) const;
private:
    int size_;
    std::vector<int> data_;
}; // end class BinarySearch

#endif ///__RECURSIVE_BINARY_SEARCH_HPP__


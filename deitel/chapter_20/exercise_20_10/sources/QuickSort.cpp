#include "headers/QuickSort.hpp"

template <typename T>
Quick<T>::Quick(const int size)
{
    const int s = (size > 0 ? size : 10);
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }
    data_.reserve(s);
    for (int i = 0; i < s; ++i) {
        data_.push_back(LOWER_LIMIT + std::rand() % UPPER_LIMIT);
    }
}

template <typename T>
Quick<T>::Quick(const std::vector<T>& rhv)
    : data_(rhv)
{
}

template <typename T>
Quick<T>::Quick(const T* begin, const T* end)
    : data_(begin, end)
{
}

template <typename T>
void
Quick<T>::quickSort()
{
    const int begin = 0;
    const int end = data_.size() - 1;
    quickSortHelper(begin , end);
}

template <typename T>
void
Quick<T>::displayData() const
{
    std::cout << "{ ";
    const int size = static_cast<int>(data_.size());
    for (int i = 0; i < size; ++i) {
        std::cout << data_[i] << (i == size - 1 ? " " : ", ");
    }
    std::cout << "}" << std::endl;
}

template <typename T>
void
Quick<T>::quickSortHelper(const int begin, const int end)
{
    if (begin < end) {
        int middle = partition(begin, end);
        quickSortHelper(begin, middle - 1);
        quickSortHelper(middle + 1, end);
    }
}

template <typename T>
int
Quick<T>::partition(const int begin, const int end)
{
    T pivot = data_[end];
    int i = begin - 1;
    for (int j = begin; j < end; ++j) {
        if (data_[j] <= pivot) {
            ++i;
            std::swap(data_[i], data_[j]);
        }
    }
    std::swap(data_[i + 1], data_[end]);
    return i + 1;
}


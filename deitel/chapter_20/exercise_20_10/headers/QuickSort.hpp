#ifndef __QUICK_SORT_HPP__
#define __QUICK_SORT_HPP__

#include <iostream>
#include <unistd.h>
#include <vector>
#include <ctime>

const long LOWER_LIMIT = 10;
const long UPPER_LIMIT = 90;

template <typename T>
class Quick
{
public:
    Quick(const int size);
    Quick(const std::vector<T>& rhv);
    Quick(const T* begin, const T* end);

    void quickSort();
    void displayData() const;
private:
    int  partition(const int begin, const int end);
    void quickSortHelper(const int begin, const int end);
private:
    std::vector<T> data_;
};

#include "sources/QuickSort.cpp"

#endif ///__QUICK_SORT_HPP__


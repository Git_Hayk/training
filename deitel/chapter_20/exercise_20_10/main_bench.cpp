#include "headers/QuickSort.hpp"
#include <benchmark/benchmark.h>

static void
BM_quick_sort(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);

    int arr[] = { 37, 2, 6, 4, 89, 8, 10, 12, 68, 45 };
    const int size = sizeof(arr) / sizeof(arr[0]);
    Quick<int> quickVector(arr, arr + size);

    while (state.KeepRunning()) {
        quickVector.quickSort();
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_quick_sort);

BENCHMARK_MAIN();


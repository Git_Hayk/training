#include "headers/QuickSort.hpp"
#include <vector>

int
main()
{
    int arr[] = { 37, 2, 6, 4, 89, 8, 10, 12, 68, 45 };
    const int size = sizeof(arr) / sizeof(arr[0]);
    Quick<int> quickVector(arr, arr + size);

    std::cout << "Unsorted vector: ";
    quickVector.displayData();

    quickVector.quickSort();

    std::cout << "Sorted vector: ";
    quickVector.displayData();

    return 0;
}


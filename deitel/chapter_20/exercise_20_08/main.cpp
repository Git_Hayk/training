#include "headers/RecursiveLinearSearch.hpp"
#include <vector>
#include <list>
#include <forward_list>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);
const int SIZE = 20;

int
main()
{
    if (isInteractive) std::srand(::time(0));

    std::vector<int> intVector;
    fill(intVector, SIZE, std::rand);
    std::cout << "IntVector: ";
    print(intVector.begin(), intVector.end());

    int value;
    if (isInteractive) std::cout << "Input value to search in IntVector: ";
    std::cin >> value;
    int index = recursiveLinearSearch(intVector.begin(), intVector.end(), value);

    if (index < 0) {
        std::cout << "Value was not found." << std::endl;
    } else {
        std::cout << "Value was found at index " << index << std::endl;
    }

    return 0;
}


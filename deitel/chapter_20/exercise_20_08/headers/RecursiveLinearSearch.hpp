#ifndef __RECURSIVE_LINEAR_SEARCH_HPP__
#define __RECURSIVE_LINEAR_SEARCH_HPP__

#include <iostream>
#include <vector>

const long UPPER_LIMIT = 90;

template <typename InputIterator, typename EqualityComparable>
int recursiveLinearSearch(InputIterator first, InputIterator last, EqualityComparable value, int index = 0);

template <typename T, typename Generator>
void fill(std::vector<T>& v, const int size, Generator gen);

template <typename ForwarIterator>
void print(ForwarIterator first, ForwarIterator last);

#include "sources/RecursiveLinearSearch.cpp"

#endif ///__RECURSIVE_LINEAR_SEARCH_HPP__


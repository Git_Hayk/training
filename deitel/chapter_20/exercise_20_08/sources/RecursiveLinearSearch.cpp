#include "headers/RecursiveLinearSearch.hpp"

template <typename InputIterator, typename EqualityComparable>
int
recursiveLinearSearch(InputIterator first, InputIterator last, EqualityComparable value, int index)
{
    if (first == last) {
        return -1;
    }
    if (value == *first) {
        return index;
    }
    return recursiveLinearSearch(++first, last, value, ++index);
}

template <typename T, typename Generator>
void
fill(std::vector<T>& v, const int size, Generator gen)
{
    assert(size >= 0);
    for (int i = 0; i < size; ++i) {
        v.push_back(gen() % UPPER_LIMIT);
    }
}

template <typename ForwarIterator>
void
print(ForwarIterator first, ForwarIterator last)
{
    for (; first != last; ++first) {
        std::cout << *first << ' ';
    }
    std::cout << std::endl;
}


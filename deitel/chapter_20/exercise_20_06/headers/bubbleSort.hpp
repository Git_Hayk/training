#ifndef __BUBBLE_SORT_HPP__
#define __BUBBLE_SORT_HPP__

#include <iostream>
#include <vector>

template <typename T, typename Generator>
void fill(std::vector<T>& v, const int size, Generator gen);
template <typename T>
void print(const std::vector<T>& v);
template <typename LessThanComparable>
void bubbleSort(std::vector<LessThanComparable>& v);

#include "sources/bubbleSort.cpp"

#endif ///__BUBBLE_SORT_HPP__


#include "headers/bubbleSort.hpp"
#include <iostream>
#include <unistd.h>
#include <vector>

int
main()
{
    const int SIZE = 20;
    std::vector<int> testVector;

    if (::isatty(STDIN_FILENO)) std::srand(::time(0));

    fill(testVector, SIZE, std::rand);
    print(testVector);

    bubbleSort(testVector);
    print(testVector);

    return 0;
}


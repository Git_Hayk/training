/*
Use a one-dimensional array to solve the following problem. Read in 20 numbers, each of which is between 10 and 100,
inclusive. As each number is read, validate it and store it in the array only if it is not a duplicate of a number
already read. After reading all the values, display only the unique values that the user entered. Provide for the
"worst case" in which all 20 numbers are different. Use the smallest possible array to solve this problem.
*/

#include <iostream>
#include <unistd.h>

const int LOWER_LIMIT = 10;
const int UPPER_LIMIT = 100;
const size_t ARRAY_SIZE = 19;

void displayMessage();
bool isInRange(const int number);
bool checkForDublicate(int array[], const int number, const size_t index);

int
main()
{
    int array[ARRAY_SIZE] = {0};

    displayMessage();
    
    for (size_t i = 0; i < ARRAY_SIZE + 1; ++i) {
        int number;
        std::cin >> number;

        if (!isInRange(number)) {
            --i;
            continue;
        }
        
        if (i != ARRAY_SIZE) {
            array[i] = number;
        }
        
        const bool isDublicate = checkForDublicate(array, number, i);

        if (!isDublicate) {
            std::cout << "Not dublicate: " << number << std::endl;
        }

    }
    return 0;
}

void
displayMessage()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << ARRAY_SIZE + 1 << " numbers in range " << LOWER_LIMIT << " to " << UPPER_LIMIT << ": ";
    }
}

bool
isInRange(const int number)
{
    if (number < LOWER_LIMIT || number > UPPER_LIMIT) {
        std::cout << "Warning 1: Not in range (" << LOWER_LIMIT << '-' << UPPER_LIMIT << ")!" << std::endl;
        return false;
    }
    return true;
}

bool
checkForDublicate(int array[], const int number, const size_t index)
{
    for (size_t j = 0; j < index; ++j) {
        if (array[j] == number) {
            return true;
        }
    }
    return false;
}


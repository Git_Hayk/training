#include <iostream>
#include <unistd.h>

void fillArray(int array[], const int arraySize);
int recursiveMinimum(const int array[], const int arraySize, int smallest = INT_MAX);

int
main()
{
    const int ARRAY_SIZE = 10;
    int array[ARRAY_SIZE];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 integers to fill the array: ";
    }
    fillArray(array, ARRAY_SIZE);
    const int minimumValue = recursiveMinimum(array, ARRAY_SIZE);
    std::cout << "Minimum value in array is " << minimumValue << std::endl;
    return 0;
}

void
fillArray(int array[], const int arraySize)
{
    for (int i = 0; i < arraySize; ++i) {
        std::cin >> array[i];
    }
}

int
recursiveMinimum(const int array[], const int arraySize, int smallest)
{
    const int index = arraySize - 1;
    if (-1 == index) {
        return smallest;
    }
    if (array[index] <= smallest) {
        smallest = array[index];
    }
    return recursiveMinimum(array, index, smallest);
}


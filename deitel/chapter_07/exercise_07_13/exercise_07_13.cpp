/*
Write single statements that perform the following one-dimensional array operations:

Initialize the 10 elements of integer array counts to zero.

Add 1 to each of the 15 elements of integer array bonus.

Read 12 values for double array monthlyTemperatures from the keyboard.

Print the 5 values of integer array bestScores in column format.
 */

#include <iostream>
#include <iomanip>

int
main()
{
    /// a) Initialize the 10 elements of integer array counts to zero.
    int counts[10] = {0};
    std::cout << counts << std::endl;

    /// b) Add 1 to each of the 15 elements of integer array bonus.
    int bonus[15];
    for (int i = 0; i < 15; ++i) {
        ++bonus[i];
    }

    /// c) Read 12 values for double array monthlyTemperatures from the keyboard.
    double monthlyTemperatures[12] = {-10.2, -9.5, -4.4, -3.9, 0.0, 0.5, 7.9, 18.3, 25.2, 10.5, 2.1, -1.2};
    std::cout << "Input month number: ";
    int month;
    std::cin >> month;
    std::cout << std::fixed << std::setprecision(1) << monthlyTemperatures[month] << std::endl;

    /// d) Print the 5 values of integer array bestScores in column format.
    int bestScores[5] = {95, 96, 97, 98, 99};
    for (int i = 0; i < 5; ++i) {
        std::cout << bestScores[i] << std::endl;
    }

    return 0;
}


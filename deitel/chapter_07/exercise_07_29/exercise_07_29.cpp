#include <iostream>
#include <cmath>

int
main()
{
    const size_t NUMBERS_COUNT = 1000;
    size_t setOfNumbers[NUMBERS_COUNT] = { [0 ... NUMBERS_COUNT - 1] = 1};

    for (size_t i = 2; i < std::sqrt(NUMBERS_COUNT); ++i) {
        if (setOfNumbers[i]) {
            for (size_t j = i * i; j < NUMBERS_COUNT; j += i) {
                setOfNumbers[j] = 0;
            }
        }
    }

    for (size_t k = 1; k < NUMBERS_COUNT; ++k) {
        std::cout << k << " - " << (setOfNumbers[k] ? "Prime" : "Not prime") << std::endl;
    }
    return 0;
}


#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <vector>

const size_t SALES_PEOPLE_COUNT = 100;
const size_t RANGES_COUNT = 11;
const int LOWER_LIMIT = 200;
const int UPPER_LIMIT = 1000;

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }

    std::vector<int> salesPeopleSalary(SALES_PEOPLE_COUNT);
    for (size_t i = 0; i < salesPeopleSalary.size(); ++i) {
        salesPeopleSalary[i] = LOWER_LIMIT + std::rand() % UPPER_LIMIT;
    }

    std::vector<size_t> rangeCounter(RANGES_COUNT, 0); /// elements under 0 and 1 index are not used
    for (size_t j = 0; j < salesPeopleSalary.size(); ++j) {
        size_t range = salesPeopleSalary[j] / 100;
        assert(range >= 2);
        
        range = (range >= RANGES_COUNT ? RANGES_COUNT - 1 : range);
        ++rangeCounter[range];
    }

    for (size_t k = 2; k < RANGES_COUNT - 1; ++k) {
        std::cout << '$' << k * 100 << " - $" << k * 100 + 99 << ": " << rangeCounter[k] << " salespeople" << std::endl;
    }
    std::cout << "$1000 and over: " << rangeCounter[RANGES_COUNT - 1] << " salespeople" << std::endl;
    return 0;
}


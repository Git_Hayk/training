/*
(Bubble Sort) In the bubble sort algorithm, smaller values gradually "bubble" their way upward to the top of the array like
air bubbles rising in water, while the larger values sink to the bottom. The bubble sort makes several passes through the
array. On each pass, successive pairs of elements are compared. If a pair is in increasing order (or the values are identical),
we leave the values as they are. If a pair is in decreasing order, their values are swapped in the array. Write a program that
sorts an array of 10 integers using bubble sort.
*/

#include <iostream>
#include <unistd.h>

void fillArray(int array[], const size_t arraySize);
void bubbleSort(int array[], const size_t arraySize);
void printArray(const int array[], const size_t arraySize);

int
main()
{
    const size_t ARRAY_SIZE = 10;
    int array[ARRAY_SIZE] = {0};
    fillArray(array, ARRAY_SIZE);

    std::cout << "Array before bubble sort: ";
    printArray(array, ARRAY_SIZE);

    bubbleSort(array, ARRAY_SIZE);

    std::cout << "Array after bubble sort: ";
    printArray(array, ARRAY_SIZE);
    
    return 0;
}

void
bubbleSort(int array[], const size_t arraySize)
{
    for (size_t i = 0; i < arraySize; ++i) {
        const size_t limit = arraySize - i - 1;
        for (size_t j = 0; j < limit; ++j) {
            if (array[j] > array[j + 1]) {
                const int temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
            }
        }
    }
}

void
fillArray(int array[], const size_t arraySize)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 integer numbers to fill the array: ";
    }
    for (size_t i = 0; i < arraySize; ++i) {
        std::cin >> array[i];
    }
}

void
printArray(const int array[], const size_t arraySize)
{
    std::cout << '{';
    for (size_t i = 0; i < arraySize; ++i) {
        std::cout << array[i] << ", ";
    }
    std::cout << '}' << std::endl;
}


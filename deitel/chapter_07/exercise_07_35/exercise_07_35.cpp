#include <iostream>
#include <unistd.h>

void fillArray(int array[], const size_t arraySize);
void printArray(const int array[], const size_t arraySize, const int begin = 0);

int
main()
{
    const size_t ARRAY_SIZE = 10;
    int array[ARRAY_SIZE];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 number to appent to array: ";
    }
    fillArray(array, ARRAY_SIZE);
    printArray(array, ARRAY_SIZE);
    return 0;
}

void
fillArray(int array[], const size_t arraySize)
{
    for (size_t i = 0; i < arraySize; ++i) {
        std::cin >> array[i];
    }
}

void
printArray(const int array[], const size_t arraySize, const int begin)
{
    if (0 == arraySize) {
        std::cout << '\n';
        return;
    }
    std::cout << array[begin] << ", ";
    printArray(array, arraySize - 1, begin + 1);
}


#include <iostream>
#include <unistd.h>

void fillArray(int array[], const size_t arraySize);
void selectionSort(int array[], const size_t arraySize, const size_t index = 0);
void printArray(const int array[], const size_t arraySize);

int
main()
{
    const size_t ARRAY_SIZE = 10;
    int array[ARRAY_SIZE] = {0};
    fillArray(array, ARRAY_SIZE);

    std::cout << "Array before recursive selection sort: ";
    printArray(array, ARRAY_SIZE);

    selectionSort(array, ARRAY_SIZE);

    std::cout << "Array after recursive selection sort: ";
    printArray(array, ARRAY_SIZE);
    
    return 0;
}

void
selectionSort(int array[], const size_t arraySize, const size_t index)
{
    if (index == arraySize - 1) {
        return;
    }
    for (size_t i = index + 1; i < arraySize; ++i) {
        if (array[i] < array[index]) {
            std::swap(array[i], array[index]);
        }
    }
    return selectionSort(array, arraySize, index + 1);
}

void
fillArray(int array[], const size_t arraySize)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 integer numbers to fill the array: ";
    }
    for (size_t i = 0; i < arraySize; ++i) {
        std::cin >> array[i];
    }
}


void
printArray(const int array[], const size_t arraySize)
{
    std::cout << '{';
    for (size_t i = 0; i < arraySize; ++i) {
        std::cout << array[i] << ", ";
    }
    std::cout << '}' << std::endl;
}


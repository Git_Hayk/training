/*
The bubble sort described in Exercise 7.11 is inefficient for large arrays. Make the following simple modifications
to improve the performance of the bubble sort:

a) After the first pass, the largest number is guaranteed to be in the highest-numbered element of the array; after the
second pass, the two highest numbers are "in place," and so on. Instead of making nine comparisons on every pass, modify
the bubble sort to make eight comparisons on the second pass, seven on the third pass, and so on.

a) The data in the array may already be in the proper order or near-proper order, so why make nine passes if fewer will
suffice? Modify the sort to check at the end of each pass if any swaps have been made. If none have been made, then
the data must already be in the proper order, so the program should terminate. If swaps have been made, then at least
one more pass is needed.
 */

#include <iostream>
#include <unistd.h>

void fillArray(int array[], const size_t arraySize);
void bubbleSort(int array[], const size_t arraySize);
void printArray(const int array[], const size_t arraySize);

int
main()
{
    const size_t ARRAY_SIZE = 10;
    int array[ARRAY_SIZE] = {0};
    fillArray(array, ARRAY_SIZE);

    std::cout << "Array before bubble sort: ";
    printArray(array, ARRAY_SIZE);

    bubbleSort(array, ARRAY_SIZE);

    std::cout << "Array after bubble sort: ";
    printArray(array, ARRAY_SIZE);
    
    return 0;
}

void
bubbleSort(int array[], const size_t arraySize)
{
    for (size_t i = 0; i < arraySize; ++i) {
        bool swaped = false;
        const size_t limit = arraySize - i - 1;
        for (size_t j = 0; j < limit; ++j) {
            if (array[j] > array[j + 1]) {
                const int temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
                swaped = true;
            }
        }
        if (!swaped) {
            break;
        }
    }
}

void
fillArray(int array[], const size_t arraySize)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 integer numbers to fill the array: ";
    }
    for (size_t i = 0; i < arraySize; ++i) {
        std::cin >> array[i];
    }
}


void
printArray(const int array[], const size_t arraySize)
{
    std::cout << '{';
    for (size_t i = 0; i < arraySize; ++i) {
        std::cout << array[i] << ", ";
    }
    std::cout << '}' << std::endl;
}


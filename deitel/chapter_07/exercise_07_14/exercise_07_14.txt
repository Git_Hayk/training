Find the error(s) in each of the following statements:

char str[5];
std::cin >> str;

std::cin must set the last character as '\0', if we input "hello", the '\0' will be the sixth
but as out array's size is 5, this will definitely lead to logical errors.


int a[3];
std::cout << a[1] << "  " << a[2] << "  " << a[3] << std::endl;
The size of an array a[3] is 3, it's maximum index is 2.
Error: array index 3 is past the end of the array.

double f[3] = { 1.1, 10.01, 100.001, 1000.0001 };
as the array's size defined 3, it can not contain 4 elemnts.
Error: excess elements in array initializer.


double d[2][10];
d[ 1, 9 ] = 2.345;
    
d[1, 9] is the same as d[9], but row's count in this array is 2.
even if the row's count would be 10 for example, d[9] will return the ninth array's adress,
that's impossible to assign a value to an adress.


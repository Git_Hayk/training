progName=exercise_07_10
CXX=g++
CXXFLAGS=-Wall -Wextra -Werror -std=c++03 -g3

SOURCES:=$(wildcard *.cpp)
DEPENDS:=$(patsubst %.cpp,%.d,$(SOURCES))
PREPROCS:=$(patsubst %.cpp,%.ii,$(SOURCES))
ASSEMBLES:=$(patsubst %.cpp,%.s,$(SOURCES))
OBJS:=$(patsubst %.cpp,%.o,$(SOURCES))

TEST_INPUTS:=$(wildcard test*.input)
TESTS:=$(patsubst %.input,%,$(TEST_INPUTS))

qa: $(TESTS)

test%: $(progName) 
	./$< < $@.input > $@.output || echo "Negative $@..."
	diff $@.output $@.expected && echo "$@ PASSED" || echo "$@ FAILED"

$(progName): $(OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) $^ -o $@

%.ii: %.cpp
	$(CXX) $(CXXFLAGS) -E $< -o $@
	$(CXX) $(CXXFLAGS) -MT $@ -MM $< > $(patsubst %.cpp,%.d,$<)

%.s: %.ii
	$(CXX) $(CXXFLAGS) -S $< -o $@

%.o: %.s
	$(CXX) $(CXXFLAGS) -c $< -o $@

.gitignore:
	echo $(progName) > .gitignore
	echo .vscode/ >> .gitignore

clean:
	rm -rf *.ii *.s *.o *.d *.output .gitignore $(progName)
	ls -alS

.PRECIOUS: $(PREPROCS) $(ASSEMBLES)
.SECONDARY: $(PREPROCS) $(ASSEMBLES)

sinclude $(DEPENDS)

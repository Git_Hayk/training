/*
Use a one-dimensional array to solve the following problem. A company pays its salespeople on a commission basis.
The salespeople each receive $200 per week plus 9 percent of their gross sales for that week. For example, a
salesperson who grosses $5000 in sales in a week receives $200 plus 9 percent of $5000, or a total of $650. Write
a program (using an array of counters) that determines how many of the salespeople earned salaries in each of the
following ranges (assume that each salesperson's salary is truncated to an integer amount):
*/

#include <iostream>
#include <cstdlib>
#include <unistd.h>

const int ARRAY_SIZE = 100;
const int RANGES_COUNT = 10;
const int LOWER_LIMIT = 200;
const int UPPER_LIMIT = 1000;

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }

    int salesPeopleSalary[ARRAY_SIZE];
    for (int i = 0; i < ARRAY_SIZE; ++i) {
        salesPeopleSalary[i] = LOWER_LIMIT + std::rand() % UPPER_LIMIT;
    }

    int rangeCounter[RANGES_COUNT] = {0}; /// elements under 0 and 1 index are not used
    for (int j = 0; j < ARRAY_SIZE; ++j) {
        int range = salesPeopleSalary[j] / 100;
        assert(range >= 2);

        if (range >= RANGES_COUNT) {
            range = RANGES_COUNT - 1;
        }
        ++rangeCounter[range];
    }

    for (int k = 2; k < RANGES_COUNT - 1; ++k) {
        std::cout << '$' << k * 100 << " - $" << k * 100 + 99 << ": " << rangeCounter[k] << " salespeople" << std::endl;
    }
    std::cout << "$1000 and over: " << rangeCounter[RANGES_COUNT - 1] << " salespeople" << std::endl;
    return 0;
}


#include <iostream>
#include <unistd.h>
#include <vector>

void fillArray(std::vector<int>& vector);
int recursiveMinimum(std::vector<int>& vector, const int vectorSize, int smallest = INT_MAX);

int
main()
{
    const int VECTOR_SIZE = 10;
    std::vector<int> intVector(VECTOR_SIZE);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 integers to fill the vector: ";
    }
    fillArray(intVector);
    const int minimumValue = recursiveMinimum(intVector, intVector.size());
    std::cout << "Minimum value in vector is " << minimumValue << std::endl;
    return 0;
}

void
fillArray(std::vector<int>& vector)
{
    for (size_t i = 0; i < vector.size(); ++i) {
        std::cin >> vector[i];
    }
}

int
recursiveMinimum(std::vector<int>& vector, const int vectorSize, int smallest)
{
    const int index = vectorSize - 1;
    if (-1 == index) {
        return smallest;
    }
    if (vector[index] <= smallest) {
        smallest = vector[index];
    }
    return recursiveMinimum(vector, index, smallest);
}


#include <iostream>
#include <unistd.h>
#include <string>

void stringReverse(char text[]);

int
main()
{
    const size_t STRING_SIZE = 128;
    char text[STRING_SIZE];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input string to print reversed: ";
    }
    std::cin.get(text, STRING_SIZE, '\n');
    std::cout << "reversed string: ";
    stringReverse(text);
    return 0;
}

void
stringReverse(char text[])
{
    int end = std::strlen(text) - 1;
    if (-1 == end) {
        std::cout << '\n';
    } else {
        std::cout << text[end];
        text[end] = '\0';
        stringReverse(text);
    }
}


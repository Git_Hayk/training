#include <iostream>
#include <unistd.h>
#include <iomanip>

const bool isInteractive = ::isatty(STDIN_FILENO);
const size_t PERSON_NUMBER = 4;
const size_t PRODUCT_NUMBER = 5;


int
main()
{
    int sale[PERSON_NUMBER][PRODUCT_NUMBER] = {{0}};
    int totalForProduct[PRODUCT_NUMBER] = {0};

    while (true) {
        if (isInteractive) {
            std::cout << "Input salesperson number 1-4 (0 to exit): ";
        }
        size_t personNumber;
        std::cin >> personNumber;

        if (0 == personNumber) {
            break;
        }
        if (personNumber < 1 || personNumber > PERSON_NUMBER) {
            std::cout << "Error 1: Invalid person number!" << std::endl;
            return 1;
        }

        if (isInteractive) {
            std::cout << "Input product number 1-5 (0 to exit): ";
        }
        size_t productNumber;
        std::cin >> productNumber;

        if (0 == productNumber) {
            break;
        }
        if (productNumber < 1 || productNumber > PRODUCT_NUMBER) {
            std::cout << "Error 2: Invalid product number!" << std::endl;
            return 2;
        }

        if (isInteractive) {
            std::cout << "Input product total sales ($): ";
        }

        int totalSales;
        std::cin >> totalSales;

        if (totalSales < 0) {
            std::cout << "Error 3: Invalid sale value!" << std::endl;
            return 3;
        }

        sale[personNumber - 1][productNumber - 1] += totalSales;
        totalForProduct[productNumber - 1] += totalSales;
    }
    
    std::cout << std::setw(18);
    for (size_t k = 1; k <= PRODUCT_NUMBER; ++k) {
        std::cout << "Product " << k << ':';
    }
    std::cout << "Person's total:" << std::endl;
    for (size_t i = 0; i < PERSON_NUMBER; ++i) {
        std::cout << "Person " << i + 1 << ": ";
        int totalForPerson = 0;
        for (size_t j = 0; j < PRODUCT_NUMBER; ++j) {
            std::cout << std::setw(9) << sale[i][j] << ':';
            totalForPerson += sale[i][j];
        }
        std::cout << std::setw(14) << totalForPerson << ':' << std::endl;
    }
    std::cout << "Product's\n   total: ";
    int totalOfTotals = 0;
    for (size_t l = 0; l < PRODUCT_NUMBER; ++l) {
        std::cout << std::setw(9) << totalForProduct[l] << ':';
        totalOfTotals += totalForProduct[l];
    }
    std::cout << std::setw(14) << totalOfTotals << ":\n";

    return 0;
}


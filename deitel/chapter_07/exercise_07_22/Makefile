progName=exercise_07_22
CXX=g++
CXXFLAGS=-Wall -Wextra -Werror -std=c++03

SOURCES:=$(wildcard *.cpp)
DEPENDS:=$(patsubst %.cpp,%.d,$(SOURCES))
PREPROCS:=$(patsubst %.cpp,%.ii,$(SOURCES))
ASSEMBLES:=$(patsubst %.cpp,%.s,$(SOURCES))
OBJS:=$(patsubst %.cpp,%.o,$(SOURCES))

TEST_INPUTS:=$(wildcard test*.input)
TESTS:=$(patsubst %.input,%,$(TEST_INPUTS))

debug: CXXFLAGS+= -g3
release: CXXFLAGS+= -g0 -DNDEBUG

debug: qa
release: qa

qa: $(TESTS)

test%: $(progName) 
	./$< < $@.input > $@.output || echo "Negative $@..."
	diff $@.output $@.expected && echo "$@ PASSED" || echo "$@ FAILED"

$(progName): $(OBJS) | .gitignore
	$(CXX) $(CXXFLAGS) $^ -o $@

%.ii: %.cpp
	$(CXX) $(CXXFLAGS) -E $< -o $@
	$(CXX) $(CXXFLAGS) -MT $@ -MM $< > $(patsubst %.cpp,%.d,$<)

%.s: %.ii
	$(CXX) $(CXXFLAGS) -S $< -o $@

%.o: %.s
	$(CXX) $(CXXFLAGS) -c $< -o $@

.gitignore:
	echo $(progName) > .gitignore
	echo .vscode/ >> .gitignore

clean:
	rm -rf *.ii *.s *.o *.d *.output .gitignore $(progName)
	ls -al

.PRECIOUS: $(PREPROCS) $(ASSEMBLES)
.SECONDARY: $(PREPROCS) $(ASSEMBLES)

sinclude $(DEPENDS)

/// Write a C++ program that uses a while statement and the tab escape sequence \t to print the table of values:

#include <iostream>

int
main()
{
    int counter = 1;
    std::cout << "N\t10*N\t100*N\t1000*N\n" << std::endl;

    while (counter <= 5) {
        std::cout << counter << "\t"
                  << counter * 10 << "\t"
                  << counter * 100 << "\t"
                  << counter * 1000 << "\t" << std::endl;
        ++counter;
    }
    return 0;
}

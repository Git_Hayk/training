/*One large chemical company pays its salespeople on a commission basis. The salespeople each receive $200 per week
  plus 9 percent of their gross sales for that week. For example, a salesperson who sells $5000 worth of chemicals in
  a week receives $200 plus 9 percent of $5000, or a total of $650. Develop a C++ program that uses a while statement
  to input each salesperson's gross sales for last week and calculates and displays that salesperson's earnings. Process
  one salesperson's figures at a time.*/

#include <iostream>

int
main()
{
    float weekEarn = 200, percentSale = 9;

    while (true) {

        int weekSale;
        std::cout << "Input sales in dollars (-1 to exit): ";
        std::cin >> weekSale;
        
        if (-1 == weekSale) {
            std::cout << "Exiting..." << std::endl;
            return 0;
        }

        if (weekSale < 0) {
            std::cout << "Error 1: Sales can not be less than 0!" << std::endl;
            weekSale = 0;
            return 1;
        }
        
        float totalEarn = weekEarn + (static_cast<float>(weekSale) * percentSale / 100);

        std::cout << "Salary is " << totalEarn << '$' << std::endl;
    }
    return 0;
}

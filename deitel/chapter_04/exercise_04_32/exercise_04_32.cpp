/// Write a program that reads three nonzero double values and determines and prints whether they could represent the sides of a triangle.

#include <iostream>

int
main()
{
    double side1, side2, side3;
    std::cout << "Input 3 sides of a triangle: ";
    std::cin >> side1 >> side2 >> side3;
    
    if (side1 <= 0) {
        std::cout << "Error 1: Triangle side can not be less than or equal to 0!" << std::endl;
        return 1;
    }

    if (side2 <= 0) {
        std::cout << "Error 1: Triangle side can not be less than or equal to 0!" << std::endl;
        return 1;
    }

    if (side3 <= 0) {
        std::cout << "Error 1: Triangle side can not be less than or equal to 0!" << std::endl;
        return 1;
    }

    if (side1 > side2 + side3) {
        std::cout << "This values can not represent the sides of a triangle!" << std::endl;
        return 0;
    }

    if (side2 > side1 + side3) {
        std::cout << "This values can not represent the sides of a triangle!" << std::endl;
        return 0;
    }

    if (side3 > side1 + side2) {
        std::cout << "This values can not represent the sides of a triangle!" << std::endl;
        return 0;
    }

    std::cout << "This values can represent the sides of a triangle." << std::endl;
    return 0;
}


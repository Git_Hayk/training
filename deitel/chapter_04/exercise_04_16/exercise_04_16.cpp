/*Develop a C++ program that uses a while statement to determine the gross pay for each of several employees.
  The company pays "straight time" for the first 40 hours worked by each employee and pays "time-and-a-half"
  for all hours worked in excess of 40 hours. You are given a list of the employees of the company, the number
  of hours each employee worked last week and the hourly rate of each employee. Your program should input this
  information for each employee and should determine and display the employee's gross pay.*/

#include <iostream>

int
main()
{
    while (true) {
        int hoursWorked;
        std::cout << "Input hours worked (-1 to exit): ";
        std::cin >> hoursWorked;

        if (-1 == hoursWorked) {
            std::cout << "Exiting..." << std::endl;
            return 0;
        }

        if (hoursWorked < 0) {
            std::cout << "Error 1: Worked hours can not be less than 0!" << std::endl;
            return 1;
        }

        float hoursRate;
        std::cout << "Input hourly rate of the worker ($ 00.00): ";
        std::cin >> hoursRate;
        
        if (hoursRate < 0) {
            std::cout << "Error 2: Hourly rate can not be less than 0!" << std::endl;
            return 2;
        }

        int straightTime = 40;
        float timeAndAHalf = 0.5;
        float salary = static_cast<float>(hoursWorked) * hoursRate;

        if (hoursWorked > straightTime) {
            salary += static_cast<float>(hoursWorked - straightTime) * timeAndAHalf * hoursRate;
        }

        std::cout << "Salary is " << salary << '$' << std::endl;
    }
    return 0;
}

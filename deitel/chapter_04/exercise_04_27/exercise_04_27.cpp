/*Input an integer containing only 0s and 1s (i.e., a "binary" integer) and print its decimal equivalent. Use the
  modulus and division operators to pick off the "binary" number's digits one at a time from right to left. Much as
  in the decimal number system, where the rightmost digit has a positional value of 1, the next digit left has a
  positional value of 10, then 100, then 1000, and so on, in the binary number system the rightmost digit has a
  positional value of 1, the next digit left has a positional value of 2, then 4, then 8, and so on. Thus the decimal
  number 234 can be interpreted as 2 * 100 + 3 * 10 + 4 * 1. The decimal equivalent of binary 1101
  is 1 * 1 + 0 * 2 + 1 * 4 + 1 * 8 or 1 + 0 + 4 + 8, or 13.*/

#include <iostream>

int
main()
{
    long int binaryNumber;
    std::cout << "Input binary number: ";
    std::cin >> binaryNumber;

    if (binaryNumber < 0) {
        std::cerr << "Error 1: Invalid binary number!" << std::endl;
        return 1;
    }
            
    int decimalNumber = 0, power = 1;

    while (binaryNumber > 0) {
        int remainder = binaryNumber % 10;
            
        if (remainder > 1) {
            std::cerr << "Error 2: Wrong digit in binary system! " << '[' << remainder << ']' << std::endl;
            return 2;
        }
        
        decimalNumber += remainder * power;
        power *= 2;
        binaryNumber /= 10;
    }
    std::cout << "Decimal equivalent is " << decimalNumber << std::endl;
    return 0;
}


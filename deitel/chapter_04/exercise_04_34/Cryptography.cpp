#include "Cryptography.hpp"
#include <iostream>
#include <iomanip>

Cryptography::Cryptography()
{

}

int
Cryptography::run()
{
    while (true) {
        printCommandMenu();
        int command = getInputFromUser();
        executeExitCommand(command);
        validateCommand(command);

        printInputNumberPromt();
        int number = getInputFromUser();
        validateNumber(number);

        int result = executeCommand(command, number);
        displayResult(result);
    }
}


void
Cryptography::printCommandMenu()
{
    std::cout << "Menu:\n"
              << "\t0 - Exit:\n"
              << "\t1 - Encrypt\n"
              << "\t2 - Decrypt\n"
              << "> Choose: ";
}

int
Cryptography::getInputFromUser()
{
    int userInput;
    std::cin >> userInput;
    return userInput;
}

void
Cryptography::executeExitCommand(int command)
{
    if (0 == command) {
        std::cout << "Exiting..." << std::endl;
        ::exit(0);
    }
}



void
Cryptography::validateCommand(int command)
{
    if (command < 0) {
        std::cerr << "Error 1: Invalid command!" << std::endl;
        ::exit(1);
    }
    
    if (command > 2) {
        std::cerr << "Error 1: Invalid command!" << std::endl;
        ::exit(1);
    }
}

void
Cryptography::printInputNumberPromt()
{
    std::cout << "Input 4 digit integer: ";
}

void
Cryptography::validateNumber(int number)
{
    if (number < 1000) {
        std::cout << "Error 2: Invalid value [less than 1000]!" << std::endl;
        ::exit(2);
    }

    if (number > 9999) {
        std::cout << "Error 3: Invlaid value [more than 9999]!" << std::endl;
        ::exit(3);
    }
}

int
Cryptography::executeCommand(int command, int number)
{
    if (1 == command) {
        return encryptNumber(number);
    }

    if (2 == command) {
        return decryptNumber(number);
    }
    return 0;
}

int
Cryptography::encryptNumber(int number)
{
    int digit4 = number % 10;
    int wholePart = number / 10;
    int digit3 = wholePart % 10;
    wholePart = wholePart / 10;
    int digit2 = wholePart % 10;
    wholePart = wholePart / 10;
    int digit1 = wholePart % 10;

    digit1 += 7;
    digit2 += 7;
    digit3 += 7;
    digit4 += 7;

    if (digit1 >= 10) { digit1 %= 10; }
    if (digit2 >= 10) { digit2 %= 10; }
    if (digit3 >= 10) { digit3 %= 10; }
    if (digit4 >= 10) { digit4 %= 10; }

    /// Spaw the first digit with third
    /// Swap the second digit with fourth
    
    digit3 *= 1000;
    digit4 *= 100;
    digit1 *= 10;
    digit2 *= 1;

    return digit1 + digit2 + digit3 + digit4;
}

int
Cryptography::decryptNumber(int number)
{
    int digit4 = number % 10;
    int wholePart = number / 10;
    int digit3 = wholePart % 10;
    wholePart = wholePart / 10;
    int digit2 = wholePart % 10;
    wholePart = wholePart / 10;
    int digit1 = wholePart % 10;

    digit1 += 3;
    digit2 += 3;
    digit3 += 3;
    digit4 += 3;

    if (digit1 >= 10) { digit1 %= 10; }
    if (digit2 >= 10) { digit2 %= 10; }
    if (digit3 >= 10) { digit3 %= 10; }
    if (digit4 >= 10) { digit4 %= 10; }

    /// Spaw the first digit with third
    /// Swap the second digit with fourth
    
    digit3 *= 1000;
    digit4 *= 100;
    digit1 *= 10;
    digit2 *= 1;

    return digit1 + digit2 + digit3 + digit4;
}

void
Cryptography::displayResult(int result)
{
    if (result < 1000) {
        std::cout << std::setfill('0') << std::setw(4) << result << std::endl;
    } else {
        std::cout << result << std::endl;
    }
}


class Cryptography
{
public:
    Cryptography();
    int  run();

private:
    void printCommandMenu();
    int  getInputFromUser();
    void executeExitCommand(int command);
    void validateCommand(int command);
    void printInputNumberPromt();
    void validateNumber(int number);
    int  executeCommand(int command,int number);
    int  encryptNumber(int number);
    int  decryptNumber(int number);
    void displayResult(int result);
};


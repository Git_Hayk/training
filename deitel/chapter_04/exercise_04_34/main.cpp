/*(Cryptography) A company wants to transmit data over the telephone, but is concerned that its phones could be tapped.
  All of the data are transmitted as four-digit integers. The company has asked you to write a program that encrypts the
  data so that it can be transmitted more securely. Your program should read a four-digit integer and encrypt it as follows:
  Replace each digit by (the sum of that digit plus 7) modulus 10. Then, swap the first digit with the third, swap the
  second digit with the fourth and print the encrypted integer. Write a separate program that inputs an encrypted fourdigit
  integer and decrypts it to form the original number.*/

#include "Cryptography.hpp"

int
main()
{       
    Cryptography transmitData;
    transmitData.run();
    return 0; 
}


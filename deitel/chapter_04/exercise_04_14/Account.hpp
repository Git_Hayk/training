class Account
{
public:
    Account(int number, float balance, float charges, float credits, float limit);

    void setAccountNumber(int number);
    void setBalance(float balance);
    void setTotalCharges(float charges);
    void setTotalCredits(float credits);
    void setCreditLimit(float limit);

    int getAccountNumber();
    float getBalance();
    float getTotalCharges();
    float getTotalCredits();
    float getCreditLimit();

    int displayMessage();

private:
    int accountNumber_;
    float initialBalance_;
    float totalCharges_;
    float totalCredits_;
    float creditLimit_;
};


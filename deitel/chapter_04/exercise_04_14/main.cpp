/*Develop a C++ program that will determine whether a department-store customer has exceeded the credit limit on a charge account.
  For each customer, the following facts are available:

1) Account number (an integer)

2) Balance at the beginning of the month

3) Total of all items charged by this customer this month

4) Total of all credits applied to this customer's account this month

5) Allowed credit limit

The program should use a while statement to input each of these facts, calculate the new balance (= beginning balance + charges
credits) and determine whether the new balance exceeds the customer's credit limit. For those customers whose credit limit is
exceeded, the program should display the customer's account number, credit limit, new balance and the message "Credit Limit Exceeded."*/

#include "Account.hpp"
#include <iostream>

int
main()
{
    while (true) {
        int accountNumber;
        std::cout << "Input account number (-1 to exit): ";
        std::cin >> accountNumber;

        if (-1 == accountNumber) {
            std::cout << "Exiting..." << std::endl;
            return 0;
        }

        float initialBalance;
        std::cout << "Input account initial balance: ";
        std::cin >> initialBalance;
    
        float totalCharges;
        std::cout << "Input total charges: ";
        std::cin >> totalCharges;

        float totalCredits;
        std::cout << "Input total credits: ";
        std::cin >> totalCredits;

        float creditLimit;
        std::cout << "Input credit limit: ";
        std::cin >> creditLimit;

        Account account(accountNumber, initialBalance, totalCharges, totalCredits, creditLimit);
        account.displayMessage();
    }

    return 0;
}


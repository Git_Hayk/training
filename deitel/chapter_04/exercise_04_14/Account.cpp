#include "Account.hpp"
#include <iostream>

Account::Account(int number, float balance, float charges, float credits, float limit)
{
    setAccountNumber(number);
    setBalance(balance);
    setTotalCharges(charges);
    setTotalCredits(credits);
    setCreditLimit(limit);
};

void
Account::setAccountNumber(int number)
{
    if (number < 100) {
        std::cout << "Warning 1: Your account number is invalid!\nResetting to 100..." << std::endl;
        accountNumber_ = 100;
        return;
    }

    accountNumber_ = number;
    return;
}

void
Account::setBalance(float balance)
{
    if (balance < 0) {
        std::cout << "Warning 2: Account balance can not be less than 0!\nResetting to 0..." << std::endl;
        initialBalance_ = 0;
        return;
    }

    initialBalance_ = balance;
    return;
}

void
Account::setTotalCharges(float charges)
{
    if (charges < 0) {
        std::cout << "Warning 3: Charges can not be less than 0!\nResetting to 0..." << std::endl;
        totalCharges_ = 0;
        return;
    }

    totalCharges_ = charges;
    return;
}

void
Account::setTotalCredits(float credits)
{
    if (credits < 0) {
        std::cout << "Warning 4: Credits can not be less than 0!\nResetting to 0..." << std::endl;
        totalCredits_ = 0;
        return;
    }

    totalCredits_ = credits;
}

void
Account::setCreditLimit(float limit)
{
    if (limit < 0) {
        std::cout << "Warning 5: Credit's limit can not be less than 0!\nResetting to 0..." << std::endl;
        creditLimit_ = 0;
        return;
    }

    creditLimit_ = limit;
    return;
}


int
Account::getAccountNumber()
{
    return accountNumber_;
}

float
Account::getBalance()
{
    return initialBalance_;
}

float
Account::getTotalCharges()
{
    return totalCharges_;
}

float
Account::getTotalCredits()
{
    return totalCredits_;
}

float
Account::getCreditLimit()
{
    return creditLimit_;
}

int
Account::displayMessage()
{
    float newBalance = initialBalance_ - totalCharges_ + totalCredits_;
    std::cout << "Account: " << getAccountNumber() << std::endl;
    std::cout << "New balance is " << newBalance << std::endl;
    std::cout << "Beggining balance: " << getBalance() << std::endl;
    
    float creditAmount = initialBalance_ + totalCredits_;
    if (creditAmount > creditLimit_) {
        std::cout << "\nCredit Limit: " << getCreditLimit()
                  << "\nCredit Limit Exceeded." << std::endl;
    }

    initialBalance_ += totalCredits_;
    
    if (initialBalance_ - totalCharges_ < 0) {
        std::cout << "\nTotal Charges: " << getTotalCharges()
                  << "\nYour charges amount exceeded sum of initial balance and credits." << std::endl;
    }
    return 0;
}


/*Write a program that displays the checkerboard pattern shown below. Your program must use only three
  output statements, one of each of the following forms:*/

#include <iostream>

int
main()
{
    int row = 1;

    while (row <= 8) {
        if (0 == row % 2) {
            std::cout << ' ';
        }

        int column = 1;
        while (column <= 8) {
            std::cout << "* ";
            ++column;
        }

        std::cout << std::endl;
        ++row;
    }
    return 0;
}


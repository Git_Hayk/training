#include "Analysis.hpp"
#include <iostream>

void
Analysis::processExamResults()
{
    int passes = 0, failures = 0, counter = 1;
    int result;

    while (counter <= 10) {

        std::cout << "Input result (1 = pass, 2 = fail): ";
        std::cin >> result;

        if (1 == result) {
            ++passes;
            ++counter;
        } else if (2 == result) {
            ++failures;
            ++counter;
        } else {
            std::cout << "Warning 1: Invalid value!" << std::endl;
        }
    }

    std::cout << "Passed " << passes << "\nFailed " << failures << std::endl;
    
    if ( passes > 8 ) {
        std::cout << "Raise tuition " << std::endl;
    }
}


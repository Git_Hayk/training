/*The examination-results program of Fig. 4.16Fig. 4.18 assumes that any value input by the user that is not a 1 must
  be a 2. Modify the application to validate its inputs. On any input, if the value entered is other than 1 or 2, keep
  looping until the user enters a correct value.*/

#include "Analysis.hpp"

int
main()
{
    Analysis application;
    application.processExamResults();
    return 0;
}


/*(Another Dangling-Else Problem) Modify the following code to produce the output shown. Use proper indentation techniques.
  You must not make any changes other than inserting braces. The compiler ignores indentation in a C++ program. We eliminated
  the indentation from the following code to make the problem more challenging. [Note: It is possible that no modification is
  necessary.]*/

#include <iostream>

int
main()
{
    int x = 5, y = 8;

    std::cout << "a)" << std::endl;

    if ( y == 8 ) {
        if ( x == 5 ) {
            std::cout << "@@@@@" << std::endl;
        } else {
            std::cout << "#####" << std::endl;
        }
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    }

    /// *******************************
    std::cout << "\nb)" << std::endl;

    if ( y == 8 ) {
        if ( x == 5 ) {
            std::cout << "@@@@@" << std::endl;
        } else {
            std::cout << "#####" << std::endl;
            std::cout << "$$$$$" << std::endl;
            std::cout << "&&&&&" << std::endl;
        }
    }

    /// *******************************
    std::cout << "\nc)" << std::endl;

    if ( y == 8 ) {
        if ( x == 5 ) {
            std::cout << "@@@@@" << std::endl;
        } else {
            std::cout << "#####" << std::endl;
            std::cout << "$$$$$" << std::endl;
        }
        std::cout << "&&&&&" << std::endl;
    }

    /// *******************************
    std::cout << "\nd)" << std::endl;

    x = 5 ,y =7;

    if ( y == 8 ) {
        if ( x == 5 ) {
            std::cout << "@@@@@" << std::endl;
        }
    } else {
        std::cout << "#####" << std::endl;
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    }
    return 0;
}


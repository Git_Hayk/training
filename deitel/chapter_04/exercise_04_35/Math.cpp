#include "Math.hpp"
#include <iostream>

Math::Math()
{
}

int
Math::factorial(int number)
{
    validateValue(number);

    int factorial = 1;
    if (0 == number) {
        return factorial;
    }
    
    while (number >= 1) {
        factorial *= number;
        --number;
    }
    return factorial;
}

double
Math::calculateEulersNumber(int accuracy)
{
    validateValue(accuracy);
    double eulersNumber = 1.0;

    if (0 == accuracy) {
        return eulersNumber;
    }

    int numberFactorial = 1, number = 1;
    while (accuracy >= 1) {
        numberFactorial *= number;
        eulersNumber += 1.0 / static_cast<double>(numberFactorial);
        ++number;
        --accuracy;
    }
    return eulersNumber;
}

double
Math::calculateExponent(int x, int accuracy)
{
    validateValue(accuracy);

    double exponent = 1.0;

    if (0 == accuracy) {
        return exponent;
    }

    int numberFactorial = 1, powerOfx = 1, number = 1;
    while (accuracy >= 1) {
        numberFactorial *= number;
        powerOfx *= x;
        exponent += static_cast<double>(powerOfx) / numberFactorial;
        ++number;
        --accuracy;
    }
    return exponent;
}

void
Math::validateValue(int number)
{
    if (number < 0) {
        std::cerr << "Error 1: Invalid value (less than 0)!" << std::endl;
        ::exit(1);
    }
}


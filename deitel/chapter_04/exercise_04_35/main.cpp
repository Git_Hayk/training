/// The factorial of a nonnegative integer n is written n! (pronounced "n factorial") and is defined as follows:

#include "Math.hpp"
#include <iostream>
#include <iomanip>

int
main()
{
    Math value;

    int number;
    std::cout << "Input number to calculate it's factorial: ";
    std::cin >> number;
    
    std::cout << value.factorial(number) << std::endl;

    int accuracy;
    std::cout << "Input an accuracy for calculating Euler's number (recommended more than 5): ";
    std::cin >> accuracy;

    std::cout << "e = " << std::setprecision(15) << value.calculateEulersNumber(accuracy) << std::endl;

    int x;
    std::cout << "Input X and accuracy for calculating an Exponent (recommended more than 5): ";
    std::cin >> x;
    std::cin >> accuracy;

    std::cout << "e^x = " << std::fixed << std::setprecision(15) << value.calculateExponent(x, accuracy) << std::endl;
    return 0;
}


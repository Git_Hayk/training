class Math
{
public:
    Math();
    int factorial(int number);
    double calculateEulersNumber(int accuracy);
    double calculateExponent(int x, int accuracy);

private:
    void validateValue(int number);
};


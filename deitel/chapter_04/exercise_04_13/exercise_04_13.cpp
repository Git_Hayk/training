/*Drivers are concerned with the mileage obtained by their automobiles. One driver has kept track of several tankfuls of
  gasoline by recording miles driven and gallons used for each tankful. Develop a C++ program that uses a while statement
  to input the miles driven and gallons used for each tankful. The program should calculate and display the miles per gallon
  obtained for each tankful and print the combined miles per gallon obtained for all tankfuls up to this point.*/

#include <iostream>

int
main()
{
    /// Initialize total values
    int totalMilesDriven = 0, totalGallonsUsed = 0;
    
    /// Create a while statement for input miles driven and galons used for each tankful
    while (true) {
        /// Initialeze variables for miles driven and gallons used
        int milesDriven;

        std::cout << "Input the miles driven (-1 to quit): ";
        std::cin >> milesDriven;
        
        /// Check first input
        if (-1 == milesDriven) {
            std::cout << "Exiting..." << std::endl;
            return 0;
        }

        if (milesDriven < -1) {
            std::cout << "Error 1: Invalid miles driven!" << std::endl;
            return 1;
        }

        int gallonsUsed;

        std::cout << "Enter gallons: ";
        std::cin >> gallonsUsed;

        /// Check second input validation
        if (gallonsUsed <= 0) {
            std::cout << "Error 2: Invalid gallons used!" << std::endl;
            return 2;
        }

        /// Save results in total
        totalMilesDriven += milesDriven;
        totalGallonsUsed += gallonsUsed;

        /// Calculate MPG for current tankful
        double thisTankfulMPG = static_cast<double>(milesDriven) / gallonsUsed;
        std::cout << "MPG this tankful: " << thisTankfulMPG << std::endl;
    
        /// Calculate total MPG
        double totalMPG = static_cast<double>(totalMilesDriven) / totalGallonsUsed;
        std::cout << "Total MPG: " << totalMPG << std::endl;
    }

    return 0;
}


/// Write a program that reads three nonzero integers and determines and prints whether they could be the sides of a right triangle.

#include <iostream>
#include <math.h>

int
main()
{
    double side1, side2, side3;
    std::cout << "Input 3 sides of a rigth triangle: ";
    std::cin >> side1;
    
    std::string errorMessage = "Error 1: Triangle side can not be less than or equal to 0!";
    if (side1 <= 0) {
        std::cout << errorMessage << std::endl;
        return 1;
    }

    std::cin >> side2;

    if (side2 <= 0) {
        std::cout << errorMessage << std::endl;
        return 1;
    }

    std::cin >> side3;

    if (side3 <= 0) {
        std::cout << errorMessage << std::endl;
        return 1;
    }
    
    int square1 = side1 * side1;
    int square2 = side2 * side2;
    int square3 = side3 * side3;
    std::string message = "This values can represent the sides of a right triangle.";

         if (square1 == square2 + square3) { std::cout << message << std::endl; }
    else if (square2 == square1 + square3) { std::cout << message << std::endl; }
    else if (square3 == square1 + square2) { std::cout << message << std::endl; }
    else { std::cout << "This values can not represent the sides of a right triangle." << std::endl; }

    return 0;
}


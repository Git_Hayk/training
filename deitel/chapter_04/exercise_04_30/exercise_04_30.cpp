/*Write a program that reads the radius of a circle (as a double value) and computes and prints the diameter,
  the circumference and the area. Use the value 3.14159 for p.*/

#include <iostream>

int
main()
{
    /// Using the appropriate formulas I write the following code
    double radius;
    std::cout << "Input the circles radius: ";
    std::cin >> radius;
    
    if (radius < 0) {
        std::cout << "Error 1: Radius can not be less than zero" << std::endl;
        return 1;
    }

    double diameter = 2 * radius;
    std::cout << "Circles diameter is " << diameter << std::endl;
    
    double circumFerence = diameter * 3.14159;
    std::cout << "Circumference is " << circumFerence << std::endl;
    
    double area = 3.14159 * (radius * radius);
    std::cout << "Circles area is " << area << std::endl;
    
    return 0;
}

/*Write a program that reads in the size of the side of a square and then prints a hollow square of that size out of
  asterisks and blanks. Your program should work for squares of all side sizes between 1 and 20.*/

#include <iostream>

int
main()
{
    int sideLength;
    std::cout << "Input side length (1-20 integer number): ";
    std::cin >> sideLength;

    if (sideLength < 1) {
        std::cout << "Error 1: Invalid value!" << std::endl;
        return 1;
    }

    if (sideLength > 20) {
        std::cout << "Error 1: Invalid value!" << std::endl;
        return 1;
    }

    int row = 1;

    while (row <= sideLength) {
        int column = 1;
        while (column <= sideLength) {
            if (1 == row)                       {std::cout << '*';} 
            else if (row == sideLength)         {std::cout << '*';}
            else if (1 == column)               {std::cout << '*';}
            else if (column == sideLength)      {std::cout << '*';} 
            else                                {std::cout << ' ';}
            ++column;
        }

        std::cout << '\n';
        ++row;
    }
    return 0;
}
        

/* A palindrome is a number or a text phrase that reads the same backwards as forwards. For example, each of the
   following five-digit integers is a palindrome: 12321, 55555, 45554 and 11611. Write a program that reads in a
   five-digit integer and determines whether it is a palindrome. [Hint: Use the division and modulus operators to
   separate the number into its individual digits.]*/

#include <iostream>

int
main()
{
    int number;
    std::cout << "Input a 5 digit integer to check if it is palindromic (-1 to exit): ";
    std::cin >> number;
        
    if (-1 == number) {
        std::cout << "Exiting..." << std::endl;
        return 0;
    }

   if (number < 10000) {
        std::cout << "Error 1: Invalid value!" << std::endl;
        return 1;
   }

    if (number > 99999) {
        std::cout << "Error 1: Invalid value!" << std::endl;
        return 1;
    }
        
    int digit1 = number / 10000;
    number %= 10000;
    int digit2 = number / 1000;
    number %= 1000;
    int digit3 = number / 100;
    number %= 100;
    int digit4 = number / 10;
    number %= 10;
    int digit5 = number / 1;
        
    if (digit1 == digit5) {
        if (digit2 == digit4) {
            std::cout << "Palindrome" << std::endl;
            return 0;
        }
    } 
    std::cout << "Not Palindrome" << std::endl;
    return 0;
}
        

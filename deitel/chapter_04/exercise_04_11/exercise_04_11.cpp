/// Identify and correct the error(s) in each of the following:



a:
if ( age >= 65 );                               /// Wrong usage of semicolon
   cout << "Age is greater than or equal to 65" << endl;
else
   cout << "Age is less than 65 << endl";       /// Wrong usage of duoble quotes (must be after 65)


b:
if ( age >= 65 )
   cout << "Age is greater than or equal to 65" << endl;
else;                                           /// Wrong usage of semicolon
   cout << "Age is less than 65 << endl";       /// Wrong usage of duoble quotes (must be after 65)


c:
int x = 1, total;                               /// The variable named total must be initialized (not only declared)

while ( x <= 10 )
{
   total += x;
   x++;
}


d:
While ( x <= 100 )                              /// There are absent shaped brackets
   total += x;                                  /// Not initialized variables (total, x)
   x++;


e: 
/*Logic error: if y is initialized as less or equal to 0, then the loop must not begin.
   and if y is initialized as bigger than 0, so the loop will run endless.*/

while ( y > 0 )                                 /// Not initialized variable (y)
{
   cout << y << endl;                           
   y++;
}

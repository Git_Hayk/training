/// Using an approach similar to that in Exercise 4.17, find the two largest values among the 10 numbers.
/// [Note: You must input each number only once.]

#include <iostream>
#include <limits.h>

int
main()
{
    int counter = 1, largest1 = INT_MIN, largest2 = INT_MIN;
    while (counter <= 10) {
        int number;
        std::cout << "Input number: ";
        std::cin >> number;

        if (number < -214748648) {
            std::cerr << "Error 1: Integer limit exceeded!" << std::endl;
            return 1;
        }

        if (number > 214748647) {
            std::cerr << "Error 1: Integer limit exceeded!" << std::endl;
            return 1;
        }

        if (number > largest1) {
            largest2 = largest1;
            largest1 = number;
        } else if (number > largest2) {
            largest2 = number;
        }

        ++counter;
    }
    std::cout << "Two largest numbers are " << largest1 << " and " << largest2 << std::endl;
    return 0;
}


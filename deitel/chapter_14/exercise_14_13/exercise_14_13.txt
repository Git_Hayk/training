If you do not use templates, you will have to define functions and classes for all possible existing data types,
this is a big waste of time for the programmer himself, and the program itself will become more massive.
The use of templates makes it possible for the compiler to determine the necessary classes or functions for a
given program, thereby increasing efficiency.

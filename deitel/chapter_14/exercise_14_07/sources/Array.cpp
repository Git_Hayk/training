#include "headers/Array.hpp"

#include <iostream>
#include <iomanip>
#include <cstdlib>

template <typename elementType, int numberOfElements>
std::istream&
operator>> (std::istream& input, Array<elementType, numberOfElements>& rhv)
{
   for (int i = 0; i < numberOfElements; ++i) {
      input >> rhv[i];
   }
   return input;
}

template <typename elementType, int numberOfElements>
std::ostream&
operator<< (std::ostream& output, const Array<elementType, numberOfElements>& rhv)
{
   int i;
   for (i = 0; i < numberOfElements; ++i) {
      output << std::setw(12) << rhv[i];
      if ((i + 1) % 4 == 0) {
         output << std::endl;
      }
   }
   if (i % 4 != 0) {
      output << std::endl;
   }
   return output;
}

template <typename elementType, int numberOfElements>
Array<elementType, numberOfElements>::Array(const elementType member)
{
   for (int i = 0; i < numberOfElements; ++i) {
      data_[i] = member;
   }
}

template <typename elementType, int numberOfElements>
Array<elementType, numberOfElements>::Array(const Array<elementType, numberOfElements>& arrayToCopy)
{
   for (int i = 0; i < numberOfElements; ++i) {
      data_[i] = arrayToCopy.data_[i];
   }
}

template <typename elementType, int numberOfElements>
Array<elementType, numberOfElements>::~Array()
{
} 

template <typename elementType, int numberOfElements>
int
Array<elementType, numberOfElements>::getSize() const
{
   return numberOfElements;
}

template <typename elementType, int numberOfElements>
const Array<elementType, numberOfElements>&
Array<elementType, numberOfElements>::operator=(const Array<elementType, numberOfElements>& rhv)
{
   for (int i = 0; i < numberOfElements; ++i) {
      data_[i] = rhv.data_[i];
   }
   return *this;
}

template <typename elementType, int numberOfElements>
bool
Array<elementType, numberOfElements>::operator==(const Array<elementType, numberOfElements>& rhv) const
{
   const int s = this->getSize();
   for (int i = 0; i < s; ++i) {
      if (data_[i] != rhv.data_[i]) {
         return false;
      }
   }
   return true;
}

template <typename elementType, int numberOfElements>
bool
Array<elementType, numberOfElements>::operator!=(const Array<elementType, numberOfElements>& rhv) const
{
    return !(*this == rhv);
}

template <typename elementType, int numberOfElements>
elementType&
Array<elementType, numberOfElements>::operator[](const int subscript)
{
   assert(subscript >= 0 && subscript < numberOfElements);
   return data_[subscript];
}

template <typename elementType, int numberOfElements>
elementType
Array<elementType, numberOfElements>::operator[](const int subscript) const
{
   assert(subscript >= 0 && subscript < numberOfElements);
   return data_[subscript];
}


#include "headers/Array.hpp"
#include <iostream>

template <typename elementType, int numberOfElements>
void
validateIndex(const int subscript, const Array<elementType, numberOfElements>& arr)
{
   if (subscript < 0 || subscript >= arr.getSize()) {
      std::cerr << "\nError 1: Subscript " << subscript
         << " out of range" << std::endl;
      std::exit(1);
   }
}

int
main()
{
   Array<int, 7> integers1;
   Array<int, 7> integers2;

   std::cout << "Size of Array integers1 is "
      << integers1.getSize()
      << "\nArray after initialization:\n" << integers1;

   std::cout << "\nSize of Array integers2 is "
      << integers2.getSize()
      << "\nArray after initialization:\n" << integers2;

   std::cout << "\nEnter 14 integers:" << std::endl;
   std::cin >> integers1 >> integers2;

   std::cout << "\nAfter input, the Arrays contain:\n"
      << "integers1:\n" << integers1
      << "integers2:\n" << integers2;

   std::cout << "\nEvaluating: integers1 != integers2" << std::endl;

   if (integers1 != integers2)
      std::cout << "integers1 and integers2 are not equal" << std::endl;

   Array<int, 7> integers3(integers1);

   std::cout << "\nSize of Array integers3 is "
      << integers3.getSize()
      << "\nArray after initialization:\n" << integers3;

   std::cout << "\nAssigning integers2 to integers1:" << std::endl;
   integers1 = integers2;

   std::cout << "integers1:\n" << integers1
      << "integers2:\n" << integers2;

   std::cout << "\nEvaluating: integers1 == integers2" << std::endl;

   if (integers1 == integers2)
      std::cout << "integers1 and integers2 are equal" << std::endl;

   std::cout << "\nintegers1[5] is " << integers1[5];

   std::cout << "\n\nAssigning 1000 to integers1[5]" << std::endl;
   integers1[5] = 1000;
   std::cout << "integers1:\n" << integers1;

   std::cout << "\nAttempt to assign 1000 to integers1[15]" << std::endl;
   validateIndex(15, integers1);
   integers1[15] = 1000;
   return 0;
}


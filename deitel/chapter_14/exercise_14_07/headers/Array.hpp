#ifndef __ARRAY_HPP__
#define __ARRAY_HPP__

#include <iostream>

template <typename elementType, int numberOfElements> class Array;

template <typename elementType, int numberOfElements>
std::istream& operator>> (std::istream& input, Array<elementType, numberOfElements>& rhv);      
template <typename elementType, int numberOfElements>
std::ostream& operator<< (std::ostream& output, const Array<elementType, numberOfElements>& rhv);

template <typename elementType, int numberOfElements = 10>
class Array
{
public:
   Array(const elementType member = elementType());
   Array(const Array<elementType, numberOfElements>& rhv);
   ~Array();
   int getSize() const;

   const Array<elementType, numberOfElements>& operator=(const Array<elementType, numberOfElements>& rhv);

   bool operator==(const Array<elementType, numberOfElements>& rhv) const;
   bool operator!=(const Array<elementType, numberOfElements>& rhv) const;

   elementType& operator[](const int subscript);
   elementType operator[](const int subscript) const;                          
private:
   elementType data_[numberOfElements];
}; // end class Array

#include "sources/Array.cpp"

#endif /// __ARRAY_HPP__


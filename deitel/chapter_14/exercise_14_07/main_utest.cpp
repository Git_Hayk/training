#include "headers/Array.hpp"
#include <gtest/gtest.h>

TEST(ArrayTest, NumberOfElements)
{
    Array<int, 5> arr;
    EXPECT_EQ(arr.getSize(), 5);
}

TEST(ArrayTest, DefNumberOfElements)
{
    Array<int, 10> arr;
    EXPECT_EQ(arr.getSize(), 10);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


#include "headers/Rectangle.hpp"
#include <iostream>

/*

if class Hello does not overload an operator==, this will result in a compilation error!

error: invalid operands to binary expression ('const Hello' and 'const Hello')
    return object1 == object2;
           ~~~~~~~ ^  ~~~~~~~
note: in instantiation of function template specialization 'isEqualTo<Hello>' requested here

 */

template <typename T>
bool isEqualTo(const T& object1, const T& object2);

int
main()
{
    Rectangle rect1(3, 5);   
    Rectangle rect2(2, 5);
    std::cout << "rect1 " << (isEqualTo(rect1, rect2) ? "is" : "is not") << " equal to rect2." << std::endl;
    rect2.setLength(3);
    std::cout << "rect1 " << (isEqualTo(rect1, rect2) ? "is" : "is not") << " equal to rect2." << std::endl;
    return 0;
}

template <typename T>
bool
isEqualTo(const T& object1, const T& object2)
{
    return object1 == object2;
}


#ifndef __PRINTARRAY_HPP__
#define __PRINTARRAY_HPP__

#include <limits.h>

template <typename T>
void printArray(const T* array, const int count, const int lowSubscript = 0, int highSubscript = INT_MAX);

template <typename T>
void fillArray(T* array, const int size);

#include "sources/PrintArray.cpp"

#endif ///__PRINTARRAY_HPP__


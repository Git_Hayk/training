#include "headers/PrintArray.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    const int A_COUNT = 5;
    const int B_COUNT = 7;
    const int C_COUNT = 6;

    int a[A_COUNT];
    if (isInteractive) {
        std::cout << "Input " << A_COUNT << " integer type members for array a!" << std::endl;
    }
    fillArray(a, A_COUNT);

    double b[B_COUNT];
    if (isInteractive) {
        std::cout << "Input " << B_COUNT << " double type members for array b!" << std::endl;
    }
    fillArray(b, B_COUNT);

    char c[C_COUNT];
    if (isInteractive) {
        std::cout << "Input " << C_COUNT << " char type members for array c!" << std::endl;
    }
    fillArray(c, C_COUNT);

    std::cout << "Array a contains:" << std::endl;
    printArray(a, A_COUNT);

    std::cout << "Array b contains:" << std::endl;
    printArray(b, B_COUNT);

    std::cout << "Array c contains:" << std::endl;
    printArray(c, C_COUNT);

    return 0;
}


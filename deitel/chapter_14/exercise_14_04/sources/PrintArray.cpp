#include <iostream>

template <typename T>
void
printArray(const T* array, const int count, const int lowSubscript, int highSubscript)
{
    if (INT_MAX == highSubscript) {
        highSubscript = count;
    }

    assert(lowSubscript >= 0 && highSubscript <= count && lowSubscript < highSubscript);

    std::cout << "{ ";
    for (int i = lowSubscript; i < highSubscript; ++i) {
        std::cout << array[i] << ((i != highSubscript - 1) ? ", " : "");
    }
    std::cout << " }" << std::endl;
}

template <typename T>
void
fillArray(T* array, const int size)
{
    assert(size > 0);
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
}


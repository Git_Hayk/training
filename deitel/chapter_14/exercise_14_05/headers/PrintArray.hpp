#ifndef __PRINTARRAY_HPP__
#define __PRINTARRAY_HPP__

#include <string>

template <typename T>
void printArray(const T* array, const int count);
void printArray(const std::string array[], const int count);

#include "sources/PrintArray.cpp"

#endif ///__PRINTARRAY_HPP__


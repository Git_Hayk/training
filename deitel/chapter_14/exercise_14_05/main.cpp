#include "headers/PrintArray.hpp"
#include <iostream>

int
main()
{
    const int A_COUNT = 5;
    const int B_COUNT = 7;
    const int C_COUNT = 6;
    const int D_COUNT = 12;

    int a[A_COUNT] = { 1, 2, 3, 4, 5 };
    double b[B_COUNT] = { 1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7 };
    char c[C_COUNT] = "HELLO";
    std::string d[D_COUNT] = { "First name", "Last name", "Age" ,
                               "Jon"       , "Doe"      , "43"  ,
                               "Hayk"      , "Harutyunyan", "27",
                               "Sean"      , "Bean"       , "66"  };

    std::cout << "Array a contains:" << std::endl;
    printArray(a, A_COUNT);

    std::cout << "Array b contains:" << std::endl;
    printArray(b, B_COUNT);

    std::cout << "Array c contains:" << std::endl;
    printArray(c, C_COUNT);

    std::cout << "Array d contains:" << std::endl;
    printArray(d, D_COUNT);

    return 0;
}


#include <iostream>
#include <iomanip>

template <typename T>
void
printArray(const T* array, const int count)
{
    for (int i = 0; i < count; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
}

void
printArray(const std::string array[], const int count)
{
    for (int i = 0; i < count; ++i) {
        if (0 == i % 3) {
            std::cout << '\n';
        }
        std::cout << std::left << std::setw(20) << array[i];
    }
    std::cout << std::endl;
}


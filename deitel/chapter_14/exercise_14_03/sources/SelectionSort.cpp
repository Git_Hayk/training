#include <iostream>
#include <unistd.h>

template <typename T>
void
selectionSort(T* const array, const size_t size)
{
    for (size_t i = 0; i < size - 1; ++i) {
        size_t smallest = i;
        for (size_t j = i + 1; j < size; ++j) {
            if (array[j] < array[smallest]) {
                smallest = j;
            }
        }
        swap(&array[i], &array[smallest]);
    }
}

template <typename T>
void
swap(T* const element1Ptr, T* const element2Ptr)
{
   T hold = *element1Ptr;
   *element1Ptr = *element2Ptr;
   *element2Ptr = hold;
}

template <typename T>
void
fillArray(T* array, const size_t arraySize)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input " << arraySize << " one type data to fill the array: ";
    }
    for (size_t i = 0; i < arraySize; ++i) {
        std::cin >> array[i];
    }
}

template <typename T>
void
printArray(const T* array, const size_t arraySize)
{
    std::cout << "{ ";
    for (size_t i = 0; i < arraySize; ++i) {
        std::cout << array[i] << ", ";
    }
    std::cout << '}' << std::endl;
}


#ifndef __SELECTIONSORT_HPP__
#define __SELECTIONSORT_HPP__

#include <cstddef>

template <typename T>
void fillArray(T* array, const size_t arraySize);
template <typename T>
void swap(T* const element1Ptr, T* const element2Ptr);
template <typename T>
void selectionSort(T* array, const size_t arraySize);
template <typename T>
void printArray(const T* array, const size_t arraySize);

#include "sources/SelectionSort.cpp"

#endif ///__SELECTIONSORT_HPP__


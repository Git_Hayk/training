#include "headers/SelectionSort.hpp"
#include <iostream>
#include <iomanip>
#include <cstddef>

int
main()
{
    std::cout << std::fixed << std::setprecision(2);

    const size_t ARRAY_SIZE = 10;
    int intArray[ARRAY_SIZE];
    fillArray(intArray, ARRAY_SIZE);

    std::cout << "Integers Array before selection sort: ";
    printArray(intArray, ARRAY_SIZE);

    selectionSort(intArray, ARRAY_SIZE);

    std::cout << "Integers Array after selection sort: ";
    printArray(intArray, ARRAY_SIZE);


    double doubleArray[ARRAY_SIZE];
    fillArray(doubleArray, ARRAY_SIZE);

    std::cout << "Doubles Array before selection sort: ";
    printArray(doubleArray, ARRAY_SIZE);

    selectionSort(doubleArray, ARRAY_SIZE);

    std::cout << "Doubles Array after selection sort: ";
    printArray(doubleArray, ARRAY_SIZE);

    char charArray[ARRAY_SIZE];
    fillArray(charArray, ARRAY_SIZE);

    std::cout << "Characters Array before selection sort: ";
    printArray(charArray, ARRAY_SIZE);

    selectionSort(charArray, ARRAY_SIZE);

    std::cout << "Characters Array after selection sort: ";
    printArray(charArray, ARRAY_SIZE);

    return 0;
}


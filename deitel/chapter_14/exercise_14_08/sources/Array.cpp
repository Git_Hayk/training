#include "headers/Array.hpp"
#include <iostream>

template <typename T>
Array<T>::Array(const int arraySize)
{
   size_ = (arraySize > 0 ? arraySize : 10);
   ptr_ = new T[size_];

   for (int i = 0; i < size_; i++) {
      ptr_[i] = 0;
   }
}

template <typename T>
Array<T>::Array(const Array<T> &arrayToCopy)
   : size_(arrayToCopy.size_)
   , ptr_(new T[size_])
{

   for (int i = 0; i < size_; i++) {
      ptr_[i] = arrayToCopy.ptr_[i];
   }
}

template <typename T>
Array<T>::~Array()
{
   if (ptr_ != NULL) {
       delete [] ptr_;
       ptr_ = NULL;
   }
}

template <typename T>
int
Array<T>::getSize() const
{
   return size_;
}


#include "headers/FloatArray.hpp"
#include <iostream>

Array<float>::Array(const int arraySize)
{
   size_ = (arraySize > 0 ? arraySize : 10);
   ptr_ = new float[size_];

   for (int i = 0; i < size_; i++) {
      ptr_[i] = 0;
   }
}

Array<float>::Array(const Array<float>& arrayToCopy)
   : size_(arrayToCopy.size_)
   , ptr_(new float[size_])
{

   for (int i = 0; i < size_; i++) {
      ptr_[i] = arrayToCopy.ptr_[i];
   }
}

Array<float>::~Array()
{
   if (ptr_ != NULL) {
       delete [] ptr_;
       ptr_ = NULL;
   }
}

int
Array<float>::getSize() const
{
   return size_;
}


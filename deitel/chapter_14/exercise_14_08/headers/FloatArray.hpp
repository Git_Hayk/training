#ifndef __ARRAY_FLOAT_HPP__
#define __ARRAY_FLOAT_HPP__

#include <iostream>

template <>
class Array<float>
{
   friend std::ostream& operator<<(std::ostream&, const Array& rhv);
   friend std::istream& operator>>(std::istream&, Array& rhv);      
public:
   Array(const int size = 10);
   Array(const Array& rhv);
   ~Array();
   int getSize() const;
private:
   int size_;
   float* ptr_;
}; // end class Array

#include "sources/FloatArray.cpp"

#endif ///__ARRAY_FLOAT_HPP__


#ifndef __ARRAY_T_HPP__
#define __ARRAY_T_HPP__

#include <iostream>

template <typename T>
class Array
{
   friend std::ostream& operator<<(std::ostream&, const Array& rhv);
   friend std::istream& operator>>(std::istream&, Array& rhv);      
public:
   Array(const int size = 10);
   Array(const Array& rhv);
   ~Array();
   int getSize() const;
private:
   int size_;
   T* ptr_;
}; // end class Array

#include "sources/Array.cpp"

#endif ///__ARRAY_T_HPP__


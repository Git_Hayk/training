#include "headers/Array.hpp"
#include "headers/FloatArray.hpp"
#include <iostream>

int
main()
{
    std::cout << "Integers ";
    Array<int> i;
    std::cout << "i.size_ = " << i.getSize() << std::endl;

    std::cout << "Flaot ";
    Array<float> f;
    std::cout << "f.size_ = " << f.getSize() << std::endl;

    return 0;
}


friend void f1();
f1() is friend for all Ct1 specialisations.

friend void f2( Ct1< T > & );
f2() is friend only for Ct1<T> specialization.

friend void C2::f3();
C2::f3() is friend for all Ct1 specializations.

friend void Ct3< T >::f4( Ct1< T > & );
Ct3<T>::f4() is friend only for Ct1<T> specialization.

friend class C4;
class C4 is friend for all Ct1 specializations.

friend class Ct5< T >;
class Ct5<T> is friend only for Ct1<T> specialization.

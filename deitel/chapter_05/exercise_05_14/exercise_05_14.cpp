/*A mail order house sells five different products whose retail prices are: product 1 $2.98, product 2$4.50,
  product 3$9.98, product 4$4.49 and product 5$6.87. Write a program that reads a series of pairs of numbers as follows:

1) product number

2) quantity sold

Your program should use a switch statement to determine the retail price for each product. Your program should
calculate and display the total retail value of all products sold. Use a sentinel-controlled loop to determine
when the program should stop looping and display the final results.*/

#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Product Menu."
                  << "\n\tProduct 1 - 2.98$" 
                  << "\n\tProduct 2 - 4.50$" 
                  << "\n\tProduct 3 - 3.98$" 
                  << "\n\tProduct 4 - 4.49$" 
                  << "\n\tProduct 5 - 6.87$" << std::endl;
    }

    double product1 = 2.98, product2 = 4.50, product3 = 3.98, product4 = 4.49, product5 = 6.87;
    double totalPrice = 0;
    while (true) {
        int productNumber;
        std::cout << "> Product number (0 to end calculation): ";
        std::cin >> productNumber;

        if (0 == productNumber) {
            break;
        }

        if (productNumber < 1 || productNumber > 5) {
            std::cerr << "Error 1: Invalid product number!" << std::endl;
            return 1;
        }

        int quantitySold;
        std::cout << "> Product " << productNumber << " quantity sold: ";
        std::cin >> quantitySold;

        if (quantitySold < 0) {
            std::cerr << "Error 2: Invalid quantity (less than 0)!" << std::endl;
            return 2;
        }
        
        switch (productNumber) {
        case 1: totalPrice += product1 * quantitySold; break;
        case 2: totalPrice += product2 * quantitySold; break;
        case 3: totalPrice += product3 * quantitySold; break;
        case 4: totalPrice += product4 * quantitySold; break;
        case 5: totalPrice += product5 * quantitySold; break;
        default: assert(0 && "There are only 5 products");
        }
    }
    std::cout << "Total price is " << totalPrice << '$' << std::endl;
    return 0;
}


/// Calculate the value of p from the infinite series
/// Print a table that shows the approximate value of p after each of the first 1,000 terms of this series.

#define _USE_MATH_DEFINES

#include <iostream>
#include <iomanip>
#include <cmath>

int
main()
{
    double pi = 4.0, announcer = 1.0;
    int multiplier = 100;
    int precision = 3;
    for (int counter = 1; counter <= 400000; ++counter) {
        double previousPiValue = pi;

        announcer += 2;
        bool memberCountIsOdd = counter % 2;
        if (memberCountIsOdd) { pi -= 4.0 / announcer; } 
        else                  { pi += 4.0 / announcer; }
        
        if (static_cast<int>(pi * multiplier) == static_cast<int>(M_PI * multiplier)) {
            int difference = static_cast<int>(pi * multiplier) - static_cast<int>(previousPiValue * multiplier);

            if (0 == difference) {
                std::cout << "Needed " << counter - 1 << " row members to get the value " << std::setprecision(precision) << pi << std::endl;
                multiplier *= 10;
                precision += 1;
            }
        }
    }
    return 0;
}


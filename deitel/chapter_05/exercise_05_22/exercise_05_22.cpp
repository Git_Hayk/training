/*De Morgan's Laws) In this chapter, we discussed the logical operators &&, || and !. De Morgan's laws can sometimes
  make it more convenient for us to express a logical expression. These laws state that the expression !( condition1 && condition2 )
  is logically equivalent to the expression !( condition1 || condition2 ). Also, the expression ! ( condition1 || condition2 )
  is logically equivalent to the expression ( !condition1 && ! condition2 ) . Use De Morgan's laws to write equivalent expressions
  for each of the following, then write a program to show that the original expression and the new expression in each case are equivalent:*/

#include <iostream>

int
main()
{
    int x = 0, y = 10;
    std::cout << "x and y conditions:" << std::endl;

    bool result1 = (!(x < 5) && !(y >= 7));
    bool result2 = (!((x < 5) || (y >= 7)));
    if (result1 == result2) {
        std::cout << "Equivalent." << std::endl;
    } else {
        std::cout << "Not equivalent." << std::endl;
    }

    int a = 1, b = 1, g = 0;
    std::cout << "a,b and g conditions:" << std::endl;

    result1 = (!(a == b) || !(g != 5));
    result2 = (!((a == b) && (g != 5)));
    if (result1 == result2) {
        std::cout << "Equivalent." << std::endl;
    } else {
        std::cout << "Not equivalent." << std::endl;
    }
    
    x = 8, y = 3;
    std::cout << "x and y conditions:" << std::endl;

    result1 = !((x <= 8) && (y > 4));
    result2 = (!(x <= 8) || !(y > 4));
    if (result1 == result2) {
        std::cout << "Equivalent." << std::endl;
    } else {
        std::cout << "Not equivalent." << std::endl;
    }
    
    int i = 2, j = 3;
    std::cout << "i and j conditions:" << std::endl;

    result1 = !((i > 4) || (j <= 6));
    result2 = (!(i > 4) && !(j <= 6));
    if (result1 == result2) {
        std::cout << "Equivalent." << std::endl;
    } else {
        std::cout << "Not equivalent." << std::endl;
    }
    return 0;
}


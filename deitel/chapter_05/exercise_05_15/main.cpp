/*Modify the GradeBook program of Fig. 5.9Fig. 5.11 so that it calculates the grade-point average for the set of
  grades. A grade of A is worth 4 points, B is worth 3 points, etc.*/

#include "GradeBook.hpp"

int
main()
{
    GradeBook myGradeBook("CS101 C++ Programming");

    myGradeBook.displayMessage();
    myGradeBook.inputGrades();
    myGradeBook.calculateAverageOfGroup();
    myGradeBook.displayGradeReport();
    return 0;
}


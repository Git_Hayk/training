/*Modify the compound interest program of Section 5.4 to repeat its steps for the interest rates 5 percent, 6 percent,
  7 percent, 8 percent, 9 percent and 10 percent. Use a for statement to vary the interest rate.*/

#include <iostream>
#include <iomanip>

int main()
{
    for (int rate = 5; rate <= 10; ++rate) {
        double principal = 1000.0;
        std::cout << "Year" << std::setw(24) << "Amount on deposit for " << rate << '%' << std::endl;
        std::cout << std::fixed << std::setprecision(2);

        double percent = (1.0 + static_cast<double>(rate) / 100);
        for (int year = 1; year <= 10; ++year) {
            principal *= percent;
            std::cout << std::setw(4) << year << std::setw(26) << principal << std::endl;
        }
        std::cout << std::endl;
    } 
    return 0;
}


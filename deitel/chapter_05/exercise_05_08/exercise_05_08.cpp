/*Write a program that uses a for statement to find the smallest of several integers. Assume that the first
  value read specifies the number of values remaining and that the first number is not one of the integers to compare.*/

#include <iostream>
#include <limits.h>

int
main()
{
    int counter, smallest = INT_MAX;
    std::cout << "Input integers to find the smallest number, using the first value as a counter: ";
    std::cin >> counter;

    if (counter < 1) {
        std::cerr << "Error 1: Invalid value for counter!" << std::endl;
        return 1;
    }

    for (int i = 0; i < counter; ++i) {
        int number;
        std::cin >> number;

        if (number < smallest) {
            smallest = number;
        }
    }
    std::cout << "Smallest number is " << smallest << std::endl;
    return 0;
}


/*Write a program that prints the following diamond shape. You may use output statements that print either
  a single asterisk (*) or a single blank. Maximize your use of repetition (with nested for statements) and
  minimize the number of output statements.*/

#include <iostream>
#include <cmath>

int
main()
{
    int rombWidth = 9;
    int size = rombWidth / 2;
    for (int axisY = size; axisY >= -size; --axisY) {
        for (int axisX = -size; axisX <= size; ++axisX) {
            std::cout << ((std::abs(axisX) + std::abs(axisY) <= size) ? '*' : ' ');
        }
        std::cout << '\n';
    }
    return 0;
}


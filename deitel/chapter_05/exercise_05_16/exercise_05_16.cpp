/*Modify the program in Fig. 5.6 so it uses only integers to calculate the compound interest. [Hint: Treat
  all monetary amounts as integral numbers of pennies. Then "break" the result into its dollar portion and
  cents portion by using the division and modulus operations. Insert a period.]*/

#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << "Year" << std::setw(21) << "Amount on deposit" << std::endl;

    int principalInCent = 100000;
    int rate = 5;
    for (int year = 1; year <= 10; ++year) {

        int result = principalInCent + (principalInCent * rate / 100);
        int principalInDollars = result / 100;
        principalInCent = result;
        result %= 100;
        std::cout << std::setw(4) << year << std::setw(18) << principalInDollars << '.' << result << std::endl;
    }

    return 0;
}


/*A criticism of the break and continue statements is that each is unstructured. Actually they statements can always
  be replaced by structured statements, although doing so can be awkward. Describe in general how you would remove
  any break statement from a loop in a program and replace it with some structured equivalent. [Hint: The break
  statement leaves a loop from within the body of the loop. Another way to leave is by failing the loop-continuation
  test. Consider using in the loop-continuation test a second test that indicates "early exit because of a 'break'
  condition."] Use the technique you developed here to remove the break statement from the program of Fig. 5.13.*/

#include <iostream>

int
main()
{
    int count;
    for (count = 1; count != 5; ++count) {
        std::cout << count << " ";
    }
    std::cout << "\nBroke out of loop at count = " << count << std::endl;
    return 0;
}


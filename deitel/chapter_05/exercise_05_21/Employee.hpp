#include <string>

class Employee
{
public:
    Employee();
    void run();

private: 
    void displayMessage();
    void printPayCodeItem(int index);
    double getInputFromUser();
    void validateUserInput(int command);
    void validateValue(double value);
    void executeCommand(int command);
    void checkForExit(int command);
    void returnWeeklySalary();

    void printManagersMenu();
    void printHourlyWorkersMenu();
    void printCommisionWorkersMenu();
    void printPieceWorkersMenu();  

    void calculateManagersSalary(double weeklySalary);
    void calculateHourlyWorkersSalary(double hourlyWage, double hoursWorked);
    void calculateCommisionWorkersSalary(double weeklySales);
    void calculatePieceWorkersSalary(double itemPrice, int quantity);  

private:
    double weeklySalary_;
};


#include "Employee.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>

Employee::Employee()
{
}

void
Employee::run()
{
    while (true) {
        displayMessage();
        int command = getInputFromUser();
        validateUserInput(command);
        checkForExit(command);
        executeCommand(command);
        returnWeeklySalary();
    }
}

void
Employee::displayMessage()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input pay code for employee:";
        for (int i = 0; i <= 4; ++i) {
            printPayCodeItem(i);
        }
        std::cout << "> Command: ";
    }
}

void
Employee::printPayCodeItem(int index)
{
    std::cout << "\n\t" << index << " - ";
    switch (index) {
    case 0: std::cout << "Exit"; break;
    case 1: std::cout << "Managers"; break;
    case 2: std::cout << "Hourly workers"; break;
    case 3: std::cout << "Commision workers"; break;
    case 4: std::cout << "Pieceworkers\n"; break;
    default: assert(0 && "There are only five commands!:");
    }
}

void
Employee::executeCommand(int command)
{
    switch (command) {
    case 1: printManagersMenu(); break;
    case 2: printHourlyWorkersMenu(); break;
    case 3: printCommisionWorkersMenu(); break;
    case 4: printPieceWorkersMenu(); break;
    default: assert(0 && "There are only four types of workers!");
    }
}

void
Employee::printManagersMenu()
{
    std::cout << "Manager:\n\tInput weekly salary: ";
    double weeklySalary = getInputFromUser();
    validateValue(weeklySalary);
    calculateManagersSalary(weeklySalary);
}

void
Employee::printHourlyWorkersMenu()
{
    std::cout << "Hourly worker:\n\tInput hourly wage: ";
    double hourlyWage = getInputFromUser();
    validateValue(hourlyWage);
    std::cout << "\tInput hours worked: ";
    double hoursWorked = getInputFromUser();
    validateValue(hoursWorked);
    calculateHourlyWorkersSalary(hourlyWage, hoursWorked);
}

void
Employee::printCommisionWorkersMenu()
{
    std::cout << "Commision Worker:\n\tInput weekly sales: ";
    int weeklySales = getInputFromUser();
    validateValue(weeklySales);
    calculateCommisionWorkersSalary(weeklySales);
}

void
Employee::printPieceWorkersMenu()
{
    std::cout << "Piece workers:\n\tInput item price: ";
    double itemPrice = getInputFromUser();
    validateValue(itemPrice);
    std::cout << "\tInput quantity: ";
    int quantity = getInputFromUser();
    validateValue(quantity);
    calculatePieceWorkersSalary(itemPrice, quantity);
}

void
Employee::calculateManagersSalary(double weeklySalary)
{
    assert(weeklySalary >= 0.0);
    weeklySalary_ = weeklySalary;
}

void
Employee::calculateHourlyWorkersSalary(double hourlyWage, double hoursWorked)
{
    assert(hourlyWage >= 0.0);
    assert(hoursWorked >= 0.0);
    weeklySalary_ = hoursWorked * hourlyWage;

    double straightTime = 40.0;
    double timeAndAHalf = 0.5;
    if (hoursWorked > straightTime) {
        weeklySalary_ += (hoursWorked - straightTime) * timeAndAHalf * hourlyWage;
    }
}
    
void
Employee::calculateCommisionWorkersSalary(double weeklySales)
{
    assert(weeklySales >= 0.0);
    weeklySalary_ = 250.0;
    if (weeklySales > 0) {
        weeklySalary_ += weeklySales * 5.7 / 100;
    }
}

void
Employee::calculatePieceWorkersSalary(double itemPrice, int quantity)
{
    assert(itemPrice >= 0.0);
    assert(quantity >= 0.0);
    weeklySalary_ = itemPrice * quantity * 0.3; ///let's say that piece workers get 30 percent of each item
}

double
Employee::getInputFromUser()
{
    double userInput;
    std::cin >> userInput;
    return userInput;
}

void
Employee::validateUserInput(int command)
{
    if (command < 0 || command > 4) {
        std::cerr << "Error 1: Invalid Command!" << std::endl;
        ::exit(1);
    }
}

void
Employee::validateValue(double value)
{
    if (value < 0) {
        std::cerr << "Error 2: Invalid Value(less than 0)!" << std::endl;
        ::exit(2);
    }
}

void
Employee::checkForExit(int command)
{
    if (0 == command) {
        std::cout << "Exiting..." << std::endl;
        ::exit(0);
    }
}

void
Employee::returnWeeklySalary()
{
    std::cout << std::fixed << std::setprecision(2);
    std::cout << "Employee's weekly salary is " << weeklySalary_ << '$' << std::endl;
}


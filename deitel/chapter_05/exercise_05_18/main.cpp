/// Write a program that prints a table of the binary, octal and hexadecimal equivalents of the decimal numbers in the range 1 through 256.

#include "Table.hpp"
#include <iostream>

int
main()
{
    Table numberSystem;
    numberSystem.run();
    return 0;
}


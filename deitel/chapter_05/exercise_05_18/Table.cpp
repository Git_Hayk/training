#include "Table.hpp"
#include <iostream>
#include <iomanip>

Table::Table()
{
}

void
Table::run()
{
    printHeadLine();
    for (int row = 1; row <= 256; ++row) {
        printTable(row);
    }
}

void
Table::printHeadLine()
{
    std::cout << "Decimal" << std::setw(10) 
              << "Binary" << std::setw(10)
              << "Octal" << std::setw(10)
              << "Hex" << std::endl;
}

void
Table::printTable(int number)
{
    std::cout << std::setw(7) << number << std::setw(10);
    int systemMultiplier = 1; 
    for (int system = 1; system <= 3; ++system) {
        int decimalPositionalValue = 1;
        int systemPositionalValue = 1;
        int numberVariable = number;
        int decimalMultiplier = 10;

        systemMultiplier *= 2;
        if (4 == systemMultiplier) { --system; continue; } /// no-existent system

        while (numberVariable / systemPositionalValue > 0) {
            decimalPositionalValue *= decimalMultiplier;
            systemPositionalValue *= systemMultiplier;
        }

        systemPositionalValue /= systemMultiplier;
        decimalPositionalValue /= decimalMultiplier;
        
        int systemNumber = 0;
        while (systemPositionalValue != 0) {
            int digit = numberVariable / systemPositionalValue;
            systemNumber += digit * decimalPositionalValue;
            numberVariable -= systemPositionalValue * digit;
            decimalPositionalValue /= decimalMultiplier;
            systemPositionalValue /= systemMultiplier;

            if (3 == system) {
                if (digit >= 10) {
                    switch (digit) {
                    case 10: std::cout << 'A'; break;
                    case 11: std::cout << 'B'; break;
                    case 12: std::cout << 'C'; break;
                    case 13: std::cout << 'D'; break;
                    case 14: std::cout << 'E'; break;
                    case 15: std::cout << 'F'; break;
                    default: assert(digit <= 15);
                    }
                } else {
                    std::cout << digit;
                }
            }
        }

        int width = 10;
        if (1 == system) { 
            std::cout << systemNumber << std::setw(width);
        } else if (2 == system) {
            if (number > 15) { width -= 1; }
            if (number > 255) { width -= 1; }
            std::cout << systemNumber << std::setw(width); 
        }
    }
    std::cout << std::endl;
}


class Table
{
public:
    Table();
    void run();

private:
    void printHeadLine();
    void printTable(int number);
};


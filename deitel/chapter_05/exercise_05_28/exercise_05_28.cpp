/*("The Twelve Days of Christmas" Song) Write a program that uses repetition and switch statements to print the song
  "The Twelve Days of Christmas." One switch statement should be used to print the day (i.e., "First," "Second," etc.).
  A separate switch statement should be used to print the remainder of each verse.
  Visit the Web site www.12days.com/library/carols/12daysofxmas.htm for the complete lyrics to the song.*/

#include <iostream>

int
main()
{
    std::cout << "The Twelve days of Christmas\n\n" << std::endl;
    for (int couplet = 1; couplet <= 12; ++couplet) {
        std::string dayNumber;
        switch (couplet) {
        case 1: dayNumber = "first"; break;
        case 2: dayNumber = "second"; break;
        case 3: dayNumber = "third"; break;
        case 4: dayNumber = "fourth"; break;
        case 5: dayNumber = "fifth"; break;
        case 6: dayNumber = "sixth"; break;
        case 7: dayNumber = "seventh"; break;
        case 8: dayNumber = "eighth"; break;
        case 9: dayNumber = "ninth"; break;
        case 10: dayNumber = "tenth"; break;
        case 11: dayNumber = "eleventh"; break;
        case 12: dayNumber = "twelfth"; break;
        default: assert(0 && "There are only 12 numbers of day's!");
        }

        std::cout << "On the " << dayNumber << " day of Christmas" << "\nmy true love sent to me:" << std::endl;

        switch (couplet) {
        case 12: std::cout << "12 Drummers Drumming" << std::endl;
        case 11: std::cout << "Eleven Pipers Piping" << std::endl;
        case 10: std::cout << "Ten Lords a Leaping" << std::endl;
        case 9: std::cout << "Nine Ladies Dancing" << std::endl;
        case 8: std::cout << "Eight Maids a Milking" << std::endl;
        case 7: std::cout << "Seven Swans a Swimming" << std::endl;
        case 6: std::cout << "Six Geese a Laying" << std::endl;
        case 5: std::cout << "Five Golden Rings" << std::endl;
        case 4: std::cout << "Four Calling Birds" << std::endl;
        case 3: std::cout << "Three French Hens" << std::endl;
        case 2: std::cout << "Two Turtle Doves\nand ";
        case 1: std::cout << "a Partridge in a Pear Tree" << std::endl; break;
        default: assert(0 && "There are only 12 couplets!");
        }
        std::cout << std::endl;
    }
    return 0;
}


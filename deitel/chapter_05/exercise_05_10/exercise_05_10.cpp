/*The factorial function is used frequently in probability problems. Using the definition of factorial in Exercise 4.35,
  write a program that uses a for statement to evaluate the factorials of the integers from 1 to 5. Print the results in
  tabular format. What difficulty might prevent you from calculating the factorial of 20?*/

#include <iostream>

int
main()
{
    std::cout << "Number\t" << "Factorial" << std::endl;
    for (int i = 1; i <= 5; ++i) {
        std::cout << i << '\t';

        int factorial = 1;
        int number = i;
        while (number > 1) {
            factorial *= number;
            --number;
        }

        std::cout << factorial << std::endl;
    }
    return 0;
}

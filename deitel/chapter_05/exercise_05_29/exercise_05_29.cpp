/*(Peter Minuit Problem) Legend has it that, in 1626, Peter Minuit purchased Manhattan Island for $24.00 in barter.
  Did he make a good investment? To answer this question, modify the compound interest program of Fig. 5.6 to begin
  with a principal of $24.00 and to calculate the amount of interest on deposit if that money had been kept on deposit
  until this year (e.g., 379 years through 2005). Place the for loop that performs the compound interest calculation in
  an outer for loop that varies the interest rate from 5 percent to 10 percent to observe the wonders of compound interest.*/

#include <iostream>
#include <iomanip>

int main()
{
    for (int rate = 5; rate <= 10; ++rate) {
        double principal = 24.00;
        std::cout << "Year" << std::setw(24) << "Amount on deposit for " << rate << '%' << std::endl;
        std::cout << std::fixed << std::setprecision(2);

        double percent = (1.0 + static_cast<double>(rate) / 100);
        for (int year = 0; year < 379; ++year) {
            principal *= percent;
            std::cout << std::setw(4) << year << std::setw(26) << principal << std::endl;
        }

        std::cout << std::endl;
    } 
    return 0;
}


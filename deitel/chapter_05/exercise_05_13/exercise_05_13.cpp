/*One interesting application of computers is the drawing of graphs and bar charts. Write a program that reads five
  numbers (each between 1 and 30). Assume that the user enters only valid values. For each number that is read, your
  program should print a line containing that number of adjacent asterisks. For example, if your program reads the
  number 7, it should print *******.*/

#include <iostream>

int
main()
{
    std::cout << "Input quantity of '*' to print in line: ";
    for (int row = 0; row < 5; ++row) {
        int column;
        std::cin >> column;

        for (int i = 0; i < column; ++i) {
            std::cout << '*';
        }

        std::cout << '\n';
    }
    return 0;
}


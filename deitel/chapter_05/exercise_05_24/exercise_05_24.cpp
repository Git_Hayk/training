/*Modify the program you wrote in Exercise 5.23 to read an odd number in the range 1 to 19 to specify the number of
  rows in the diamond, then display a diamond of the appropriate size.*/

#include <iostream>
#include <cmath>

int
main()
{
    int width;
    std::cout << "Input an odd number for width of diamond(1 to 19): ";
    std::cin >> width;
    
    if (width < 1 || width > 19) {
        std::cerr << "Error 1: Out of range 1 to 19!" << std::endl;
        return 1;
    }

    if (0 == width % 2) {
        std::cerr << "Error 2: Inputed an even number!" << std::endl;
        return 2;
    }
    
    std::cout << std::endl;
    int size = width / 2;
    for (int valueY = size; valueY >= -size; --valueY) {
        for (int valueX = -size; valueX <= size; ++valueX) {
            std::cout << (std::abs(valueX) + std::abs(valueY) <= size ? '*' : ' ');
        }
        std::cout << '\n';
    }
    return 0;
}


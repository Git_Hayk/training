/*Write a program that uses for statements to print the following patterns separately, one below the other.
  Use for loops to generate the patterns. All asterisks (*) should be printed by a single statement of the
  form cout << '*'; (this causes the asterisks to print side by side). [Hint: The last two patterns require
  that each line begin with an appropriate number of blanks. Extra credit: Combine your code from the four
  separate problems into a single program that prints all four patterns side by side by making clever use of
  nested for loops.]*/

#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << "a)" << std::endl;
    for (int row = 1; row <= 10; ++row) {
       for (int column = 1; column <= row; ++column) {
           std::cout << '*';
        }
       std::cout << '\n';
    }
    
    std::cout << "b)" << std::endl;
    for (int row = 1; row <= 10; ++row) {
        for (int column = 10; column >= row; --column) {
            std::cout << '*';
        }
        std::cout << '\n';
    }

    std::cout << "c)" << std::endl;
    for (int row = 1; row <= 10; ++row) {
        for (int column = 1; column <= 10; ++column) {
            if (column >= row) {
                std::cout << '*';
            } else {
                std::cout << ' ';
            }
        }
        std::cout << '\n';
    }

    std::cout << "d)" << std::endl;
    for (int row = 1; row <= 10; ++row) {
        for (int column = 10; column >= 1; --column) {
            if (column <= row) {
                std::cout << '*';
            } else {
                std::cout << ' ';
            }
        }
        std::cout << '\n';
    }
    

    std::cout << "\n(a)" << std::setw(13) << "(b)" << std::setw(13) << "(c)" << std::setw(13) << "(d)" << std::endl;

    for (int row = 1; row <= 10; ++row) {

        for (int column = 1; column <= 10; ++column) {
            if (column <= row) { std::cout << '*'; }
            else               { std::cout << ' '; }
        }
        
        std::cout << std::setw(4);
        for (int column = 10; column >= 1; --column) {
            if (column >= row) { std::cout << '*'; }
            else               { std::cout << ' '; }
        }

        std::cout << std::setw(4);
        for (int column = 1; column <= 10; ++column) {
            if (column >= row) { std::cout << '*'; }
            else               { std::cout << ' '; }
        }

        std::cout << std::setw(4);
        for (int column = 10; column >= 1; --column) {
            if (column <= row) { std::cout << '*'; }
            else               { std::cout << ' '; }
        }
    std::cout << '\n';
    }
    return 0;
}


/*Write a program that uses a for statement to calculate and print the average of several integers.
  Assume the last value read is the sentinel 9999. A typical input sequence might be

      10 8 11 7 9 9999

indicating that the program should calculate the average of all the values preceding 9999.*/

#include <iostream>

int
main()
{
    int sum = 0, quantity = 0;
    std::cout << "Input integers for calculating average (9999 to end): ";
    while (true) {
        int number;
        std::cin >> number;

        if (9999 == number) {
            break;
        }
        sum += number;
        ++quantity;
    }

    if (0 == quantity) {
        std::cout << "No integers inputed." << std::endl;
        return 0;
    }

    double average = static_cast<double>(sum) / quantity;
    std::cout << "Average is " << average << std::endl;

    return 0;
}
        

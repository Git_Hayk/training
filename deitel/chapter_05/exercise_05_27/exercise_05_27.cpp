/*Describe in general how you would remove any continue statement from a loop in a program and replace it with
  some structured equivalent. Use the technique you developed here to remove the continue statement from the
  program of Fig. 5.14.*/

#include <iostream>

int
main()
{
    for (int count = 1; count <= 10; ++count) {
        if (count != 5) {
            std::cout << count << " ";
        }
    }

    std::cout << "\nUsed continue to skip printing 5" << std::endl;
    return 0;
}


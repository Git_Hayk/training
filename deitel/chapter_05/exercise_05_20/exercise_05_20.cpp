/*(Pythagorean Triples) A right triangle can have sides that are all integers. A set of three integer values for the
  sides of a right triangle is called a Pythagorean triple. These three sides must satisfy the relationship that the
  sum of the squares of two of the sides is equal to the square of the hypotenuse. Find all Pythagorean triples for
  side1, side2 and hypotenuse all no larger than 500. Use a triple-nested for loop that tries all possibilities. This
  is an example of brute force computing. You will learn in more advanced computer-science courses that there are many
  interesting problems for which there is no known algorithmic approach other than sheer brute force.*/

#include <iostream>
#include <cmath>

int
main()
{
    int counter = 0;
    int conditionCounter = 0;
    for (int side1 = 3; side1 < 500; ++side1) {
        int side1Square = side1 * side1;
        for (int side2 = side1 + 1; side2 < 500; ++ side2) {
            int side2Square = side2 * side2;
            for (int hypotenuse = std::sqrt(side1Square + side2Square); hypotenuse <= 500 && hypotenuse != static_cast<int>(std::sqrt(side1Square + side2Square) + 1); ++hypotenuse) {
                int hypotenuseSquare = hypotenuse * hypotenuse;

                if (hypotenuseSquare == side1Square + side2Square) {
                    ++counter;
                    std::cout << counter << ") a = " << side1
                                         << ", b = " << side2
                                         << ", c = " << hypotenuse << std::endl;
                }
                ++conditionCounter;
            }
        }
    }
    std::cout << "Count of conditions is " << conditionCounter << std::endl;
    return 0;
}
   

/*Write a program that uses a for statement to sum a sequence of integers. Assume that the first integer
  read specifies the number of values remaining to be entered. Your program should read only one value per
  input statement. A typical input sequence might be

      5 100 200 300 400 500

where the 5 indicates that the subsequent 5 values are to be summed.*/

#include <iostream>

int
main()
{
    int counter;
    std::cout << "Input quantity of numbers to sum: ";
    std::cin >> counter;
    
    int sum = 0;
    for (int i = 0; i < counter; ++i) {
        int number;
        std::cin >> number;
        sum += number;
    }
    std::cout << "Sum of inputed numbers is " << sum << std::endl;
    return 0;
}


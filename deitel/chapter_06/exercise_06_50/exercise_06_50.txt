What does the following program do?

#include <iostream>

using std::cout;
using std::cin;
using std::endl;

int mystery( int, int ); 

int main()
{
    int x, y;

    cout << "Enter two integers: ";
    cin >> x >> y;
    cout << "The result is " << mystery( x, y ) << endl;

    return 0;
}

int mystery( int a, int b )
{
    if ( b == 1 )
        return a;
    else
        return a + mystery( a, b - 1 );
}


This program adds the value of x to itself while the y value isn't equal to 1.
It's the same, as if it would return (x * y).

a) What does it mean to choose numbers "at random?"
to choose numbers in range 0 to RAND_MAX

b) Why is the rand function useful for simulating games of chance?
In certain cases, this makes it possible to control the outcome of actions

c) Why would you randomize a program by using srand? Under what circumstances is it desirable not to randomize?
To hide the pseudorandom randomization of numbers.
It is useful do not use srand to make sure that the program is working properly.

d) Why is it often necessary to scale or shift the values produced by rand?
Бecause in different situations we need numbers in different ranges.

e) Why is computerized simulation of real-world situations a useful technique?
Тhe ability to create many programs and games based on randomness

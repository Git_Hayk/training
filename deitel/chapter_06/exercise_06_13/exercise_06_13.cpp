/*An application of function floor is rounding a value to the nearest integer. The statement

y = floor( x + .5 );

rounds the number x to the nearest integer and assigns the result to y. Write a program that reads
several numbers and uses the preceding statement to round each of these numbers to the nearest integer.
For each number processed, print both the original number and the rounded number.*/

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <unistd.h>

int roundToInteger(const float number);

int
roundToInteger(const float number)
{
    const int result = static_cast<int>(std::floor(number + .5));
    return result;
}

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    if (isInteractive) {
        std::srand(::time(0));
    }
    std::cout << std::fixed << std::setprecision(2);
    std::cout << "Original Number" << std::setw(20) << "Rounded number" << std::endl;
    for (int i = 0; i < 10; ++i) {
        const float originalNumber = static_cast<float>(std::rand()) / static_cast<float>(RAND_MAX / 99);
        const int roundedNumber = roundToInteger(originalNumber);

        std::cout << originalNumber << std::setw(18) << roundedNumber << std::endl;
    }
    return 0;
}


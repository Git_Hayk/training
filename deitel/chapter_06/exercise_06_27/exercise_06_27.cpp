/*
(Celsius and Fahrenheit Temperatures) Implement the following integer functions:

1) Function celsius returns the Celsius equivalent of a Fahrenheit temperature.

2) Function fahrenheit returns the Fahrenheit equivalent of a Celsius temperature.

3) Use these functions to write a program that prints charts showing the Fahrenheit equivalents of all Celsius
   temperatures from 0 to 100 degrees, and the Celsius equivalents of all Fahrenheit temperatures from 32 to 212 degrees.
   Print the outputs in a neat tabular format that minimizes the number of lines of output while remaining readable.
 */

#include <iostream>
#include <iomanip>

const double FAHRENHEIT_ZERO = 32.0;
const double COEFFICIENT = 1.8;

double celsius(const int temperatureInFahrenheit);
double fahrenheit(const int temperatureInCelsius);

int
main()
{
    std::cout << std::fixed << std::setprecision(1);

    std::cout << "\nCelsius" << std::setw(12) << "Fahrenheit\n" << std::endl;
    for (int i = 0; i <= 100; ++i) {
        std::cout << std::setw(3) << i << "ºC" << std::setw(8) << fahrenheit(i) << "ºF" << std::endl;
    }

    std::cout << "\nFahrenheit" << std::setw(9) << "Celsius\n" << std::endl;
    for (int i = 32; i <= 212; ++i) {
        std::cout << std::setw(4) << i << "ºF" << std::setw(8) << celsius(i) << "ºC"<< std::endl;
    }
    return 0;
}


double
celsius(const int temperatureInFahrenheit)
{
    return (static_cast<double>(temperatureInFahrenheit) - FAHRENHEIT_ZERO) / COEFFICIENT;
}

double
fahrenheit(const int temperatureInCelsius)
{
    return FAHRENHEIT_ZERO + static_cast<double>(temperatureInCelsius) * COEFFICIENT;
}


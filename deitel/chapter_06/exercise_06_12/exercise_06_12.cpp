/*A parking garage charges a $2.00 minimum fee to park for up to three hours. The garage charges an additional $0.50
  per hour for each hour or part thereof in excess of three hours. The maximum charge for any given 24-hour period is
  $10.00. Assume that no car parks for longer than 24 hours at a time. Write a program that calculates and prints the
  parking charges for each of three customers who parked their cars in this garage yesterday. You should enter the
  hours parked for each customer. Your program should print the results in a neat tabular format and should calculate
  and print the total of yesterday's receipts. The program should use the function calculateCharges to determine the
  charge for each customer. Your outputs should appear in the following format:*/

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cmath>

float calculateCharges(float hours);

float
calculateCharges(const float hours)
{
    static const float MINIMUM_FEE = 2.00;
    static const float THREE_HOURS = 3.0;
    static const float RATE_PER_HOUR = 0.50;

    if (hours <= THREE_HOURS) {
        return MINIMUM_FEE;
    }

    if (hours >= 19.00) {
        return 10.00;
    }

    const float charge = MINIMUM_FEE + (std::ceil(hours) - THREE_HOURS) * RATE_PER_HOUR;
    return charge;
}

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    std::cout << std::fixed;
    std::cout << "Car" << std::setw(10) << "Hours" << std::setw(10) << "Charge" << std::endl;

    float totalHours = 0.0, totalCharges = 0.0;
    for (int car = 1; car <= 3; ++car) {
        float parkingHours = 0.0;
        if (isInteractive) {
            std::cout << "Input the number of parking hours: " << std::endl;
        }
        std::cin >> parkingHours;
        
        const float charges = calculateCharges(parkingHours);
        std::cout << car << std::setw(12) << std::setprecision(1) <<  parkingHours << std::setw(10) << std::setprecision(2) <<  charges << std::endl;
        totalHours += parkingHours;
        totalCharges += charges;
    }
    std::cout << "TOTAL" << std::setw(8) << std::setprecision(1) << totalHours << std::setw(10) << std::setprecision(2) << totalCharges << std::endl;
    return 0;
}


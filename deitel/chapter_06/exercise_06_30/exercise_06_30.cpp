/*
(PrimeNumbers) An integer is said to be prime if it is divisible by only 1 and itself. For example, 2, 3, 5 and 7 are prime, but 4, 6, 8 and 9 are not.

1) Write a function that determines whether a number is prime.

2) Use this function in a program that determines and prints all the prime numbers between 2 and 10,000. How many of
these numbers do you really have to test before being sure that you have found all the primes?

3) Initially, you might think that n/2 is the upper limit for which you must test to see whether a number is prime,
but you need only go as high as the square root of n. Why? Rewrite the program, and run it both ways. Estimate the performance improvement.
 */

#include <iostream>
#include <cmath>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

void validateNumber(const int number);
bool isPrimeNumber(const int number);
void displayMessage(const int number, const bool result);

int
main()
{
    if (isInteractive) {
        std::cout << "Input integer number to check it if it's prime: ";
    }
    int number;
    std::cin >> number;
    validateNumber(number);
    const bool result = isPrimeNumber(number);
    displayMessage(number, result);

    for (int number = 0; number <= 10000; ++number) {
        const bool result = isPrimeNumber(number);
        displayMessage(number, result);
    }
    return 0;
}

void
validateNumber(const int number)
{
    if (number < 0) {
        std::cout << "Error 1: Invalid number!" << std::endl;
        ::exit(1);
    }
}

bool
isPrimeNumber(const int number)
{
    if (0 == number || 1 == number) {
        return false;
    }
    const int range = std::floor(std::sqrt(number));
    for (int i = 2; i <= range; ++i) {
        if (number % i == 0) {
            return false;
        }
    }
    return true;
}

void 
displayMessage(const int number, const bool result)
{
    if (result) {
        std::cout << number << " - Prime" << std::endl;
    } else {
        std::cout << number << " - Not Prime" << std::endl;
    }
}


/*
Modify the function created in Exercise 6.22 to form the square out of whatever character is contained
in character parameter fillCharacter. Thus, if side is 5 and fillCharacter is #, then this function should
print the following:
 */

#include <iostream>
#include <unistd.h>
#include <limits.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

void printSquare(const int side, const char character);
void fillCharacter(const char character);
void checkForExit(const int side);
void validateValue(const int side);

int
main()
{
    while (true) {
        if (isInteractive) {
            std::cout << "Input the side size to print square (0 to exit): ";
        }
        int side; 
        std::cin >> side;
        checkForExit(side);
        validateValue(side);

        if (isInteractive) {
            std::cout << "Input character to print: ";
        }
        char symbol;
        std::cin >> symbol;

        fillCharacter(symbol);
        printSquare(side, symbol);
    }
    return 0;
}

void
printSquare(const int side, const char character)
{
    assert(side > 0);
    assert(std::isprint(character) != 0);
    for (int row = 0; row < side; ++row) {
        for (int column = 0; column < side; ++column) {
            std::cout << character;
        }
        std::cout << '\n';
    }
}

void
fillCharacter(const char character)
{
    if (!std::isprint(character)) {
        std::cout << "Error 2: Non printable character!" << std::endl;
        ::exit(2);
    }
}

void
checkForExit(const int side)
{
    if (0 == side) {
        std::cout << "Exiting..." << std::endl;
        ::exit(0);
    }
}

void
validateValue(const int side)
{
    if (side < 0) {
        std::cout << "Error 3: Invalid Value!" << std::endl;
        ::exit(3);
    }
}


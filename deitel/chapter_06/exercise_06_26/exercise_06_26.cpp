/*
Write a function that takes the time as three integer arguments (hours, minutes and seconds) and returns the
number of seconds since the last time the clock "struck 12." Use this function to calculate the amount of time
in seconds between two times, both of which are within one 12-hour cycle of the clock.
 */

#include <iostream>
#include <unistd.h>
#include <cmath>

const bool isInteractive = ::isatty(STDIN_FILENO);
const int ONE_HOUR_TO_SECONDS = 3600;
const int ONE_MINUTE_TO_SECONDS = 60;

void inputTime(int& hours, int& minutes, int& seconds);
int  calculateTimeSeconds(const int hours, const int minutes, const int seconds);
int calculateDifference(const int time1, const int time2);
void validateTime(const int hours, const int minutes, const int seconds);

int
main()
{
    int hours1, minutes1, seconds1;
    inputTime(hours1, minutes1, seconds1);
    validateTime(hours1, minutes1, seconds1);
    const int time1 = calculateTimeSeconds(hours1, minutes1, seconds1);

    int hours2, minutes2, seconds2;
    inputTime(hours2, minutes2, seconds2);
    validateTime(hours2, minutes2, seconds2);
    const int time2 = calculateTimeSeconds(hours2, minutes2, seconds2);
    
    std::cout << "Difference of two times is " << calculateDifference(time1, time2) << " seconds" << std::endl;
    return 0;
}

void
inputTime(int& hours, int& minutes, int& seconds)
{
    if (isInteractive) { std::cout << "Input time (HH:MM:SS): "; }
    std::cin >> hours >> minutes >> seconds;
}

int
calculateTimeSeconds(const int hours, const int minutes, const int seconds)
{
    assert(hours >= 0 && hours <= 24);
    assert(minutes >= 0 && minutes <= 60);
    assert(seconds >= 0 && seconds <= 60);
    return seconds + (minutes * ONE_MINUTE_TO_SECONDS) + (hours * ONE_HOUR_TO_SECONDS);
}

int
calculateDifference(const int time1, const int time2)
{
    return std::abs(time1 - time2);
}

void
validateTime(const int hours, const int minutes, const int seconds)
{
    if (hours < 0 || hours > 24) {
        std::cerr << "Error 1: Invalid value for hour [" << hours << "]!" << std::endl;
        ::exit(1);
    }

    if (minutes < 0 || minutes > 60) {
        std::cerr << "Error 2: Invalid value for minutes [" << minutes << "]!" << std::endl;
        ::exit(2);
    }

    if (seconds < 0 || seconds > 60) {
        std::cerr << "Error 3: Invalid value for seconds [" << seconds << "]!" << std::endl;
        ::exit(3);
    }
}


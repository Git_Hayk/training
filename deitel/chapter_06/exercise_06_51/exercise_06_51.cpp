/*
After you determine what the program of Exercise 6.50 does, modify the program to function properly
after removing the restriction that the second argument be nonnegative.
*/

#include <iostream>
#include <unistd.h>

int mystery(const int a, const int b);

int main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Enter two integers: ";
    }
    int x, y;
    std::cin >> x >> y;
    std::cout << "The result is " << mystery(x, y) << std::endl;

    return 0;
}

int
mystery(const int a, const int b)
{
    if (b < 0) {
        return a + mystery(-a, -b + 1);
    }

    if (1 == b) {
        return a;
    }
    return a + mystery(a, b - 1);
}


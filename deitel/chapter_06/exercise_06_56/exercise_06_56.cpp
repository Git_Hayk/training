/*

Write a complete C++ program with the two alternate functions specified below, of which each simply triples
the variable count defined in main. Then compare and contrast the two approaches. These two functions are

1) function tripleByValue that passes a copy of count by value, triples the copy and returns the new value and

1) function tripleByReference that passes count by reference via a reference parameter and triples the original
value of count tHRough its alias (i.e., the reference parameter).

 */

#include <iostream>

int tripleByValue(int count);
int tripleByReference(int& count);

int
main()
{
    int count = 2;

    std::cout << "Triple by value: ";
    for (int value = 0; value < 10; ++value) {
        tripleByValue(count);
        std::cout << count << ", ";
    }
    std::cout << '\n';

    std::cout << "Triple by reference: ";
    for (int reference= 0; reference < 10; ++reference) {
        tripleByReference(count);
        std::cout << count << ", ";
    }
    std::cout << '\n';
    return 0;
}

int
tripleByValue(int count)
{
    count *= 3;
    return count;
}

int
tripleByReference(int& count)
{
    count *= 3;
    return count;
}

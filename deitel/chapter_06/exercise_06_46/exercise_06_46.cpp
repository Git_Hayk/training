/*
Can main be called recursively on your system? Write a program containing a function main.
Include static local variable count and initialize it to 1. Postincrement and print the value of count each
time main is called. Compile your program. What happens?
 */

#include <iostream>

int
main()
{
    static int count = 1;
    std::cout << count << std::endl;
    if (count == 100) 
        return 0;
    ++count;
    return main();
}


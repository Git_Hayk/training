/*
The greatest common divisor (GCD) of two integers is the largest integer that evenly divides each of the numbers.
Write a function gcd that returns the greatest common divisor of two integers.
 */

#include <iostream>
#include <unistd.h>
#include <cmath>

const bool isInteractive = ::isatty(STDIN_FILENO);

int gcd(int number1, int number2);

int
main()
{
    while (true) {
        if (isInteractive) {
            std::cout << "Input two integers to find their greatest common divisor: ";
        }

        int number1, number2;
        std::cin >> number1 >> number2;

        if (0 == number1 || 0 == number2) {
            std::cout << "Exiting..." << std::endl;
            break;
        }

        std::cout << "GCD(" << number1 << ", " << number2 << ") = " << gcd(number1, number2) << std::endl;
    }
    return 0;
}

int
gcd(int number1, int number2)
{
    number1 = std::abs(number1);
    number2 = std::abs(number2);

    while (true) {
        const int remainder = number1 % number2;
        
        if (0 == remainder) {
            return number2;
        }
        number1 = number2;
        number2 = remainder;
    }
    return 0;
}


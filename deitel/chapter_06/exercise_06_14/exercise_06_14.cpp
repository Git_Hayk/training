/*
Function floor can be used to round a number to a specific decimal place. The statement

y = floor( x * 10 + .5 ) / 10;

rounds x to the tenths position (the first position to the right of the decimal point). The statement

y = floor( x * 100 + .5 ) / 100;

rounds x to the hundredths position (the second position to the right of the decimal point).
Write a program that defines four functions to round a number x in various ways:

roundToInteger( number )

roundToTenths( number )

roundToHundredths( number )

roundToThousandths( number )

For each value read, your program should print the original value, the number rounded to the nearest integer,
the number rounded to the nearest tenth, the number rounded to the nearest hundredth and the number rounded to the nearest thousandth.
 */

#include <iostream>
#include <cmath>
#include <iomanip>

int roundToInteger(const float number);
float roundToTenths(const float number);
float roundToHundredths(const float number);
float roundToThousandths(const float number);

int
roundToInteger(const float number)
{
    const int result = static_cast<int>(std::floor(number));
    return result;
}

float
roundToTenths(const float number)
{
    const float result = std::floor(number * 10 + .5) / 10;
    return result;
}
    
float
roundToHundredths(const float number)
{
    const float result = std::floor(number * 100 + .5) / 100;
    return result;
}
    
float
roundToThousandths(const float number)
{
    const float result = std::floor(number * 1000 + .5) / 1000;
    return result;
}

int
main()
{
    std::cout << "Input float number to rount(0 to end)." << std::endl;
    std::cout << std::fixed;
    do {
        float number;
        std::cin >> number;

        if (0 == number) {
            std::cout << "Exiting..." << std::endl;
            break;
        }

        const int toInteger = roundToInteger(number);
        const float toTenths = roundToTenths(number);
        const float toHundredths = roundToHundredths(number);
        const float toThousandths = roundToThousandths(number);

        std::cout << "Integer" << std::setw(10)
                  << "Tenths" << std::setw(18)
                  << "Hundredths" << std::setw(18)
                  << "Thousandths" << std::endl;

        std::cout << std::setw(7) << toInteger << std::setw(10)
                  << std::setprecision(1) << toTenths << std::setw(18)
                  << std::setprecision(2) << toHundredths << std::setw(18)
                  << std::setprecision(3) << toThousandths << std::endl;

    } while (true);

    return 0;
}


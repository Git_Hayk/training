/*
Write program segments that accomplish each of the following:

1) Calculate the integer part of the quotient when integer a is divided by integer b.

2) Calculate the integer remainder when integer a is divided by integer b.

3) Use the program pieces developed in (a) and (b) to write a function that inputs an integer between 1 and
   32767 and prints it as a series of digits, each pair of which is separated by two spaces. For example,
   the integer 4562 should print as follows:

4 5 6 2

 */

#include <iostream>
#include <limits.h>
#include <unistd.h>
#include <string>

const bool isInteractive = ::isatty(STDIN_FILENO);

void printMenu();
void displayMessage(const int command);
int  getInputFromUser();
void validateCommand(const int command);
int  calculateIntegerWholePart(const int number1, const int number2);
int  calculateIntegerRemainder(const int number1, const int number2);
void printDigitSeries(int number);
void executeExitCommand(const int command);
void validateValue1(const int number);
void validateValue2(const int number1, const int number2);

int
main()
{
    while (true) {
        printMenu();
        
        const int command = getInputFromUser();
        validateCommand(command);

        int number1, number2, number;
        switch (command) {
        case 0: executeExitCommand(command);
                break;
        case 1: displayMessage(command);
                number1 = getInputFromUser();
                number2 = getInputFromUser();
                validateValue2(number1, number2);
                std::cout << "Whole part is " << calculateIntegerWholePart(number1, number2);
                break;
        case 2: displayMessage(command);
                number1 = getInputFromUser();
                number2 = getInputFromUser();
                validateValue2(number1, number2);
                std::cout << "Remainder is " << calculateIntegerRemainder(number1, number2);
                break;
        case 3: displayMessage(command);
                number = getInputFromUser();
                validateValue1(number);
                printDigitSeries(number);
                break;
        default: assert(0 && "There are four commands to choose!");
        }
        std::cout << std::endl;
    }
    return 0;
}

void
printMenu()
{
    if (isInteractive) {
        std::cout << "Input command:" 
                  << "\n\t0 - exit"
                  << "\n\t1 - calculate whole part"
                  << "\n\t2 - calculate remainder"
                  << "\n\t3 - print digit series"
                  << "\n> Command: ";
    }
}

void
displayMessage(const int command)
{
    assert(command >= 0 && command <= 3);
    if (isInteractive) {
        switch (command) {
        case 1: std::cout << "Input 2 integer numbers: "; break;
        case 2: std::cout << "Input 2 integer numbers: "; break;
        case 3: std::cout << "Input integer number: ";    break;
        default: assert(0 && "Three aveilable commands!");
        }
    }
}

int
getInputFromUser()
{
    int userInput;
    std::cin >> userInput;
    return userInput;
}

void
validateCommand(const int command)
{
    if (command < 0 || command > 3) {
        std::cout << "Error 1: Invalid Command!" << std::endl;
        ::exit(1);
    }
}

int
calculateIntegerWholePart(const int number1, const int number2)
{
    assert(number2 != 0);
    return number1 / number2;
}

int
calculateIntegerRemainder(const int number1, const int number2)
{
    assert(number2 != 0);
    return number1 % number2;
}

void
printDigitSeries(int number)
{
    assert(number > 0);
    std::cout << "Result: " ;
    std::string str = std::to_string(number);
    for (int i = 0; str[i] != '\0'; ++i) {
        std::cout << str[i] << "  ";
    }
}

void
executeExitCommand(const int command)
{
    if (0 == command) {
        std::cout << "Exiting..." << std::endl;
        ::exit(0);
    }
}

void
validateValue1(const int number)
{
    if (number <= 0) {
        std::cerr << "Error 2: Invalid value (less than or equal zero)!" << std::endl;
        ::exit(2);
    }

    if (number > SHRT_MAX) {
        std::cerr << "Error 3: Limit exceded!" << std::endl;
        ::exit(3);
    }
}

void
validateValue2(const int number1, const int number2)
{
    if (0 == number2) {
        std::cerr << "Error 4: Devision by zero!" << std::endl;
        ::exit(4);
    }

    if (number1 > SHRT_MAX || number1 < SHRT_MIN) {
        std::cerr << "Error 5: First integer limit exceded!" << std::endl;
        ::exit(5);
    }

    if (number2 > SHRT_MAX || number2 < SHRT_MIN) {
        std::cerr << "Error 6: Second integer limit exceded!" << std::endl;
        ::exit(6);
    }
}


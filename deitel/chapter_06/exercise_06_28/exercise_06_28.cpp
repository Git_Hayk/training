/*
Write a program that inputs three double-precision, floating-point numbers and passes them to a function that returns the smallest number.
 */

#include <iostream>
#include <iomanip>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

double getSmallestDouble(const double number1, const double number2, const double number3);

int
main()
{
    std::cout << std::fixed << std::setprecision(2);
    double number1, number2, number3;
    if (isInteractive) {
        std::cout << "Input three double-precision floating-point numbers to get the smallest: ";
    }
    std::cin >> number1 >> number2 >> number3;

    std::cout << "Smallest double number is " << getSmallestDouble(number1, number2, number3) << std::endl;
    return 0;
}

double
getSmallestDouble(const double number1, const double number2, const double number3)
{
    double smallest = number1;

    if (number2 < smallest) {
        smallest = number2;
    }
    
    if (number3 < smallest) {
        smallest = number3;
    }

    return smallest;
}


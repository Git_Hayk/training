/*
Write a C++ program that prompts the user for the radius of a circle then calls inline function circleArea to calculate the area of that circle.
*/

#define _USE_MATH_DEFINES

#include <iostream>
#include <cmath>

double circleArea(const double radius);
void validateRadius(const int radius);

int
main()
{
    std::cout << "Input the circles raius: ";
    double radius;
    std::cin >> radius;
    validateRadius(radius);
    std::cout << "Circle's area with radius " << radius << " is " << circleArea(radius) << std::endl;
    return 0;
}

double
circleArea(const double radius)
{
    assert(radius >= 0);
    return M_PI * radius * radius;
}

void
validateRadius(const int radius)
{
    if (radius < 0) {
        std::cerr << "Error 1: Less than zero!" << std::endl;
        ::exit(1);
    }
}


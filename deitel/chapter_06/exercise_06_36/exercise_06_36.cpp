/*
(Computer Assisted Instruction) The use of computers in education is referred to as computer-assisted instruction (CAI).
One problem that develops in CAI environments is student fatigue. This can be eliminated by varying the computer's dialogue
to hold the student's attention. Modify the program of Exercise 6.35 so the various comments are printed for each correct
answer and each incorrect answer as follows:

Responses to a correct answer

   Very good!
   Excellent!
   Nice work!
   Keep up the good work!

Responses to an incorrect answer

   No. Please try again.
   Wrong. Try once more.
   Don't give up!
   No. Keep trying.

Use the random number generator to choose a number from 1 to 4 to select an appropriate response to each answer.
Use a switch statement to issue the responses.
*/

#include <iostream>
#include <cstdlib>
#include <unistd.h>

const int ITERATION = 20;
const int MESSAGE_COUNT = 4;
const bool isInteractive = ::isatty(STDIN_FILENO);

int  randomInRange(const int lowerLimit, const int upperLimit);
void learnMultiplication(const int number1, const int number2);
int  getInputFromUser();
void printPositiveMessage();
void printNegativeMessage();

int
main()
{
    for (int i = 0; i < ITERATION; ++i) {
        if (isInteractive) {
            std::srand(::time(0));
        }

        const int number1 = randomInRange(0, 10);
        const int number2 = randomInRange(0, 10);

        learnMultiplication(number1, number2);
    }
    return 0;
}

void
learnMultiplication(const int number1, const int number2)
{
    while (true) {
        if (isInteractive) {
            std::cout << "How much is " << number1 << " times " << number2 << "?\n> ";
        }
        
        const int studentAnswer = getInputFromUser();
        if (studentAnswer == number1 * number2) {
            printPositiveMessage();
            break;
        }
        printNegativeMessage();
    }
}

int
randomInRange(const int lowerLimit, const int upperLimit)
{
    return std::rand() % (upperLimit - lowerLimit) + lowerLimit;
}

int
getInputFromUser()
{
    int input;
    std::cin >> input;
    return input;
}

void
printPositiveMessage()
{
    const int generator = randomInRange(0, MESSAGE_COUNT);
    std::cout << "\033[1;32m";
    switch (generator) {
    case 0: std::cout << "Very good!" << std::endl; break;
    case 1: std::cout << "Excellent!" << std::endl; break;
    case 2: std::cout << "Nice work!" << std::endl; break;
    case 3: std::cout << "Keep up the good work!" << std::endl; break;
    default: assert(0 && "There are four types of messages!");
    }
    std::cout << "\033[0m";
}

void
printNegativeMessage()
{
    const int generator = randomInRange(0, MESSAGE_COUNT);
    std::cout << "\033[1;31m";
    switch (generator) {
    case 0: std::cout << "No. Please try again." << std::endl; break;
    case 1: std::cout << "Wrong. Try once more." << std::endl; break;
    case 2: std::cout << "Don't give up!" << std::endl; break;
    case 3: std::cout << "No. Keep trying." << std::endl; break;
    default: assert(0 && "There are four types of messages!");
    }
    std::cout << "\033[0m";
}


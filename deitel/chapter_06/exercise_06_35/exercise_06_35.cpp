/*
(Computers in Education) Computers are playing an increasing role in education. Write a program that
helps an elementary school student learn multiplication. Use rand to produce two positive one-digit integers.
It should then type a question such as

How much is 6 times 7?

The student then types the answer. Your program checks the student's answer. If it is correct, print "Very good!",
then ask another multiplication question. If the answer is wrong, print "No. Please try again.", then let the student
try the same question repeatedly until the student finally gets it right.
*/

#include <iostream>
#include <cstdlib>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

void learnMultiplication(const int number1, const int number2);
int randomInRange(const int lowerLimit, const int upperLimit);

int
main()
{
    for (int i = 0; i < 10; ++i) {
        if (isInteractive) {
            std::srand(::time(0));
        }
        const int number1 = randomInRange(0, 10);
        const int number2 = randomInRange(0, 10);

        learnMultiplication(number1, number2);
    }
    return 0;
}

void
learnMultiplication(const int number1, const int number2)
{
    while (true) {
        std::cout << "How much is " << number1 << " times " << number2 << "?\n> ";
        int studentAnswer;
        std::cin >> studentAnswer;

        if (studentAnswer == number1 * number2) {
            std::cout << "Very good!" << std::endl;
            break;
        }
        std::cout << "No. Please try again." << std::endl;
    }
}

int
randomInRange(const int lowerLimit, const int upperLimit)
{
    return lowerLimit + std::rand() % upperLimit;
}


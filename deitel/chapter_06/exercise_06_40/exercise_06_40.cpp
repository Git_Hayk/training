/*
Write a recursive function power ( base, exponent ) that, when invoked, returns

base exponent

For example, power( 3, 4 ) = 3 * 3 * 3 * 3. Assume that exponent is an integer greater than or equal to 1.
Hint: The recursion step would use the relationship

base exponent = base · base exponent - 1

and the terminating condition occurs when exponent is equal to 1, because

base1 = base
*/

#include <iostream>
#include <unistd.h>
#include <limits.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int integerPower(const int base, const int exponent, int leftedOut = 1);
void validateExponent(const int exponent);
void validateInteger(const int result, const int base);

int
main()
{
    do {
        if (isInteractive) {
            std::cout << "Input base and exponent to calculate power!";
        }

        int base, exponent;
        std::cin >> base >> exponent;
        validateExponent(exponent);
        const int result = integerPower(base, exponent);
        std::cout << base << '^' << exponent << '=' << result << std::endl;
    } while (true);
    return 0;
}

int
integerPower(const int base, const int exponent, int leftedOut)
{
    assert(exponent > 0);
    if (exponent != 1) {
        if (1 == exponent % 2) {
            leftedOut *= base;
        }
        return integerPower(base * base, exponent / 2, leftedOut);
    }
    return leftedOut * base;
}

void
validateExponent(const int exponent)
{
    if (exponent <= 0) {
        std::cout << "Error 1: Invalid exponent!" << std::endl;
        ::exit(1);
    }
}



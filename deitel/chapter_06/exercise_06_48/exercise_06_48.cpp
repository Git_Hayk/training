/*
Write function distance that calculates the distance between two points (x1, y1) and (x2, y2).
All numbers and return values should be of type double.
*/

#include <iostream>
#include <unistd.h>
#include <cmath>

const bool isInteractive = ::isatty(STDIN_FILENO);

double distance(const double point1X, const double point1Y, const double point2X, const double point2Y);

int
main()
{
    if (isInteractive) {
        std::cout << "Input first points coordinates:";
    }
    double x1, y1;
    std::cin >> x1 >> y1;
    if (isInteractive) {
        std::cout << "Input second points coordinates:";
    }
    double x2, y2;
    std::cin >> x2 >> y2;

    std::cout << "Distance of two points is " << distance(x1, y1, x2, y2) << std::endl;
    return 0;
}

double distance(const double point1X, const double point1Y, const double point2X, const double point2Y)
{
    const double side1 = point1X - point2X;
    const double side2 = point1Y - point2Y;
    return std::sqrt(side1 * side1 + side2 * side2);
}


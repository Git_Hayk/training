/*
Write a program that uses a function template called min to determine the smaller of two arguments.
Test the program using integer, character and floating-point number arguments.
 */

#include <iostream>

template <typename T>
T
min(const T& value1, const T& value2)
{
    return (value2 < value1) ? value2 : value1;
}

int
main()
{
    int integerValue1, integerValue2;
    std::cout << "Input two integer values: ";
    std::cin >> integerValue1 >> integerValue2;
    std::cout << min(integerValue1, integerValue2) << std::endl;

    double doubleValue1, doubleValue2;
    std::cout << "Input two double values: ";
    std::cin >> doubleValue1 >> doubleValue2;
    std::cout << min(doubleValue1, doubleValue2) << std::endl;

    char characterValue1, characterValue2;
    std::cout << "Input two characters: ";
    std::cin >> characterValue1 >> characterValue2;
    std::cout << min(characterValue1, characterValue2) << std::endl;
    return 0;
}


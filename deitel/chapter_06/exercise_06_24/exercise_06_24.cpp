/*
Use techniques similar to those developed in Exercise 6.22 and Exercise 6.23 to produce a program that graphs a wide range of shapes.
 */

#include <iostream>
#include <unistd.h>
#include <limits.h>
#include <cmath>

const bool isInteractive = ::isatty(STDIN_FILENO);

void validateCommand(const int command);
void printSquare(const int size, const char character);
void printDiamond(const int size, const char character);
void printRightTriangle(const int diameter, const char character);
void fillCharacter(const char character);
void validateValue(const int size);

int
main()
{
    while (true) {
        if (isInteractive) {
            std::cout << "Input shape to print:"
                      << "\n\t0 - exit"
                      << "\n\t1 - print square"
                      << "\n\t2 - print diamond"
                      << "\n\t3 - print right triangle"
                      << "\n> Command: ";
        }
        int command;
        std::cin >> command;
        validateCommand(command);

        if (0 == command) {
            std::cout << "Exiting..." << std::endl;
            return 0;
        }

        if (isInteractive) {
            std::cout << "Input the size: ";
        }
        int size; 
        std::cin >> size;
        validateValue(size);
        if (isInteractive) {
            std::cout << "Input character to print with: ";
        }
        char symbol;
        std::cin >> symbol;
        fillCharacter(symbol);

        switch (command) {
        case 1: printSquare(size, symbol); break;
        case 2: printDiamond(size, symbol); break;
        case 3: printRightTriangle(size, symbol); break;
        default: assert(command <= 3 && "Three types of shape!");
        }
    }
    return 0;
}

void
validateCommand(const int command)
{
    if (command < 0 || command > 3) {
        std::cerr << "Error 1: Invalid command!" << std::endl;
        ::exit(1);
    }
}

void
printSquare(const int size, const char character)
{
    assert(size > 0);
    assert(std::isprint(character) != 0);
    for (int row = 0; row < size; ++row) {
        for (int column = 0; column < size; ++column) {
            std::cout << character;
        }
        std::cout << '\n';
    }
}

void
printDiamond(const int size, const char character)
{
    assert(size > 0);
    assert(std::isprint(character) != 0);
    const int halfSize = size / 2;
    for (int valueY = halfSize; valueY >= -halfSize; --valueY) {
        for (int valueX = -halfSize; valueX <= halfSize; ++valueX) {
            std::cout << (std::abs(valueX) + std::abs(valueY) <= halfSize ? character : ' ');
        }
        std::cout << '\n';
    }
}

void
printRightTriangle(const int size, const char character)
{
    assert(size > 0);
    assert(std::isprint(character) != 0);
    for (int row = 1; row < size; ++row) {
        for (int column = 1; column < row; ++column) {
            std::cout << character;
        }
        std::cout << '\n';
    }
}

void
fillCharacter(const char character)
{
    if (!std::isprint(character)) {
        std::cout << "Error 2: Non printable character!" << std::endl;
        ::exit(2);
    }
}
        
void
validateValue(const int size)
{
    if (size <= 0) {
        std::cout << "Error 3: Invalid Value!" << std::endl;
        ::exit(3);
    }
}


/*
(Fibonacci Series) The Fibonacci series

0, 1, 1, 2, 3, 5, 8, 13, 21, ...

begins with the terms 0 and 1 and has the property that each succeeding term is the sum of the two preceding terms.
(a) Write a nonrecursive function fibonacci( n ) that calculates the nth Fibonacci number. (b) Determine the largest
int Fibonacci number that can be printed on your system. Modify the program of part (a) to use double instead of int
to calculate and return Fibonacci numbers, and use this modified program to repeat part (b).
*/

#include <iostream>
#include <limits.h>
#include <float.h>
#include <unistd.h>
#include <iomanip>

template <typename T>
T fibonacci(const T& number);

template <typename T>
void validateNumber(const T number);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input a number to print the Fibonacci series up to it: ";
    }

    std::cout << "Fobonacci numbers for type 'char'.               - n" << fibonacci(CHAR_MAX) << std::endl;
    std::cout << "Fobonacci numbers for type 'unsigned char'.      - n" << fibonacci(UCHAR_MAX) << std::endl;
    std::cout << "Fobonacci numbers for type 'short'.              - n" << fibonacci(SHRT_MAX) << std::endl;
    std::cout << "Fobonacci numbers for type 'unsigned short'.     - n" << fibonacci(USHRT_MAX) << std::endl;
    std::cout << "Fobonacci numbers for type 'int'.                - n" << fibonacci(INT_MAX) << std::endl;
    std::cout << "Fobonacci numbers for type 'unsigned int'.       - n" << fibonacci(UINT_MAX) << std::endl;
    std::cout << "Fobonacci numbers for type 'long long'.          - n" << fibonacci(LLONG_MAX) << std::endl;
    std::cout << "Fobonacci numbers for type 'unsigned long long'. - n" << fibonacci(ULLONG_MAX) << std::endl;
    std::cout << "Fobonacci numbers for type 'float'.              - n" << fibonacci(FLT_MAX) << std::endl;
    std::cout << "Fobonacci numbers for type 'double'.             - n" << fibonacci(DBL_MAX) << std::endl;
    std::cout << "Fobonacci numbers for type 'long double'.        - n" << fibonacci(LDBL_MAX) << std::endl;

    return 0;
}

template <typename T>
T
fibonacci(const T& number)
{
    T counter = 1;
    T previousNumber = 0;
    T currentNumber = 1;
    
    while (currentNumber <= number) {
        const T temp = previousNumber;
        previousNumber = currentNumber;
        currentNumber += temp;
        ++counter;
        //std::cout  << previousNumber;
        //std::cout << '\n';

        if (currentNumber < previousNumber) {
            break;
        }
    }
    return counter;
}

template <typename T>
void
validateNumber(const T number)
{
    if (static_cast<int>(number <= 0)) {
        std::cerr << "Error 1: Invalid number (less than 0)!" << std::endl;
        ::exit(1);
    }
}


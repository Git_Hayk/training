/*
Write a function multiple that determines for a pair of integers whether the second is a multiple of the first.
The function should take two integer arguments and return true if the second is a multiple of the first, false
otherwise. Use this function in a program that inputs a series of pairs of integers.
 */

#include <iostream>
#include <unistd.h>

bool multiple(const int number1, const int number2);
void validateValue(const int number);
void checkForExit(const int number1, const int number2);

bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    do {
    int number1, number2;
    std::cout << "Input two integers to check for multiple (0 0 to exit): ";
    std::cin >> number1 >> number2;
    checkForExit(number1, number2);
    validateValue(number1);
    std::cout << "First number " <<  (multiple(number1, number2) ? "is" : "is not") << " a multiple of the second." << std::endl;

    } while (true);

    return 0;
}

bool
multiple(const int number1, const int number2)
{
    assert(number1 != 0);
    return number2 % number1 != 0;
}

void
validateValue(const int number)
{
    if (0 == number) {
        std::cout << "Error 1: Devision by zero!" << std::endl;
        ::exit(1);
    }
}

void
checkForExit(const int number1, const int number2)
{
    if (0 == number1 && 0 == number2) {
        std::cout << "Exiting..." << std::endl;
        ::exit(0);
    }
}

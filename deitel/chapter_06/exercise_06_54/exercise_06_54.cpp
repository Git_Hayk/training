/*
Modify the craps program of Fig. 6.11 to allow wagering. Package as a function the portion of the program that runs
one game of craps. Initialize variable bankBalance to 1000 dollars. Prompt the player to enter a wager. Use a while
loop to check that wager is less than or equal to bankBalance and, if not, prompt the user to reenter wager until a
valid wager is entered. After a correct wager is entered, run one game of craps. If the player wins, increase bankBalance
by wager and print the new bankBalance. If the player loses, decrease bankBalance by wager, print the new bankBalance,
check on whether bankBalance has become zero and, if so, print the message "Sorry. You busted!" As the game progresses,
print various messages to create some "chatter" such as "Oh, you're going for broke, huh?", "Aw cmon, take a chance!" or
"You're up big. Now's the time to cash in your chips!".
*/

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <ctime>

enum Status { CONTINUE, WON, LOST };

int rollDice();
int runCraps();
int setBalance(const int wager = 0, const int gameStatus = WON);
int getWager(const int bankBalance);
void randomMessage();

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }

    int bankBalance = setBalance();
    std::cout << "Your initial balance is " << bankBalance << std::endl;
    while (true) {
        const int wager = getWager(bankBalance);

        const int gameStatus = runCraps();
        assert(gameStatus == WON || gameStatus == LOST);

        bankBalance = setBalance(wager, gameStatus);
        std::cout << "New balance is " << bankBalance << std::endl;
        randomMessage();
    }
    return 0;
}

int
rollDice()
{
    int die1 = 1 + std::rand() % 6; // first die roll
    int die2 = 1 + std::rand() % 6; // second die roll

    int sum = die1 + die2;

    std::cout << "Player rolled " << die1 << " + " << die2 << " = " << sum << std::endl;
    return sum;
}

int
runCraps()
{
    int myPoint;
    Status gameStatus;
    int sumOfDice = rollDice(); /// first roll of the dice

    /// determine game status and point (if needed) based on first roll
    switch (sumOfDice) {
    case 7:
    case 11: gameStatus = WON; break;
    case 2:
    case 3:
    case 12: gameStatus = LOST; break;
    default: gameStatus = CONTINUE;
             myPoint = sumOfDice;
             std::cout << "Point is " << myPoint << std::endl;
    }

    /// while game is not complete
    while (gameStatus == CONTINUE) { /// Not WON or LOST
        sumOfDice = rollDice(); /// roll dice again

        // determine game status
        if (sumOfDice == myPoint) { /// win by making point
            gameStatus = WON;
        } else if (sumOfDice == 7) { /// lose by rolling 7 before point
            gameStatus = LOST;
        }
    }

     /// display won or lost message
    if (gameStatus == WON) {
        std::cout << "Player wins" << std::endl;
    } else {
        std::cout << "Player loses" << std::endl;
    }
    
    return gameStatus;
}

int
getWager(const int bankBalance)
{
    int wager;
    while (true) {
        std::cout << "Please input your wager: ";
        std::cin >> wager;

        if (0 == wager) {
            std::cout << "Your final balance is " << bankBalance << ": Congratulations." << std::endl;
            ::exit(0);
        }
        if (wager > bankBalance) {
            std::cout << "Your wager exceeds the balance! Input again." << std::endl;
        } else if (wager < 0) {
            std::cout << "Wager can not be less than zero! Input again." << std::endl;
        } else {
            break;
        }
    }
    return wager;
}

int
setBalance(const int wager, const int gameStatus)
{
    static int bankBalance = 1000;
    if (WON == gameStatus) {
        bankBalance += wager;
    } else {
        bankBalance -= wager;
    }

    if (bankBalance == 0) {
        std::cout << "Sorry. You busted!" << std::endl;
        ::exit(0);
    }
    return bankBalance;
}

void
randomMessage()
{
    const int message = 1 + std::rand() % 3;
    switch (message) {
    case 1: std::cout << "Oh, you're going for broke, huh?" << std::endl; break;
    case 2: std::cout << "Aw cmon, take a chance!" << std::endl; break;
    case 3: std::cout << "You're up big. Now's the time to cash in your chips!" << std::endl; break;
    default: assert(message == 0);
    }
}


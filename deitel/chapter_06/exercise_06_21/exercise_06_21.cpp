/*
Write a program that inputs a series of integers and passes them one at a time to function even, which
uses the modulus operator to determine whether an integer is even. The function should take an integer
argument and return true if the integer is even and false otherwise.
*/

#include <iostream>
#include <unistd.h>

bool even(const int number);
void checkForExit(const int number);

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    do {
        if (isInteractive) {
            std::cout << "Input number to check it for even(0 to exit): ";
        }
        int number;
        std::cin >> number;

        checkForExit(number);
        if (even(number)) {
            std::cout << "Number " << number << " is even." << std::endl;
            continue;
        }
        std::cout << "Number " << number << " is odd." << std::endl;

    } while (true);

    return 0;
}

bool
even(const int number)
{
    return number % 2 == 0;
}

void
checkForExit(const int number)
{
    if (0 == number) {
        std::cout << "Exiting..." << std::endl;
        ::exit(0);
    }
}


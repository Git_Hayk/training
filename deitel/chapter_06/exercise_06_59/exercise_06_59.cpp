/*
Write a program that uses a function template called max to determine the largest of three arguments.
Test the program using integer, character and floating-point number arguments.
*/

#include <iostream>

template <typename T>
T
max(const T& value1, const T& value2, const T& value3)
{
    T biggestValue = value1;

    if (value2 > biggestValue) {
        biggestValue = value2;
    }

    return (value3 > biggestValue) ? value3 : biggestValue;
}

int
main()
{
    std::cout << "Input three integer values: ";
    int a, b, c;
    std::cin >> a >> b >> c;
    std::cout << max(a, b, c) << std::endl;

    std::cout << "Input three double values: ";
    double i, j, k;
    std::cin >> i >> j >> k;
    std::cout << max(i, j, k) << std::endl;

    std::cout << "Input three character values: ";
    char x, y, z;
    std::cin >> x >> y >> z;
    std::cout << max(x, y, z) << std::endl;

    return 0;
}


/*
Write a program that simulates coin tossing. For each toss of the coin, the program should print Heads or Tails.
Let the program toss the coin 100 times and count the number of times each side of the coin appears. Print the results.
The program should call a separate function flip that takes no arguments and returns 0 for tails and 1 for heads.
[Note: If the program realistically simulates the coin tossing, then each side of the coin should appear approximately
half the time.]
*/

#include <iostream>
#include <cstdlib>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

bool flip();

int
main()
{
    if (isInteractive) {
        std::srand(::time(0));
    }

    int totalHeads = 0, totalTails = 0;
    for (int i = 1; i <= 100; ++i) {
        if (flip()) {
            std::cout << i << " - " << "Tails" << std::endl;
            ++totalTails;
        } else { 
            std::cout << i << " - " << "Heads" << std::endl;
            ++totalHeads;
        }
    }
    std::cout << "Total heads: " << totalHeads << std::endl;
    std::cout << "Total tails: " << totalTails << std::endl;
    return 0;
}

bool
flip()
{
    return 1 == std::rand() % 2;
}


/*
Write a function that displays at the left margin of the screen a solid square of asterisks whose side is
specified in integer parameter side. For example, if side is 4, the function displays the following:

****
****
****
****

 */

#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

void printSquare(const int side);
void checkForExit(const int side);
void validateValue(const int side);

int
main()
{
    while (true) {
        if (isInteractive) {
            std::cout << "Input the side size to print square(0 to exit): ";
        }
        int side;
        std::cin >> side;
        std::cout << std::endl;

        checkForExit(side);
        validateValue(side);
        printSquare(side);
    }
    return 0;
}

void
printSquare(const int side)
{
    assert(side > 0);
    for (int row = 0; row < side; ++row) {
        for (int column = 0; column < side; ++column) {
            std::cout << '*';
        }
        std::cout << '\n';
    }
}

void
checkForExit(const int side)
{
    if (0 == side) {
        std::cout << "Exiting..." << std::endl;
        ::exit(0);
    }
}

void
validateValue(const int side)
{
    if (side < 0) {
        std::cout << "Error 1: Invalid Value!" << std::endl;
        ::exit(1);
    }
}


/*
(Guess the Number Game) Write a program that plays the game of "guess the number" as follows:
Your program chooses the number to be guessed by selecting an integer at random in the range 1 to 1000.
The program then displays the following:

I have a number between 1 and 1000.
Can you guess my number?
Please type your first guess.



The player then types a first guess. The program responds with one of the following:

1.  Excellent! You guessed the number!
    Would you like to play again (y or n)?
2.  Too low. Try again.
3.  Too high. Try again.



[Page 321]
If the player's guess is incorrect, your program should loop until the player finally gets the number right.
Your program should keep telling the player Too high or Too low to help the player "zero in" on the correct answer.
*/

#include <iostream>
#include <cstdlib>
#include <unistd.h>

const int LOWER_LIMIT = 1;
const int UPPER_LIMIT = 1000;
const bool isInteractive = ::isatty(STDIN_FILENO);

int randomizer();
void displayMessage();
int checkUserAnswer(const int computerNumber);
char gameStatus();

int
main()
{
    if (isInteractive) {
        std::srand(::time(0));
    }
    do {
        const int computerNumber = randomizer();
        displayMessage();
        checkUserAnswer(computerNumber);

    } while (gameStatus() != 'n');
    return 0;
}

void
displayMessage()
{
    if (isInteractive) {
        std::cout << "I have a number between 1 and 1000."
                  << "\nCan you guess my number?"
                  << "\nPlease type your first gues (0 to exit). ";
    }
}

int
randomizer()
{
    const int number = LOWER_LIMIT + std::rand() % UPPER_LIMIT;
    return number;
}

int
checkUserAnswer(const int computerNumber)
{
    while (true) {
        int userNumber;
        std::cin >> userNumber;

        if (userNumber == computerNumber) {
            std::cout << "Excellent! You guessed the number!" << std::endl;
            return 0;
        }
        if (0 == userNumber) {
            std::cout << "Exiting..." << std::endl;
            ::exit(0);
        }
        if (userNumber < LOWER_LIMIT || userNumber > UPPER_LIMIT) {
            std::cout << "Out of range. Tray again. ";
        } else if (userNumber < computerNumber) {
            std::cout << "Too low. Try again. ";
        } else if (userNumber > computerNumber) {
            std::cout << "Too high. Try again. ";
        }
    }
    return 0;
}

char
gameStatus()
{
    std::cout << "Would you like to play again (y or n)? ";
    while (true) { 
        char answer;
        std::cin >> answer;
        if (answer == 'y' || answer == 'n') {
            return answer;
        }
        std::cout << "Warning 1: Wrong letter (y or n)! ";
    }
    return 0;
}


/*
Write a function integerPower ( base, exponent ) that returns the value of

base exponent

For example, integerPower( 3, 4 ) = 3 * 3 * 3 * 3. Assume that exponent is a positive, nonzero
integer and that base is an integer. The function integerPower should use for or while to control
the calculation. Do not use any math library functions.
*/

#include <iostream>
#include <unistd.h>
#include <limits.h>

int integerPower(int base, int exponent);
void validateExponent(const int exponent);

int
integerPower(int base,int exponent)
{
    assert(exponent > 0);
    int  leftedOut = 1;
    while (exponent != 1) {
        if (1 == exponent % 2) {
            leftedOut *= base;
        }
        exponent /= 2;
        base *= base;
    }

    return leftedOut * base;
}

void
validateExponent(const int exponent)
{
    if (exponent <= 0) {
        std::cout << "Error 1: Invalid exponent!" << std::endl;
        ::exit(1);
    }
}

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    do {
        if (isInteractive) {
            std::cout << "Input base and exponent to calculate power!";
        }

        int base, exponent;
        std::cin >> base >> exponent;
        validateExponent(exponent);
        const int result = integerPower(base, exponent);
        std::cout << base << '^' << exponent << '=' << result << std::endl;
    } while (true);
    return 0;
}


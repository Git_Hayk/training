/*
Determine whether the following program segments contain errors. For each error, explain how it can be corrected.
[Note: For a particular program segment, it is possible that no errors are present in the segment.]
 */

template <typename A>

int sum(int num1, int num2, int num3)
{
   return num1 + num2 + num3;
}
/* (candidate template ignored: couldn't infer template argument 'A'), this error can be fixed by deleting the third line,
   or by changing function return type and the types of three arguments from 'int' to 'A':*/



void printResults(int x, int y)
{
     cout << "The sum is " << x + y << '\n';
     return x + y;
}
/* void function can not return another types. It can be fixed by changing function's return type from 'void' to 'int', or we can just use
   return without using (x + y) expression.*/

template <A>
A product( A num1, A num2, A num3 )
{
   return num1 * num2 * num3;
}
/* Here is missing a keyword "typename' or 'class' before type 'A' (template <typename A>).*/


double cube(int);
int cube(int);
/* No two functions with the same name can exist, even if they have different return types*/

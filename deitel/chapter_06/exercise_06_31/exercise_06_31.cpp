/*
(Reverse Digits) Write a function that takes an integer value and returns the number with its digits reversed.
For example, given the number 7631, the function should return 1367.
 */

#include <iostream>
#include <unistd.h>

void printReverseNumber(int number);

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    while (true) {
        if (isInteractive) {
            std::cout << "Input number above zero to print it's reversed (0 to exit): ";
        }
        int number;
        std::cin >> number;

        if (0 == number) {
            std::cout << "Exiting..." << std::endl;
            break;
        }
        if (number < 0) {
            std::cout << "Error 1: Invalid value (less than 0)!" << std::endl;
            return 1;
        }

        printReverseNumber(number);
    }

    return 0;
}

void
printReverseNumber(int number)
{
    while (number != 0) {
        const int digit = number % 10;
        std::cout << digit;
        number /= 10;
    }
    std::cout << std::endl;
}


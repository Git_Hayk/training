/*
Modify the program of Exercise 6.38 to count the number of guesses the player makes. If the number is 10 or fewer,
print "Either you know the secret or you got lucky!" If the player guesses the number in 10 tries, then print "Ahah!
You know the secret!" If the player makes more than 10 guesses, then print "You should be able to do better!" Why
should it take no more than 10 guesses? Well, with each "good guess" the player should be able to eliminate half of
the numbers. Now show why any number from 1 to 1000 can be guessed in 10 or fewer tries.
*/

#include <iostream>
#include <cstdlib>
#include <unistd.h>

enum Status { NONE, LOW, HIGH };

const int LOWER_LIMIT = 1;
const int GUESS_LIMIT = 10;
const int UPPER_LIMIT = 1000;
const bool isInteractive = ::isatty(STDIN_FILENO);

int randomizer();
int getInput();
int checkAnswer(const int computerNumber, const int guessNumber, int& trial, Status& numberStatus);
void displayMessage();
void printGuessMenu(const int trial, const int finder);
char gameStatus();
int findNumber(const int trial, const int numberStatus);

int
main()
{
    if (isInteractive) {
        std::srand(::time(0));
    }

    do {
        const int computerNumber = randomizer();
        displayMessage();
        int finder = getInput();
        int trial = 1;
        int guessNumber;
        Status numberStatus = NONE;

        do {
            if (0 == finder) {
                guessNumber = findNumber(trial, numberStatus);
            } else if (1 == trial) {
                guessNumber = finder;
            } else {
                guessNumber = getInput();
            }
            
            checkAnswer(computerNumber, guessNumber, trial, numberStatus);
        } while (computerNumber != guessNumber);

        printGuessMenu(trial, finder);
    } while (gameStatus() != 'n');
    return 0;
}

void
displayMessage()
{
    if (isInteractive) {
        std::cout << "I have a number between 1 and 1000."
                  << "\nCan you guess my number?"
                  << "\nPlease type your first gues (-1 to exit) or "
                  << "\ninput 0 to guess number by computer: ";
    }
}

int
randomizer()
{
    int number = LOWER_LIMIT + std::rand() % UPPER_LIMIT;
    return number;
}

int
getInput()
{
    int number;
    std::cin >> number;
    return number;
}

int
checkAnswer(const int computerNumber , const int guessNumber, int& trial, Status& numberStatus)
{
    if (guessNumber == computerNumber) {
        std::cout << "Excellent! You guessed the number!" << std::endl;
        return 0;
    }
    if (-1 == guessNumber) {
        std::cout << "Exiting..." << std::endl;
        ::exit(0);
    }
    if (guessNumber < LOWER_LIMIT || guessNumber > UPPER_LIMIT) {
        std::cout << "Out of range. Tray again. ";
    } else if (guessNumber < computerNumber) {
        ++trial;
        std::cout << "Too low. Try again. ";
        numberStatus = LOW;
    } else if (guessNumber > computerNumber) {
        ++trial;
        std::cout << "Too high. Try again. ";
        numberStatus = HIGH;
    }
    return 0;
}

void
printGuessMenu(const int trial, const int finder)
{
    if (trial < GUESS_LIMIT) {
       std::cout << "Either you know the secret or you got lucky!";
    } else if ( trial == GUESS_LIMIT) {
       std::cout << "Ahah! You know the secret!";
    } else if ( trial > GUESS_LIMIT) {
        if (0 == finder) {              /// if was chosen guess by computer, program should not get here!
            assert(0);
        }
        std::cout << "You should be able to do better!";
    }
    std::cout << '\n';
}

char
gameStatus()
{
    std::cout << "Would you like to play again (y or n)? ";
    while (true) { 
        char answer;
        std::cin >> answer;
        if (answer == 'y' || answer == 'n') {
            return answer;
        }
        std::cout << "Warning 1: Wrong letter (y or n)! ";
    }
    assert(0);
    return 0;
}


int
findNumber(const int trial, const int numberStatus)  /// function that find's number created bt function randomizer();
{
    static int guessNumber;
    static int upperLimit;
    static int lowerLimit;

    if (1 == trial) {
        guessNumber = UPPER_LIMIT / 2;
        upperLimit = UPPER_LIMIT;
        lowerLimit = LOWER_LIMIT;
        std::cout << "Trial NO " << trial << ") " << guessNumber << std::endl;
        return guessNumber;
    }
    
    assert(LOW == numberStatus || HIGH == numberStatus);
    if (LOW == numberStatus) {
        lowerLimit = guessNumber;
        guessNumber = (upperLimit + lowerLimit) / 2;
        std::cout << "Trial NO " << trial << ") " << guessNumber << std::endl;
        return guessNumber;
    }

    if (HIGH == numberStatus) {
        upperLimit = guessNumber;
        guessNumber = (upperLimit + lowerLimit) / 2;
        std::cout << "Trial NO " << trial << ") " << guessNumber << std::endl;
        return guessNumber;
    }
    return 0;
}


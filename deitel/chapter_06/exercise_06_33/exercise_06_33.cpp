/*
Write a function qualityPoints that inputs a student's average and returns 4 if a student's average is 90100,
3 if the average is 8089, 2 if the average is 7079, 1 if the average is 6069 and 0 if the average is lower than 60.
*/

#include <iostream>
#include <unistd.h>
#include <cmath>

const bool isInteractive = ::isatty(STDIN_FILENO);

int qualityPoints(const double average);
void checkForExit(const double average);
void validateValue(const double average);

int
main()
{
    while (true) {
        if (isInteractive) {
            std::cout << "Input students avreage to display quality point: ";
        }
        double average;
        std::cin >> average;

        checkForExit(average);
        validateValue(average);
        std::cout << "Student's quality point is " << qualityPoints(average) << std::endl;
    }
    return 0;
}

int qualityPoints(const double average)
{
    assert(average != -1);
    assert(average >= 0 && average <= 100);

    if (100 == average) {
        return 4;
    }
    if (average < 50) {
        return 0;
    }
    return static_cast<int>(std::floor(average / 10.0)) % 5;
}

void
checkForExit(const double average)
{
    if (-1 == average) {
        std::cout << "Exiting..." << std::endl;
        ::exit(0);
    }
}

void
validateValue(const double average)
{
    if (average < 0 || average > 100) {
        std::cerr << "Error 1: Invalid value for average!" << std::endl;
        ::exit(1);
    }
}


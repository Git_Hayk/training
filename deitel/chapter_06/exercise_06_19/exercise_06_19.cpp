/*
(Hypotenuse) Define a function hypotenuse that calculates the length of the hypotenuse of a right triangle
when the other two sides are given. Use this function in a program to determine the length of the hypotenuse
for each of the triangles shown below. The function should take two double arguments and return the hypotenuse
as a double.
*/

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cmath>

double calculateHypotenuse(const double side1, const double side2);
int validateSides(const double side1, const double side2);
void checkForExit(const double side1, const double side2);

double
calculateHypotenuse(const double side1, const double side2)
{
    const double hypotenuse = std::sqrt(side1 * side1 + side2 * side2);
    return hypotenuse;
}

int
validateSides(const double side1, const double side2)
{
    if (side1 < 0 || side2 < 0) {
        std::cout << "Invalid Value!" << std::endl;
        ::exit(1);
    }
    return 0;
}

void
checkForExit(const double side1, const double side2)
{
    if (0 == side1 || 0 == side2) {
            std::cout << "Exiting..." << std::endl;
            ::exit(0);
    }
}


    
const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    std::cout << std::fixed << std::setprecision(1);

    int triangle = 1;
    while (true) {
        std::cout << "Triangle" << std::setw(7)
                  << "Side1" << std::setw(7)
                  << "Side2" << std::setw(15)
                  << "Hypotenuse" << std::endl;

        double side1, side2;
        if (isInteractive) {
            std::cout << "Input two sides of a right triangle(0 to exit): ";
        }
        std::cin >> side1 >> side2;
        validateSides(side1, side2);
        checkForExit(side1, side2);

        const double hypotenuse = calculateHypotenuse(side1, side2);
        std::cout << triangle << std::setw(12)
                  << side1 << std::setw(8)
                  << side2 << std::setw(10)
                  << hypotenuse << std::endl;
        ++triangle;
    }
    return 0;
}


/*
(Perfect Numbers) An integer is said to be a perfect number if the sum of its factors, including 1 (but not the number itself),
is equal to the number. For example, 6 is a perfect number, because 6 = 1 + 2 + 3. Write a function perfect that determines
whether parameter number is a perfect number. Use this function in a program that determines and prints all the perfect numbers
between 1 and 1000. Print the factors of each perfect number to confirm that the number is indeed perfect. Challenge the power
of your computer by testing numbers much larger than 1000.
 */

#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

void validateNumber(const int number);
bool perfect(const int number);

int
main()
{
    if (isInteractive) {
        std::cout << "Input an integer number to check it for perfect number: ";
    }
    int number;
    std::cin >> number;
    validateNumber(number);

    if (perfect(number)) {
        std::cout << "(Perfect number.)" << std::endl;
        return 0;
    }
    std::cout << "(Not perfect number.)" << std::endl;
    return 0;
}

void
validateNumber(const int number)
{
    if (number <= 0) {
        std::cout << "Error 1: Invalid number (less or equal to 0)!" << std::endl;
        ::exit(1);
    }
}

bool
perfect(const int number)
{
    assert(number > 0);
    std::cout << "Factors: ";
    int total = 0;
    for (int factor = 1; factor <= number / 2; ++factor) {
        if (number % factor == 0) {
            std::cout << factor << ", ";
            total += factor;
        }
    }
    std::cout << '\n';
    return total == number;
}


/*
 (Visualizing Recursion) It is interesting to watch recursion "in action." Modify the factorial function of Fig. 6.29
 to print its local variable and recursive call parameter. For each recursive call, display the outputs on a separate
 line and add a level of indentation. Do your utmost to make the outputs clear, interesting and meaningful. Your goal
 here is to design and implement an output format that helps a person understand recursion better. You may want to add
 such display capabilities to the many other recursion examples and exercises throughout the text.
 */

#include <iostream>
#include <iomanip>

unsigned factorial(unsigned number);
int visualizeRecursion(unsigned number, const int value, int width = 50);

int
main()
{
    for (int counter = 0; counter <= 10; counter++) {
        const int value = counter;
        visualizeRecursion(counter, value);
        std::cout << counter << "!= " << factorial(counter) << ".\n" << std::endl;
    }
    return 0;
}

unsigned
factorial(unsigned number)
{
    if (number <= 1) {
        return 1;
    }
    return number * factorial(number - 1);
}

int
visualizeRecursion(unsigned number, const int value, int width)
{
    std::cout << std::setw(width);
    int stackNumber = value - number + 1;
    std::cout << "Opening new stack for factorial function...\n"
              << std::setw(width - 29) << "Stack NO: " << stackNumber << ":\n" 
              << std::setw(width - 3) << "Calculating the factorial of number " << number << "..." <<  std::endl;
    if (number <= 1) {
        std::cout << "Returning result..." << std::endl;
        return 1;
    }
    return visualizeRecursion(number - 1, value, width + 5);
}


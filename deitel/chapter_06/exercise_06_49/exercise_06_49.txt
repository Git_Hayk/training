What is wrong with the following program?

#include <iostream>
using std::cin;
using std::cout;

int main()
{
    int c;

    if ( ( c = cin.get() ) != EOF )
    {
        main();
        cout << c;
    }

    return 0;
} 

It is technically possible to call a main function recursively, 
but according to the C++ standard, it cannot be called within a program.

3.6.1 Main function.
The function main shall not be used within a program. The linkage (3.5) of main is implementation-defined.
A program that defines main as deleted or that declares main to be inline, static, or constexpr is illformed.
The name main is not otherwise reserved. [ Example: member functions, classes, and enumerations
can be called main, as can entities in other namespaces. — end example ]


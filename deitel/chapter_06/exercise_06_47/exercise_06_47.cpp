/*
Exercises 6.356.37 developed a computer-assisted instruction program to teach an elementary school student
multiplication. This exercise suggests enhancements to that program.

Modify the program to allow the user to enter a grade-level capability. A grade level of 1 means to use only
single-digit numbers in the problems, a grade level of 2 means to use numbers as large as two digits, etc.

Modify the program to allow the user to pick the type of arithmetic problems he or she wishes to study. An
option of 1 means addition problems only, 2 means subtraction problems only, 3 means multiplication problems
only, 4 means division problems only and 5 means a random mix of problems of all these types.
 */

#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <cmath>

const int TEST_QUANTITY = 20;
const int PASSING_GRADE = 75;
const int MESSAGE_COUNT = 4;
const bool isInteractive = ::isatty(STDIN_FILENO);

void displayMessage1();
void validateLevel(const int level);
void displayMessage2();
void validateOperation(const int operation);
void displayExpression(const int& operation, const int& number1, const int& number2);

int  randomInRange(const int lowerLimit, const int upperLimit);
void learnMultiplication(const int number1, const int number2, const int operation, int& index, int& rightAnswer);
int  getInputFromUser();
void printPositiveMessage();
void printNegativeMessage();
void calculateAverage(const int rightAnswer);

int add(const int number1, const int number2);
int substract(const int number1, const int number2);
int multiply(const int number1, const int number2); 
int divide(const int number1, const int number2);

int
main()
{
    if (isInteractive) {
        std::srand(::time(0));
    }
    displayMessage1();
    const int level = getInputFromUser();
    validateLevel(level);

    displayMessage2();
    const int input = getInputFromUser();
    validateOperation(input);

    int rightAnswer = 0;
    for (int i = 0 ; i < TEST_QUANTITY; ++i) {

        int number1 = randomInRange(0, std::pow(10, level));
        int number2 = randomInRange(0, std::pow(10, level));

        int operation = input;
        if (5 == input) {
            operation = randomInRange(1, 4);
        }

        if (0 == number2 && 4 == operation) { /// if divided to 0;
            --i;
            continue;
        }

        learnMultiplication(number1 , number2, operation, i, rightAnswer);
    }
    calculateAverage(rightAnswer);
    
    return 0;
}

void
displayMessage1()
{
    if (isInteractive) {
        std::cout << "Input grade level:"
                  << "\n\t1 - use single-digit numbers"
                  << "\n\t2 - use two-digit numbers"
                  << "\n> Level: ";
    }
}

void
displayMessage2()
{
    if (isInteractive) {
        std::cout << "Pick arithmetic problem: "
                  << "\n\t1 - addition"
                  << "\n\t2 - substraction"
                  << "\n\t3 - multiplication"
                  << "\n\t4 - division"
                  << "\n\t5 - random mix"
                  << "\n> Level: ";
    }
}

void
validateLevel(const int level)
{
    if (level < 1 || level > 2) {
        std::cerr << "Error 1: Invalid level!" << std::endl;
        ::exit(1);
    }
}
void
validateOperation(const int operation)
{
    if (operation < 1 || operation > 5) {
        std::cerr << "Error 2: Invalid operation!" << std::endl;
        ::exit(2);
    }
}

void
displayExpression(const int& operation, const int& number1, const int& number2)
{
    if (isInteractive) {
        std::cout << "How much is " << number1;
        switch (operation) {
        case 1: std::cout << " + "; break;
        case 2: std::cout << " - "; break;
        case 3: std::cout << " * "; break;
        case 4: std::cout << " / "; break;
        default: assert(operation >= 1 && operation <= 4);
        }
        std::cout << number2 << '?' << std::endl;
    }
}

void
learnMultiplication(const int number1, const int number2, const int operation, int& index, int& rightAnswer)
{
    while (true) {
        displayExpression(operation, number1, number2);
        

        int result = 0;
        switch (operation) {
        case 1: result = add(number1, number2); break;
        case 2: result = substract(number1, number2); break;
        case 3: result = multiply(number1, number2); break;
        case 4: result = divide(number1, number2); break;
        default: assert(operation >= 1 && operation <= 4);
        }

        const int studentAnswer = getInputFromUser();
        
        if (studentAnswer == result) {
            ++rightAnswer;
            printPositiveMessage();
            return;
        }
        ++index;
        if (index == TEST_QUANTITY) {
            return;
        }
        printNegativeMessage();
    }
}

int
randomInRange(const int lowerLimit, const int upperLimit)
{
    return std::rand() % (upperLimit - lowerLimit) + lowerLimit;
}

int
getInputFromUser()
{
    int input;
    std::cin >> input;
    return input;
}

void
printPositiveMessage()
{
    const int generator = randomInRange(0, MESSAGE_COUNT);
    switch (generator) {
    case 0: std::cout << "Very good!"; break;
    case 1: std::cout << "Excellent!"; break;
    case 2: std::cout << "Nice work!"; break;
    case 3: std::cout << "Keep up the good work!"; break;
    default: assert(0 && "There are four types of comments!");
    }
    std::cout << std::endl;
}

void
printNegativeMessage()
{
    const int generator = randomInRange(0, MESSAGE_COUNT);
    switch (generator) {
    case 0: std::cout << "No. Please try again."; break;
    case 1: std::cout << "Wrong. Try once more."; break;
    case 2: std::cout << "Don't give up!"; break;
    case 3: std::cout << "No. Keep trying."; break;
    default: assert(0 && "There are four types of comments!");
    }
    std::cout << std::endl;
}

void
calculateAverage(const int rightAnswer)
{
    const double average = static_cast<double>(rightAnswer) / static_cast<double>(TEST_QUANTITY) * 100;
    std::cout << "Your average is " << average << '%' << std::endl;
    if (average < PASSING_GRADE) {
        std::cout << "Please ask your instructor for extra help!" << std::endl;
    }
}

int
add(const int number1, const int number2)
{
    return number1 + number2;
}

int
substract(const int number1, const int number2)
{
    return number1 - number2;
}

int
multiply(const int number1, const int number2)
{
    return number1 * number2;
}

int
divide(const int number1, const int number2)
{
    assert(number2 != 0);
    return number1 / number2;
}


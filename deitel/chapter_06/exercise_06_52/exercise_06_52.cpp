/*
Write a program that tests as many of the math library functions in Fig. 6.2 as you can. Exercise each of
these functions by having your program print out tables of return values for a diversity of argument values.
*/

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cstdlib>
#include <cmath>

void test(const double value1, const int index, const double value2 = 0);

int
main()
{
    std::cout << std::fixed << std::setprecision(1);
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }

    for (int i = 0; i < 60; ++i) {
        const double number1 = static_cast<double>(-100 + std::rand() % 201) / 10.0;
        if (i >= 50 && i <= 59) {
            const double number2 = static_cast<double>(-100 + std::rand() % 201) / 10.0;
            test(number1, i, number2);
            continue;
        }
        test(number1, i);
    }

    return 0;
}

void
test(const double value1, const int index, const double value2)
{
    assert(value1 >= -10.0 && value1 <= 10.0);
    assert(value2 >= -10.0 && value2 <= 10.0);
    std::cout << "When x = " << value1;
    switch (index / 5) {
    case 0:  std::cout << ", std::ceil(x) = "  << std::ceil(value1)  << std::endl; break;
    case 1:  std::cout << ", std::cos(x) = "   << std::cos(value1)   << std::endl; break;
    case 2:  std::cout << ", std::exp(x) = "   << std::exp(value1)   << std::endl; break;
    case 3:  std::cout << ", std::fabs(x) = "  << std::fabs(value1)  << std::endl; break;
    case 4:  std::cout << ", std::floor(x) = " << std::floor(value1) << std::endl; break;
    case 5:  std::cout << ", std::log(x) = "   << std::log(value1)   << std::endl; break;
    case 6:  std::cout << ", std::log10(x) = " << std::log10(value1) << std::endl; break;
    case 7:  std::cout << ", std::sin(x) = "   << std::sin(value1)   << std::endl; break;
    case 8:  std::cout << ", std::sqrt(x) = "  << std::sqrt(value1)  << std::endl; break;
    case 9:  std::cout << ", std::tan(x) = "   << std::tan(value1)   << std::endl; break;
    case 10: std::cout << " and y = " << value2 << ", std::fmod(x, y) = " << std::fmod(value1, value2) << std::endl; break;
    case 11: std::cout << " and y = " << value2 << ", std::pow(x, y) = " << std::pow(value1, value2) << std::endl; break;
    default: assert(0 && "Used 12 functions!");
    }
}


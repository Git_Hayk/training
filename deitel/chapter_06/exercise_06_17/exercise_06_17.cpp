/*
For each of the following sets of integers, write a single statement that prints a number at random from the set:

2, 4, 6, 8, 10.

3, 5, 7, 9, 11.

6, 10, 14, 18, 22.
 */

#include <iostream>
#include <cstdlib>
#include <unistd.h>

void printRandomFromSetA();
void printRandomFromSetB();
void printRandomFromSetC();

void
printRandomFromSetA()
{
    for (int i = 0; i < 20; ++i) {
        const int number = (std::rand() % 5) * 2 + 2;
        std::cout << number << ", ";
    }
    std::cout << std::endl;
}

void
printRandomFromSetB()
{
    for (int i = 0; i < 20; ++i) {
        const int number = (std::rand() % 5) * 2 + 3;
        std::cout << number << ", ";
    }
    std::cout << std::endl;
}

void
printRandomFromSetC()
{
    for (int i = 0; i < 20; ++i) {
        const int number = ((std::rand() % 5) * 2 + 3) * 2;
        std::cout << number << ", ";
    }
    std::cout << std::endl;
}

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    if (isInteractive) {
        std::srand(time(0));
    }

    std::cout << "A)";
    printRandomFromSetA();
    std::cout << "B)";
    printRandomFromSetB();
    std::cout << "C)";
    printRandomFromSetC();

    return 0;
}


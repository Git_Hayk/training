/*
More sophisticated computer-aided instruction systems monitor the student's performance over a period of time.
The decision to begin a new topic often is based on the student's success with previous topics. Modify the program
of Exercise 6.36 to count the number of correct and incorrect responses typed by the student. After the student
types 10 answers, your program should calculate the percentage of correct responses. If the percentage is lower
than 75 percent, your program should print "Please ask your instructor for extra help" and terminate.
*/

#include <iostream>
#include <cstdlib>
#include <unistd.h>

const int TEST_QUANTITY = 10;
const int PASSING_GRADE = 75;
const int MESSAGE_COUNT = 4;
const bool isInteractive = ::isatty(STDIN_FILENO);

int  randomInRange(const int lowerLimit, const int upperLimit);
void learnMultiplication(const int number1, const int number2, int& index, int& rightAnswer);
int  getInputFromUser();
void printPositiveMessage();
void printNegativeMessage();
void calculateAverage(const int rightAnswer);

int
main()
{
    if (isInteractive) {
        std::srand(::time(0));
    }
    int rightAnswer = 0;
    for (int i = 0 ; i < TEST_QUANTITY; ++i) {
        const int number1 = randomInRange(0, 10);
        const int number2 = randomInRange(0, 10);

        learnMultiplication(number1 , number2, i, rightAnswer);
    }
    calculateAverage(rightAnswer);
    
    return 0;
}

void
learnMultiplication(const int number1, const int number2, int& index, int& rightAnswer)
{
    while (true) {
        if (isInteractive) {
            std::cout << "How much is " << number1 << " times " << number2 << "?\n> ";
        }
        const int studentAnswer = getInputFromUser();

        if (studentAnswer == number1 * number2) {
            ++rightAnswer;
            printPositiveMessage();
            return;
        }
        ++index;
        if (index == TEST_QUANTITY) {
            return;
        }
        printNegativeMessage();
    }
}

int
randomInRange(const int lowerLimit, const int upperLimit)
{
    return std::rand() % (upperLimit - lowerLimit) + lowerLimit;
}

int
getInputFromUser()
{
    int input;
    std::cin >> input;
    return input;
}

void
printPositiveMessage()
{
    const int generator = randomInRange(0, MESSAGE_COUNT);
    std::cout << "\033[1;32m";
    switch (generator) {
    case 0: std::cout << "Very good!"; break;
    case 1: std::cout << "Excellent!"; break;
    case 2: std::cout << "Nice work!"; break;
    case 3: std::cout << "Keep up the good work!"; break;
    default: assert(0 && "There are four types of comments!");
    }
    std::cout << "\033[0m" << std::endl;
}

void
printNegativeMessage()
{
    const int generator = randomInRange(0, MESSAGE_COUNT);
    std::cout << "\033[1;31m";
    switch (generator) {
    case 0: std::cout << "No. Please try again."; break;
    case 1: std::cout << "Wrong. Try once more."; break;
    case 2: std::cout << "Don't give up!"; break;
    case 3: std::cout << "No. Keep trying."; break;
    default: assert(0 && "There are four types of comments!");
    }
    std::cout << "\033[0m" << std::endl;
}

void
calculateAverage(const int rightAnswer)
{
    const double average = static_cast<double>(rightAnswer) / static_cast<double>(TEST_QUANTITY) * 100;
    std::cout << "Your average is " << average << '%' << std::endl;
    if (average < PASSING_GRADE) {
        std::cout << "Please ask your instructor for extra help!" << std::endl;
    }
}


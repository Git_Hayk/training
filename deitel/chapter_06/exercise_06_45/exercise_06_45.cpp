/*
(Recursive Greatest Common Divisor) The greatest common divisor of integers x and y is the largest integer that
evenly divides both x and y. Write a recursive function gcd that returns the greatest common divisor of x and y,
defined recursively as follows: If y is equal to 0, then gcd( x, y ) is x; otherwise, gcd( x, y ) is gcd( y, x % y ),
where % is the modulus operator. [Note: For this algorithm, x must be larger than y.]
*/

#include <iostream>
#include <unistd.h>
#include <cmath>

const bool isInteractive = ::isatty(STDIN_FILENO);

int gcd(int number1, int number2);

int
main()
{
    while (true) {
        if (isInteractive) {
            std::cout << "Input two integers to find their greatest common divisor (0 to exit): ";
        }

        int number1, number2;
        std::cin >> number1 >> number2;

        if (0 == number1 || 0 == number2) {
            std::cout << "Exiting..." << std::endl;
            break;
        }

        std::cout << "GCD(" << number1 << ", " << number2 << ") = " << std::abs(gcd(number1, number2)) << std::endl;
    }
    return 0;
}

int
gcd(int number1, int number2)
{
    assert(number1 != 0 && number2 != 0);
    const int remainder = number1 % number2;
    if (remainder != 0) {
        number1 = number2;
        number2 = remainder;
        return gcd(number1, number2);
    }
    return number2;
}


/*
Write statements that assign random integers to the variable n in the following ranges:

1 <= n <= 2

1 <= n <= 100

0 <= n <= 9

1000 <= n <= 1112

1 <= n <= 1

3 <= n <= 11
 */

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <unistd.h>

const bool isInteractive = (::isatty(STDIN_FILENO));

int
main()
{
    std::cout << "a) 1 ≤ n ≤ 2|" << std::setw(14)
              << "b) 1 ≤ n ≤ 100|" << std::setw(14)
              << "c) 0 ≤ n ≤ 9|" << std::setw(14)
              << "d) 1000 ≤ n ≤ 1112|" << std::setw(14)
              << "e) -1 ≤ n ≤ 1|" << std::setw(14)
              << "f) -3 ≤ n ≤ 11|\n" << std::endl;

    if (isInteractive) {
        std::srand(::time(0));
    }

    for (int i = 0; i < 20; ++i) {
        const int numberForA = std::rand() % 2 + 1;
        const int numberForB = std::rand() % 100 + 1;
        const int numberForC = std::rand() % 10;
        const int numberForD = std::rand() % 113 + 1000;
        const int numberForE = std::rand() % 3 - 1;
        const int numberForF = std::rand() % 15 - 3;

        std::cout << std::setw(12) << numberForA << '|'
                  << std::setw(14) << numberForB << '|'
                  << std::setw(12) << numberForC << '|'
                  << std::setw(18) << numberForD << '|'
                  << std::setw(13) << numberForE << '|'
                  << std::setw(14) << numberForF << '|' << std::endl;
    }
    return 0;
}


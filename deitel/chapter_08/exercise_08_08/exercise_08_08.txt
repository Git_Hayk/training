a) true

b) false
They can not be used identically in all cases, while a non-constant arrayPtr can change the address stored in itself to another one using the expression arrayPtr += 1 ,
the array gets its address as a constant on its first element, then the expression array += 1 cannot be implemented.

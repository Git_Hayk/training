#include "DeckOfCards.hpp"
#include <iostream>
#include <unistd.h>
#include <iomanip>
#include <cstdlib> /// prototypes for rand and srand
#include <ctime>


/// DeckOfCards default constructor initializes deck
DeckOfCards::DeckOfCards()
{
    size_t counter = 0;
    for (int row = 0; row < SUIT_COUNT; ++row) {
        for (int column = 0; column < FACE_COUNT; ++column) {
            ++counter;
            deck_[row][column] = counter; /// initialize slot of deck to 0
        }
    }
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }
}

void
DeckOfCards::printDeck()
{
    for (int row = 0; row < SUIT_COUNT; ++row) {
        for (int column = 0; column < FACE_COUNT; ++column) {
            std::cout << std::setw(4) << deck_[row][column];
        }
        std::cout << '\n';
    }
    std::cout << '\n';
}

/// shuffle cards in deck
void
DeckOfCards::shuffle()
{
   /// for each of the 52 cards, choose a slot of the deck randomly
    for (int suit = 0; suit < SUIT_COUNT; ++suit) {
        for (int face = 0; face < FACE_COUNT; ++face) {
            const int row = std::rand() % SUIT_COUNT;
            const int column = std::rand() % FACE_COUNT;
            std::swap(deck_[row][column], deck_[suit][face]);
        }
    }
}

/// deal cards in deck
void
DeckOfCards::deal()
{
   /// initialize suit array
   static const char* suit[SUIT_COUNT] = {
       "Hearts", "Diamonds", "Clubs", "Spades"
   };
   /// initialize face array
   static const char* face[FACE_COUNT] = { 
       "Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven",
       "Eight", "Nine", "Ten", "Jack", "Queen", "King" 
   };        
    
   /// for each of the 52 cards
    for (int card = 1; card <= SUIT_COUNT * FACE_COUNT; ++card) {
        bool handedOut = false;
        for (int row = 0; row < SUIT_COUNT; ++row) {
            for (int column = 0; column < FACE_COUNT; ++column) {
                if (deck_[row][column] == card) {
                    std::cout << std::setw(5) << std::right << face[column]    
                              << " of " << std::setw(8) << std::left << suit[row]
                              << (card % 2 == 0 ? '\n' : '\t');
                    handedOut = true;
                    break;
                }
            }
            if (handedOut) {
                break;
            }
        }
    }
}


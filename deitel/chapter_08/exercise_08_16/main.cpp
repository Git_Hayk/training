#include "DeckOfCards.hpp"

int
main()
{
   DeckOfCards deckOfCards;
   deckOfCards.printDeck();

   deckOfCards.deal();
   deckOfCards.shuffle();
   deckOfCards.printDeck();
   deckOfCards.deal();

   return 0;
}


const int SUIT_COUNT = 4;
const int FACE_COUNT = 13;

class DeckOfCards
{
public:
   DeckOfCards();

   void printDeck();
   void shuffle();
   void deal();

private:
   int deck_[SUIT_COUNT][FACE_COUNT];
};


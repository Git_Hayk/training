#include <iostream>
#include <unistd.h>
#include <cstdlib>

const size_t WIDTH  = 12;
const size_t HEIGHT = 12;

enum Dir { RIGHT, DOWN, LEFT, UP, DIR_COUNT};

void printLabirint(const int larbirint[][WIDTH], int* entry);
void mazeTraverse(int labirint[][WIDTH], int* currentPos, int travelDir = 1);
int* findEntry(int labirint[][WIDTH]);
int* chooseStart(int* entry1, int* entry2);
Dir  defineDirection();

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }
    int labirint[HEIGHT][WIDTH] = {
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
        {0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0}, 
        {1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0}, 
        {0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0}, 
        {0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 1}, 
        {0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0}, 
        {0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0}, 
        {0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0}, 
        {0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0}, 
        {0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0}, 
        {0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0}, 
        {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
    };
    int* currentPos = findEntry(labirint);
    Dir travelDir = defineDirection();
    mazeTraverse(labirint, currentPos, travelDir);
    return 0;
}

void
printLabirint(const int labirint[][WIDTH], int* entry)
{
    for (size_t i = 0; i < WIDTH; ++i) {
        for (size_t j = 0; j < HEIGHT; ++j) {
            if (entry == &labirint[i][j]) {
                std::cout << "+ ";
            } else if (labirint[i][j] == 0) {
                std::cout << "# ";
            } else if (labirint[i][j] == 1) {
                std::cout << ". ";
            } else if (labirint[i][j] == -1) {
                std::cout << "x ";
            }
        }
        std::cout << '\n';
    }
    std::cout << *(entry + WIDTH) << entry << std::endl;
}

void
mazeTraverse(int labirint[][WIDTH], int* currentPos, int travelDir = 1)
{
    printLabirint(labirint, currentPos);

    if (currentPos == &labirint[4][WIDTH - 1]) {
        return;
    }
    
    if (!*(currentPos + WIDTH)) {
        if (!*(currentPos + 1)) {
            mazeTraverse(labirint, currentPos, );
        }
        *currentPos = -1;
        currentPos += 1;
        mazeTraverse(labirint, currentPos, RIGHT);
    }
    mazeTraverse(labirint, currentPos, RIGHT); break;

    
    switch (travelDir) {
    case RIGHT: 
    case UP   : if (!*(currentPos + 1)) {
                    if (!*(currentPos - HEIGHT)) {
                        mazeTraverse(labirint, currentPos, LEFT);
                    }
                    *currentPos = -1;
                    currentPos -= HEIGHT;
                    mazeTraverse(labirint, currentPos, UP);
                }
                mazeTraverse(labirint, currentPos, RIGHT); break;
    case LEFT: if (!*(currentPos - HEIGHT)) {
                   if (!*(currentPos - 1)) {
                        mazeTraverse(labirint, currentPos, DOWN);
                   }
                    *currentPos = -1;
                    currentPos -= 1;
                    mazeTraverse(labirint, currentPos, LEFT);
                }
                mazeTraverse(labirint, currentPos, RIGHT); break;
    case DOWN: if (!*(currentPos - 1) ) {
                   if (!*(currentPos + HEIGHT)) {
                        mazeTraverse(labirint, currentPos, RIGHT);
                   }
                    *currentPos = -1;
                    currentPos += HEIGHT;
                    mazeTraverse(labirint, currentPos, DOWN);
                } 
                mazeTraverse(labirint, currentPos, RIGHT); break;
    default: assert(0 && "Four directions");
    }
}

int*
findEntry(int labirint[][WIDTH])
{
    int* entry1;         ///stores the adress of the first entry
    int* entry2;         ///stores the adress of the second entry
    for (size_t i = 0; i < WIDTH; ++i) {
        if (labirint[0][i]) {
            entry1 = &labirint[0][i];
        }
        if (labirint[HEIGHT - 1][i]) {
            entry2 = &labirint[HEIGHT - 1][i];
        }
    }

    for (size_t i = 1; i < HEIGHT - 1; ++i) {
        if (labirint[i][0]) {
            entry1 = &labirint[i][0];
        }
        if (labirint[i][WIDTH - 1]) {
            entry2 = &labirint[i][WIDTH - 1];
        }
    }
    return chooseStart(entry1, entry2);
}

int*
chooseStart(int* entry1, int* entry2)
{
    if (std::rand() % 2) {
        return entry1;
    }
    return entry2;
}

Dir
defineDirection()
{
    int column = 0;
    int row    = 1;

    if (0 == column)         { return RIGHT; }
    if (WIDTH - 1 == column) { return LEFT;  }
    if (0 == row)            { return DOWN; }
    if (HEIGHT - 1 == row)   { return UP; }
    assert(0);
    return DIR_COUNT;
}

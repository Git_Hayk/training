#include <iostream>
#include <cstdlib>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);
const int BUFFER = 64;
const int WORDS_IN_SENTENCE = 6;
const int WORD_COUNT = 5;
const int SENTENCE_COUNT = 20;

int randomInRange(const int lowerLimit, const int upperLimit);
void createSentence(const char** array[]);

int
main()
{
    if (isInteractive) { std::srand(::time(0)); }

    const char* article[]     = { "the", "a", "one", "some", "any" };
    const char* noun[]        = { "boy", "girl", "dog", "town", "car" };
    const char* verb[]        = { "drove", "jumped", "ran", "wacked", "skipped" };
    const char* preposition[] = { "to", "from", "over", "under", "on" };

    const char** sentenceStruct[WORDS_IN_SENTENCE] = { article, noun, verb, preposition, article, noun };
    
    for (int i = 0; i < SENTENCE_COUNT; ++i) {
        createSentence(sentenceStruct);
    }
    return 0;
}

int
randomInRange(const int lowerLimit, const int upperLimit)
{
    return std::rand() % (upperLimit - lowerLimit) + lowerLimit;
}

void
createSentence(const char** array[])
{
    char sentence[BUFFER] = "";
    for (int j = 0; j < WORDS_IN_SENTENCE; ++j) {
        const char* word = array[j][randomInRange(0, WORD_COUNT)];
        const char* space = " ";
        std::strcat(sentence, word);

        if (WORDS_IN_SENTENCE - 1 != j) {
            std::strcat(sentence, space);
        }
    }
    std::strcat(sentence, ".");
    sentence[0] = std::toupper(sentence[0]);
    std::cout << sentence << std::endl;
}


#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <thread>
#include <chrono>

const int FAST_PLOD = 3;
const int SLIP = -6;
const int SLOW_PLOD = 1;
const int SLEEP = 0;
const int BIG_HOP = 9;
const int BIG_SLIP = -12;
const int SMALL_HOP = 1;
const int SMALL_SLIP = -2;

inline void beginRace();
void fillArray(int* const arrayPtr, const int size);
int  turtMove();
int  hareMove();
void getWinner(const int turtPosition, const int harePosition, const int distance);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }
    const int DISTANCE = 70;
    int road[DISTANCE];

    fillArray(road, DISTANCE);

    int turtPosition = 0;
    int harePosition = 0;
    
    beginRace();
    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        ::system("clear");
        
        if (turtPosition < 0) { turtPosition = 0; }
        if (harePosition < 0) { harePosition = 0; }

        std::cout << "START |";
        if (turtPosition == harePosition) {
            for (int step = 0; step < DISTANCE; ++step) {
                std::cout << (road[step] == road[turtPosition] ? "OUCH!!!" : "  ");
            }
        } else {
            for (int step = 0; step < DISTANCE; ++step) {
                std::cout << (road[step] == road[turtPosition] ? "♎︎" : " ");
                std::cout << (road[step] == road[harePosition] ? "♉︎" : " ");
            }
        }
        std::cout << "| FINISH" << std::endl;

        getWinner(turtPosition, harePosition, DISTANCE);

        turtPosition += turtMove();
        harePosition += hareMove();
    }
    return 0;
}

inline void
beginRace()
{
    std::cout << "BANG !!!!!"
              << "\nAND THEY'RE OFF !!!!!" << std::endl;
}

void
fillArray(int* const arrayPtr, const int size)
{
    for (int i = 0; i < size; ++i) {
        *(arrayPtr + i) = i + 1;
    }
}

int
turtMove()
{
    const int randomNumber = 1 + std::rand() % 10;
    if (randomNumber >= 1 && randomNumber <= 5)  { return FAST_PLOD; }
    if (randomNumber >= 6 && randomNumber <= 7)  { return SLIP; }
    if (randomNumber >= 8 && randomNumber <= 10) { return SLOW_PLOD; }
    
    assert(randomNumber >= 1 && randomNumber <= 10);
    return 0;
}

int
hareMove()
{
    const int randomNumber = 1 + std::rand() % 10;
    if (randomNumber >= 1 && randomNumber <= 2)  { return SLEEP; }
    if (randomNumber >= 3 && randomNumber <= 4)  { return BIG_HOP; }
    if (randomNumber == 5)                       { return BIG_SLIP; }
    if (randomNumber >= 6 && randomNumber <= 8)  { return SMALL_HOP; }
    if (randomNumber >= 9 && randomNumber <= 10) { return SMALL_SLIP; } 
    
    assert(randomNumber >= 1 && randomNumber <= 10);
    return 0;
}

void
getWinner(const int turtPosition, const int harePosition, const int distance)
{
    if (turtPosition >= distance) {
        std::cout << "TORTOISE WINS!!! YAY!!!" << std::endl;
        ::exit(0);
    }
    if (harePosition >= distance) {
        std::cout << "Hare wins. Yuch." << std::endl;
        ::exit(0);
    }
    if (turtPosition >= distance && harePosition >= distance) {
        std::cout << "It's a tie." << std::endl;
        ::exit(0);
    }
}


#include <iostream>
#include <cstring>
#include <unistd.h>

const int BUFFER = 128;

void printLatinWord(char* lex);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input sentence to print it in Pig Latin: ";
    }
    char string[BUFFER] = "";
    std::cin.getline(string, BUFFER, '\n');
    for (char* lex = std::strtok(string, " "); lex != NULL; lex = std::strtok(NULL, " ")) {
        printLatinWord(lex);
    }
    std::cout << '\n';
    return 0;
}

void
printLatinWord(char* lex)
{
    const char firstLetter = lex[0]; /// kipping first letter
    for (int i = 1; lex[i] != '\0'; ++i) {
        lex[i - 1] = lex[i];   /// changing letters index one above
    }
    lex[std::strlen(lex) - 1] = firstLetter;    /// setting first letter to the end
    char latinWord[std::strlen(lex) + 3];  /// creating new latin word bigger than lex. +3 for 'a', 'y' and ' ';
    std::strcpy(latinWord, lex);
    std::strcat(latinWord, "ay ");
    std::cout << latinWord;
}


#include <iostream>
#include <cstring>
#include <unistd.h>

const int BUFFER = 128;

int
main()
{
    char text[BUFFER] = "";

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input sentense to print in rversed order: ";
    }

    std::cin.getline(text, BUFFER, '\n');

    char stringReverse[BUFFER] = "";

    for (char* lex = std::strtok(text, " "); lex != NULL; lex = std::strtok(NULL, " ")) {
        const int size = std::strlen(lex);
        for (int i = std::strlen(stringReverse); i >= 0; --i) {
            stringReverse[i + size + 1] = stringReverse[i];
        }
        stringReverse[size] = ' ';
        for (int j = 0; j < size; ++j) {
            stringReverse[j] = lex[j];
        }
    }
    std::cout << stringReverse << std::endl;
    return 0;
}


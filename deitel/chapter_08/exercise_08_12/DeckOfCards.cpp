#include "DeckOfCards.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cstdlib> // prototypes for rand and srand
#include <ctime>

// DeckOfCards default constructor initializes deck
DeckOfCards::DeckOfCards()
{
   for (int row = 0; row <= 3; ++row) {
      for (int column = 0; column <= 12; ++column) {
         deck[row][column] = 0; // initialize slot of deck to 0
      }
   }
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }
}

// shuffle cards in deck
void
DeckOfCards::shuffle()
{
   int row;
   int column;

   // for each of the 52 cards, choose a slot of the deck randomly
   for (int card = 1; card <= 52; ++card) {
      do {
         row = std::rand() % 4;
         column = std::rand() % 13;
      } while (deck[row][column] != 0);

      deck[row][column] = card;
   }
}

// deal cards in deck
void
DeckOfCards::deal()
{
   // initialize suit array                       
   static const char *suit[4] =                 
      { "Hearts", "Diamonds", "Clubs", "Spades" };

   // initialize face array                                     
   static const char *face[13] =                              
      { "Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven",
      "Eight", "Nine", "Ten", "Jack", "Queen", "King" };        

   // for each of the 52 cards
   for (int card = 1; card <= 52; ++card) {
      for (int row = 0; row <= 3; ++row) {
         for (int column = 0; column <= 12; ++column) {
            if (deck[row][column] == card) {
                std::cout << std::setw(5) << std::right << face[column]    
                  << " of " << std::setw(8) << std::left << suit[row]
                  << (card % 5 == 0 ? '\n' : '\t');
            }
         }
      }
   }
   std::cout << '\n';
}

#include "DeckOfCards.hpp"
#include <iostream>
#include <unistd.h>
#include <iomanip>
#include <cstdlib> // prototypes for rand and srand
#include <ctime>


// DeckOfCards default constructor initializes deck
DeckOfCards::DeckOfCards()
{
    size_t counter = 0;
    for (int row = 0; row < SUIT; ++row) {
        for (int column = 0; column < FACE; ++column) {
            ++counter;
            deck_[row][column] = counter; // initialize slot of deck to 0
        }
    }
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }
}

void
DeckOfCards::printDeck()
{
    for (int row = 0; row < SUIT; ++row) {
        for (int column = 0; column < FACE; ++column) {
            std::cout << std::setw(4) << deck_[row][column];
        }
        std::cout << '\n';
    }
    std::cout << '\n';
}

void
DeckOfCards::shuffleAndDeal()
{
   static const char* suit[SUIT] = { 
       "Hearts", "Diamonds", "Clubs", "Spades" 
   };
   static const char* face[FACE] = { 
       "Ace", "Deuce", "Three", "Four", "Five", "Six", "Seven",
      "Eight", "Nine", "Ten", "Jack", "Queen", "King" 
   };        

    for (int i = 0; i < SUIT; ++i) {
        for (int j = 0; j < FACE; ++j) {
            const int row = std::rand() % SUIT;
            const int column = std::rand() % FACE;
            if (deck_[row][column] != 0) {
                std::swap(deck_[row][column], deck_[i][j]);
            }
            std::cout << std::setw(5) << std::right << face[(deck_[i][j] - 1) % FACE]
                  << " of " << std::setw(8) << std::left << suit[(deck_[i][j] - 1) / FACE] << std::endl;
            deck_[i][j] = 0;
        }
    }
}


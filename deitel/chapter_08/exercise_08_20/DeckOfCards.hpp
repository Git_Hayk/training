const int SUIT = 4;
const int FACE = 13;

class DeckOfCards
{
public:
   DeckOfCards();
   void printDeck();
   void shuffleAndDeal();

private:
   int deck_[SUIT][FACE];
};


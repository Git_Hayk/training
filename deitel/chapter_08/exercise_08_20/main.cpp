#include "DeckOfCards.hpp"

int
main()
{
   DeckOfCards deckOfCards;

   deckOfCards.printDeck();
   deckOfCards.shuffleAndDeal();
   return 0;
}


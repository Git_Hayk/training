#include <iostream>
#include <unistd.h>

const size_t BUFFER = 128;
const bool isInteractive = ::isatty(STDIN_FILENO);

void validateNumber(const int number);
void compareString(const char* string1, const char* string2, const int number);

int
main()
{
    char string1[BUFFER];
    if (isInteractive) { std::cout << "Input string1: "; }
    std::cin.getline(string1, BUFFER, '\n');

    char string2[BUFFER];
    if (isInteractive) { std::cout << "Input string2: "; }
    std::cin.getline(string2, BUFFER, '\n');

    int number;
    if (isInteractive) { std::cout << "Input count of characters to be compared: "; }
    std::cin >> number;

    validateNumber(number);
    compareString(string1, string2, number);

    return 0;
}

void
validateNumber(const int number)
{
    if (number <= 0) {
        std::cerr << "Error 1: Invalid count!" << std::endl;
        ::exit(1);
    }
}

void
compareString(const char* string1, const char* string2, const int number)
{

    const int result = std::strncmp(string1, string2, number);

    if (result < 0)  { std::cout << "First " << number << " characters of string1 is less than first " << number << " characters of string2" << std::endl; }
    if (0 == result) { std::cout << "First " << number << " characters of string1 is equal to first "  << number << " characters of string2" << std::endl; }
    if (result > 0)  { std::cout << "First " << number << " characters of string1 is more than first " << number << " characters of string2" << std::endl; }
}


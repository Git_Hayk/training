#include <iostream>

int
main()
{
    const size_t BUFFER = 128;
    char string1[BUFFER];
    std::cin.getline(string1, BUFFER, '\n');
    char string2[BUFFER];
    std::cin.getline(string2, BUFFER, '\n');

    const int result = std::strcmp(string1, string2);

    if (result < 0)  { std::cout << "string1 is less than string2" << std::endl; return 0; }
    if (0 == result) { std::cout << "string1 is equal to string2"  << std::endl; return 0; }
    if (result > 0)  { std::cout << "string1 is more than string2" << std::endl; return 0; }
    return 0;
}


#include "headers/SavingsAccount.hpp"
#include <gtest/gtest.h>

TEST(SavingsAccountTest, DefaulConst)
{
    SavingsAccount saver;
    EXPECT_EQ(saver.getBalance(), 0);
    EXPECT_EQ(static_cast<int>(saver.getRate()), 0);
}

TEST(SavingsAccountTest, calculateMonthlyInterestTest)
{
    SavingsAccount saver1(2000);
    SavingsAccount saver2(3000);
    SavingsAccount::modifyInterestRate(3);

    EXPECT_EQ(static_cast<int>(saver1.getRate()), 3);
    EXPECT_EQ(static_cast<int>(saver2.getRate()), 3);

    EXPECT_EQ(saver1.getBalance(), 2000);
    EXPECT_EQ(saver2.getBalance(), 3000);

    saver1.calculateMonthlyInterest();
    saver2.calculateMonthlyInterest();
    EXPECT_EQ(saver1.getBalance(), 2005);
    EXPECT_EQ(saver2.getBalance(), 3007);

    SavingsAccount::modifyInterestRate(4);

    saver1.calculateMonthlyInterest();
    saver2.calculateMonthlyInterest();
    EXPECT_EQ(saver1.getBalance(), 2011);
    EXPECT_EQ(saver2.getBalance(), 3017);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


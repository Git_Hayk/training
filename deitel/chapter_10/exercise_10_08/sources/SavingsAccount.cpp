#include "headers/SavingsAccount.hpp"
#include <iostream>

size_t
SavingsAccount::annualInterestRate_ = 0;

SavingsAccount::SavingsAccount(const int balance)
{
    savingsBalance_ = (balance >= 0) ? balance : 0;
}

void
SavingsAccount::calculateMonthlyInterest()
{
    const size_t monthlyIncrease = savingsBalance_ * annualInterestRate_ / 100 / MONTHS_COUNT;
    increaseBalance(monthlyIncrease);
}

void
SavingsAccount::increaseBalance(const size_t monthlyIncrease)
{
    savingsBalance_ += monthlyIncrease;
}

void
SavingsAccount::printBalance() const
{
    std::cout << "Balance: " << savingsBalance_ << std::endl;
}

void
SavingsAccount::modifyInterestRate(const size_t rate)
{
    annualInterestRate_ = rate;
}


#ifndef __SAVINGS_ACCOUNT_HPP__
#define __SAVINGS_ACCOUNT_HPP__

#include <cstddef>

class SavingsAccount
{
private:
    const static size_t MONTHS_COUNT = 12;
    static size_t annualInterestRate_;

public:
    SavingsAccount(const int balance = 0);
    void calculateMonthlyInterest();
    void increaseBalance(const size_t monthlyIncrease);
    void printBalance() const;
    static void modifyInterestRate(const size_t rate);

    int getBalance() const { return savingsBalance_; };
    size_t getRate() const { return annualInterestRate_; };
private:
    double savingsBalance_;
};

#endif /// __SAVINGS_ACCOUNT_HPP__


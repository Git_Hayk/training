/*(Date Class) Create a class called Date that includes three pieces of information as data membersa month (type int),
  a day (type int) and a year (type int). Your class should have a constructor with three parameters that uses the
  parameters to initialize the three data members. For the purpose of this exercise, assume that the values provided
  for the year and day are correct, but ensure that the month value is in the range 112; if it is not, set the month
  to 1. Provide a set and a get function for each data member. Provide a member function displayDate that displays the
  month, day and year separated by forward slashes (/). Write a test program that demonstrates class Date's capabilities.*/

#include "Date.hpp"
#include <iostream>

int
main()
{
    Date date1(29, 9, 1995);
    date1.displayDate();

    Date date2(20, 3, 2022);
    date2.displayDate();

    Date date3(31, 19, 2021);
    date3.displayDate();

    return 0;
}


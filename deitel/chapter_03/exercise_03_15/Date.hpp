class Date
{
public:
    Date(int day, int month, int year);
    
    void setDay(int day);
    int setMonth(int month);
    void setYear(int year);

    void displayDate();
    int getDay();
    int getMonth();
    int getYear();

private:
    int day_;
    int month_;
    int year_;
};


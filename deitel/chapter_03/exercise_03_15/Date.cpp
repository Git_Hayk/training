#include "Date.hpp"
#include <iostream>

Date::Date(int day, int month, int year)
{
    setDay(day);
    setMonth(month);
    setYear(year);
}

void
Date::setDay(int day)
{
    day_ = day;
}

int
Date::setMonth(int month)
{
    if (month > 0) {
        if (month <= 12) {
            month_ = month;
            return 0;
        }
    }
    std::cout << "\nWarning 1: Month should be in range 1 to 12!\nResetting to 1..." << std::endl;
    month_ = 1;
    return 1;
}

void
Date::setYear(int year)
{
    year_ = year;
}

void
Date::displayDate()
{
    std::cout << "Date: " << getDay() << "/"
                          << getMonth() << "/"
                          << getYear() << std::endl;
}

int
Date::getDay()
{
    return day_;
}

int
Date::getMonth()
{
    return month_;
}

int
Date::getYear()
{
    return year_;
}


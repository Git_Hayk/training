#include <string>

class GradeBook
{
public:
    GradeBook(std::string courseName, std::string instructorName);  
    void setCourseName(std::string courseName);
    void setInstructorName(std::string instructorName);
    std::string getCourseName();
    std::string getInstructorName();   
    void displayMessage();
private:
    std::string courseName_;
    std::string instructorName_;
};


//(Modifying Class GradeBook) Modify class GradeBook (Figs. 3.113.12) as follows:
//
//Include a second string data member that represents the course instructor's name.
//
//Provide a set function to change the instructor's name and a get function to retrieve it.
//
//Modify the constructor to specify two parametersone for the course name and one for the instructor's name.
//
//Modify member function displayMessage such that it first outputs the welcome message and course name, then outputs "This course is presented by: " followed by the instructor's name.
//
//Use your modified class in a test program that demonstrates the class's new capabilities.

#include "GradeBook.hpp"
#include <iostream>

int main()
{
    GradeBook gradeBook1("CS101 Introduction to C++ Programming", "Artak");
    GradeBook gradeBook2("CS102 Data Structures in C++", "Hayk");
    
    gradeBook1.displayMessage();
    std::cout << "gradeBook1 created for course: " << gradeBook1.getCourseName() << std::endl;
    gradeBook2.displayMessage();
    std::cout << "gradeBook2 created for course: " << gradeBook2.getCourseName() << std::endl;
    return 0;
}


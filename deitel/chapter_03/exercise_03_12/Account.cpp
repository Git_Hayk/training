#include "Account.hpp"
#include <iostream>

Account::Account(int initialBalance)
{
    if (initialBalance < 0) {
        std::cout << "Warning 1: your initial balance was invalid!\nResetting to 0..." << std::endl;
        balance_ = 0;
        return;
    }

    balance_ = initialBalance;
};

int
Account::credit(int credit)
{
    if (credit < 0) {
        std::cout << "Warning 2: Credit can not be less than zero!\nSkipping operation..." <<std::endl;
        return 2;
    }

    balance_ = balance_ + credit;
    return 0;
};

int
Account::debit(int debit)
{
    if (debit < 0) {
        std::cout << "Warning 3: Debit can not be less than zero!\nSkipping operation..." <<std::endl;
        return 3;
    }

    if (debit > balance_) {
        std::cout << "Warning 4: Debit amount exceeded account balance!\nSkipping operation..." << std::endl;
        return 4;
    }

    balance_ = balance_- debit;
    return 0;
};

void
Account::getBalance()
{
    std::cout << balance_ << std::endl;
};


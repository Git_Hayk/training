/*(Account Class) Create a class called Account that a bank might use to represent customers' bank accounts.
  Your class should include one data member of type int to represent the account balance. [Note: In subsequent
  chapters, we'll use numbers that contain decimal points (e.g., 2.75)called floating-point valuesto represent
  dollar amounts.] Your class should provide a constructor that receives an initial balance and uses it to
  initialize the data member. The constructor should validate the initial balance to ensure that it is greater
  than or equal to 0. If not, the balance should be set to 0 and the constructor should display an error message,
  indicating that the initial balance was invalid. The class should provide three member functions. Member function
  credit should add an amount to the current balance. Member function debit should withdraw money from the Account
  and should ensure that the debit amount does not exceed the Account's balance. If it does, the balance should be
  left unchanged and the function should print a message indicating "Debit amount exceeded account balance."
  Member function getBalance should return the current balance. Create a program that creates two Account objects
  and tests the member functions of class Account.*/

#include "Account.hpp"
#include <iostream>

int
main()
{
    int initialBalance;

    std::cout << "Please input your first account initial balance: ";
    std::cin >> initialBalance;
    Account account1(initialBalance);
    std::cout << "Please input your second account initial balance: ";
    std::cin >> initialBalance;
    Account account2(initialBalance);

    std::cout << "\n\nYour first accounts balance is ";
    account1.getBalance();
    std::cout << "Your second accounts balance is ";
    account2.getBalance();
    
    int credit;
    std::cout << "\n\nHow much do you want to add to your first account? ";
    std::cin >> credit;
    account1.credit(credit);
    std::cout << "How much do you want to add to your second account? ";
    std::cin >> credit;
    account2.credit(credit);

    std::cout << "\n\nYour first accounts balance is ";
    account1.getBalance();
    std::cout << "Your second accounts balance is ";
    account2.getBalance();
    
    int debit;
    std::cout << "\n\nHow much do you want to withdraw from your first account? ";
    std::cin >> debit;
    account1.debit(debit);
    std::cout << "How much do you want to withdraw from your second account? ";
    std::cin >> debit;
    account2.debit(debit);

    std::cout << "\n\nYour first accounts balance is ";
    account1.getBalance();
    std::cout << "Your second accounts balance is ";
    account2.getBalance();

    return 0;
}


class Account
{
public:
    Account(int initialBalance);
    int credit(int credit);
    int debit(int debit);
    void getBalance();

private:
    int balance_;
};


#include <string>

class Invoice
{
public:
    Invoice(std::string partNumber, std::string partDescription, int partQuantity, int pricePerItem);

    int setPartNumber(std::string partNumber);
    int setDescription(std::string partDescription);
    int setQuantity(int partQuantity);
    int setItemPrice(int pricePerItem);

    std::string getPartNumber();
    std::string getDescription();
    int getQuantity();
    int getItemPrice();
    
    int getInvoiceAmount();

private:
    std::string partNumber_;
    std::string partDescription_;
    int partQuantity_;
    int pricePerItem_;
};


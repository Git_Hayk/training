/*(Invoice Class) Create a class called Invoice that a hardware store might use to represent an invoice for
  an item sold at the store. An Invoice should include four pieces of information as data membersa part number
  (type string), a part description (type string), a quantity of the item being purchased (type int) and a price
  per item (type int). [Note: In subsequent chapters, we'll use numbers that contain decimal points (e.g., 2.75)
  called floating-point valuesto represent dollar amounts.] Your class should have a constructor that initializes
  the four data members. Provide a set and a get function for each data member. In addition, provide a member
  function named getInvoiceAmount that calculates the invoice amount (i.e., multiplies the quantity by the price
  per item), then returns the amount as an int value. If the quantity is not positive, it should be set to 0. If
  the price per item is not positive, it should be set to 0. Write a test program that demonstrates class Invoice's
  capabilities.*/

#include "Invoice.hpp"
#include <iostream>

int
main()
{
    Invoice invoice1("DMG-2930", "MacBook Air", 3, 950);
    
    std::cout << "Invoice 1 data:\n\tPart Number: " << invoice1.getPartNumber();
    std::cout << "\n\tDescription: " << invoice1.getDescription();
    std::cout << "\n\tQuantity: " << invoice1.getQuantity();
    std::cout << "\n\tPrice: " << invoice1.getItemPrice() << " $";
    std::cout << "\n\tAmount: " << invoice1.getInvoiceAmount() << " $";

    Invoice invoice2("01305745", "Praxit LLC", 6, 56000);
    
    std::cout << "\nInvoice 2 data:\n\tPart Number: " << invoice2.getPartNumber();
    std::cout << "\n\tDescription: " << invoice2.getDescription();
    std::cout << "\n\tQuantity: " << invoice2.getQuantity();
    std::cout << "\n\tPrice: " << invoice2.getItemPrice() << " $";
    std::cout << "\n\tAmount: " << invoice2.getInvoiceAmount() << " $" << std::endl;

    Invoice invoice3("av-85bc", "Tesla Model Y", -5, -200);
    
    std::cout << "Invoice 3 data:\n\tPart Number: " << invoice3.getPartNumber();
    std::cout << "\n\tDescription: " << invoice3.getDescription();
    std::cout << "\n\tQuantity: " << invoice3.getQuantity();
    std::cout << "\n\tPrice: " << invoice3.getItemPrice() << " $";
    std::cout << "\n\tAmount: " << invoice3.getInvoiceAmount() << " $" << std::endl;

    return 0;
}


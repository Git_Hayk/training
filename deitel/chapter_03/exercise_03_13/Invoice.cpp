#include "Invoice.hpp"
#include <string>
#include <iostream>

Invoice::Invoice(std::string partNumber, std::string partDescription, int partQuantity, int pricePerItem)
{
    setPartNumber(partNumber);
    setDescription(partDescription);
    setQuantity(partQuantity);
    setItemPrice(pricePerItem);
}

int
Invoice::setPartNumber(std::string partNumber)
{
    partNumber_ = partNumber;
    return 0;
}

int
Invoice::setDescription(std::string partDescription)
{
    partDescription_ = partDescription;
    return 0;
}

int
Invoice::setQuantity(int partQuantity)
{
    if (partQuantity < 0) {
        std::cout << "\nWarning 1: Quantity should not be less than zero!\nSetting to 0." << std::endl;
        partQuantity_ = 0;
        return 1;
    }

    partQuantity_ = partQuantity;
    return 0;
}

int
Invoice::setItemPrice(int pricePerItem)
{
    if (pricePerItem < 0) {
        std::cout << "\nWarning 2: Item Price should not be less than zero!\nSetting to 0." << std::endl;
        pricePerItem_ = 0;
        return 2;
    }

    pricePerItem_ = pricePerItem;
    return 0;
}

std::string
Invoice::getPartNumber()
{
    return partNumber_;
}

std::string
Invoice::getDescription()
{
    return partDescription_;
}

int
Invoice::getQuantity()
{
    return partQuantity_;
}

int
Invoice::getItemPrice()
{
    return pricePerItem_;
}

int
Invoice::getInvoiceAmount()
{
    return (partQuantity_ * pricePerItem_);
}


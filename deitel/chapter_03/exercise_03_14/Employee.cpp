#include "Employee.hpp"
#include <iostream>

Employee::Employee(std::string firstName, std::string lastName, int monthlySalary)
{
    setFirstName(firstName);
    setLastName(lastName);
    setMonthlySalary(monthlySalary);
}

void
Employee::setFirstName(std::string firsName)
{
    firstName_ = firsName;
}

void
Employee::setLastName(std::string lastName)
{
    lastName_ = lastName;
}

int
Employee::setMonthlySalary(int monthlySalary)
{
    if (monthlySalary < 0) {
        std::cout << "\nWarning 1: Salary should not be less than zero!\nSetting to 0..." << std::endl;
        monthlySalary_ = 0;
        return 1;
    }

    monthlySalary_ = monthlySalary;
    return 0;
}

void
Employee::raisePercent()
{
    monthlySalary_ = monthlySalary_ + (monthlySalary_ * 10 / 100);
}

std::string
Employee::getFirstName()
{
    return firstName_;
}

std::string
Employee::getLastName()
{
    return lastName_;
}

int
Employee::getMonthlySalary()
{
    return monthlySalary_;
}

int
Employee::getYearlySalary()
{
    return (monthlySalary_ * 12);
}


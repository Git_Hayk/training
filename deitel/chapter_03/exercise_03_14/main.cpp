/*(Employee Class) Create a class called Employee that includes three pieces of information as data membersa first
  name (type string), a last name (type string) and a monthly salary (type int). [Note: In subsequent chapters,
  we'll use numbers that contain decimal points (e.g., 2.75)called floating-point valuesto represent dollar amounts.]
  Your class should have a constructor that initializes the three data members. Provide a set and a get function for
  each data member. If the monthly salary is not positive, set it to 0. Write a test program that demonstrates class
  Employee's capabilities. Create two Employee objects and display each object's yearly salary. Then give each Employee
  a 10 percent raise and display each Employee's yearly salary again.*/

#include "Employee.hpp"
#include <iostream>

int
main()
{
    Employee employee1("Hayk", "Harutyunyan", 3000);
    
    std::cout << "Employee 1:"
              << "\n\tFirst Name: " << employee1.getFirstName()
              << "\n\tLast Name: " << employee1.getLastName()
              << "\n\tMonthly Salary: " << employee1.getMonthlySalary() << "$"
              << "\n\tYearly Salary: " << employee1.getYearlySalary() << "$" << std::endl;
    
    employee1.raisePercent();
    std::cout << "\tYearly salary (raised): " << employee1.getYearlySalary() << "$" << std::endl;

    Employee employee2("Alex", "Merser", -400);
    
    std::cout << "Employee 2:"
              << "\n\tFirst Name: " << employee2.getFirstName()
              << "\n\tLast Name: " << employee2.getLastName()
              << "\n\tMonthly Salary: " << employee2.getMonthlySalary() << "$"
              << "\n\tYearly Salary: " << employee2.getYearlySalary() << "$" << std::endl;
    
    employee2.raisePercent();
    std::cout << "\tYearly salary (raised): " << employee2.getYearlySalary() << "$" << std::endl;

    return 0;
}


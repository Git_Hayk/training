#include <string>

class Employee
{
public:
    Employee(std::string firstName, std::string lastName, int monthSalary);

    void setFirstName(std::string firstName);
    void setLastName(std::string lastName);
    int setMonthlySalary(int monthlySalary);
    
    void raisePercent();
    std::string getFirstName();
    std::string getLastName();
    int getMonthlySalary();
    int getYearlySalary();

private:
    std::string firstName_;
    std::string lastName_;
    int monthlySalary_;
};


/*
a)
Class HugeInt has an array named integer_, which contains digits in range 0-9
of type short (short int) whit size of BUFFER = 30.
It have two constructors.
first takes an argument of type long, (0 by default), and second takes of type char* (which is string).
There are three implementation of an operator+, wich one is takes it's own type of right hand value
The first implementation of operator+, which takes argument as HugeInt& rhv is the base of all
In other two cases the addition operation should be done by converting them in to HugeInt (int or char*)

b)
As the integer_ can hold only digits, all operations can be carried out only with positive numbers.
 */


#ifndef __HUGEINT_HPP__
#define __HUGEINT_HPP__

#include <iostream>

class HugeInt
{
    friend std::ostream& operator<<(std::ostream& output, const HugeInt& rhv);
private:
    const static int BUFFER = 30;
public:
    HugeInt(long number = 0); // conversion/default constructor
    HugeInt(const char* number); // conversion constructor

    int getLength() const;

    HugeInt operator+(const HugeInt& rhv) const; /// in source
    HugeInt operator+(const int rhv) const   { return *this + HugeInt(rhv); };
    HugeInt operator+(const char* rhv) const { return *this + HugeInt(rhv); };

    HugeInt operator-(const HugeInt& rhv) const; /// in source
    HugeInt operator-(const int rhv) const    { return *this - HugeInt(rhv); };
    HugeInt operator-(const char* rhv) const  { return *this - HugeInt(rhv); };

    HugeInt operator*(const HugeInt& rhv) const; /// in source
    HugeInt operator*(const int rhv) const   { return *this * HugeInt(rhv); };
    HugeInt operator*(const char* rhv) const { return *this * HugeInt(rhv); };

    HugeInt operator/(const HugeInt& rhv) const; /// in source
    HugeInt operator/(const int rhv) const   { return *this / HugeInt(rhv); };
    HugeInt operator/(const char* rhv) const { return *this / HugeInt(rhv); };

    bool operator==(const HugeInt& rhv) const; /// in source
    bool operator==(const int rhv) const   { return *this == HugeInt(rhv); };
    bool operator==(const char* rhv) const { return *this == HugeInt(rhv); };

    bool operator!=(const HugeInt& rhv) const  { return !(*this == rhv); };
    bool operator!=(const int rhv) const       { return !(*this == HugeInt(rhv)); };
    bool operator!=(const char* rhv) const     { return !(*this == HugeInt(rhv)); };

    bool operator<=(const HugeInt& rhv) const  { return !(*this > rhv); };
    bool operator<=(const int rhv) const       { return *this <= HugeInt(rhv); };
    bool operator<=(const char* rhv) const     { return *this <= HugeInt(rhv); };

    bool operator>=(const HugeInt& rhv) const  { return !(*this < rhv); };
    bool operator>=(const int rhv) const       { return *this >= HugeInt(rhv); };
    bool operator>=(const char* rhv) const     { return *this >= HugeInt(rhv); };

    bool operator<(const HugeInt& rhv) const; /// in source
    bool operator<(const int rhv) const    { return *this < HugeInt(rhv); };
    bool operator<(const char* rhv) const  { return *this < HugeInt(rhv); };

    bool operator>(const HugeInt& rhv) const; /// in source
    bool operator>(const int rhv) const       { return *this > HugeInt(rhv); };
    bool operator>(const char* rhv) const     { return *this > HugeInt(rhv); };
private:
    HugeInt cutOfTheEnd(const int size);
    HugeInt insertFromTheEnd(const HugeInt& rhv, const int size);
private:
    short integer_[BUFFER];
};

#endif ///__HUGEINT_HPP__


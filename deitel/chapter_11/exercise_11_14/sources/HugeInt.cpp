#include "headers/HugeInt.hpp"
#include <iostream>
#include <cctype> // isdigit function prototype
#include <cstring> // strlen function prototype

// overloaded output operator
std::ostream&
operator<<(std::ostream& output, const HugeInt& num)
{
    int i = 0;
    for ( ; (0 == num.integer_[i]) && (i < HugeInt::BUFFER); ++i); // skip leading zeros

    if (HugeInt::BUFFER ==  i) {
        output << 0;
        return output;
    }
    for ( ; i < HugeInt::BUFFER; ++i) {
        output << num.integer_[i];
    }
    return output;
}

// default constructor; conversion constructor that converts
// a long integer_ into a HugeInt object
HugeInt::HugeInt(long value)
{
    // initialize array to zero
    for (int i = 0; i < BUFFER; ++i) {
        integer_[i] = 0;
    }

    // place digits of argument into array
    for (int j = BUFFER - 1; value != 0 && j >= 0; --j) {
        integer_[j] = value % 10;
        value /= 10;
    }
}

// conversion constructor that converts a character string
// representing a large integer_ into a HugeInt object
HugeInt::HugeInt(const char* str)
{
    // initialize array to zero
    for (int i = 0; i < BUFFER; i++) {
        integer_[i] = 0;
    }

    // place digits of argument into array
    int length = strlen(str);

    for (int j = BUFFER - length, k = 0; j < BUFFER; ++j, ++k) {
        if (isdigit(str[k])) {
            integer_[j] = str[k] - '0';
        }
    }
}

int
HugeInt::getLength() const
{
    int i = 0;
    for ( ; (0 == integer_[i]) && (i < HugeInt::BUFFER); ++i); // skip leading zeros
    if (BUFFER == i) { return 1; }
    return BUFFER - i;
}

// addition operator; HugeInt + HugeInt
HugeInt
HugeInt::operator+(const HugeInt& rhv) const
{
    HugeInt temp;
    int carry = 0;

    for (int i = BUFFER - 1; i >= 0; --i) {
        temp.integer_[i] = integer_[i] + rhv.integer_[i] + carry;

      // determine whether to carry a 1
        if (temp.integer_[i] > 9) {
            temp.integer_[i] %= 10; // reduce to 0-9
            carry = 1;
        } else { 
            carry = 0;
        }
    }
    return temp;
}

HugeInt
HugeInt::operator-(const HugeInt& rhv) const
{
    HugeInt temp;
    int carry = 0;

    for (int i = BUFFER - 1; i >= 0; --i) {
        short int tempDigit = integer_[i] - rhv.integer_[i] - carry;
        if (tempDigit < 0) {
            tempDigit += 10;
            carry = integer_[i] - 1;
        } else {
            carry = 0;
        }
        temp.integer_[i] = tempDigit;
    }
    return temp;
}

HugeInt
HugeInt::operator*(const HugeInt& rhv) const
{
    const int thisStart = BUFFER - this->getLength();
    const int rhvStart = BUFFER - rhv.getLength();

    const int rhvSize = rhv.getLength();
    HugeInt multiples[rhvSize];

    int excess = 0;
    int capacity = 0;

    for (int i = BUFFER - 1; i >= rhvStart; --i) {
        HugeInt temp;
        for (int j = BUFFER - 1; (j >= thisStart) || (0 != excess); --j) {
            short tempDigit = integer_[j] * rhv.integer_[i] + excess; /// max value is 99
            if (tempDigit > 9) {
                excess = tempDigit / 10;
                tempDigit %= 10;
            } else {
                excess = 0;
            }
            temp.integer_[j - capacity] = tempDigit;
        }
        ++capacity;
        multiples[BUFFER - 1 - i] = temp;
    }
    
    HugeInt result;
    for (int k = 0; k < rhvSize; ++k) {
        result = result + multiples[k];
    }
    return result;
}

HugeInt
HugeInt::operator/(const HugeInt& rhv) const
{
    if (1 == rhv.getLength() && 0 == rhv.integer_[BUFFER - 1]) { /// if rhv is zero
        std::cerr << "Error 1: Division by zero!" << std::endl;
        ::exit(1);
    }
    
    if (*this < rhv) { return 0; }
    
    int sizeDiff = this->getLength() - rhv.getLength();
    bool canBePlacedIn = false;
    HugeInt temp = *this;
    
    while (sizeDiff != 0( {
        for (int i = BUFFER - rhv.getLength(); i < BUFFER; ++i) { /// if we are here , it means that rhv < this;
            if (integer_[i - sizeDiff] < rhv.integer_[i]) {
                --sizeDiff;
                break;
            }
        }
        temp.cutOfTheEnd(sizeDiff);
        int multiplier = 1;
        while (temp - rhv > rhv) {
            ++multiplier;
            temp = temp - (rhv * multiplier);
        }
        temp.insertFromTheEnd(*this, sizeDiff);
        --sizeDiff;
    }
    return rhv;
}

bool
HugeInt::operator==(const HugeInt& rhv) const
{
    if (this == &rhv) { return true; }

    if (this->getLength() != rhv.getLength()) {
        return false;
    }

    if (this->getLength() == rhv.getLength()) {
        const int limit = BUFFER - getLength();
        for (int i = BUFFER - 1; i >= limit; --i) {
            if (integer_[i] != rhv.integer_[i]) {
                return false;
            }
        }
    }
    return true;
}

bool
HugeInt::operator<(const HugeInt& rhv) const
{
    if (this == &rhv) { return false; }

    if (this->getLength() < rhv.getLength()) {
        return true;
    }
    
    if (this->getLength() == rhv.getLength()) {
        const int limit = BUFFER - getLength();
        for (int i = limit; i < BUFFER; ++i) {
            if (integer_[i] < rhv.integer_[i]) {
                return true;
            } else if (integer_[i] > rhv.integer_[i]) {
                return false;
            }
        }
    }
    return false;
}

bool
HugeInt::operator>(const HugeInt& rhv) const
{
    if (this == &rhv) { return false; }

    if (this->getLength() > rhv.getLength()) {
        return true;
    }

    if (this->getLength() == rhv.getLength()) {
        const int limit = BUFFER - getLength();
        for (int i = limit; i < BUFFER; ++i) {
            if (integer_[i] > rhv.integer_[i]) {
                return true;
            } else if (integer_[i] < rhv.integer_[i]) {
                return false;
            }
        }
    }
    return false;
}

HugeInt
HugeInt::cutOfTheEnd(const int size)
{
    HugeInt temp;
    const int limit = this->getSize();

    for (int i = BUFFER - 1; i >= limit || 0 < i - size; --i) {
        temp.integer_[i] = this->integer_[i - size];
    }
    return temp;
}

HugeInt
HugeInt::insertFromTheEnd(const HugeInt& rhv, const int size);
{
    
}

#include "headers/HugeInt.hpp"
#include <gtest/gtest.h>

#define h_int HugeInt

TEST(HugeIntTest, isEqualToTest)
{
    h_int number1 = 123456;
    h_int number2 = "123456";

    EXPECT_TRUE(number1 == number2);
    EXPECT_TRUE(number1 == 123456);
    EXPECT_TRUE(number1 == "123456");
    EXPECT_FALSE(number1 == 123456789);
    EXPECT_FALSE(number1 == "123");
}

TEST(HugeIntTest, isNotEqualToTest)
{
    h_int number1 = 123456789;
    h_int number2 = "987654321";

    EXPECT_TRUE(number1 != number2);
    EXPECT_TRUE(number1 != 987654321);
    EXPECT_TRUE(number1 != "987654321");
    EXPECT_FALSE(number1 != 123456789);
    EXPECT_FALSE(number1 != "123456789");
}

TEST(HugeIntTest, isLessThanTest)
{
    h_int number1 = 123456789;
    h_int number2 = "987654321";

    EXPECT_TRUE(number1 < number2);
    EXPECT_TRUE(number1 < 987654321);
    EXPECT_TRUE(number1 < "987654321");
    EXPECT_FALSE(number2 < number1);
    EXPECT_FALSE(number2 < 123456789);
    EXPECT_FALSE(number2 < "123456789");
}

TEST(HugeIntTest, isGreaterThanTest)
{
    h_int number1 = 987654321;
    h_int number2 = "123456789";

    EXPECT_TRUE(number1 > number2);
    EXPECT_TRUE(number1 > 123456789);
    EXPECT_TRUE(number1 > "1234567");
    EXPECT_FALSE(number2 > number1);
    EXPECT_FALSE(number1 > 999999999);
    EXPECT_FALSE(number1 > "100000000000000");
}

TEST(HugeIntTest, isLessThanOrEqualToTest)
{
    h_int number1 = 123456789;
    h_int number2 = "987654321";

    EXPECT_TRUE(number1 <= number2);
    EXPECT_TRUE(number1 <= 987654321);
    EXPECT_TRUE(number1 <= "987654321");
    EXPECT_TRUE(number1 <= 123456789);
    EXPECT_TRUE(number2 <= "987654321");
    EXPECT_FALSE(number2 <= number1);
    EXPECT_FALSE(number2 <= 123456789);
    EXPECT_FALSE(number2 <= "123456789");
}

TEST(HugeIntTest, isGreaterThanOrEqualToTest)
{
    h_int number1 = 987654321;
    h_int number2 = "123456789";

    EXPECT_TRUE(number1 >= number2);
    EXPECT_TRUE(number1 >= 123456789);
    EXPECT_TRUE(number1 >= "987654321");
    EXPECT_FALSE(number2 >= number1);
    EXPECT_FALSE(number1 >= 999999999);
    EXPECT_FALSE(number1 >= "100000000000000");
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


#include "headers/Complex.hpp"
#include <gtest/gtest.h>

TEST(ComplexTest, equalityTest)
{
    Complex number1(5.5, 3.4);
    Complex number2(number1);

    EXPECT_TRUE(number1 == number2);
    EXPECT_FALSE(number1 != number2);

    Complex number3 = number1 + number2;

    EXPECT_TRUE(number3 != number2);
    EXPECT_FALSE(number3 == number2);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


#include "headers/Complex.hpp"
#include <iostream>

int
main()
{
    Complex x;
    Complex y(4.3, 8.2);
    Complex z(3.3, 1.1);

    std::cout << "x: " << x << std::endl;
    std::cout << "y: " << y << std::endl;
    std::cout << "z: " << z << std::endl;

    x = y + z;
    std::cout << "\nx = y + z:\n" << x << " = " << y << " + " << z << std::endl;

    x = y - z;
    std::cout << "\nx = y - z:\n" << x << " = " << y << " - " << z << std::endl;

    x = y * z;
    std::cout << "\nx = y * z:\n" << x << " = " << y << " * " << z << std::endl;

    return 0;
}


#include "headers/Complex.hpp"
#include <iostream>

std::istream&
operator>>(std::istream& input, Complex& rhv)
{
    input >> rhv.real_ >> rhv.imaginary_;
    return input;
}

std::ostream&
operator<<(std::ostream& output, const Complex& rhv)
{
    output << '(' << rhv.real_ << ", " << rhv.imaginary_ << ')';
    return output;
}

Complex::Complex(const double realPart, const double imaginaryPart)
    : real_(realPart), imaginary_(imaginaryPart)
{
}

Complex 
Complex::operator+(const Complex& rhv) const
{
    return Complex(this->real_ + rhv.real_, this->imaginary_ + rhv.imaginary_);
}

Complex
Complex::operator-(const Complex& rhv) const
{
    return Complex(this->real_ - rhv.real_, this->imaginary_ - rhv.imaginary_);
}

Complex
Complex::operator*(const Complex& rhv) const
{
    /// (a + bi) * (c + di) = ac + bci + adi +bdi^2 = (ac + bdi^2) + (bc + ad)i = (ac - bd) + (bc + ad)i
    return Complex(this->real_ * rhv.real_ - this->imaginary_ * rhv.imaginary_, this->imaginary_ * rhv.real_ + this->real_ * rhv.imaginary_);
}

bool
Complex::operator==(const Complex& rhv) const
{
    return (this->real_ == rhv.real_ && this->imaginary_ == rhv.imaginary_);
}

bool
Complex::operator!=(const Complex& rhv) const
{
    return !(*this == rhv);
}


#include "headers/DoubleSubscriptedArray.hpp"
#include <gtest/gtest.h>

TEST(DoubleSubscriptedArrayTest, DefaultConstTest)
{
    DoubleSubscriptedArray box;
    const size_t row = box.getRowCount();
    const size_t col = box.getColumnCount();
    EXPECT_EQ(static_cast<int>(row), 3);
    EXPECT_EQ(static_cast<int>(col), 3);

    for (size_t i = 0; i < row; ++i) {
        for(size_t j = 0; j < col; ++j) {
            EXPECT_EQ(box(i, j), 0);
        }
    }
}

TEST(DoubleSubscriptedArrayTest, CopyConstTest)
{
    DoubleSubscriptedArray box(5, 6);
    DoubleSubscriptedArray copy(box);

    const size_t row = copy.getRowCount();
    const size_t col = copy.getColumnCount();
    EXPECT_EQ(static_cast<int>(row), 5);
    EXPECT_EQ(static_cast<int>(col), 6);

    for (size_t i = 0; i < row; ++i) {
        for(size_t j = 0; j < col; ++j) {
            EXPECT_EQ(copy(i, j), 0);
        }
    }
}

TEST(DoubleSubscriptedArrayTest, operatorTest)
{
    DoubleSubscriptedArray array1;
    DoubleSubscriptedArray array2(4, 4);

    EXPECT_TRUE(array1 != array2);
    EXPECT_FALSE(array1 == array2);

    array1 = array2;
    EXPECT_TRUE(array1 == array2);
    EXPECT_FALSE(array1 != array2);

    for (size_t i = 0; i < 4; ++i) {
        for(size_t j = 0; j < 4; ++j) {
            array1(i, j) = i * j;
        }
    }

    EXPECT_TRUE(array1 != array2);
    EXPECT_FALSE(array1 == array2);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


#include "headers/DoubleSubscriptedArray.hpp"
#include <iostream>

int
main()
{
    DoubleSubscriptedArray defaultArray;
    std::cout << "Array created by default constructor:" << std::endl;
    std::cout << defaultArray;
    
    for (size_t i = 0; i < defaultArray.getRowCount(); ++i) {
        for (size_t j = 0; j < defaultArray.getColumnCount(); ++j) {
            defaultArray(i, j) = i + j;
        }
    }
    
    std::cout << "Array after changes." << std::endl;
    std::cout << defaultArray;
    std::cout << "defaultArray[2][2] = " << defaultArray(2, 2) << std::endl;

    DoubleSubscriptedArray newArray = defaultArray;

    if (newArray == defaultArray) {
        std::cout << "newArray is equal to defaultArray." << std::endl;
    }

    newArray(1,1) = 9;

    if (newArray != defaultArray) {
        std::cout << "newArray is not equal to defaultArray." << std::endl;
    }

    newArray = defaultArray;
    if (newArray == defaultArray) {
        std::cout << "newArray is again equal to defaultArray." << std::endl;
    }
    
    return 0;
}


#ifndef __DOUBLE_SUBSCRIPTED_ARRAY_HPP__
#define __DOUBLE_SUBSCRIPTED_ARRAY_HPP__

#include <iostream>
#include <cstddef>

class DoubleSubscriptedArray
{
    friend std::istream& operator>>(std::istream& input, DoubleSubscriptedArray& in);
    friend std::ostream& operator<<(std::ostream& output, const DoubleSubscriptedArray& outputArray);
public:
    DoubleSubscriptedArray(const size_t row = 3, const size_t column = 3);
    DoubleSubscriptedArray(const DoubleSubscriptedArray& rhv);
    ~DoubleSubscriptedArray();

    size_t getRowCount() const { return row_; };
    size_t getColumnCount() const { return column_; };

    bool operator==(const DoubleSubscriptedArray& rhv) const;
    bool operator!=(const DoubleSubscriptedArray& rhv) const;

    const DoubleSubscriptedArray& operator=(const DoubleSubscriptedArray& rhv);

    int& operator()(const size_t rowIndex, const size_t columnIndex);
    const int& operator()(const size_t rowIndex, const size_t columnIndex) const;

private:
    void validateIndex(const size_t rowIndex, const size_t columnIndex) const;

private:
    size_t row_;
    size_t column_;
    int* array_;

};

#endif ///__DOUBLE_SUBSCRIPTED_ARRAY_HPP__


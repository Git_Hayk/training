#include "headers/DoubleSubscriptedArray.hpp"
#include <iostream>
#include <iomanip>

std::istream&
operator>>(std::istream& input, DoubleSubscriptedArray& in)
{
    const size_t row = in.getRowCount();
    const size_t col = in.getColumnCount();
    for (size_t i = 0; i < row * col; ++i) {
        input >> in.array_[i];
    }
    return input;
}

std::ostream&
operator<<(std::ostream& output, const DoubleSubscriptedArray& out)
{
    const size_t row = out.getRowCount();
    const size_t col = out.getColumnCount();
    const size_t limit = row * col;
    output << "{ " << std::endl;
    for (size_t i = 0; i < limit; ++i) {
        if (i % col == 0) {
            output << std::setw(4) << "{ ";
        }
        output << out.array_[i] << ", ";
        if (i != 0 && (i + 1) % col == 0) {
            output << "}," << std::endl;
        }
    }
    output << '}' << std::endl;
    return output;
}

DoubleSubscriptedArray::DoubleSubscriptedArray(const size_t row, const size_t column)
    : row_(row), column_(column), array_(new int[row_ * column_])
{
    for (size_t i = 0; i < row_ * column_; ++i) {
        array_[i] = 0;
    }
}

DoubleSubscriptedArray::DoubleSubscriptedArray(const DoubleSubscriptedArray& rhv)
    : row_(rhv.row_), column_(rhv.column_), array_(new int[row_ * column_])
{
    for (size_t i = 0; i < row_ * column_; ++i) {
        array_[i] = rhv.array_[i];
    }
}

DoubleSubscriptedArray::~DoubleSubscriptedArray()
{
    if (array_ != NULL) {
        delete [] array_;
        array_ = NULL;
    }
}

bool
DoubleSubscriptedArray::operator==(const DoubleSubscriptedArray& rhv) const
{
    if (this->row_ != rhv.row_ || this->column_ != rhv.column_) { return false; }
    
    for (size_t i = 0; i < row_ * column_; ++i) {
        if (this->array_[i] != rhv.array_[i]) {
            return false;
        }
    }
    return true;
}

bool
DoubleSubscriptedArray::operator!=(const DoubleSubscriptedArray& rhv) const
{
    return !(*this == rhv);
}

const DoubleSubscriptedArray&
DoubleSubscriptedArray::operator=(const DoubleSubscriptedArray& rhv)
{
    if (this == &rhv) { return *this; }

    delete [] array_;
    row_ = rhv.getRowCount();
    column_ = rhv.getColumnCount();

    array_ = new int[row_ * column_];
    for (size_t i = 0; i < row_ * column_; ++i) {
        array_[i] = rhv.array_[i];
    }
    return *this;
}

int&
DoubleSubscriptedArray::operator()(const size_t rowIndex, const size_t columnIndex)
{
    validateIndex(rowIndex, columnIndex);
    const int realIndex = rowIndex * column_ + columnIndex;
    return array_[realIndex];
}

const int&
DoubleSubscriptedArray::operator()(const size_t rowIndex, const size_t columnIndex) const
{
    validateIndex(rowIndex, columnIndex);
    const int realIndex = rowIndex * column_ + columnIndex;
    return array_[realIndex];
}

void
DoubleSubscriptedArray::validateIndex(const size_t rowIndex, const size_t columnIndex) const
{
    if (rowIndex < 0 || rowIndex >= row_) {
        std::cerr << "Error 1: Row index [" << rowIndex << "] is out of range!" << std::endl;
        ::exit(1);
    }
    if (columnIndex < 0 || columnIndex >= column_) {
        std::cerr << "Error 2: Column index [" << columnIndex << "] is out of range!" << std::endl;
        ::exit(2);
    }
}


#include "headers/Rational.hpp"
#include <iostream>
#include <iomanip>

int
gcd(int number1, int number2) /// greatest common divisor
{
    number1 = std::abs(number1);
    number2 = std::abs(number2);

    while (true) {
        const int remainder = number1 % number2;
        
        if (0 == remainder) {
            return number2;
        }
        number1 = number2;
        number2 = remainder;
    }
    return 0;
}

int
lcm(const int number1, const int number2) /// least common multiple
{
    return std::abs(number1 * number2) / gcd(number1, number2);
}

Rational::Rational(int numerator, int denominator)
{
    const int greatestCommonDivisor = gcd(numerator, denominator);
    
    if (greatestCommonDivisor != 1) {
        numerator /= greatestCommonDivisor;
        denominator /= greatestCommonDivisor;
    }

    numerator_ = numerator;
    denominator_ = denominator;
}

Rational
Rational::add(const Rational& other)
{

    const int denominator = lcm(this->denominator_, other.denominator_);
    const int numerator = (denominator / this->denominator_) * this->numerator_ +
                     (denominator / other.denominator_) * other.numerator_;

    const Rational sum(numerator, denominator);
    return sum;
}

Rational
Rational::substract(const Rational& other)
{

    const int denominator = lcm(this->denominator_, other.denominator_);
    const int numerator = (denominator / this->denominator_) * this->numerator_ -
                     (denominator / other.denominator_) * other.numerator_;

    const Rational difference(numerator, denominator);
    return difference;
}

Rational
Rational::multiply(const Rational& other)
{
    const int numerator = this->numerator_ * other.numerator_;
    const int denominator = this->denominator_ * other.denominator_;

    const Rational multiply(numerator, denominator);
    return multiply;
}

Rational
Rational::divide(const Rational& other)
{
    const int numerator = this->denominator_ * other.numerator_;
    const int denominator = this->numerator_ * other.denominator_;

    const Rational division(numerator, denominator);
    return division;
}

void
Rational::print()
{
    std::cout << numerator_ << '/' << denominator_ << std::endl;
}

void
Rational::printFloatingPointFormat()
{
    const double result = static_cast<double>(numerator_) / static_cast<double>(denominator_);
    std::cout << std::fixed << std::setprecision(2) << result << std::endl;
}


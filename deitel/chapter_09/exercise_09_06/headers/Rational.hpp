#ifndef __RATIONAL_HPP__
#define __RATIONAL_HPP__

class Rational
{
public:
    Rational(int numerator = 1, int denominator = 2);
    Rational add(const Rational& other);
    Rational substract(const Rational& other);
    Rational multiply(const Rational& other);
    Rational divide(const Rational& other);
    
    void print();
    void printFloatingPointFormat();
private:
    int numerator_;
    int denominator_;
};

#endif /// __RATIONAL_HPP__

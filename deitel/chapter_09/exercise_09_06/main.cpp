#include "headers/Rational.hpp"
#include <iostream>

int
main()
{
    Rational number1;
    Rational number2(3,4);
    Rational number3(5,15);
    Rational number4(12,12);
    
    std::cout << "number1 = ";
    number1.print();
    std::cout << "number2 = ";
    number2.print();
    std::cout << "number3 = ";
    number3.print();
    std::cout << "number4 = ";
    number4.print();

    std::cout << "Floating point number1 = ";
    number1.printFloatingPointFormat();
    std::cout << "Floating point number2 = ";
    number2.printFloatingPointFormat();
    std::cout << "Floating point number3 = ";
    number3.printFloatingPointFormat();
    std::cout << "Floating point number4 = ";
    number4.printFloatingPointFormat();

    Rational sum1 = number1.add(number2);
    std::cout << "number1 + number2 = ";
    sum1.print();
    Rational sum2 = number2.add(number3);
    std::cout << "number2 + number3 = ";
    sum2.print();

    Rational dif1 = number2.substract(number3);
    std::cout << "number2 - number3 = ";
    dif1.print();
    Rational dif2 = number3.substract(number2);
    std::cout << "number3 - number2 = ";
    dif2.print();

    Rational mult1 = number1.multiply(number3);
    std::cout << "number1 * number3 = ";
    mult1.print();
    Rational mult2 = number2.multiply(number2);
    std::cout << "number2 * number2 = ";
    mult2.print();

    Rational div1 = number1.divide(number3);
    std::cout << "number1 / number3 = ";
    div1.print();
    Rational div2 = number2.divide(number2);
    std::cout << "number2 / number2 = ";
    div2.print();

    return 0;
}


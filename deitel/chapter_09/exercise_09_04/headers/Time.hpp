#ifndef __TIME_HPP__
#define __TIME_HPP__

class Time
{
public:
   Time(); // default constructor
   Time(const int hour, const int minute, const int second);

   // set functions
   void setTime(const int hour, const int minute, const int second);
   void setHour(const int hour);
   void setMinute(const int minute);
   void setSecond(const int second);

   // get functions
   int getHour();
   int getMinute();
   int getSecond();

   void printUniversal(); // output time in universal-time format
   void printStandard(); // output time in standard-time format
private:
   int hour_;
   int minute_;
   int second_;
};

#endif /// __TIME_HPP__


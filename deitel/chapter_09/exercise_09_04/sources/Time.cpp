#include "../headers/Time.hpp"

#include <iostream>
#include <iomanip>
#include <ctime>

Time::Time()
{
   int currentTime = std::time(NULL);
   const int GMT = 4;

   const int second = currentTime % 60;
   currentTime = currentTime / 60;
   const int minute = currentTime % 60;
   currentTime = currentTime / 60;
   const int hour = (currentTime % 24) + GMT;
   setTime(hour, minute, second);
}

Time::Time(const int hour, const int minute, const int second)
{
    setTime(hour, minute, second);
}

void
Time::setTime(const int hour, const int minute, const int second)
{
   setSecond(second);
   setMinute(minute);
   setHour(hour);
}

void
Time::setHour(const int hour)
{
   hour_ = (hour >= 0 && hour < 24) ? hour : 0;
}

void
Time::setMinute(const int minute)
{
   minute_ = (minute >= 0 && minute < 60) ? minute : 0;
}

void
Time::setSecond(const int second)
{
   second_ = (second >= 0 && second < 60) ? second : 0;
}

int
Time::getHour()
{
   return hour_;
}

int
Time::getMinute()
{
   return minute_;
}

int
Time::getSecond()
{
   return second_;
}

void
Time::printUniversal()
{
   const char previous = std::cout.fill('0');
   std::cout << std::setw(2) << getHour() << ":"
      << std::setw(2) << getMinute() << ":" << std::setw(2) << getSecond();
   std::cout.fill(previous);
}

void
Time::printStandard()
{
   const char previous = std::cout.fill('0');
   std::cout << ((getHour() == 0 || getHour() == 12 ) ? 12 : getHour() % 12)
      << ":" << std::setw(2) << getMinute()
      << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM");
   std::cout.fill(previous);
}


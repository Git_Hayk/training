#include "headers/Time.hpp"

#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
   Time time1;

   std::cout << "Current time Universal: ";
   time1.printUniversal();
   std::cout << "\nCurrent time Standard: ";
   time1.printStandard();
   std::cout << '\n';

   int hour, minute, second;
   if (isInteractive) { std::cout << "Input hour: "; }
   std::cin >> hour;
   if (isInteractive) { std::cout << "Input minute: "; }
   std::cin >> minute;
   if (isInteractive) { std::cout << "Input second: "; }
   std::cin >> second;
   Time time2(hour, minute, second);

   std::cout << "Time established by hand Universal: ";
   time2.printUniversal();
   std::cout << "\nTime established by hand Standard: ";
   time2.printStandard();
   std::cout << '\n';
   return 0;
}


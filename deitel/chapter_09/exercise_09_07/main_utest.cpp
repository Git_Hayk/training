#include "headers/Time.hpp"

#include <gtest/gtest.h>

TEST(TestTimeTick, nextSecond)
{
   Time t(9, 34, 25);
   t.tick();

   EXPECT_EQ(t.getSecond(), 26);
}

TEST(TestTimeTick, nextMinute)
{
   Time t(9, 34, 59);
   t.tick();

   EXPECT_EQ(t.getMinute(), 35);
   EXPECT_EQ(t.getSecond(), 0);
}

TEST(TestTimeTick, nextHour)
{
   Time t(9, 59, 59);
   t.tick();

   EXPECT_EQ(t.getHour(), 10);
   EXPECT_EQ(t.getMinute(), 0);
   EXPECT_EQ(t.getSecond(), 0);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


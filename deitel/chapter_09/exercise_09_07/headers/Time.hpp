#ifndef __TIME_HPP__
#define __TIME_HPP__

class Time
{
public:
    Time();
    Time(const int hour, const int minute, const int second);
    
    void tick();

    // set functions
    void setTime(const int hour, const int minute, const int second);
    void setHour(const int hour);
    void setMinute(const int minute);
    void setSecond(const int second);

    // get functions
    int getHour() const;
    int getMinute() const;
    int getSecond() const;

    void printUniversal() const;
    void printStandard() const;
private:
    int hour_;
    int minute_;
    int second_;
};

#endif /// __TIME_HPP__

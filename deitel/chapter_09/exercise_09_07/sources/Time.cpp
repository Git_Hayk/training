#include "headers/Time.hpp"

#include <iostream>
#include <iomanip>
#include <unistd.h>

Time::Time()
{
    int currentTime = std::time(NULL);
    const int GMT = 4;
    
    const int second = currentTime % 60;
    currentTime = currentTime / 60;
    const int minute = currentTime % 60;
    currentTime = currentTime / 60;
    const int hour = (currentTime % 24) + GMT;
    setTime(hour, minute, second);
}

Time::Time(const int hour, const int minute, const int second)
{
    setTime(hour, minute, second);
}

void
Time::tick()
{
    ++second_;
    if (60 == second_) {
        second_ = 0;
        ++minute_;
        if (60 == minute_) {
            minute_ = 0;
            ++hour_;
            if (24 == hour_) {
                hour_ = 0;
            }
        }
    }
    printStandard();
    ///sleep(1);
    ///system("clear");
}

void
Time::setTime(const int hour, const int minute, const int second)
{
    setHour(hour);
    setMinute(minute);
    setSecond(second);
}

void
Time::setHour(const int hour)
{
    hour_ = (hour >= 0 && hour < 24) ? hour : 0;
}

void
Time::setMinute(const int minute)
{
    minute_ = (minute >= 0 && minute < 60 ) ? minute : 0;
}

void
Time::setSecond(const int second)
{
    second_ = (second >= 0 && second < 60) ? second : 0;
}

int
Time::getHour() const
{
    return hour_;
}

int
Time::getMinute() const
{
    return minute_;
}

int
Time::getSecond() const
{
    return second_;
}

void
Time::printUniversal() const
{
    const char previousFill = std::cout.fill('0');
    std::cout << std::setw(2) << getHour() << ":"
       << std::setw(2) << getMinute() << ":" << std::setw(2) << getSecond();
    std::cout.fill(previousFill);
}

void
Time::printStandard() const
{
    const char previousFill = std::cout.fill('0');
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
       << ":" << std::setw(2) << getMinute()
       << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM") << std::endl;
    std::cout.fill(previousFill);
}


#include "headers/Time.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    int hour, minute, second;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input time in HH:MM:SS format: ";
    }
    std::cin >> hour >> minute >> second;
    Time time1(hour, minute, second);
    for (int i = 0; i < 65; ++i) {
        time1.tick();
    }

    return 0;
}


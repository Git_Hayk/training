#include "headers/Complex.hpp"
#include <iostream>

Complex::Complex(const double realPart, const double imaginaryPart)
{
    setRealPart(realPart);
    setImaginaryPart(imaginaryPart);
}

void
Complex::setRealPart(const double realPart)
{
    realPart_ = realPart;
}

void
Complex::setImaginaryPart(const double imaginaryPart)
{
    imaginaryPart_ = imaginaryPart;
}

Complex
Complex::add(const Complex& other)
{
    const double realPart = this->realPart_ + other.realPart_;
    const double imaginaryPart = this->imaginaryPart_ + other.imaginaryPart_;
    const Complex sum(realPart, imaginaryPart);
    return sum;
}

Complex
Complex::substract(const Complex& other)
{
    const double realPart = this->realPart_ - other.realPart_;
    const double imaginaryPart = this->imaginaryPart_ - other.imaginaryPart_;
    const Complex subtract(realPart, imaginaryPart);
    return subtract;
}
    
void
Complex::print() const
{
    std::cout << '(' << realPart_ << ", " << imaginaryPart_ << ')' << std::endl;
}


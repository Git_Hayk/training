#include "headers/Complex.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    double realPart1, imaginaryPart1;
    if (isInteractive) { std::cout << "Input complex number1 real and imahinary parts: "; }
    std::cin >> realPart1 >> imaginaryPart1;
    Complex number1(realPart1, imaginaryPart1);

    double realPart2, imaginaryPart2;
    if (isInteractive) { std::cout << "Input complex number2 real and imahinary parts: "; }
    std::cin >> realPart2 >> imaginaryPart2;
    Complex number2(realPart2, imaginaryPart2);

    std::cout << "Complex number1 = ";
    number1.print();
    std::cout << "Complex number2 = ";
    number2.print();

    const Complex sum = number1.add(number2);
    std::cout << "Sum of number1 and number2 is ";
    sum.print();

    const Complex difference = number1.substract(number2);
    std::cout << "Difference of number1 and number2 is ";
    difference.print();

    return 0;
}


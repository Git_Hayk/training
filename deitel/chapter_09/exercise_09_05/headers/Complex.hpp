#ifndef __COMPLEX_HPP__
#define __COMPLEX_HPP__

class Complex
{
public:
    Complex(const double realPart = 1, const double imaginaryPart = 2);

    void setRealPart(const double realPart);
    void setImaginaryPart(const double imaginaryPart);

    Complex add(const Complex& other);
    Complex substract(const Complex& other);
    void print() const;

private:
    double realPart_;
    double imaginaryPart_;
};

#endif /// __COMPLEX_HPP__

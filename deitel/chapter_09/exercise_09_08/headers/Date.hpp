#ifndef __DATE_HPP__
#define __DATE_HPP__


class Date
{
private:
    const static int MONTH_COUNT = 12;
    const static int monthDays_[MONTH_COUNT];

public:
    Date(const int month = 1, const int day = 1, const int year = 2000);
    void printDate();
    void nextDay();
    bool isLeapYear() const;

    void setMonth(const int month);
    void setDay(const int day);
    void setYear(const int year);
    
    int getLastDayOfMonth() const;
    int getMonth() const { return month_; };
    int getDay() const {return day_; };
    int getYear() const { return year_; };
private:
    int month_;
    int day_;
    int year_;
};

#endif /// __DATE_HPP__


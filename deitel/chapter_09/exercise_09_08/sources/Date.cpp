#include "headers/Date.hpp"
#include <iostream>
#include <iomanip>

const int
Date::monthDays_[MONTH_COUNT] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

Date::Date(const int month, const int day, const int year)
{
    setYear(year);
    setMonth(month);
    setDay(day);
}

void
Date::nextDay()
{
    ++day_;
    if (day_ > getLastDayOfMonth()) {
        day_ = 1;
        ++month_;
        if (month_ > MONTH_COUNT) {
            month_ = 1;
            ++year_;
        }
    }
}

bool
Date::isLeapYear() const
{
    return ((0 == year_ % 4 && 0 != year_ % 100) || (0 == year_ % 400));
}

int
Date::getLastDayOfMonth() const
{
    /// february 29.
    return (isLeapYear() && 2 == month_) ? 29 : monthDays_[month_ - 1];
}

void
Date::setMonth(const int month)
{
    month_ = (month > 0 && month <= MONTH_COUNT) ? month : 1;
}

void
Date::setDay(const int day)
{
    day_ = (day > 0 && day <= getLastDayOfMonth()) ? day : 1;
}

void
Date::setYear(const int year)
{
    year_ = (year >= 0) ? year : 1970;
}

void Date::printDate()
{
    const char previousFill = std::cout.fill('0');
    std::cout << std::setw(2) << getMonth() << '/' << std::setw(2) << getDay() << '/' << getYear() << std::endl;
    std::cout.fill(previousFill);
}


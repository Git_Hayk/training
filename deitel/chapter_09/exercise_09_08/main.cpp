#include "headers/Date.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    int month, day, year;
    if (isInteractive) {
        std::cout << "Input date in MM/DD/YY format: ";
    }
    std::cin >> month >> day >> year;
    Date date1(month, day, year);

    std::cout << "Inputed date is` ";
    date1.printDate();
    
    date1.nextDay();
    std::cout << "Next day is` ";
    date1.printDate();

    return 0;
}


#include "headers/Date.hpp"
#include <gtest/gtest.h>

TEST(DateTest, DefaultConst)
{
    Date date1;
    EXPECT_EQ(date1.getMonth(), 1);
    EXPECT_EQ(date1.getDay(), 1);
    EXPECT_EQ(date1.getYear(), 2000);
}

TEST(DateTest, setYear)
{
    Date date2(2, 10, -50);
    EXPECT_EQ(date2.getYear(), 1970);
}

TEST(DateTest, nextDayMonth)
{
    Date date3(7, 31, 2022);
    date3.nextDay();
    EXPECT_EQ(date3.getMonth(), 8);
    EXPECT_EQ(date3.getDay(), 1);
    EXPECT_EQ(date3.getYear(), 2022);
}

TEST(DateTest, nextDayYear)
{
    Date date4(12, 31, 2022);
    date4.nextDay();
    EXPECT_EQ(date4.getMonth(), 1);
    EXPECT_EQ(date4.getDay(), 1);
    EXPECT_EQ(date4.getYear(), 2023);
}

TEST(DateTest, nextDayNotLeap)
{
    Date date5(2, 28, 2022);
    date5.nextDay();
    EXPECT_EQ(date5.getMonth(), 3);
    EXPECT_EQ(date5.getDay(), 1);
    EXPECT_EQ(date5.getYear(), 2022);
}

TEST(DateTest, nextDayLeap)
{
    Date date6(2, 28, 2020);
    date6.nextDay();
    EXPECT_EQ(date6.getMonth(), 2);
    EXPECT_EQ(date6.getDay(), 29);
    EXPECT_EQ(date6.getYear(), 2020);
    date6.nextDay();
    EXPECT_EQ(date6.getMonth(), 3);
    EXPECT_EQ(date6.getDay(), 1);
}

TEST(DateTest, lastDayOfMonth)
{
    Date date(9, 29, 1995);
    EXPECT_EQ(date.getLastDayOfMonth(), 30);

    date.setMonth(1);
    EXPECT_EQ(date.getLastDayOfMonth(), 31);

    date.setMonth(2);
    EXPECT_EQ(date.getLastDayOfMonth(), 28);

    date.setYear(2020);
    EXPECT_EQ(date.getLastDayOfMonth(), 29);

    date.setMonth(8);
    EXPECT_EQ(date.getLastDayOfMonth(), 31);
}

TEST(DateTest, isLeapYear)
{
    Date date(7, 27, 2022);
    
    EXPECT_FALSE(date.isLeapYear());

    date.setYear(2020);
    EXPECT_TRUE(date.isLeapYear());

    date.setYear(2000);
    EXPECT_TRUE(date.isLeapYear());

    date.setYear(1900);
    EXPECT_FALSE(date.isLeapYear());

    date.setYear(1600);
    EXPECT_TRUE(date.isLeapYear());
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


#include "headers/Rectangle.hpp"
#include <iostream>

Rectangle::Rectangle()
{
    setLength();
    setWidth();
}

Rectangle::Rectangle(const double length, const double width)
{
    setLength(length);
    setWidth(width);
}

double
Rectangle::perimeter() const
{
    return (length_ + width_) * 2;
}

double
Rectangle::area() const
{
    return length_ * width_;
}

void
Rectangle::setLength(const double length)
{
    length_ = (length > 0.0 && length < 20.0) ? length : 1;
}

void
Rectangle::setWidth(const double width)
{
    width_ = (width > 0.0 && width < 20.0) ? width : 1;
}


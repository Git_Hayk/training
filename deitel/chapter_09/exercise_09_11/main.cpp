#include "headers/Rectangle.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    const Rectangle rectangle1;
    std::cout << "Rectangle 1 with default arguments:"
              << "\n\tPerimeter: " << rectangle1.perimeter()
              << "\n\tArea: " << rectangle1.area() << std::endl;

    double length, width;
    if (isInteractive) { std::cout << "\nInput length for rectangle 2: "; }
    std::cin >> length;
    if (isInteractive) { std::cout << "Input width for rectangle 2: "; }
    std::cin >> width;

    const Rectangle rectangle2(length, width);
    std::cout << "Rectangle 2 with inputed arguments:"
              << "\n\tPerimeter: " << rectangle2.perimeter()
              << "\n\tArea: " << rectangle2.area() << std::endl;

    return 0;
}


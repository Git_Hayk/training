#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

class Rectangle
{
public:
    Rectangle();
    Rectangle(const double length, const double width);

    double perimeter() const;
    double area() const;

    void setLength(const double length = 1);
    void setWidth(const double width = 1);

private:
    double length_;
    double width_;
};

#endif /// __RECTANGLE_HPP__


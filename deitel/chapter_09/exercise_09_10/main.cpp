#include "headers/Time.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    int hour, minute, second;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input time in HH:MM:SS format: ";
    }
    std::cin >> hour >> minute >> second;
    Time time1(hour, minute, second);

    const int errorIndicator = time1.getErrorIndicator();
    if (errorIndicator) {
        std::cerr << "Error " << errorIndicator << ": Invalid ";
        switch (errorIndicator) {
        case 0: assert(0 && "Error indicator is zero!"); break;
        case 1: std::cerr << "hour!" << std::endl; return errorIndicator;
        case 2: std::cerr << "minute!" << std::endl; return errorIndicator;
        case 3: std::cerr << "second!" << std::endl; return errorIndicator;
        default: assert(0 && "Unknown error!");
        }
    }
    time1.printStandard();

    return 0;
}


#include "headers/Time.hpp"

#include <iostream>
#include <iomanip>
#include <unistd.h>

Time::Time(const int hour, const int minute, const int second)
{
    setTime(hour, minute, second);
}

void
Time::setTime(const int hour, const int minute, const int second)
{
    setHour(hour);
    if (getErrorIndicator()) { return; }
    setMinute(minute);
    if (getErrorIndicator()) { return; }
    setSecond(second);
}

void
Time::setHour(const int hour)
{
    const bool result = (hour >= 0 && hour < 24);
    hour_ = result ? hour : 0;
    setErrorIndicator(result ? 0 : 1);
}

void
Time::setMinute(const int minute)
{
    const bool result = (minute >= 0 && minute < 60);
    minute_ = result ? minute : 0;
    setErrorIndicator(result ? 0 : 2);
}

void
Time::setSecond(const int second)
{
    const bool result = (second >= 0 && second < 60);
    second_ = result ? second : 0;
    setErrorIndicator(result ? 0 : 3);
}

void
Time::setErrorIndicator(const int indicator)
{
    errorIndicator_ = indicator;
}

int
Time::getErrorIndicator() const
{
    return errorIndicator_;
}

int
Time::getHour() const
{
    return hour_;
}

int
Time::getMinute() const
{
    return minute_;
}

int
Time::getSecond() const
{
    return second_;
}

void
Time::printUniversal() const
{
    const char previousFill = std::cout.fill('0');
    std::cout << std::setw(2) << getHour() << ":"
       << std::setw(2) << getMinute() << ":" << std::setw(2) << getSecond();
    std::cout.fill(previousFill);
}

void
Time::printStandard() const
{
    const char previousFill = std::cout.fill('0');
    std::cout << ((getHour() == 0 || getHour() == 12) ? 12 : getHour() % 12)
       << ":" << std::setw(2) << getMinute()
       << ":" << std::setw(2) << getSecond() << (hour_ < 12 ? " AM" : " PM") << std::endl;
    std::cout.fill(previousFill);
}


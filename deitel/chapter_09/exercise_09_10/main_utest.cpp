#include "headers/Time.hpp"

#include <gtest/gtest.h>

TEST(TestTimeTick, nextSecond)
{
   Time t(9, 34, 25);

   EXPECT_EQ(t.getSecond(), 25);
}

TEST(TestTimeTick, nextMinute)
{
   Time t(9, 34, 59);

   EXPECT_EQ(t.getMinute(), 34);
   EXPECT_EQ(t.getSecond(), 59);
}

TEST(TestTimeTick, getErrorIndicatorTest)
{
   Time time(25, 59, 59);
   EXPECT_EQ(time.getErrorIndicator(), 1);

   time.setMinute(70);
   EXPECT_EQ(time.getErrorIndicator(), 2);

   time.setSecond(70);
   EXPECT_EQ(time.getErrorIndicator(), 3);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


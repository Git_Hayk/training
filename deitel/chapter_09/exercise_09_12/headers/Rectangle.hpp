#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

#include "Point.hpp"

class Rectangle
{
public:
    static bool isRectangle(const Point& point1, const Point& point2,
                            const Point& point3, const Point& point4);
public:
    Rectangle();
    Rectangle(const Point& point1, const Point& point2,
              const Point& point3, const Point& point4);

    void setCoordinates(const Point& point1, const Point& point2,
                        const Point& point3, const Point& point4);

    bool isSquare() const;
    int  perimeter() const;
    int  area() const;
    void printPerimeterAndArea() const;

private:
    Point point1_;
    Point point2_;
    Point point3_;
    Point point4_;
};

#endif /// __RECTANGLE_HPP__


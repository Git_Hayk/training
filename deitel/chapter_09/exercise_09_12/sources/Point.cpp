#include "headers/Point.hpp"

Point::Point()
{
    setCoordinates(0.0, 0.0);
}

Point::Point(const double x, const double y)
{
    setCoordinates(x, y);
}

void
Point::setCoordinates(const double x, const double y)
{
    coordX_ = (x >= 0.0 && x < 20.0) ? x : 0.0;
    coordY_ = (y >= 0.0 && y < 20.0) ? y : 0.0;
}


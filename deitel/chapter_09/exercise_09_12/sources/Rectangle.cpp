#include "headers/Rectangle.hpp"
#include <iostream>
#include <cmath>

Rectangle::Rectangle()
{
    const Point point1(1, 1);
    const Point point2(1, 2);
    const Point point3(2, 2);
    const Point point4(2, 1);
    setCoordinates(point1, point2, point3, point4);
}

Rectangle::Rectangle(const Point& point1, const Point& point2,
                     const Point& point3, const Point& point4)
{
    setCoordinates(point1, point2, point3, point4);
}

void
Rectangle::setCoordinates(const Point& point1, const Point& point2,
                          const Point& point3, const Point& point4)
{
    assert(isRectangle(point1, point2, point3, point4));

    point1_ = point1;
    point2_ = point2;
    point3_ = point3;
    point4_ = point4;

}

int
Rectangle::perimeter() const
{
    return (std::abs(point1_.getY() - point2_.getY()) + std::abs(point3_.getX() - point2_.getX())) * 2;
}

int
Rectangle::area() const
{
    return std::abs((point1_.getY() - point2_.getY()) * (point3_.getX() - point2_.getX()));
}

bool
Rectangle::isSquare() const
{
    return (std::abs(point1_.getY() - point2_.getY()) == std::abs(point3_.getX() - point2_.getX()));
}

bool
Rectangle::isRectangle(const Point& point1, const Point& point2,
                       const Point& point3, const Point& point4)
{
    return (point1.getX() == point2.getX() && point2.getY() == point3.getY() && point3.getX() == point4.getX() && point4.getY() == point1.getY());
}

void
Rectangle::printPerimeterAndArea() const
{
    std::cout << "\tPerimeter: " << perimeter() << std::endl;
    std::cout << "\tArea: " << area() << std::endl;
}


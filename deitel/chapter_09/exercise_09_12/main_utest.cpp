#include "headers/Point.hpp"
#include "headers/Rectangle.hpp"
#include <gtest/gtest.h>

TEST(PointTest, DefaultConst)
{
    Point point;
    EXPECT_EQ(point.getX(), 0);
    EXPECT_EQ(point.getY(), 0);
}

TEST(RectangleTest, DefaultConstSquare)
{
    Rectangle rect;
    EXPECT_TRUE(rect.isSquare());
}

TEST(RectangleTest, isRectangleTest)
{
    Point p1(1,1);
    Point p2(1,4);
    Point p3(4,4);
    Point p4(4,1);

    EXPECT_TRUE(Rectangle::isRectangle(p1, p2, p3, p4));
    
    p1.setCoordinates(1,1);
    p2.setCoordinates(3,4);
    p3.setCoordinates(4,4);
    p4.setCoordinates(4,1);

    EXPECT_FALSE(Rectangle::isRectangle(p1, p2, p3, p4));
}

TEST(RectangleTest, isSquareTest)
{
    Point p1(5.0, 5.0);
    Point p2(5.0, 15.0);
    Point p3(15.0, 15.0);
    Point p4(15.0, 5.0);

    Rectangle rectangle1(p1, p2, p3, p4);

    EXPECT_TRUE(rectangle1.isSquare());

    p1.setCoordinates(3.0, 3.0);
    p2.setCoordinates(3.0, 8.0);
    p3.setCoordinates(15.0, 8.0);
    p4.setCoordinates(15.0, 3.0);

    rectangle1.setCoordinates(p1, p2, p3, p4);

    EXPECT_FALSE(rectangle1.isSquare());
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


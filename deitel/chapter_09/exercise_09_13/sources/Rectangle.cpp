#include "headers/Rectangle.hpp"
#include "headers/Point.hpp"
#include <iostream>
#include <cmath>

Rectangle::Rectangle()
{
    clear();
    point1_.setCoordinates(0.0, HEIGHT - 1.0 - 0.0);
    point2_.setCoordinates(0.0, HEIGHT - 1.0 - 0.0);
    point3_.setCoordinates(0.0, HEIGHT - 1.0 - 0.0);
    point4_.setCoordinates(0.0, HEIGHT - 1.0 - 0.0);
}                               

Rectangle::Rectangle(const Point& point1, const Point& point2,
                     const Point& point3, const Point& point4)
{
    clear();
    setCoordinates(point1, point2, point3, point4);
}

void
Rectangle::setCoordinates(const Point& point1, const Point& point2,
                          const Point& point3, const Point& point4)
{
    assert(isRectangle(point1, point2, point3, point4));

    const double x1 = point1.getX();
    const double x2 = point2.getX();
    const double x3 = point3.getX();
    const double x4 = point4.getX();
    const double y1 = point1.getY();
    const double y2 = point2.getY();
    const double y3 = point3.getY();
    const double y4 = point4.getY();
    /// As in general the constsruction of an array is more like
    /// the fourth quadrant of coordinate system,
    /// coordinates of Y axis should be setted as reversed using for example 
    /// HEIGHT - 1 - y1 instead of y1

    point1_.setCoordinates(x1, HEIGHT - 1 - y1);
    point2_.setCoordinates(x2, HEIGHT - 1 - y2);
    point3_.setCoordinates(x3, HEIGHT - 1 - y3);
    point4_.setCoordinates(x4, HEIGHT - 1 - y4);
}

void
Rectangle::setFillCharacter(const char character)
{
    fillCharacter_ = character;
}

void
Rectangle::setPerimeterCharacter(const char character)
{
    perimeterCharacter_ = character;
}

int
Rectangle::perimeter() const
{
    return (std::abs(point1_.getY() - point2_.getY()) + std::abs(point3_.getX() - point2_.getX())) * 2;
}

int
Rectangle::area() const
{
    return std::abs((point1_.getY() - point2_.getY()) * (point3_.getX() - point2_.getX()));
}

bool
Rectangle::isSquare() const
{
    return (std::abs(point1_.getY() - point2_.getY()) == std::abs(point3_.getX() - point2_.getX()));
}

bool
Rectangle::isRectangle(const Point& point1, const Point& point2,
                       const Point& point3, const Point& point4)
{
    return (point1.getX() == point2.getX() && point2.getY() == point3.getY() && point3.getX() == point4.getX() && point4.getY() == point1.getY());
}

void
Rectangle::printPerimeterAndArea() const
{
    std::cout << "\tPerimeter: " << perimeter() << std::endl;
    std::cout << "\tArea: " << area() << std::endl;
}

void
Rectangle::draw()
{
    /// drawing space borders
    for (int i = 0; i < WIDTH; ++i) {
        field_[0][i] = HORIZONTAL_SIDE;
        field_[WIDTH - 1][i] = HORIZONTAL_SIDE;
    }
    for (int j = 0; j < HEIGHT; ++j) {
        field_[j][0] = VERTICAL_SIDE;
        field_[j][HEIGHT - 1] = VERTICAL_SIDE;
    }
    
    const int x1 = static_cast<int>(point1_.getX());
    const int x2 = static_cast<int>(point2_.getX());
    const int x3 = static_cast<int>(point3_.getX());
    const int x4 = static_cast<int>(point4_.getX());
    const int y1 = static_cast<int>(point1_.getY());
    const int y2 = static_cast<int>(point2_.getY());
    const int y4 = static_cast<int>(point4_.getY());

    /// drawing vertical sides
    for (int i = 0; y1 - i != y2; ++i) {
        field_[y1 - i][x1] = perimeterCharacter_;
        field_[y4 - i][x4] = perimeterCharacter_;
    }
    /// drawing horizontal sides
    for (int j = 0; x1 + j != x4 + 1; ++j) {
        field_[y1][x1 + j] = perimeterCharacter_;
        field_[y2][x2 + j] = perimeterCharacter_;
    }
    /// filling rectangle
    for (int i = y2 + 1; i < y1; ++i) {
        for (int j = x2 + 1; j < x3; ++j) {
            field_[i][j] = fillCharacter_;
        }
    }

    print();
}

void
Rectangle::clear()
{
    for (int i = 0; i < HEIGHT; ++i) {
        for (int j = 0; j < WIDTH; ++j) {
            field_[i][j] = EMPTY_SPACE;
        }
    }
}

void
Rectangle::print()
{
    for (int i = 0; i < HEIGHT; ++i) {
        for (int j = 0; j < WIDTH; ++j) {
            std::cout << ' ' << field_[i][j];
        }
        std::cout << '\n';
    }
}

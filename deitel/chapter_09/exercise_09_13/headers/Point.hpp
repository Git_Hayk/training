#ifndef __POINT_HPP_
#define __POINT_HPP_

class Point
{
public:
    Point();
    Point(const double x, const double y);

    void setCoordinates(const double x, const double y);

    double getX() const { return coordX_; };
    double getY() const { return coordY_; };

private:
    double coordX_;
    double coordY_;
};

#endif /// __POINT_HPP_


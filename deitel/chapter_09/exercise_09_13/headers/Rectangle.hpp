#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

#include "Point.hpp"

class Rectangle
{
    friend class Point;
private:
    const static int WIDTH = 25;
    const static int HEIGHT = 25;
    const static char HORIZONTAL_SIDE = '*';
    const static char VERTICAL_SIDE   = '*';
    const static char EMPTY_SPACE     = ' ';
public:
    static bool isRectangle(const Point& point1, const Point& point2,
                            const Point& point3, const Point& point4);
public:
    Rectangle();
    Rectangle(const Point& point1, const Point& point2,
              const Point& point3, const Point& point4);

    void setCoordinates(const Point& point1, const Point& point2,
                        const Point& point3, const Point& point4);
    void setPerimeterCharacter(const char character);
    void setFillCharacter(const char character);

    int  perimeter() const;
    int  area() const;

    void printPerimeterAndArea() const;
    void draw();
    void clear();
    void print();

    bool isSquare() const;
private:
    Point point1_;
    Point point2_;
    Point point3_;
    Point point4_;
    char fillCharacter_;
    char perimeterCharacter_;
    char field_[HEIGHT][WIDTH];
};

#endif /// __RECTANGLE_HPP__


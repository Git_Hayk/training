#include "headers/Rectangle.hpp"
#include "headers/Point.hpp"
#include <iostream>
#include <unistd.h>

const bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    std::cout << "Rectangle 1 with default coordinates:" << std::endl;
    Rectangle rectangle1;
    rectangle1.setPerimeterCharacter('@');
    rectangle1.setFillCharacter('*');
    rectangle1.draw();
    rectangle1.printPerimeterAndArea();

    if (rectangle1.isSquare()) { std::cout << "\tThis rectangle is square." << std::endl; }

    int x, y;
    if (isInteractive) { std::cout << "Input first point's coordinates (x1, y1): "; }
    std::cin >> x >> y;
    Point point1(x, y);

    if (isInteractive) { std::cout << "Input second point's coordinates (x2, y2): "; }
    std::cin >> x >> y;
    Point point2(x, y);

    if (isInteractive) { std::cout << "Input third point's coordinates (x3, y3): "; }
    std::cin >> x >> y;
    Point point3(x, y);

    if (isInteractive) { std::cout << "Input fourth point's coordinates (x4, y4): "; }
    std::cin >> x >> y;
    Point point4(x, y);

    if (!Rectangle::isRectangle(point1, point2, point3, point4)) {
        std::cout << "Error 1: Not rectangle!" << std::endl;
        ::exit(1);
    }

    std::cout << "Rectangle 2 with inputed coordinates:" << std::endl;
    Rectangle rectangle2(point1, point2, point3, point4);
    rectangle2.printPerimeterAndArea();

    char perimeterCharacter, fillCharacter;
    if (isInteractive) { std::cout << "Input symbols for perimeter and for fill: "; }
    std::cin >> perimeterCharacter >> fillCharacter;

    rectangle2.setPerimeterCharacter(perimeterCharacter);
    rectangle2.setFillCharacter(fillCharacter);
    rectangle2.draw();

    return 0;
}


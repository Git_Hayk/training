#include "headers/BasePlusCommissionEmployee.hpp"
#include <iostream>

BasePlusCommissionEmployee::BasePlusCommissionEmployee(const std::string &first, const std::string& last, const std::string& ssn,
        const double sales, const double rate, const double salary)
{
    setFirstName(first);
    setLastName(last);
    setSocialSecurityNumber(ssn);
    setGrossSales(sales);
    setCommissionRate(rate);
    setBaseSalary(salary);
}

void
BasePlusCommissionEmployee::setFirstName(const std::string& first)
{
    employee_.setFirstName(first);
}

std::string
BasePlusCommissionEmployee::getFirstName() const
{
   return employee_.getFirstName();
}

void
BasePlusCommissionEmployee::setLastName(const std::string& last)
{
    employee_.setLastName(last);
}

std::string
BasePlusCommissionEmployee::getLastName() const
{
   return employee_.getLastName();
}

void
BasePlusCommissionEmployee::setSocialSecurityNumber(const std::string& ssn)
{
    employee_.setSocialSecurityNumber(ssn);
}

std::string
BasePlusCommissionEmployee::getSocialSecurityNumber() const
{
   return employee_.getSocialSecurityNumber();
}

void
BasePlusCommissionEmployee::setGrossSales(double sales)
{
    employee_.setGrossSales(sales);
}

double
BasePlusCommissionEmployee::getGrossSales() const
{
   return employee_.getGrossSales();
}

void
BasePlusCommissionEmployee::setCommissionRate(double rate)
{
    employee_.setCommissionRate(rate);
}

double
BasePlusCommissionEmployee::getCommissionRate() const
{
   return employee_.getCommissionRate();
}

void
BasePlusCommissionEmployee::setBaseSalary(const double salary)
{
   baseSalary_ = (salary < 0.0) ? 0.0 : salary;
}

double
BasePlusCommissionEmployee::getBaseSalary() const
{
   return baseSalary_;
}

double
BasePlusCommissionEmployee::earnings() const
{
   return getBaseSalary() + employee_.earnings();
}

void
BasePlusCommissionEmployee::print() const
{
    std::cout << "base-salaried ";

   employee_.print();                 

   std::cout << "\nbase salary: " << getBaseSalary();
}


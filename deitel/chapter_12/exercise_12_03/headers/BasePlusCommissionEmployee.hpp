#ifndef __BASEPLUSCOMMISSIONEMPLOYEE_HPP__
#define __BASEPLUSCOMMISSIONEMPLOYEE_HPP__

#include "CommissionEmployee.hpp"

class BasePlusCommissionEmployee
{
public:
   BasePlusCommissionEmployee(const std::string& first, const std::string& last, const std::string& ssn,
           const double sales = 0.0, const double rate = 0.0, const double salary = 0.0 );

   void setFirstName(const std::string& first);
   std::string getFirstName() const;

   void setLastName(const std::string& last);
   std::string getLastName() const;

   void setSocialSecurityNumber(const std::string& ssn);
   std::string getSocialSecurityNumber() const;

   void setGrossSales(double sales);
   double getGrossSales() const;

   void setCommissionRate(double rate);
   double getCommissionRate() const;

   void setBaseSalary(const double salary);
   double getBaseSalary() const;

   double earnings() const;
   void print() const;
private:
   CommissionEmployee employee_;
   double baseSalary_;
};

#endif /// __BASEPLUSCOMMISSIONEMPLOYEE_HPP__


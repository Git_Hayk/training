#ifndef __PACKAGE_HPP__
#define __PACKAGE_HPP__

#include <string>

class Package
{
public:
    Package();
    Package(const std::string& name, const std::string& adress,
            const std::string& city,const std::string& state,
            const std::string& sender,const std::string& recipient,
            const double weight, const double price);

    void setName(const std::string name);
    void setAdress(const std::string adress);
    void setCity(const std::string city);
    void setState(const std::string state);
    void setSenderZIP(const std::string sender);
    void setRecipientZIP(const std::string recipient);
    void setWeightInOunces(const double weight);
    void setFeePerOunce(const double price);

    std::string getName() const { return name_; };
    std::string getAdress() const { return adress_; };
    std::string getCity() const { return city_; };
    std::string getState() const { return state_; };
    std::string getSenderZIP() const { return senderZIP_; };
    std::string getRecipientZIP() const { return recipientZIP_; };
    double getWeightInOunces() const { return weightInOunces_; };
    double getFeePerOunce() const { return feePerOunce_; };

    double calculateCost() const { return weightInOunces_ * feePerOunce_; };
    void print() const;
private:
    std::string name_;
    std::string adress_;
    std::string city_;
    std::string state_;
    std::string senderZIP_;
    std::string recipientZIP_;
    double weightInOunces_;
    double feePerOunce_;

};

#endif ///__PACKAGE_HPP__


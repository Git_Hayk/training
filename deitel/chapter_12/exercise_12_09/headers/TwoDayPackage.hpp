#ifndef __TWO_DAY_PACKAGE_HPP__
#define __TWO_DAY_PACKAGE_HPP__

#include "headers/Package.hpp"

class TwoDayPackage : public Package
{
public:
    TwoDayPackage();
    TwoDayPackage(const std::string& name, const std::string& adress,
                  const std::string& city,const std::string& state,
                  const std::string& sender,const std::string& recipient,
                  const double weight, const double price);

    TwoDayPackage(const std::string& name, const std::string& adress,
                  const std::string& city,const std::string& state,
                  const std::string& sender,const std::string& recipient,
                  const double weight, const double price, const double flatFee);

    void setFlatFee(const double flatFee);
    double getFlatFee() const { return flatFee_; };
    
    double calculateCost() const;
    void print() const;
private:
    double flatFee_;
};

#endif ///__TWO_DAY_PACKAGE_HPP__

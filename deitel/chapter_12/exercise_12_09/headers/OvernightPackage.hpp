#ifndef __OVERNIGHT_PACKAGE_HPP__
#define __OVERNIGHT_PACKAGE_HPP__

#include "Package.hpp"

class OvernightPackage : public Package
{
public:
    OvernightPackage();
    OvernightPackage(const std::string& name, const std::string& adress,
                     const std::string& city,const std::string& state,
                     const std::string& sender,const std::string& recipient,
                     const double weight, const double price);

    OvernightPackage(const std::string& name, const std::string& adress,
                     const std::string& city,const std::string& state,
                     const std::string& sender,const std::string& recipient,
                     const double weight, const double price, const double additionalFee);

    void setAdditionalFee(const double additionalFee);
    double getAdditionalFee() const { return additionalFee_; };
    
    double calculateCost() const;
    void print() const;
private:
    double additionalFee_;
};

#endif ///__OVERNIGTH_PACKAGE_HPP__

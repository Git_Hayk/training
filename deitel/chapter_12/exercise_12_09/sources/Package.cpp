#include "headers/Package.hpp"
#include <iostream>

Package::Package()
    : name_("-"), adress_("-"), city_("-"), state_("-"), senderZIP_("-"), recipientZIP_("-"), weightInOunces_(0.0), feePerOunce_(0.0)
{
    /// empty body.
}

Package::Package(const std::string& name, const std::string& adress,
                 const std::string& city,const std::string& state,
                 const std::string& sender,const std::string& recipient,
                 const double weight, const double price)
{
    setName(name);
    setAdress(adress);
    setCity(city);
    setState(state);
    setSenderZIP(sender);
    setRecipientZIP(recipient);
    setWeightInOunces(weight);
    setFeePerOunce(price);
}

void
Package::setName(const std::string name)
{
    name_ = name;
}

void
Package::setAdress(const std::string adress)
{
    adress_ = adress;
}

void
Package::setCity(const std::string city)
{
    city_ = city;
}

void
Package::setState(const std::string state)
{
    state_ = state;
}

void
Package::setSenderZIP(const std::string sender)
{
    senderZIP_ = sender;
}

void
Package::setRecipientZIP(const std::string recipient)
{
    recipientZIP_ = recipient;
}
void
Package::setWeightInOunces(const double weight)
{
    weightInOunces_ = (weight >= 0.0) ? weight : 0.0;
}

void
Package::setFeePerOunce(const double price)
{
    feePerOunce_ = (price >= 0.0) ? price : 0.0;
}

void
Package::print() const
{
    std::cout << "Name: " << getName()
              << "\nAdress: " << getAdress()
              << "\nCity: " << getCity()
              << "\nState: " << getState()
              << "\nSender-ZIP: " << getSenderZIP()
              << "\nRecipient-ZIP: " << getRecipientZIP()
              << "\nPackage weigth: " << getWeightInOunces() << " oz"
              << "\nFee per ounce: " << getFeePerOunce() << '$';
}


#include "headers/Package.hpp"
#include "headers/OvernightPackage.hpp"
#include <iostream>

OvernightPackage::OvernightPackage()
    : Package(), additionalFee_(1.0)
{
    /// Epmty body
}

OvernightPackage::OvernightPackage(const std::string& name, const std::string& adress,
                                   const std::string& city,const std::string& state,
                                   const std::string& sender,const std::string& recipient,
                                   const double weight, const double price)
    : Package(name, adress, city, state, sender, recipient, weight, price), additionalFee_(1.0)
{
    /// Empty body
}

OvernightPackage::OvernightPackage(const std::string& name, const std::string& adress,
                                   const std::string& city,const std::string& state,
                                   const std::string& sender,const std::string& recipient,
                                   const double weight, const double price, const double additionalFee)
    : Package(name, adress, city, state, sender, recipient, weight, price)
{
    setAdditionalFee(additionalFee);
}

void
OvernightPackage::setAdditionalFee(const double additionalFee)
{
    additionalFee_ = (additionalFee_ >= 1.0 && additionalFee <= 5.0) ? additionalFee : 1.0;
}
    
double
OvernightPackage::calculateCost() const
{
    return Package::calculateCost() + Package::getWeightInOunces() * getAdditionalFee();
}

void
OvernightPackage::print() const
{
    Package::print();
    std::cout << "\nAdditional fee per ounce: " << getAdditionalFee() << '$' << std::endl;
}


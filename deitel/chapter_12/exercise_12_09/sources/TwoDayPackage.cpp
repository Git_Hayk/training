#include "headers/TwoDayPackage.hpp"
#include "headers/Package.hpp"
#include <iostream>

TwoDayPackage::TwoDayPackage()
    : Package(), flatFee_(0.0)
{
    /// Empty body
}

TwoDayPackage::TwoDayPackage(const std::string& name, const std::string& adress,
                             const std::string& city,const std::string& state,
                             const std::string& sender,const std::string& recipient,
                             const double weight, const double price)
    : Package(name, adress, city, state, sender, recipient, weight, price), flatFee_(15.0)
{
    /// Empty body
}

TwoDayPackage::TwoDayPackage(const std::string& name, const std::string& adress,
                             const std::string& city,const std::string& state,
                             const std::string& sender,const std::string& recipient,
                             const double weight, const double price, const double flatFee)
    : Package(name, adress, city, state, sender, recipient, weight, price)
{
    setFlatFee(flatFee);
}

void
TwoDayPackage::setFlatFee(const double flatFee)
{
    flatFee_ = (flatFee >= 0.0 && flatFee <= 100.0) ? flatFee : 15.0;
}

double
TwoDayPackage::calculateCost() const
{
    return getFlatFee() + Package::calculateCost();
}

void
TwoDayPackage::print() const
{
    Package::print();
    std::cout << "\nFlat fee: " << getFlatFee() << '$' << std::endl;
}


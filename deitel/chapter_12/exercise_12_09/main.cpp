#include "headers/Package.hpp"
#include "headers/TwoDayPackage.hpp"
#include "headers/OvernightPackage.hpp"

#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << std::fixed << std::setprecision(2);

    Package package("Sean Bean", "W Jackson Blvd-49", "Cicago", "Illinois", "IL 60001", "IL 61788", 3.76, 12.49);
    package.print();
    std::cout << "Total cost: " << package.calculateCost() << '$' << std::endl;

    std::cout << std::endl;

    TwoDayPackage twoDayPackage("Dan Bilz", "Central Avenue/24", "Frankfort", "Kentucky", "KY 42001", "KY 41158", 5.33, 10.98);
    twoDayPackage.print();
    std::cout << "Total cost: " << twoDayPackage.calculateCost() << '$' << std::endl;

    std::cout << std::endl;

    OvernightPackage overnightPackage("Kate Watson", "Paln-street 22/7", "Las Vegas", "Nevada", "NV 88901", "NV 89201", 23.0, 32.5, 3.9);
    overnightPackage.print();
    std::cout << "Total cost: " << overnightPackage.calculateCost() << '$' << std::endl;

    std::cout << std::endl;

    Package defaultPackage;
    defaultPackage.print();
    std::cout << "\n\n";

    TwoDayPackage defaultTwoDayPackage;
    defaultTwoDayPackage.print();

    std::cout << std::endl;
    OvernightPackage defaultOvernightPackage;
    defaultOvernightPackage.print();
    
    return 0;
}


It is good to use the protected access specifier when a base class should provide a member function only to its derived classes and friends, not to clients
or when it is necessary and possible to get some performance due to direct access to the data of the base class in the place of the next function stack
call (these are mainly setters and getters). But this is in case, when changes to these data do not require any validation checks or any other additional actions.


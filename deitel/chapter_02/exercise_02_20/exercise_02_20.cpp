/*Write a program that reads in the radius of a circle as an integer and prints the circle's diameter, circumference and area. Use the constant value 3.14159 for p. Do all calculations in output statements. [Note: In this chapter, we have discussed only integer constants and variables. In Chapter 4 we discuss floating-point numbers, i.e., values that can have decimal points.]*/

#include <iostream>

int
main()
{
    /// Using the appropriate formulas I write the following code
    int radius;
    std::cout << "Input the circles radius: ";
    std::cin >> radius;
    
    if (radius < 0) {
        std::cout << "Error 1: Radius can not be less than zero" << std::endl;
        return 1;
    }

    int diameter = 2 * radius;
    std::cout << "Circles diameter is " << diameter << std::endl;
    
    int circumFerence = diameter * 3.14159;
    std::cout << "Circumference is " << circumFerence << std::endl;
    
    int area = 3.14159 * (radius * radius);
    std::cout << "Circles area is " << area << std::endl;
    
    return 0;
}

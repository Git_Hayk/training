/* Write a program that reads an integer and determines and prints whether it is odd or even.
[Hint: Use the modulus operator. An even number is a multiple of two. Any multiple of two leaves a 
remainder of zero when divided by 2.] */

#include <iostream>

int
main()
{
    int value;
    std::cout << "Input an integer value: " << std::endl;
    std::cin >> value;
    
    if (value % 2 == 0) {
        std::cout << "This number is even." << std::endl;
        return 0;
    }
    
    std::cout << "This number is odd." << std::endl;
    return 0;
}

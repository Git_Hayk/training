/* Write a program that asks the user to enter two numbers, obtains the two numbers from the user and prints the sum,
 product, difference, and quotient of the two numbers. */

#include <iostream> /// standart input and output

int
main()
{
   /// variable declarations
   int number1, number2;

   std::cout << "Enter the first number:\n";
   std::cin >> number1;

   std::cout << "Enter the second number:\n";
   std::cin >> number2;

   int sum = number1 + number2; /// counting the sum of two numbers
   std::cout << "The sum of two numbers: " << sum << std::endl; /// printing result

   int difference = number1 - number2; /// counting the difference of two numbers
   std::cout << "The difference of two numbers: " << difference << std::endl; /// printing result

   int product = number1 * number2; /// counting the product of two numbers
   std::cout << "The product of two numbers: " << product << std::endl; /// printing result
   
   if (0 == number2) {
       std::cout << "Error 1: division by 0 " << std::endl;
       return 1;
   }

   int quotient = number1 / number2; ///  counting the qoutient of two numbers
   std::cout << "The quotient of two numbers: " << quotient << std::endl; /// printing result

   return 0;
} /// ending main function

/*Write a program that reads in two integers and determines and prints if the first is a multiple of the second.
[Hint: Use the modulus operator.] */

#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Input two integer values: ";
    std::cin >> number1 >> number2;
    
    if (0 == number2) {
        std::cout << "Error 1: division by 0";
        return 1;
    }
    
    if (number1 % number2 != 0) {
        std::cout << number1 << " is not multiple of " << number2;
        return 0;
    }
    
    std::cout << number1 << " is a multiple of " << number2;
    return 0;
}

/* Write a program that inputs a five-digit integer, separates the integer into its individual digits and prints the digits separated 
from one another by three spaces each. [Hint: Use the integer division and modulus operators.] */

#include <iostream>

int
main()
{
    int myNumber;
    std::cout << "Intput an integer with 5 digits: " << std::endl;
    std::cin >> myNumber;
    
    if (myNumber < 0) {
        myNumber = myNumber * (-1);
    }
    
    int digit1 = myNumber / 10000;
    if (0 == digit1) {
        std::cout << "Error 1: Input only 5 digits integer not starting with 0!" << std::endl;
        return 1;
    }
    
    if (digit1 > 9) {
        std::cout << "Error 1: Input only 5 digits integer not starting with 0!" << std::endl;
        return 1;
    }
    
    myNumber = myNumber % 10000;
    int digit2 = myNumber / 1000;
    myNumber = myNumber % 1000;
    int digit3 = myNumber / 100;
    myNumber = myNumber % 100;
    int digit4 = myNumber / 10;
    myNumber = myNumber % 10;
    int digit5 = myNumber / 1;
    myNumber = myNumber % 1;
        
    std::cout << digit1 << "   " << digit2 << "   " << digit3 << "   " << digit4 << "   " << digit5;
            
    return 0;
}

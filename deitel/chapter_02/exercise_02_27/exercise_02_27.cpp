/* Write a program that prints the integer equivalent of a character typed at the keyboard. 
Test your program several times using uppercase letters, lowercase letters, digits and special characters (like $) */

#include <iostream>

int
main()
{
    std::cout << "An integer equivalent of 'A' character is " << static_cast<int>('A') << std::endl;
    std::cout << "An integer equivalent of 'B' character is " << static_cast<int>('B') << std::endl;
    std::cout << "An integer equivalent of 'C' character is " << static_cast<int>('C') << std::endl;
    std::cout << "An integer equivalent of 'a' character is " << static_cast<int>('a') << std::endl;
    std::cout << "An integer equivalent of 'b' character is " << static_cast<int>('b') << std::endl;
    std::cout << "An integer equivalent of 'c' character is " << static_cast<int>('c') << std::endl;
    std::cout << "An integer equivalent of '0' character is " << static_cast<int>('0') << std::endl;
    std::cout << "An integer equivalent of '1' character is " << static_cast<int>('1') << std::endl;
    std::cout << "An integer equivalent of '2' character is " << static_cast<int>('2') << std::endl;
    std::cout << "An integer equivalent of '&' character is " << static_cast<int>('&') << std::endl;
    std::cout << "An integer equivalent of '*' character is " << static_cast<int>('*') << std::endl;
    std::cout << "An integer equivalent of '+' character is " << static_cast<int>('+') << std::endl;
    std::cout << "An integer equivalent of '/' character is " << static_cast<int>('/') << std::endl;
    std::cout << "An integer equivalent of ' ' character is " << static_cast<int>(' ') << std::endl;
    std::cout << "An integer equivalent of '\\n' character is " << static_cast<int>('\n') << std::endl;
    std::cout << "An integer equivalent of '\\t' character is " << static_cast<int>('\t') << std::endl;
    return 0;
}


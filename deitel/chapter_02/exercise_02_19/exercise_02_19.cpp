/* Write a program that inputs three integers from the keyboard and prints the sum, average, product, 
smallest and largest of these numbers. The screen dialog should appear as follows: */

#include <iostream>

int
main()
{
    /// Variable declarations
    int number1, number2, number3;
    
    std::cout << "Input three different integers: " << std::endl; ///promt user for data
    std::cin >> number1 >> number2 >> number3; /// attributing integers to variables
    
    int sum = number1 + number2 + number3;
    std::cout << "Sum is " << sum << std::endl;
    
    int average = sum / 3;
    std::cout << "Average is " << average << std::endl;
    
    int product = number1 * number2 * number3;
    std::cout << "Product is " << product << std::endl;
    
    /// Variable declarations (suppose that the "number1" is the smallest and the largest at the same time)
    int smallest = number1, largest = number1;
    
    if (smallest > number2) {
        smallest = number2;
    }
    
    if (largest < number2) {
        largest = number2;
    }
    
    if (smallest > number3) {
        smallest = number3;
    }
    
    if (largest < number3) {
        largest = number3;
    }
    
    std::cout << "Largest is " << largest << std::endl;
    std::cout << "Smallest is " << smallest << std::endl;
    
    return 0;
}

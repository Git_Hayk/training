/* Display the following checkerboard pattern with eight output statements,
then display the same pattern using as few statements as possible. */

#include <iostream>

int
main()
{
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n\n\n";
    
    
    std::cout << "* * * * * * * *\n * * * * * * * *\n* * * * * * * *\n * * * * * * * *\n* * * * * * * *\n * * * * * * * *\n* * * * * * * *\n * * * * * * * *\n";
    
    return 0;
}

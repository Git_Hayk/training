/* Using only the techniques you learned in this chapter, write a program that calculates the squares and cubes of 
the integers from 0 to 10 and uses tabs to print the following neatly formatted table of values: */

#include <iostream>

int
main()
{
    std::cout << "interger  " << "square  " << "cube" << std::endl;
    
    int x = 0;
    std::cout << x << "\t  " << x * x << "\t  " << x * x * x << std::endl;
    x = 1;
    std::cout << x << "\t  " << x * x << "\t  " << x * x * x << std::endl;
    x = 2;
    std::cout << x << "\t  " << x * x << "\t  " << x * x * x << std::endl;
    x = 3;
    std::cout << x << "\t  " << x * x << "\t  " << x * x * x << std::endl;
    x = 4;
    std::cout << x << "\t  " << x * x << "\t  " << x * x * x << std::endl;
    x = 5;
    std::cout << x << "\t  " << x * x << "\t  " << x * x * x << std::endl;
    x = 6;
    std::cout << x << "\t  " << x * x << "\t  " << x * x * x << std::endl;
    x = 7;
    std::cout << x << "\t  " << x * x << "\t  " << x * x * x << std::endl;
    x = 8;
    std::cout << x << "\t  " << x * x << "\t  " << x * x * x << std::endl;
    x = 9;
    std::cout << x << "\t  " << x * x << "\t  " << x * x * x << std::endl;
    x = 10;
    std::cout << x << "\t  " << x * x << "\t  " << x * x * x << std::endl;
    
    return 0;
}

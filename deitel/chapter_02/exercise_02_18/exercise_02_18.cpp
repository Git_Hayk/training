/* Write a program that asks the user to enter two integers, obtains the numbers from the user, then prints the larger 
number followed by the words "is larger." If the numbers are equal, print the message "These numbers are equal." */

#include <iostream>

int
main()
{
    /// Variable declarations
    int number1, number2;
    std::cout << "Enter two integers to compare: "; /// prompt user for data

    std::cin >> number1 >> number2; /// attributing integers to variables

    if (number1 > number2) {
        std::cout << number1 << " is larger " << number2 << std::endl;
        return 1;
    }

    if (number1 < number2) {
        std::cout << number2 << " is larger " << number1 << std::endl;
        return 2;
    }

    std::cout << "This numbers are equal" << std::endl;
    return 0;
}

/*Write a program that prints a box, an oval, an arrow and a diamond as follows:*/

#include <iostream>

int
main()
{
    std::cout << "*********\t***\t  *\t    *\n";
    std::cout << "*\t*     *\t    *\t ***\t   * *\n";
    std::cout << "*\t*    *\t     *\t*****\t  *   *\n";
    std::cout << "*\t*    *\t     *\t  *\t *     *\n";
    std::cout << "*\t*    *\t     *\t  *\t*       *\n";
    std::cout << "*\t*    *\t     *\t  *\t *     *\n";
    std::cout << "*\t*    *\t     *\t  *\t  *   *\n";
    std::cout << "*\t*     *\t    *\t  *\t   * *\n";
    std::cout << "*********\t***\t  *\t    *\n";
    
    return 0;
}

/* Write a program that reads in five integers and determines and prints the largest and the smallest integers in the group.
Use only the programming techniques you learned in this chapter. */

#include <iostream>

int
main()
{
    int number1, number2, number3, number4, number5;
    std::cout << "Input five integer values: " << std::endl;
    std::cin >> number1 >> number2 >> number3 >> number4 >> number5;
    
    int smallest = number1, largest = number1; /// Suppose that the first numbers is the largest and the smallest at the same time
    
    if (number2 > largest) {
        largest = number2;
    }
    
    if (number2 < smallest) {
        smallest = number2;
    }
    
    if (number3 > largest) {
        largest = number3;
    }
    
    if (number3 < smallest) {
        smallest = number3;
    }
    
    if (number4 > largest) {
        largest = number4;
    }
    
    if (number4 < smallest) {
        smallest = number4;
    }
    
    if (number5 > largest) {
        largest = number5;
    }
    
    if (number5 < smallest) {
        smallest = number5;
    }
    
    std::cout << "Largest is " << largest << std::endl;
    std::cout << "Smallest is " << smallest << std::endl;
    
    return 0;
}

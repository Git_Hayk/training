Virtual function has an implementation and gives the derived class the option of overriding the function.
Pure virtual function does not provide an implementation and requires the derived class to override
the function (for that derived class to be concrete; otherwise the derived class remains abstract).

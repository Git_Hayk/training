#include "headers/SalariedEmployee.hpp"
#include <iostream>

SalariedEmployee::SalariedEmployee(const std::string& first, const std::string& last, const std::string& ssn, const double salary)
   : Employee(first, last, ssn)
{
   setWeeklySalary(salary);
}

void
SalariedEmployee::setWeeklySalary(const double salary)
{
   weeklySalary_ = (salary < 0.0) ? 0.0 : salary;
}

double
SalariedEmployee::getWeeklySalary() const
{
   return weeklySalary_;
}

// override pure virtual function earnings in Employee
double
SalariedEmployee::earnings() const
{
   return getWeeklySalary();
}

void
SalariedEmployee::print() const
{
   std::cout << "salaried employee: ";
   Employee::print(); // reuse abstract base-class print function
   std::cout << "\nweekly salary: " << getWeeklySalary();
}


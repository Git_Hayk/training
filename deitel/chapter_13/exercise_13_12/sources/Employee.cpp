#include "headers/Employee.hpp"
#include <iostream>

Employee::Employee(const std::string& first, const std::string& last, const std::string& ssn)
   : firstName_(first), lastName_(last), socialSecurityNumber_(ssn)
{
   // empty body
}

void
Employee::setFirstName(const std::string& first)
{
   firstName_ = first;
}

std::string
Employee::getFirstName() const
{
   return firstName_;
}

void
Employee::setLastName(const std::string& last)
{
   lastName_ = last;
}

std::string
Employee::getLastName() const
{
   return lastName_;
}

void
Employee::setSocialSecurityNumber(const std::string& ssn)
{
   socialSecurityNumber_ = ssn;
}

std::string
Employee::getSocialSecurityNumber() const
{
   return socialSecurityNumber_;
}

void
Employee::print() const
{
   std::cout << getFirstName() << ' ' << getLastName()
      << "\nsocial security number: " << getSocialSecurityNumber();
}


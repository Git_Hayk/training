#include "headers/BasePlusCommissionEmployee.hpp"
#include <iostream>

BasePlusCommissionEmployee::BasePlusCommissionEmployee(const std::string &first, const std::string &last, const std::string &ssn,
                                                       const double sales, const double rate, const double salary)
   : CommissionEmployee(first, last, ssn, sales, rate)
{
   setBaseSalary(salary);
}

void
BasePlusCommissionEmployee::setBaseSalary(const double salary)
{
   baseSalary_ = ((salary < 0.0 ) ? 0.0 : salary);
}

double
BasePlusCommissionEmployee::getBaseSalary() const
{
    return baseSalary_;
}

double
BasePlusCommissionEmployee::earnings() const
{
    return getBaseSalary() + CommissionEmployee::earnings();
}

void
BasePlusCommissionEmployee::print() const
{
   std::cout << "base-salaried ";
   CommissionEmployee::print(); // code reuse
   std::cout << "; base salary: " << getBaseSalary();
}


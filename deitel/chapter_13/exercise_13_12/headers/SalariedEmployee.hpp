#ifndef __SALARIED_EMPLOYEE_HPP__
#define __SALARIED_EMPLOYEE_HPP__

#include "Employee.hpp"

class SalariedEmployee : public Employee
{
public:
   SalariedEmployee(const std::string& first, const std::string& last, const std::string& ssn, const double salary = 0.0 );

   void setWeeklySalary(const double salary);
   double getWeeklySalary() const;

   // keyword virtual signals intent to override               
   virtual double earnings() const; // calculate earnings      
   virtual void print() const; // print SalariedEmployee object
private:
   double weeklySalary_;
};

#endif // __SALARIED_EMPLOYEE_HPP__

#ifndef __HOURLY_EMPLOYEE_HPP__
#define __HOURLY_EMPLOYEE_HPP__

#include "Employee.hpp" // Employee class definition

class HourlyEmployee : public Employee
{
public:
   HourlyEmployee(const std::string& first, const std::string& last, const std::string& ssn, const double hourlyWage = 0.0, const double hoursWorked = 0.0 );

   void setWage(const double hourlyWage);
   double getWage() const;

   void setHours(const double hoursWorked);
   double getHours() const;

   // keyword virtual signals intent to override
   virtual double earnings() const;
   virtual void print() const;
private:
   double wage_;
   double hours_;
};

#endif //__HOURLY_EMPLOYEE_HPP__


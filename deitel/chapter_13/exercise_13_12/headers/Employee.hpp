#ifndef __EMPLOYEE_HPP__
#define __EMPLOYEE_HPP__

#include <string> // C++ standard std::string class

class Employee
{
public:
   Employee(const std::string& first, const std::string& last, const std::string& ssn);

   void setFirstName(const std::string& firstName);
   std::string getFirstName() const;

   void setLastName(const std::string& lastName);
   std::string getLastName() const;

   void setSocialSecurityNumber(const std::string& ssn);
   std::string getSocialSecurityNumber() const;

   virtual double earnings() const = 0; // pure virtual
   virtual void print() const; // virtual
private:
   std::string firstName_;
   std::string lastName_;
   std::string socialSecurityNumber_;
};

#endif ///__EMPLOYEE_HPP__


#ifndef __BASEPLUSCOMMISSIONEMPLOYEE_HPP__
#define __BASEPLUSCOMMISSIONEMPLOYEE_HPP__

#include "CommissionEmployee.hpp"

class BasePlusCommissionEmployee : public CommissionEmployee
{
public:
   BasePlusCommissionEmployee(const std::string& first, const std::string& last, const std::string& ssn,
                              const double sales = 0.0, const double rate = 0.0, const double salary = 0.0);

   void setBaseSalary(const double salary);
   double getBaseSalary() const;

   // keyword virtual signals intent to override
   virtual double earnings() const; // calculate earnings
   virtual void print() const; // print BasePlusCommissionEmployee object
private:
   double baseSalary_;
};

#endif /// __BASEPLUSCOMMISSIONEMPLOYEE_HPP__


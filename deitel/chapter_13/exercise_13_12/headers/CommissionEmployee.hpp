#ifndef __COMMISSION_EMPLOYEE_HPP__
#define __COMMISSION_EMPLOYEE_HPP__

#include "Employee.hpp"

class CommissionEmployee : public Employee
{
public:
   CommissionEmployee(const std::string& first, const std::string& lsat, const std::string& ssn, const double sales = 0.0, const double rate = 0.0);

   void setCommissionRate(const double rate);
   double getCommissionRate() const;

   void setGrossSales(const double sales);
   double getGrossSales() const;

   // keyword virtual signals intent to override                 
   virtual double earnings() const;
   virtual void print() const;
private:
   double grossSales_;
   double commissionRate_;
};

#endif /// __COMMISSION_EMPLOYEE_HPP__


// include definitions of classes in Employee hierarchy
#include "headers/Employee.hpp"
#include "headers/SalariedEmployee.hpp"
#include "headers/HourlyEmployee.hpp"
#include "headers/CommissionEmployee.hpp"
#include "headers/BasePlusCommissionEmployee.hpp"

#include <iostream>
#include <iomanip>
#include <vector>

void virtualViaPointer(const Employee* const baseClassPtr); // prototype
void virtualViaReference(const Employee& baseClassRef); // prototype

int
main()
{
   // set floating-point output formatting
   std::cout << std::fixed << std::setprecision(2);

   // create derived-class objects
   SalariedEmployee salariedEmployee(
      "John", "Smith", "111-11-1111", 800);
   HourlyEmployee hourlyEmployee(
      "Karen", "Price", "222-22-2222", 16.75, 40);
   CommissionEmployee commissionEmployee(
      "Sue", "Jones", "333-33-3333", 10000, 0.06);
   BasePlusCommissionEmployee basePlusCommissionEmployee(
      "Bob", "Lewis", "444-44-4444", 5000, 0.04, 300);

   std::cout << "Employees processed individually using static binding:\n\n";

   // output each Employee's information and earnings using static binding
   salariedEmployee.print();
   std::cout << "\nearned $" << salariedEmployee.earnings() << "\n\n";
   hourlyEmployee.print();
   std::cout << "\nearned $" << hourlyEmployee.earnings() << "\n\n";
   commissionEmployee.print();
   std::cout << "\nearned $" << commissionEmployee.earnings() << "\n\n";
   basePlusCommissionEmployee.print();
   std::cout << "\nearned $" << basePlusCommissionEmployee.earnings()
             << "\n\n";

   // create std::vector of four base-class pointers
   std::vector<Employee*> employees(4);

   // initialize std::vector with Employees
   employees[0] = &salariedEmployee;
   employees[1] = &hourlyEmployee;
   employees[2] = &commissionEmployee;
   employees[3] = &basePlusCommissionEmployee;

   std::cout << "Employees processed polymorphically via dynamic binding:\n\n";

   // call virtualViaPointer to print each Employee's information
   // and earnings using dynamic binding
   std::cout << "Virtual function calls made off base-class pointers:\n\n";

   for (size_t i = 0; i < employees.size(); i++) {
      virtualViaPointer(employees[i]);
   }

   // call virtualViaReference to print each Employee's information
   // and earnings using dynamic binding
   std::cout << "Virtual function calls made off base-class references:\n\n";

   for ( size_t i = 0; i < employees.size(); i++ ) {
      virtualViaReference(*employees[i]); // note dereferencing
   }

   return 0;
}

// call Employee virtual functions print and earnings off a   
// base-class pointer using dynamic binding                   
void
virtualViaPointer(const Employee* const baseClassPtr) 
{                                                             
   baseClassPtr->print();                                     
   std::cout << "\nearned $" << baseClassPtr->earnings() << "\n\n";
}

// call Employee virtual functions print and earnings off a  
// base-class reference using dynamic binding                
void
virtualViaReference(const Employee& baseClassRef)     
{                                                            
   baseClassRef.print();
   std::cout << "\nearned $" << baseClassRef.earnings() << "\n\n";
}


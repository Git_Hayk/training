#include "headers/Tree.hpp"
#include <benchmark/benchmark.h>


static void
BM_pre_order(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    Tree<int> intTree;
    int intValue;
    for (int i = 0; i < 10; ++i) {
        std::cin >> intValue;
        intTree.insertNode(intValue);
    }
    while (state.KeepRunning()) {
        intTree.preOrderTraversal();
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_pre_order);

static void
BM_in_order(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    Tree<int> intTree;
    int intValue;
    for (int i = 0; i < 10; ++i) {
        std::cin >> intValue;
        intTree.insertNode(intValue);
    }
    while (state.KeepRunning()) {
        intTree.inOrderTraversal();
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_in_order);

static void
BM_post_order(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    Tree<int> intTree;
    int intValue;
    for (int i = 0; i < 10; ++i) {
        std::cin >> intValue;
        intTree.insertNode(intValue);
    }
    while (state.KeepRunning()) {
        intTree.postOrderTraversal();
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_post_order);

static void
BM_level_order(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    Tree<int> intTree;
    int intValue;
    for (int i = 0; i < 10; ++i) {
        std::cin >> intValue;
        intTree.insertNode(intValue);
    }
    while (state.KeepRunning()) {
        intTree.levelOrderTraversal();
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_level_order);

BENCHMARK_MAIN();


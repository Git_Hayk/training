#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__

#include "List.hpp"
#include <iostream>

template <typename QUEUETYPE>
class Queue : private List<QUEUETYPE>
{
public:
    void enqueue(const QUEUETYPE& data) {
        List<QUEUETYPE>::insertAtBack(data);
    }

    bool dequeue() {
        return List<QUEUETYPE>::removeFromFront();
    } 

    QUEUETYPE queueBack() const {
        return List<QUEUETYPE>::getBack();
    }

    QUEUETYPE queueFront() const {
        return List<QUEUETYPE>::getFront();
    }

    bool isQueueEmpty() const {
        return List<QUEUETYPE>::isEmpty();
    }

    void printQueue() const {
        List<QUEUETYPE>::print();
    }
};

#endif ///__QUEUE_HPP__


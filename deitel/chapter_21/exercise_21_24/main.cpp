#include "headers/Tree.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <string>

int
main()
{
    Tree<int> intTree;
    int intValue;

    if (::isatty(STDIN_FILENO))
        std::cout << "Enter 10 integer values:\n";

    for (int i = 0; i < 10; ++i) {
        std::cin >> intValue;
        intTree.insertNode(intValue);
    }

    std::cout << "\nPreorder traversal\n";
    intTree.preOrderTraversal();

    std::cout << "\nInorder traversal\n";
    intTree.inOrderTraversal();

    std::cout << "\nPostorder traversal\n";
    intTree.postOrderTraversal();

    std::cout << "\nLevelorder traversal\n";
    intTree.levelOrderTraversal();

    std::cout << '\n';

    return 0;
}


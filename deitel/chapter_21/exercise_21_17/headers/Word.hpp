#ifndef __WORD_HPP__
#define __WORD_HPP__

const size_t WORD_SIZE = 40;

class Word
{
    friend std::istream& operator>>(std::istream& in, Word& rhv);
    friend std::ostream& operator<<(std::ostream& out, const Word& rhv);
public:
    Word() : word_(NULL) {/*empty body*/}
    Word(char* text) : word_(text) {/*empty body*/}

    size_t size() const { return std::strlen(word_); }
    bool empty() const { return word_ == NULL; }
    char* get()       { return word_; }
    char* get() const { return word_; }

    char operator[](const size_t subscript);
    char operator[](const size_t subscript) const;
    bool operator<(const Word& rhv) const;
    bool operator>(const Word& rhv) const;
private:
    char* word_;
};

#include "sources/Word.cpp"

#endif // __WORD_HPP__


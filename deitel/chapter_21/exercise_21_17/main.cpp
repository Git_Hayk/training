#include "headers/Tree.hpp"
#include "headers/Word.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <string>

const size_t BUFFER = 256;

int
main()
{
    
    if (::isatty(STDIN_FILENO)) std::cout << "Input line of text:\n";

    char* text = new char[BUFFER];
    std::cin.getline(text, BUFFER);

    Tree<Word> wordTree;

    for (Word w = std::strtok(text, " "); w.get() != NULL; w = std::strtok(NULL, " ")) {
        wordTree.insertNode(w);
    }

    std::cout << "\nPreorder traversal\n";
    wordTree.preOrderTraversal();

    std::cout << "\nInorder traversal\n";
    wordTree.inOrderTraversal();

    std::cout << "\nPostorder traversal\n";
    wordTree.postOrderTraversal();
    std::cout << '\n';

    return 0;
}


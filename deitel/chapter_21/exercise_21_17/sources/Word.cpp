#include "headers/Word.hpp"

std::istream& operator>>(std::istream& in, Word& rhv)
{
    if (rhv.get() == NULL) {
        rhv.word_= new char[WORD_SIZE];
    }
    in.getline(rhv.word_, WORD_SIZE);
    return in;
}

std::ostream& operator<<(std::ostream& out, const Word& rhv)
{
    out << rhv.get();
    return out;
}

char
Word::operator[](const size_t subscript)
{
    return word_[subscript];
}

char
Word::operator[](const size_t subscript) const
{
    return word_[subscript];
}

bool
Word::operator<(const Word& rhv) const
{
    const size_t min = std::min(this->size(), rhv.size());
    for (size_t i = 0; i != min; ++i) {
        if (word_[i] < rhv.word_[i]) {
            return true;
        }
        if (word_[i] > rhv.word_[i]) {
            return false;
        }
    }
    return this->size() < rhv.size();
}

bool
Word::operator>(const Word& rhv) const
{
    return rhv < *this;
}


#ifndef __TREENODE_HPP__
#define __TREENODE_HPP__

#include <cstddef>

template< typename NODETYPE > class Tree;

template <typename NODETYPE>
class TreeNode
{
    friend class Tree<NODETYPE>;
public:
    TreeNode(const NODETYPE& d)
        : leftPtr_(NULL), data(d), rightPtr_(NULL) { }

    NODETYPE getData() const { return data; }
private:
    TreeNode<NODETYPE>* leftPtr_;
    NODETYPE data;
    TreeNode<NODETYPE>* rightPtr_;
};

#endif /// __TREENODE_HPP__


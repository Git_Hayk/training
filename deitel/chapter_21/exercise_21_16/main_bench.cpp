#include "headers/Tree.hpp"
#include <benchmark/benchmark.h>

const int ITER_COUNT = 2<<10;

static void
BM_insert(benchmark::State& state)
{
    //std::cout.setstate(std::ios_base::failbit);
    Tree<int> intTree;
    while (state.KeepRunning()) {
        for (int i = ITER_COUNT; i > 0; --i) {
            intTree.insertNode(i);
        }
    }
    //std::cout.clear();
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_insert);

BENCHMARK_MAIN();


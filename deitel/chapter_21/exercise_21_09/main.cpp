#include "headers/Reverse.hpp"
#include <iostream>
#include <unistd.h>
#include <list>

int
main()
{
    const char chars[] = { 'a', '/', '?', '-', '+', '7', '.', 'L', '~', '}' };
    std::list<char> initialList(std::begin(chars), std::end(chars));
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Initial list is: ";
    }
    printList(initialList);

    std::list<char> reversedList = reverseList(initialList);
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Reversed list is: ";
    }
    printList(reversedList);

    return 0;
}


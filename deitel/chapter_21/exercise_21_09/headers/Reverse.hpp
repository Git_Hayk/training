#ifndef __REVERSE_HPP__
#define __REVERSE_HPP__

#include <list>

template <typename T>
const std::list<T> reverseList(const std::list<T>& l);

template <typename T>
void printList(const std::list<T>& l);

#include "sources/Reverse.cpp"

#endif ///__REVERSE_HPP__


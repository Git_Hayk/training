#include "headers/Reverse.hpp"
#include <benchmark/benchmark.h>
#include <list>

const int SIZE = 10;

static void
BM_reverse(benchmark::State& state)
{
    const char chars[SIZE] = { 'a', '/', '?', '-', '+', '7', '.', 'L', '~', '}' };
    std::list<char> newList(std::begin(chars), std::end(chars));
    while (state.KeepRunning()) {
        std::list<char> reversedList = reverseList(newList);
    }

}
BENCHMARK(BM_reverse);

BENCHMARK_MAIN();


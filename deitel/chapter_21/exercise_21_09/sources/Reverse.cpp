#include "headers/Reverse.hpp"
#include <iostream>

template <typename T>
const std::list<T>
reverseList(const std::list<T>& l)
{
    std::list<T> newList;
    for (typename std::list<T>::const_iterator it = l.begin(); it != l.end(); ++it) {
        newList.push_front(*it);
    }
    return newList;
}

template <typename T>
void
printList(const std::list<T>& l)
{
    for (typename std::list<T>::const_iterator it = l.begin(); it != l.end(); ++it) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;
}


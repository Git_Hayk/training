#ifndef __TREE_HPP__
#define __TREE_HPP__

#include "TreeNode.hpp"
#include <iostream>

template <typename NODETYPE> class Tree
{
public:
   Tree();

   void insertNode(const NODETYPE& value);
   void preOrderTraversal() const;     
   void inOrderTraversal() const;      
   void postOrderTraversal() const;
   void levelOrderTraversal() const;
   void outputTree() const;
private:
   // utility functions
   void insertNodeHelper(TreeNode<NODETYPE>** ptr, const NODETYPE& value);
   void preOrderHelper(TreeNode<NODETYPE>* ptr) const;
   void inOrderHelper(TreeNode<NODETYPE>* ptr) const;
   void postOrderHelper(TreeNode<NODETYPE>* ptr) const;
   void levelOrderHelper(TreeNode<NODETYPE>* ptr) const;
   void outputTreeHelper(TreeNode<NODETYPE>* ptr, const size_t totalSpaces = 0) const;
private:
   TreeNode<NODETYPE>* rootPtr_;
};

#include "sources/Tree.cpp"

#endif /// __TREE_HPP__


#include "headers/Tree.hpp"
#include "headers/Queue.hpp"

template <typename NODETYPE>
Tree<NODETYPE>::Tree()
    : rootPtr_(NULL)
{
}

template <typename NODETYPE>
void
Tree<NODETYPE>::insertNode(const NODETYPE& value)
{
   insertNodeHelper(&rootPtr_, value);
}

// utility function called by insertNode; receives a pointer
// to a pointer so that the function can modify pointer's value
template <typename NODETYPE>
void
Tree<NODETYPE>::insertNodeHelper(TreeNode<NODETYPE>** ptr, const NODETYPE& value)
{
    // subtree is empty; create new TreeNode containing value
    if (*ptr == NULL) {
        *ptr = new TreeNode<NODETYPE>(value);
    } else { // subtree is not empty

        if (value < (*ptr)->data_) {
            insertNodeHelper(&((*ptr)->leftPtr_), value);
        } else if (value > (*ptr)->data_) {
            insertNodeHelper(&((*ptr)->rightPtr_), value);
        }

    }
}

// begin preorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::preOrderTraversal() const
{
    preOrderHelper(rootPtr_);
}

// utility function to perform preorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::preOrderHelper(TreeNode<NODETYPE>* ptr) const
{
    if (ptr != 0) {
        std::cout << ptr->data_ << ' '; // process node
        preOrderHelper(ptr->leftPtr_); // traverse left subtree
        preOrderHelper(ptr->rightPtr_); // traverse right subtree
    }
}

// begin inorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::inOrderTraversal() const
{
    inOrderHelper(rootPtr_);
}

// utility function to perform inorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::inOrderHelper(TreeNode<NODETYPE>* ptr) const
{
    if (ptr != 0) {
        inOrderHelper(ptr->leftPtr_); // traverse left subtree  
        std::cout << ptr->data_ << ' '; // process node
        inOrderHelper(ptr->rightPtr_); // traverse right subtree
    }
}

// begin postorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::postOrderTraversal() const
{
    postOrderHelper(rootPtr_);
}

// utility function to perform postorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::postOrderHelper(TreeNode<NODETYPE>* ptr) const
{
    if (ptr != 0) {
        postOrderHelper(ptr->leftPtr_); // traverse left subtree  
        postOrderHelper(ptr->rightPtr_); // traverse right subtree
        std::cout << ptr->data_ << ' '; // process node
    }
}

// begin levelorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::levelOrderTraversal() const
{
    levelOrderHelper(rootPtr_);
}

// utility function to perform levelorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::levelOrderHelper(TreeNode<NODETYPE>* ptr) const
{
    Queue<TreeNode<NODETYPE>* > treeNodeQueue;
    treeNodeQueue.enqueue(ptr);
    while (!treeNodeQueue.isQueueEmpty()) {
        TreeNode<NODETYPE>* node = treeNodeQueue.queueFront();
        std::cout << node->data_ << ' ';
        treeNodeQueue.dequeue();
        if (node->leftPtr_ != NULL) {
            treeNodeQueue.enqueue(node->leftPtr_);
        }
        if (node->rightPtr_ != NULL) {
            treeNodeQueue.enqueue(node->rightPtr_);
        }
    }
}

template <typename NODETYPE>
void
Tree<NODETYPE>::outputTree() const
{
    outputTreeHelper(rootPtr_);
}

template <typename NODETYPE>
void
Tree<NODETYPE>::outputTreeHelper(TreeNode<NODETYPE>* ptr, const size_t totalSpaces) const
{
    if (ptr != NULL) {
        outputTreeHelper(ptr->rightPtr_, totalSpaces + 5); // traverse right subtree
        for (size_t i = 0; i < totalSpaces; ++i) {
            std::cout << ' ';
        }
        std::cout << ptr->data_ << std::endl;
        outputTreeHelper(ptr->leftPtr_, totalSpaces + 5); // traverse left subtree  
    }
}


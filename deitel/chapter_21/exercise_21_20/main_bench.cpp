#include "headers/ListBackward.hpp"
#include <benchmark/benchmark.h>
#include <numeric>

const int SIZE = 10;

static void
BM_recursive_list_print(benchmark::State& state)
{
    std::list<int> test_list(SIZE);
    std::iota(test_list.begin(), test_list.end(), 0);
    std::cout.setstate(std::ios_base::failbit);
    while (state.KeepRunning()) {
        cd06::printListBackward(test_list, test_list.end());
    }
    std::cout.clear();
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_recursive_list_print);

BENCHMARK_MAIN();


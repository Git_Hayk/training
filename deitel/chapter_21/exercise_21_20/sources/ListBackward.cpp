#include "headers/ListBackward.hpp"
#include <iostream>

namespace cd06 {

template <typename T, typename ForwardIterator>
void
printListBackward(const std::list<T>& l, ForwardIterator end)
{
    if (end != l.begin()) {
        --end;
        std::cout << *end << ", ";
        return printListBackward(l, end);
    }
    std::cout << std::endl;
    return;
}

} /// namespace cd06


#include "headers/ListBackward.hpp"
#include <iostream>
#include <numeric>

int
main()
{
    const size_t LIST_SIZE = 20;
    std::list<int> intList(LIST_SIZE);
    std::iota(intList.begin(), intList.end(), 0);
    cd06::printListBackward(intList, intList.end());
    
    return 0;
}


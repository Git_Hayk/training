#ifndef __LISTBACKWARD_HPP__
#define __LISTBACKWARD_HPP__

#include <list>

namespace cd06 {

template <typename T, typename ForwardIterator>
void printListBackward(const std::list<T>& l, ForwardIterator end);

} // namespace cd06

#include "sources/ListBackward.cpp"

#endif ///__LISTBACKWARD_HPP__


#ifndef __CLASSNAME_HPP__
#define __CLASSNAME_HPP__

#include <list>

void printList(const std::list<char>& listToPrint);
void concatenate(std::list<char>& list1, const std::list<char>& list2);

#endif ///__CLASSNAME_HPP__


#include "headers/Concat.hpp"
#include <iostream>

void
printList(const std::list<char>& l)
{
    for (std::list<char>::const_iterator it = l.begin(); it != l.end(); ++it) {
        std::cout << *it;
    }
    std::cout << std::endl;
}

void
concatenate(std::list<char>& list1, const std::list<char>& list2)
{
    for (std::list<char>::const_iterator it = list2.begin(); it != list2.end(); ++it) {
        list1.push_back(*it);
    }
}


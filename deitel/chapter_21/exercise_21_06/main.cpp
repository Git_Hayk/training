#include "headers/Concat.hpp"
#include <iostream>
#include <list>

int
main()
{
    const char chars1[] = { 'H', 'e', 'l', 'l', 'o' };
    std::list<char> charList1(std::begin(chars1), std::end(chars1));
    std::cout << "charList1 is: ";
    printList(charList1);

    const char chars2[] = { 'W', 'o', 'r', 'l', 'd' };
    std::list<char> charList2(std::begin(chars2), std::end(chars2));
    std::cout << "charList2 is: ";
    printList(charList2);

    concatenate(charList1, charList2);
    std::cout << "New character list after concatenation: ";
    printList(charList1);
    
    return 0;
}


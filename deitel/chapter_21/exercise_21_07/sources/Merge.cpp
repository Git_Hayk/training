#include "headers/Merge.hpp"
#include <iostream>


void
printList(const int_list& l)
{
    for (int_list::const_iterator it = l.begin(); it != l.end(); ++it) {
        std::cout << *it << ' ';
    }
    std::cout << std::endl;
}

void
merge(const int_list& list1, const int_list& list2, int_list& newList)
{
    newList = list1;
    for (int_list::const_iterator it = list2.begin(); it != list2.end(); ++it) {
        newList.push_back(*it);
    }
    newList.sort();
}
 

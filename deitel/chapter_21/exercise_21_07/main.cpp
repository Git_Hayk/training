#include "headers/Merge.hpp"
#include <iostream>
#include <list>

int
main()
{
    const int array1[] = { 0, 2, 4, 6, 8 };
    const std::list<int> intList1(array1, array1 + sizeof(array1) / sizeof(int));
    printList(intList1);

    const int array2[] = { -5, -2, 1, 4, 7, 10 };
    const std::list<int> intList2(array2, array2 + sizeof(array2) / sizeof(int));
    printList(intList2);

    std::list<int> mergedOrderedList;
    merge(intList1, intList2, mergedOrderedList);
    printList(mergedOrderedList);

    return 0;
}


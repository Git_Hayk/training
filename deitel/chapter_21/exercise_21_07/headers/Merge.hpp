#ifndef __MERGE_HPP__
#define __MERGE_HPP__

#include <list>
typedef std::list<int> int_list;

void printList(const int_list& listToPrint);
void merge(const int_list& list1, const int_list& list2, int_list& newList);

#endif ///__MERGE_HPP__


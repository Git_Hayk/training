#include "headers/Stack.hpp"
#include <benchmark/benchmark.h>

const size_t ITER_COUNT = 2<<10;

static void
BM_push(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    while (state.KeepRunning()) {
        Stack<int> intStack;
        for (size_t i = 0; i < ITER_COUNT; ++i) {
            intStack.push(i);
        }
    }
    std::cout.clear();
    state.SetBytesProcessed(ITER_COUNT * state.iterations());
}
BENCHMARK(BM_push);

BENCHMARK_MAIN();


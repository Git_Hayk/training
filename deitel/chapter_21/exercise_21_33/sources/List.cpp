#include "headers/List.hpp"

template <typename NODETYPE>
List<NODETYPE>::List()
   : firstPtr_(NULL), lastPtr_(NULL)
{
}

template <typename NODETYPE>
List <NODETYPE>::~List()
{
   if (!isEmpty()) {

      ListNode<NODETYPE>* currentPtr = firstPtr_;
      ListNode<NODETYPE>* tempPtr;

      while (currentPtr != 0) { // delete remaining nodes
         tempPtr = currentPtr;
         std::cout << tempPtr->data_ << '\n';
         currentPtr = currentPtr->nextPtr_;
         delete tempPtr;
      }
   }
}

template <typename NODETYPE>
inline void
List<NODETYPE>::insertAtFront(const NODETYPE& value)
{
    ListNode<NODETYPE>* newPtr = getNewNode(value);

    if (isEmpty()) {
        firstPtr_ = lastPtr_ = newPtr; // new list has only one node
    } else {
        newPtr->nextPtr_ = firstPtr_; // point new node to previous 1st node
        firstPtr_ = newPtr; // aim firstPtr_ at new node
    }
}

template <typename NODETYPE>
inline void
List<NODETYPE>::insertAtBack(const NODETYPE& value)
{
    ListNode<NODETYPE>* newPtr = getNewNode(value);

    if (isEmpty()) {
        firstPtr_ = lastPtr_ = newPtr; // new list has only one node
    } else {
        lastPtr_->nextPtr_ = newPtr; // update previous last node
        lastPtr_ = newPtr; // new last node
    }
}

template <typename NODETYPE>
inline void
List<NODETYPE>::insertBeforePos(size_t pos, const NODETYPE& value)
{
    if (0 == pos) {
        insertAtFront(value);
        return;
    }
    assert(pos != getSize());
    ListNode<NODETYPE>* tmpPtr = firstPtr_;
    for (size_t it = 1; it < pos; ++it) {
        tmpPtr = tmpPtr->nextPtr_;
    }
    ListNode<NODETYPE>* newPtr = new ListNode<NODETYPE>(value, tmpPtr->nextPtr_);
    tmpPtr->nextPtr_ = newPtr;
}

template <typename NODETYPE>
inline bool
List<NODETYPE>::removeFromFront(NODETYPE& value)
{
    if (isEmpty()) {
        return false;
    }
    ListNode<NODETYPE>* tempPtr = firstPtr_; // hold tempPtr to delete

    if (firstPtr_ == lastPtr_) {
        firstPtr_ = lastPtr_ = NULL; // no nodes remain after removal
    } else {
        firstPtr_ = firstPtr_->nextPtr_; // point to previous 2nd node
    }

    value = tempPtr->data_; // return data_ being removed
    delete tempPtr; // reclaim previous front node
    return true;
}

template <typename NODETYPE>
inline bool
List<NODETYPE>::removeFromBack(NODETYPE& value)
{
    if (isEmpty()) {
        return false;
    }
    ListNode<NODETYPE> *tempPtr = lastPtr_; // hold tempPtr to delete

    if (firstPtr_ == lastPtr_) { // List has one element
        firstPtr_ = lastPtr_ = NULL; // no nodes remain after removal
    } else {
        ListNode<NODETYPE> *currentPtr = firstPtr_;

        // locate second-to-last element
        while (currentPtr->nextPtr_ != lastPtr_) {
            currentPtr = currentPtr->nextPtr_; // move to next node
        }

        lastPtr_ = currentPtr; // remove last node
        currentPtr->nextPtr_ = NULL; // this is now the last node
    }

    value = tempPtr->data_; // return value from old last node
    delete tempPtr; // reclaim former last node
    return true;
}

template <typename NODETYPE>
inline bool
List<NODETYPE>::removeBeforePos(size_t pos, NODETYPE& value) 
{
    if (isEmpty()) return false;
    if (1 == pos) {
        return removeFromFront(value);
    } 
    assert(pos <= getSize());
    ListNode<NODETYPE> *tempPtr1 = firstPtr_; // hold tempPtr to delete
    for (size_t i = 0; i != pos - 2; ++i) {
      tempPtr1 = tempPtr1->nextPtr_;
    }
    ListNode<NODETYPE> *tempPtr2 = tempPtr1; // hold tempPtr to delete
    tempPtr1 = tempPtr1->nextPtr_;
    tempPtr2->nextPtr_ = tempPtr2->nextPtr_->nextPtr_;
    value = tempPtr1->data_;
    delete tempPtr1;
    tempPtr1 = NULL;
    return true;
}

template <typename NODETYPE>
inline bool
List<NODETYPE>::isEmpty() const
{
    return firstPtr_ == NULL;
}

template <typename NODETYPE>
inline size_t
List<NODETYPE>::getSize() const
{
    if (firstPtr_ == NULL) return 0;
    size_t size = 1;
    ListNode<NODETYPE> *currentPtr = firstPtr_;
    while (currentPtr->nextPtr_ != NULL) {
        currentPtr = currentPtr->nextPtr_; // move to next node
        ++size;
    }
    return size;
}

template <typename NODETYPE>
inline ListNode<NODETYPE>*
List<NODETYPE>::getNewNode(const NODETYPE& value)
{
    return new ListNode<NODETYPE>(value);
}

template <typename NODETYPE>
inline void
List<NODETYPE>::print() const
{
    if (isEmpty()) {
        std::cout << "The list is empty\n\n";
        return;
    }

    ListNode<NODETYPE>* currentPtr = firstPtr_;

    std::cout << "The list is: ";

    while (currentPtr != NULL) {
        std::cout << currentPtr->data_ << ' ';
        currentPtr = currentPtr->nextPtr_;
    }

    std::cout << "\n\n";
}


#ifndef __STACKCOMPOSITION_HPP__
#define __STACKCOMPOSITION_HPP__

#include "List.hpp"
#include "ListNode.hpp"

template <typename STACKTYPE>
class Stack
{
public:
    void push(const STACKTYPE& data)
    {
        stackList_.insertAtFront(data);
    }

    bool pop(STACKTYPE& data)
    {
        return stackList_.removeFromFront(data);
    }

    bool isStackEmpty() const
    {
        return stackList_.isEmpty();
    }

    void printStack() const
    {
        stackList_.print();
    }
private:
    List<STACKTYPE> stackList_;
};

#endif /// __STACKCOMPOSITION_HPP__


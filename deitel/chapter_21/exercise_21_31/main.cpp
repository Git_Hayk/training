#include "headers/List.hpp"
#include <iostream>
#include <string>
#include <unistd.h>

void
instructions()
{
   if (::isatty(STDIN_FILENO)) {
      std::cout << "Enter one of the following:\n"
         << "  1 to insert at beginning of list\n"
         << "  2 to insert at end of list\n"
         << "  3 to delete from beginning of list\n"
         << "  4 to delete from end of list\n"
         << "  5 to insert before position\n"
         << "  6 to delete before position\n"
         << "  7 to end list processing\n";
   }
}

template <typename T>
void
validatePosition1(List<T>& listObject, size_t position)
{
    if (position == listObject.getSize()) {
        std::cerr << "Error 1: Position out of list range!" << std::endl;
        ::exit(1);
    }
}

template <typename T>
void
validatePosition2(List<T>& listObject, size_t position)
{
    if (0 == position || position > listObject.getSize()) {
        std::cerr << "Error 1: Position out of list range!" << std::endl;
        ::exit(1);
    }
}

// function to test a List
template <typename T>
void
testList(List<T>& listObject, const std::string& typeName)
{
   std::cout << "Testing a List of " << typeName << " values\n";
   instructions(); // display instructions

   int choice;
   size_t position;
   T value;

   do {
      std::cout << "? ";
      std::cin >> choice;

      switch (choice) {
         case 1: // insert at beginning
            std::cout << "Enter " << typeName << ": ";
            std::cin >> value;
            listObject.insertAtFront(value);
            listObject.print();
            break;
         case 2: // insert at end
            std::cout << "Enter " << typeName << ": ";
            std::cin >> value;
            listObject.insertAtBack(value);
            listObject.print();
            break;
         case 3: // remove from beginning
            if (listObject.removeFromFront(value)) {
               std::cout << value << " removed from front\n";
            }
            listObject.print();
            break;
         case 4: // remove from end
            if (listObject.removeFromBack(value)) {
               std::cout << value << " removed from back\n";
            }
            listObject.print();
            break;
         case 5: // remove from end
            std::cout << "Enter " << typeName << ": ";
            std::cin >> value;
            std::cout << "Enter position" << ": ";
            std::cin >> position;
            validatePosition1(listObject, position);
            listObject.insertBeforePos(position, value);
            listObject.print();
            break;
         case 6: // remove from pos
            std::cout << "Enter position" << ": ";
            std::cin >> position;
            validatePosition2(listObject, position);
            if (listObject.removeBeforePos(position, value)) {
               std::cout << value << " removed before position " << position << '\n';
            }
            listObject.print();
            break;
       } // end switch
   } while (choice != 7); // end do...while

   std::cout << "End list test\n\n";
} // end function testList

int
main()
{
   List<int> integerList;
   testList(integerList, "integer");

   List<double> doubleList;
   testList(doubleList, "double");
   return 0;
}


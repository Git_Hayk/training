#ifndef __LIST_HPP__
#define __LIST_HPP__

#include "ListNode.hpp"
#include <iostream>

template <typename NODETYPE>
class List
{
public:
   List();
   ~List();
   void insertAtFront(const NODETYPE& value);
   void insertAtBack(const NODETYPE& value);
   void insertBeforePos(size_t pos, const NODETYPE& value);
   bool removeFromFront(NODETYPE& value);
   bool removeFromBack(NODETYPE& value);
   bool removeBeforePos(size_t position, NODETYPE& value);

   bool isEmpty() const;
   size_t getSize() const;
   void print() const;
private:
   ListNode<NODETYPE>* getNewNode(const NODETYPE& value);
private:
   ListNode<NODETYPE>* firstPtr_;
   ListNode<NODETYPE>* lastPtr_;
};

#include "sources/List.cpp"

#endif ///__LIST_HPP__


#include "headers/List.hpp"
#include <benchmark/benchmark.h>

static void
BM_example(benchmark::State& state)
{
    int i = 0;
    while (state.KeepRunning()) {
        ++i;
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_example);

BENCHMARK_MAIN();


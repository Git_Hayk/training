#ifndef __INDEXED_LIST_HPP__
#define __INDEXED_LIST_HPP__

#include "List.hpp"
#include <vector>

template <typename NODETYPE>
class IndexedList : public List<NODETYPE>
{
public:
    //IndexedList() : indices_(new List<NODETYPE>::List()) {}
    //~IndexedList();
    IndexedList(const NODETYPE& value);

    void insertInIndexedList(const NODETYPE& value);
    List<NODETYPE>* searchInIndexedList(const NODETYPE& value);
    bool deleteFromIndexedList(const NODETYPE& value);

private:
    std::pair<size_t, bool> searchHelper(const NODETYPE& value);

private:
    std::vector<List<NODETYPE> > indices_;
};

#include "sources/IndexedList.cpp"

#endif /// __INDEXED_LIST_HPP__

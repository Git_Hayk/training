#include "headers/IndexedList.hpp"
#include <iostream>
#include <string>
#include <unistd.h>

int
main()
{
   std::string text1;
   std::cin >> text1;
   IndexedList<std::string> stringIndexedList(text1);
   return 0;
}


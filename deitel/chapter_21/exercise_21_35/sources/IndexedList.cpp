#include "headers/IndexedList.hpp"

template <typename NODETYPE>
IndexedList<NODETYPE>::IndexedList(const NODETYPE& value)
{
    List<NODETYPE> firstList;
    firstList.insertAtBack(value);
    indices_.reserve(10);
    indices_.push_back(firstList);
}

//template <typename NODETYPE>
//IndexedList<NODETYPE>::~IndexedList()
//{
//}


template <typename NODETYPE>
void
IndexedList<NODETYPE>::insertInIndexedList(const NODETYPE& value)
{
    if (indices_.empty()) {
        List<NODETYPE> newList;
        newList.insertAtBack(value);
        indices_.push_back(newList);
        return;
    }
    std::pair<size_t, bool> result = searchHelper(value);
    if (!result.second) {
        List<NODETYPE> newList;
        newList.insertAtBack(value);
        indices_.push_back(newList);
    } else {
        indices_[result.first].insertAtBack(value);
    }
}

template <>
std::pair<size_t, bool>
IndexedList<std::string>::searchHelper(const std::string& value)
{
    std::pair<size_t, bool> result(0, false);
    for (size_t i = 0; i < indices_.size(); ++i) {
        std::string temp = indices_[i].getFirst();
        if (temp[0] == value[0]) {
            result.first = i;
            result.second = true;
        }
    }
    return result;
}


#include "headers/Tree.hpp"

template <typename NODETYPE>
Tree<NODETYPE>::Tree()
    : rootPtr_(NULL)
{
}

template <typename NODETYPE>
void
Tree<NODETYPE>::insertNode(const NODETYPE& value)
{
   insertNodeHelper(&rootPtr_, value);
}

// utility function called by insertNode; receives a pointer
// to a pointer so that the function can modify pointer's value
template <typename NODETYPE>
void
Tree<NODETYPE>::insertNodeHelper(TreeNode<NODETYPE>** ptr, const NODETYPE& value)
{
    // subtree is empty; create new TreeNode containing value
    if (*ptr == NULL) {
        *ptr = new TreeNode<NODETYPE>(value);
    } else { // subtree is not empty

        if (value < (*ptr)->data_) {
            insertNodeHelper(&((*ptr)->leftPtr_), value);
        } else if (value > (*ptr)->data_) {
            insertNodeHelper(&((*ptr)->rightPtr_), value);
        }

    }
}

template <typename NODETYPE>
size_t
Tree<NODETYPE>::depth() const
{
    return depthHelper(rootPtr_);
}

template <typename NODETYPE>
size_t
Tree<NODETYPE>::depthHelper(const TreeNode<NODETYPE>* ptr) const
{
    if (ptr == NULL) {
        return 0;
    }
    return std::max(depthHelper(ptr->leftPtr_), depthHelper(ptr->rightPtr_)) + 1;
}

// begin preorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::preOrderTraversal() const
{
    preOrderHelper(rootPtr_);
}

// utility function to perform preorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::preOrderHelper(TreeNode<NODETYPE>* ptr) const
{
    if (ptr != 0) {
        std::cout << ptr->data_ << ' '; // process node
        preOrderHelper(ptr->leftPtr_); // traverse left subtree
        preOrderHelper(ptr->rightPtr_); // traverse right subtree
    }
}

// begin inorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::inOrderTraversal() const
{
    inOrderHelper(rootPtr_);
}

// utility function to perform inorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::inOrderHelper(TreeNode<NODETYPE>* ptr) const
{
    if (ptr != 0) {
        inOrderHelper(ptr->leftPtr_); // traverse left subtree  
        std::cout << ptr->data_ << ' '; // process node
        inOrderHelper(ptr->rightPtr_); // traverse right subtree
    }
}

// begin postorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::postOrderTraversal() const
{
    postOrderHelper(rootPtr_);
}

// utility function to perform postorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::postOrderHelper(TreeNode<NODETYPE>* ptr) const
{
    if (ptr != 0) {
        postOrderHelper(ptr->leftPtr_); // traverse left subtree  
        postOrderHelper(ptr->rightPtr_); // traverse right subtree
        std::cout << ptr->data_ << ' '; // process node
    }
}

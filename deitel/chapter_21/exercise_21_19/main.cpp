#include "headers/Tree.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>

int
main()
{
    Tree<double> doubleTree;
    double doubleValue;

    std::cout << std::fixed << std::setprecision(1);

    if (::isatty(STDIN_FILENO))
        std::cout << "\n\n\nEnter 10 double values:\n";

    for (int j = 0; j < 10; ++j) {
        std::cin >> doubleValue;
        doubleTree.insertNode(doubleValue);
    }

    std::cout << "\nDepth of tree\n" << doubleTree.depth() << std::endl;

    std::cout << "\nPreorder traversal\n";
    doubleTree.preOrderTraversal();

    std::cout << "\nInorder traversal\n";
    doubleTree.inOrderTraversal();

    std::cout << "\nPostorder traversal\n";
    doubleTree.postOrderTraversal();

    std::cout << std::endl;
    return 0;
}


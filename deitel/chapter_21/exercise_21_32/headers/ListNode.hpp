#ifndef __LISTNODE_HPP__
#define __LISTNODE_HPP__

#include <cstdlib>

template <typename NODETYPE> class List;                            

template <typename NODETYPE>
class ListNode
{
   friend class List<NODETYPE>;
public:
   ListNode(const NODETYPE&, ListNode<NODETYPE>* ptr = NULL);
   NODETYPE getData() const;
private:
   NODETYPE data_;
   ListNode<NODETYPE> *nextPtr_;
};

template <typename NODETYPE>
ListNode<NODETYPE>::ListNode(const NODETYPE& info, ListNode<NODETYPE>* ptr)
    : data_(info), nextPtr_(ptr)
{
}

template <typename NODETYPE>
NODETYPE
ListNode<NODETYPE>::getData() const
{
   return data_;
}

#endif /// __LISTNODE_HPP__


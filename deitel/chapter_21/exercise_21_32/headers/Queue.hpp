#ifndef __QUEUE_HPP__
#define __QUEUE_HPP__

#include "List.hpp"
#include <iostream>

template <typename QUEUETYPE>
class Queue : private List<QUEUETYPE>
{
public:
    void enqueue(const QUEUETYPE& data) {
        List<QUEUETYPE>::insertAtBack(data);
    }

    bool dequeue(QUEUETYPE& data) {
        return List<QUEUETYPE>::removeFromFront(data);
    } 

    bool isQueueEmpty() const {
        return List<QUEUETYPE>::isEmpty();
    }

    void printQueue() const {
        List<QUEUETYPE>::print();
    }
};

#endif ///__QUEUE_HPP__


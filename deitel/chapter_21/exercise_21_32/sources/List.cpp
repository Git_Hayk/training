#include "headers/List.hpp"
#include <iostream>

template <typename NODETYPE>
List<NODETYPE>::List()
   : firstPtr_(NULL)
{
}

template <typename NODETYPE>
List <NODETYPE>::~List()
{
   if (!isEmpty()) {

      ListNode<NODETYPE>* currentPtr = firstPtr_;
      ListNode<NODETYPE>* tempPtr;

      while (currentPtr != 0) { // delete remaining nodes
         tempPtr = currentPtr;
         std::cout << tempPtr->data_ << '\n';
         currentPtr = currentPtr->nextPtr_;
         delete tempPtr;
      }
   }
}

template <typename NODETYPE>
void
List<NODETYPE>::insertAtFront(const NODETYPE& value)
{
    ListNode<NODETYPE>* newPtr = getNewNode(value);

    if (isEmpty()) {
        firstPtr_ = newPtr;
    } else {
        newPtr->nextPtr_ = firstPtr_; // point new node to previous 1st node
        firstPtr_ = newPtr; // aim firstPtr_ at new node
    }
}

template <typename NODETYPE>
void
List<NODETYPE>::insertAtBack(const NODETYPE& value)
{
    ListNode<NODETYPE>* newPtr = getNewNode(value);

    if (isEmpty()) {
        firstPtr_ = newPtr;
    } else {
        ListNode<NODETYPE>* temp = firstPtr_;
        while (temp->nextPtr_ != NULL) {
            temp = temp->nextPtr_;
        }
        temp->nextPtr_ = newPtr; // update previous last node
    }
}

template <typename NODETYPE>
bool
List<NODETYPE>::removeFromFront(NODETYPE& value)
{
    if (isEmpty()) {
        return false;
    }
    ListNode<NODETYPE>* tempPtr = firstPtr_; // hold tempPtr to delete

    if (firstPtr_->nextPtr_ == NULL) {
        firstPtr_ = NULL; // no nodes remain after removal
    } else {
        firstPtr_ = firstPtr_->nextPtr_; // point to previous 2nd node
    }

    value = tempPtr->data_; // return data_ being removed
    delete tempPtr; // reclaim previous front node
    return true;
}

template <typename NODETYPE>
bool
List<NODETYPE>::removeFromBack(NODETYPE& value)
{
    if (isEmpty()) {
        return false;
    }
    ListNode<NODETYPE> *tempPtr = firstPtr_; // hold tempPtr to delete
    while (tempPtr->nextPtr_ != NULL) {
        tempPtr = tempPtr->nextPtr_;
    }

    if (firstPtr_->nextPtr_ == NULL) { // List has one element
        firstPtr_ = NULL; // no nodes remain after removal
    } else {
        ListNode<NODETYPE> *currentPtr = firstPtr_;

        // locate second-to-last element
        while (currentPtr->nextPtr_ != tempPtr) {
            currentPtr = currentPtr->nextPtr_; // move to next node
        }

        currentPtr->nextPtr_ = NULL; // this is now the last node
    }

    value = tempPtr->data_; // return value from old last node
    delete tempPtr; // reclaim former last node
    return true;
}

template <typename NODETYPE>
bool
List<NODETYPE>::isEmpty() const
{
    return firstPtr_ == NULL;
}

template <typename NODETYPE>
size_t
List<NODETYPE>::getSize() const
{
    if (firstPtr_ == NULL) return 0;
    size_t size = 1;
    ListNode<NODETYPE> *currentPtr = firstPtr_;
    while (currentPtr->nextPtr_ != NULL) {
        currentPtr = currentPtr->nextPtr_; // move to next node
        ++size;
    }
    return size;
}

template <typename NODETYPE>
ListNode<NODETYPE>*
List<NODETYPE>::getNewNode(const NODETYPE& value)
{
    return new ListNode<NODETYPE>(value);
}

template <typename NODETYPE>
void
List<NODETYPE>::print() const
{
    if (isEmpty()) {
        std::cout << "The list is empty\n\n";
        return;
    }

    ListNode<NODETYPE>* currentPtr = firstPtr_;

    std::cout << "The list is: ";

    while (currentPtr != NULL) {
        std::cout << currentPtr->data_ << ' ';
        currentPtr = currentPtr->nextPtr_;
    }

    std::cout << "\n\n";
}

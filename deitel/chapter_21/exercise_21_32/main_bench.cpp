#include "headers/Queue.hpp"
#include <benchmark/benchmark.h>

static void
BM_example(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    Queue<int> intQueue;
    while (state.KeepRunning()) {
        intQueue.enqueue(1);
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_example);

BENCHMARK_MAIN();


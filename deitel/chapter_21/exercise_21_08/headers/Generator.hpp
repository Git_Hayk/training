#ifndef __GENERATOR_HPP__
#define __GENERATOR_HPP__

#include <list>

namespace cd06 {

template <typename Generator>
void generate(std::list<int>& randList, const unsigned int count, Generator gen);

template <typename ForwardIterator>
void printList(ForwardIterator first, const ForwardIterator last);

template <typename ForwardIterator>
unsigned int calculateSum(ForwardIterator first, const ForwardIterator last);

double calculateAverage(const unsigned int sum, const unsigned int count);

} /// namespace cd06

#include "sources/Generator.cpp"

#endif /// __GENERATOR_HPP__

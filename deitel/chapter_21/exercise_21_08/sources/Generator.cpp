#include "headers/Generator.hpp"
#include <iostream>

namespace cd06 {

template <typename Generator>
void
generate(std::list<int>& randList, const unsigned int count, Generator gen)
{
    const int LIMIT = 101;
    for (unsigned int i = 0; i < count; ++i) {
        const int number = gen() % LIMIT;
        randList.push_back(number);
    }
}

template <typename ForwardIterator>
void
printList(ForwardIterator first, const ForwardIterator last)
{
    for ( ; first != last; ++first) {
        std::cout << *first << ", ";
    }
    std::cout << std::endl;
}

template <typename ForwardIterator>
unsigned int
calculateSum(ForwardIterator first, const ForwardIterator last)
{
    unsigned int sum = 0;
    for ( ; first != last; ++first) {
        sum += static_cast<unsigned int>(*first);
    }
    return sum;
}

double
calculateAverage(const unsigned int sum, const unsigned int count)
{
    return static_cast<double>(sum) / count;
}

} /// namesapce cd06


#include "headers/Generator.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <list>

int
main()
{
    const unsigned int NUMBER_COUNT = 25;
    if (::isatty(STDIN_FILENO)) {
        std::srand(::time(0));
    }
    std::list<int> randomNumbers;
    cd06::generate(randomNumbers, NUMBER_COUNT, std::rand);
    cd06::printList(randomNumbers.begin(), randomNumbers.end());
    
    const unsigned int sum = cd06::calculateSum(randomNumbers.begin(), randomNumbers.end());
    std::cout << "Sum of list elements is " << sum << std::endl;

    const double average = cd06::calculateAverage(sum , NUMBER_COUNT);
    std::cout << std::fixed << std::setprecision(2);
    std::cout << "Average of list lements is " << average << std::endl;

    return 0;
}


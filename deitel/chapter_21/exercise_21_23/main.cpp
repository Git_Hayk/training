#include "headers/Tree.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>

bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    Tree<int> intTree;
    int intValue;

    std::cout << std::fixed << std::setprecision(1);

    if (isInteractive)
        std::cout << "Input 10 integer values:\n";

    for (int j = 0; j < 10; ++j) {
        std::cin >> intValue;
        intTree.insertNode(intValue);
    }

    std::cout << "\nInorder traversal\n";
    intTree.inOrderTraversal();
    std::cout << '\n';
    
    bool result;
    do {
        int value = 1.0;
        if (isInteractive) {
            std::cout << "\nInput value to search in tree of integers: ";
        }
        std::cin >> value;
        result = static_cast<bool>(intTree.binaryTreeSearch(value));
        std::cout << "value " << value << " was" << (result ? " found" : " not found") << " in tree of integers." << std::endl;
    } while (result);

    return 0;
}


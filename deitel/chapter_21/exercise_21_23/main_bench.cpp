#include "headers/Tree.hpp"
#include <benchmark/benchmark.h>

static void
BM_example(benchmark::State& state)
{
    ///std::cout.setstate(std::ios_base::failbit);
    Tree<int> intTree;
    int intValue;
    for (int j = 0; j < 10; ++j) {
        std::cin >> intValue;
        intTree.insertNode(intValue);
    }
    while (state.KeepRunning()) {
        intTree.binaryTreeSearch(intValue);
    }
    ///std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_example);

BENCHMARK_MAIN();


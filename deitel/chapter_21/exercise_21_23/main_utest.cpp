#include "headers/Tree.hpp"
#include <gtest/gtest.h>

TEST(Tree, binarySearch)
{
    Tree<int> tree;
    for (int i = 0; i < 20; i += 2) {
        tree.insertNode(i);
    }
    EXPECT_TRUE(tree.binaryTreeSearch(0));
    EXPECT_TRUE(tree.binaryTreeSearch(8));
    EXPECT_TRUE(tree.binaryTreeSearch(10));
    EXPECT_TRUE(tree.binaryTreeSearch(18));
    EXPECT_TRUE(tree.binaryTreeSearch(12));
    EXPECT_FALSE(tree.binaryTreeSearch(1));
    EXPECT_FALSE(tree.binaryTreeSearch(3));
    EXPECT_FALSE(tree.binaryTreeSearch(11));
    EXPECT_FALSE(tree.binaryTreeSearch(17));
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


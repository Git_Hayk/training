#include "headers/Tree.hpp"
#include <iostream>
#include <iomanip>
#include <unistd.h>

bool isInteractive = ::isatty(STDIN_FILENO);

int
main()
{
    Tree<int> intTree;
    int intValue;

    std::cout << std::fixed << std::setprecision(1);

    if (isInteractive)
        std::cout << "Input 10 integer values:\n";

    for (int j = 0; j < 10; ++j) {
        std::cin >> intValue;
        intTree.insertNode(intValue);
    }

    std::cout << "\nInorder traversal\n";
    intTree.inOrderTraversal();
    std::cout << '\n';

    int value = 1.0;
    while (value != 0.0) {
        if (isInteractive) {
            std::cout << "Input value to delete from tree (0 to quit): ";
        }
        std::cin >> value;
        intTree.deleteNode(value);
        std::cout << "\nInorder traversal\n";
        intTree.inOrderTraversal();
        std::cout << '\n';
    }

    return 0;
}


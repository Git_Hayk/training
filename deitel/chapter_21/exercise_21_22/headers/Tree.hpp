#ifndef __TREE_HPP__
#define __TREE_HPP__

#include "TreeNode.hpp"
#include <iostream>

template <typename NODETYPE> class Tree
{
public:
    Tree();

    void insertNode(const NODETYPE& value);
    void deleteNode(const NODETYPE& value);

    void preOrderTraversal() const;     
    void inOrderTraversal() const;      
    void postOrderTraversal() const;

    TreeNode<NODETYPE>* binaryTreeSearch(const NODETYPE& value) const;
private:
    // utility functions
    void insertNodeHelper(TreeNode<NODETYPE>** ptr, const NODETYPE& value);
    void deleteNodeHelper(TreeNode<NODETYPE>** ptr, const NODETYPE& value);
    void preOrderHelper(TreeNode<NODETYPE>* ptr) const;
    void inOrderHelper(TreeNode<NODETYPE>* ptr) const;                
    void postOrderHelper(TreeNode<NODETYPE>* ptr) const;


    TreeNode<NODETYPE>* findParent(TreeNode<NODETYPE>* ptr, const NODETYPE& value) const;
    TreeNode<NODETYPE>* binaryTreeSearchHelper(TreeNode<NODETYPE>* ptr, const NODETYPE& value) const;
    TreeNode<NODETYPE>* mostLeft(TreeNode<NODETYPE>* ptr) const;
    TreeNode<NODETYPE>* mostRight(TreeNode<NODETYPE>* ptr) const;
    void swapData(TreeNode<NODETYPE>* ptr1, TreeNode<NODETYPE>* ptr2);
private:
    TreeNode<NODETYPE>* rootPtr_;
};

#include "sources/Tree.cpp"

#endif /// __TREE_HPP__


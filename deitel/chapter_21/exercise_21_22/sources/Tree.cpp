#include "headers/Tree.hpp"

template <typename NODETYPE>
Tree<NODETYPE>::Tree()
    : rootPtr_(NULL)
{
}

template <typename NODETYPE>
void
Tree<NODETYPE>::insertNode(const NODETYPE& value)
{
   insertNodeHelper(&rootPtr_, value);
}

// utility function called by insertNode; receives a pointer
// to a pointer so that the function can modify pointer's value
template <typename NODETYPE>
void
Tree<NODETYPE>::insertNodeHelper(TreeNode<NODETYPE>** ptr, const NODETYPE& value)
{
    // subtree is empty; create new TreeNode containing value
    if (*ptr == NULL) {
        *ptr = new TreeNode<NODETYPE>(value);
    } else { // subtree is not empty

        if (value < (*ptr)->data_) {
            insertNodeHelper(&((*ptr)->leftPtr_), value);
        } else if (value > (*ptr)->data_) {
            insertNodeHelper(&((*ptr)->rightPtr_), value);
        }

    }
}

template <typename NODETYPE>
void
Tree<NODETYPE>::deleteNode(const NODETYPE& value)
{
    if (NULL == rootPtr_) return;
    deleteNodeHelper(&rootPtr_, value);
}

template <typename NODETYPE>
void
Tree<NODETYPE>::deleteNodeHelper(TreeNode<NODETYPE>** ptr, const NODETYPE& value)
{
    TreeNode<NODETYPE>* target = binaryTreeSearchHelper(*ptr, value);
    bool exist = static_cast<bool>(target);
    if (!exist) return;
    if (target->leftPtr_ == NULL && target->rightPtr_ == NULL) {
        delete target;
        target = NULL;
        return;
    }
    if (target->leftPtr_ != NULL && target->rightPtr_ != NULL) {
        TreeNode<NODETYPE>* rightMostLeft = mostLeft(target->rightPtr_);
        swapData(rightMostLeft, target);
        delete rightMostLeft;
        rightMostLeft = NULL;
    } else if (target->rightPtr_ == NULL) {
        TreeNode<NODETYPE>* leftMostRight = mostRight(target->leftPtr_);
        swapData(leftMostRight, target);
        delete leftMostRight;
        leftMostRight = NULL;
    }
}

template <typename NODETYPE>
TreeNode<NODETYPE>*
Tree<NODETYPE>::mostLeft(TreeNode<NODETYPE>* ptr) const
{
    while (ptr->leftPtr_ != NULL) {
        ptr = ptr->leftPtr_;
    }
    return ptr;
}

template <typename NODETYPE>
TreeNode<NODETYPE>*
Tree<NODETYPE>::mostRight(TreeNode<NODETYPE>* ptr) const
{
    while (ptr->rightPtr_ != NULL) {
        ptr = ptr->rightPtr_;
    }
    return ptr;
}

template <typename NODETYPE>
void
Tree<NODETYPE>::swapData(TreeNode<NODETYPE>* ptr1, TreeNode<NODETYPE>* ptr2)
{
    NODETYPE temp = ptr1->data_;
    ptr1->data_ = ptr2->data_;
    ptr2->data_ = temp;
}

// begin preorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::preOrderTraversal() const
{
    preOrderHelper(rootPtr_);
}

// utility function to perform preorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::preOrderHelper(TreeNode<NODETYPE>* ptr) const
{
    if (ptr != 0) {
        std::cout << ptr->data_ << ' '; // process node
        preOrderHelper(ptr->leftPtr_); // traverse left subtree
        preOrderHelper(ptr->rightPtr_); // traverse right subtree
    }
}

// begin inorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::inOrderTraversal() const
{
    inOrderHelper(rootPtr_);
}

// utility function to perform inorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::inOrderHelper(TreeNode<NODETYPE>* ptr) const
{
    if (ptr != 0) {
        inOrderHelper(ptr->leftPtr_); // traverse left subtree  
        std::cout << ptr->data_ << ' '; // process node
        inOrderHelper(ptr->rightPtr_); // traverse right subtree
    }
}

// begin postorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::postOrderTraversal() const
{
    postOrderHelper(rootPtr_);
}

// utility function to perform postorder traversal of Tree
template <typename NODETYPE>
void
Tree<NODETYPE>::postOrderHelper(TreeNode<NODETYPE>* ptr) const
{
    if (ptr != 0) {
        postOrderHelper(ptr->leftPtr_); // traverse left subtree  
        postOrderHelper(ptr->rightPtr_); // traverse right subtree
        std::cout << ptr->data_ << ' '; // process node
    }
}

template <typename NODETYPE>
TreeNode<NODETYPE>*
Tree<NODETYPE>::binaryTreeSearch(const NODETYPE& value) const
{
    return binaryTreeSearchHelper(rootPtr_, value);
}

template <typename NODETYPE>
TreeNode<NODETYPE>*
Tree<NODETYPE>::binaryTreeSearchHelper(TreeNode<NODETYPE>* ptr, const NODETYPE& value) const
{
    if (ptr == NULL || value == ptr->data_) {
        return ptr;
    } else {

        if (value < ptr->data_) {
            ptr = binaryTreeSearchHelper(ptr->leftPtr_, value);
        } else if (value > ptr->data_) {
            ptr = binaryTreeSearchHelper(ptr->rightPtr_, value);
        }

    }
    return ptr;
}

template <typename NODETYPE>
TreeNode<NODETYPE>*
Tree<NODETYPE>::findParent(TreeNode<NODETYPE>* ptr, const NODETYPE& value) const
{

    if (ptr != NULL) {
        if (ptr->leftPtr_ != NULL && ptr->leftPtr_->data_ == value) {
            return ptr;
        }
        if (ptr->rightPtr_ != NULL && ptr->rightPtr_->data_ == value) {
            return ptr;
        }
    } else {

        if (value < ptr->data_) {
            ptr = binaryTreeSearchHelper(ptr->leftPtr_, value);
        } else if (value > ptr->data_) {
            ptr = binaryTreeSearchHelper(ptr->rightPtr_, value);
        }

    }
    return ptr;

}


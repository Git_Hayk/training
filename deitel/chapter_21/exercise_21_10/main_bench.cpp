#include "headers/PrintReversed.hpp"
#include <benchmark/benchmark.h>

static void
BM_print_rversed_stack(benchmark::State& state)
{
    const char* text = "Hello World";
    while (state.KeepRunning()) {
        printReversed(text); /// print text reversed using stack
    }
}
BENCHMARK(BM_print_rversed_stack);

BENCHMARK_MAIN();


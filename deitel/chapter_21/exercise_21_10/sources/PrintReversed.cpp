#include "headers/PrintReversed.hpp"
#include <iostream>
#include <limits.h>
#include <stack>

void
printReversed(const char* const text)
{
    const size_t s = strlen(text);
    std::stack<char> charStack;
    for (size_t i = s - 1; i != ULLONG_MAX; --i) {
        charStack.push(*(text + i));
        std::cout << charStack.top();
        charStack.pop();
    }
}


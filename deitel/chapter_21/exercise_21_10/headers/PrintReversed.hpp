#ifndef __PRINTREVERSED_HPP__
#define __PRINTREVERSED_HPP__

void printReversed(const char* text);

#include "sources/PrintReversed.cpp"
#endif ///__PRINTREVERSED_HPP__


#include "headers/PrintReversed.hpp"
#include <iostream>

int
main()
{
    const char* const text = "Hello World";
    std::cout << text << std::endl;
    printReversed(text);
    return 0;
}


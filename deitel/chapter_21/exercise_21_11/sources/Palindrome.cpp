#include "headers/Palindrome.hpp"
#include <iostream>
#include <stack>

bool
isPalindrome(const std::string& text)
{
    const size_t size = text.size();
    std::stack<char> s1;
    std::stack<char> s2;

    for (size_t i = 0, j = size - 1; i < size; ++i, --j) {
        if (::isalpha(text[i])) {
            s1.push(text[i]);
        }
        if (::isalpha(text[j])) {
            s2.push(text[j]);
        }
    }
    return s1 == s2;
}


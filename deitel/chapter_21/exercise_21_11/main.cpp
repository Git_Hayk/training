#include "headers/Palindrome.hpp"
#include <iostream>
#include <unistd.h>

const int BUFFER = 256;

int
main()
{
    char* text = new char[BUFFER];
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input word or sentence to chek if it is palindrome: ";
    }
    std::cin.getline(text, BUFFER);
    std::cout << "This text is " << (isPalindrome(text) ? "palindrome" : "not palindrome") << std::endl;

    return 0;
}


#ifndef __PALINDROME_HPP__
#define __PALINDROME_HPP__

#include <string>

bool isPalindrome(const std::string& text);

#endif ///__PALINDROME_HPP__


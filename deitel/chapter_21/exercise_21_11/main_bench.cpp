#include "headers/Palindrome.hpp"
#include <benchmark/benchmark.h>

static void
BM_isPalindrome_ch_ptr(benchmark::State& state)
{
    const char* text = "racecar";
    while (state.KeepRunning()) {
        isPalindrome(text);
    }
}
BENCHMARK(BM_isPalindrome_ch_ptr);

static void
BM_isPalindrome_std_string(benchmark::State& state)
{
    const std::string text = "racecar";
    while (state.KeepRunning()) {
        isPalindrome(text);
    }
}
BENCHMARK(BM_isPalindrome_std_string);


BENCHMARK_MAIN();


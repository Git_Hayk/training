# Training

This practical project has been developed by me since the beginning of 2022. It solves about 200 exercises from Deitel's book, implements a data structure and algorithms based on the C++ language, also known as STL, and much more. See below for a more detailed description of each section.


*To clone a project, create an empty folder with any name and follow the instructions*


```
mkdir example
cd example
git clone https://gitlab.com/Git_Hayk/training
```

***

## Section and their descriptions.

_**Assoc**_

A small associative program that takes an integer value as input and returns the name as a string, or vice versa receives the string name of a digit and returns an integer value in constant time.

_**MiniComputer**_

А small computing program designed to learn the basics of OOP in the C++ language.

_**benchmarking**_

Various logical and interesting performance tasks.

_**deitel**_

Exercises from Deitel book. The requirements of each exercise are described in the comments in .cpp file.

_**dsa**_

Data Structures and Algorithms. A large number of algorithms and data structures based on the C++ STL have been developed here from scratch.
Тhis is probably the biggest and most important project so far.

_**socket_app**_

A Client-Server application developed in the C language that uses multithreading to connect to multiple clients, receive requests from them, and send back the results.
More details are described in it.

## Links

>[LinkedIn](https://www.linkedin.com/in/hayk-harutyunyan-59094b226/)

>[CodeWars](https://www.codewars.com/users/Hayk_Harutyunyan95)


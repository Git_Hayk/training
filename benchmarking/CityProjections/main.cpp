#include "headers/CityProjections.hpp"
#include <cstddef>

const size_t CITY_WIDTH = 3;
const size_t CITY_LENGTH = 6;

int
main()
{
    size_t city[CITY_WIDTH][CITY_LENGTH] = { { 1, 0, 4, 2, 6, 4 },
                                             { 4, 3, 7, 0, 2, 5 },
                                             { 3, 1, 9, 5, 8, 0 }, };
    projectionsAreas(city);

    return 0;
}


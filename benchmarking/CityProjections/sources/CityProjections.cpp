#include "headers/CityProjections.hpp"

template <size_t width, size_t length>
void
projectionsAreas(size_t (&city)[width][length])
{
    double areas[PROJECTIONS];
    size_t buildCount = 0; /// general count of buildings
    size_t maxFromFrontal = 0; /// sores big buildings floor count looking from frontal plane
    size_t maxFromSagittal = 0; /// sores big buildings floor count looking from sagittal plane
    size_t lastAdded[length] = {0}; /// stores last added highest value in column

    for (size_t row = 0; row < width; ++row) {
        size_t biggestInRow = city[row][0]; /// stores highest value in row
        for (size_t column = 0; column < length; ++column) {
            if (0 != city[row][column]) {
                ++buildCount;
            }
            if (city[row][column] > biggestInRow) {
                biggestInRow = city[row][column];
            }
            if (0 != row && city[row][column] > lastAdded[column]) {
                maxFromSagittal -= lastAdded[column];
                lastAdded[column] = city[row][column];
                maxFromSagittal += city[row][column];
            } else if (0 == row) {
                lastAdded[column] = city[row][column];
                maxFromSagittal += city[row][column];
            }
        }
        maxFromFrontal += biggestInRow;
    }

    areas[0] = buildCount * BUILDING_WIDTH * BUILDING_LENGTH;
    areas[1] = maxFromFrontal * BUILDING_HEIGHT * BUILDING_WIDTH;
    areas[2] = maxFromSagittal * BUILDING_HEIGHT * BUILDING_LENGTH;

    printArray(areas);
}

template <typename T, size_t size>
void
printArray(const T (&arr)[size])
{
    std::cout << "{ ";
    for (size_t i = 0; i < size; ++i) {
        std::cout << arr[i] << (i == size - 1 ? " }" : ", ");
    }
    std::cout << '\n';
}


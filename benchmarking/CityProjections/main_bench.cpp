#include "headers/CityProjections.hpp"
#include <benchmark/benchmark.h>

const size_t CITY_WIDTH  = 3;
const size_t CITY_LENGTH = 6;

static void
BM_example(benchmark::State& state)
{
    size_t city[CITY_WIDTH][CITY_LENGTH] = { { 1, 0, 4, 2, 6, 4 },
                                             { 4, 3, 7, 0, 2, 5 },
                                             { 3, 1, 9, 5, 8, 0 }, };
    std::cout.setstate(std::ios_base::failbit);
    while (state.KeepRunning()) {
        projectionsAreas(city);
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_example);

BENCHMARK_MAIN();


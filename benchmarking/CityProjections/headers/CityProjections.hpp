#ifndef __CITYPROJECTIONS_HPP__
#define __CITYPROJECTIONS_HPP__

#include <iostream>

const double BUILDING_WIDTH  = 20.0;
const double BUILDING_LENGTH = 20.0;
const double BUILDING_HEIGHT = 3.2;
const size_t PROJECTIONS = 3;

template <size_t width, size_t length>
void projectionsAreas(size_t (&city)[width][length]);

template <typename T, size_t size>
void printArray(const T (&arr)[size]);

#include "sources/CityProjections.cpp"

#endif ///__CITYPROJECTIONS_HPP__


#include "headers/TextJustification.hpp"

template <size_t size>
std::vector<std::string>
justificate(std::string (&text)[size])
{
    std::string newText[SIZE];
    for (size_t i = 0; i < size; ++i) {
        newText.push_back(text[i]);
    }
    return newText;
}

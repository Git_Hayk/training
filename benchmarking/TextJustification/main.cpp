#include "headers/TextJustification.hpp"

const size_t BUFFER = 256;

int
main()
{
    std::string text[BUFFER] = { "The", "quick", "brown", "fox", "jumbed", "over", "the", "lazy", "dog" };

    std::string newText[] = justificate(text, width);
    return 0;
}


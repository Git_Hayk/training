#ifndef __TEXT_JUSTIFICATION_HPP__
#define __TEXT_JUSTIFICATION_HPP__

#include <iostream>
#include <string>

const size_t SIZE = 256;

template <size_t size>
std::string justificate(std::string (&text)[size]);

#include "sources/TextJustification.cpp"

#endif ///__TEXT_JUSTIFICATION_HPP__


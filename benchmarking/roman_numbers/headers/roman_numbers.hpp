#ifndef __ROMAN_NUMBERS_HPP__
#define __ROMAN_NUMBERS_HPP__

#include <iostream>
#include <sstream>
#include <string>
#include <map>

class RomanNumber;

std::istream& operator>>(std::istream& input, RomanNumber& rhv);
std::ostream& operator<<(std::ostream& output, const RomanNumber& rhv);

class RomanNumber
{
    friend std::istream& operator>>(std::istream& input, RomanNumber& rhv);
    friend std::ostream& operator<<(std::ostream& output, const RomanNumber& rhv);

private:
    static const int ROMAN_CHARACTERS_COUNT;
    static const char symbol[];
    /// std::map<std::string, int> values;

public:
    RomanNumber();
    RomanNumber(const int intNumber);
    RomanNumber(const std::string& romanNumber);

    void setNumber(const int intNumber);
    void setNumber(const std::string& romanNumber);
    std::string getNumber() const;

    std::string convertToRoman(int intNumber);
    int convertToInteger(const std::string& romanNumber);

private:
    bool isValidRomNumber(const std::string& romanNumber);

private:
    std::string romanNumber_;
};

#endif ///__ROMAN_NUMBERS_HPP__


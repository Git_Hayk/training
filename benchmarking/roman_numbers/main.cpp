#include "headers/roman_numbers.hpp"

int
main()
{
    RomanNumber rom;
    std::cout << "Roman number craeted by default costructor = " << rom << std::endl;

    std::string romanNum;

    while (true) {
        std::cout << "\nInput Roman number using { I, V, X, L, C, D, M } characters!" << std::endl;
        std::cin >> romanNum;
        const int converted = rom.convertToInteger(romanNum);
        std::cout << converted << std::endl;
    }

    int number = 1;
    while (number != 0) {
        std::cin >> number;
        RomanNumber romanNum1(number);
        std::cout << romanNum1 << std::endl;;
    }
    
    return 0;
}


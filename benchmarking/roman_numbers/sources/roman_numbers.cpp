#include "headers/roman_numbers.hpp"

const int
RomanNumber::ROMAN_CHARACTERS_COUNT = 7;

const char
RomanNumber::symbol[ROMAN_CHARACTERS_COUNT] = { 'I', 'V', 'X', 'L', 'C', 'D', 'M' };

//std::map<std::string, int>::value_type arr[] = {
//    std::make_pair("I"   , 1), std::make_pair("X"   , 10), std::make_pair("C"   , 100),
//    std::make_pair("II"  , 2), std::make_pair("XX"  , 20), std::make_pair("CC"  , 200),
//    std::make_pair("III" , 3), std::make_pair("XXX" , 30), std::make_pair("CCC" , 300),
//    std::make_pair("IV"  , 4), std::make_pair("XL"  , 40), std::make_pair("CD"  , 400),
//    std::make_pair("V"   , 5), std::make_pair("L"   , 50), std::make_pair("D"   , 500),
//    std::make_pair("VI"  , 6), std::make_pair("LX"  , 60), std::make_pair("DC"  , 600),
//    std::make_pair("VII" , 7), std::make_pair("LXX" , 70), std::make_pair("DCC" , 700),
//    std::make_pair("VIII", 8), std::make_pair("LXXX", 80), std::make_pair("DCCC", 800),
//    std::make_pair("IX"  , 9), std::make_pair("XC"  , 90), std::make_pair("CM"  , 900),
//                                                           std::make_pair("M"   , 1000) };
//
//std::map<std::string, int> values(arr, arr + (sizeof(arr) / sizeof(arr[0])));

std::istream& operator>>(std::istream& input, RomanNumber& rhv)
{
    input >> rhv.romanNumber_;
    return input;
}

std::ostream& operator<<(std::ostream& output, const RomanNumber& rhv)
{
    output << rhv.getNumber();
    return output;
}

RomanNumber::RomanNumber() : romanNumber_("I") {}

RomanNumber::RomanNumber(const int intNumber)
{
    setNumber(intNumber);   
}

RomanNumber::RomanNumber(const std::string& romanNumber)
{
    setNumber(romanNumber);
}

void
RomanNumber::setNumber(const int intNumber)
{
    if (romanNumber_.length() != 0) {
        romanNumber_.clear();
    }
    romanNumber_ = convertToRoman(intNumber);
}

void
RomanNumber::setNumber(const std::string& romanNumber)
{
    if (isValidRomNumber(romanNumber)) {
        romanNumber_ = romanNumber;
        return;
    }
    std::cerr << "Error 1: Undifined symbol for Roman Number!" << std::endl;
    ::exit(1);
}

std::string
RomanNumber::getNumber() const
{
    return romanNumber_;
}

std::string
RomanNumber::convertToRoman(int intNumber)
{
    std::string toString = std::to_string(intNumber);
    const int numberRank = toString.length();
    int count; /// serves as counter for 'M', then as an index for arrays
    std::string result;

    const std::string hundredthsValue[] = { "", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM" };
    const std::string decimalValue[]    = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
    const std::string digitValue[]      = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
    
    switch (numberRank) {
    case 5:
    case 4: count = intNumber / 1000;
            for (int i = 0; i < count; ++i) {
                result.append("M");
            }
            intNumber %= 1000;
    case 3: count = intNumber / 100;
            result.append(hundredthsValue[count]);
            intNumber %= 100;
    case 2: count = intNumber / 10;
            result.append(decimalValue[count]);
            intNumber %= 10;
    case 1: count = intNumber;
            result.append(digitValue[count]);
            break;
    default: assert(0 && "Invalid case!");
    }
    return result;
}

int
RomanNumber::convertToInteger(const std::string& romanNumber)
{
    if (!isValidRomNumber(romanNumber))
    {
        std::cerr << "Error 1: Undifined symbol for Roman Number!" << std::endl;
        ::exit(1);
    }
    std::map<std::string, int> values;

    values["I"]    = 1; values["X"]    = 10; values["C"]    = 100;
    values["II"]   = 2; values["XX"]   = 20; values["CC"]   = 200;
    values["III"]  = 3; values["XXX"]  = 30; values["CCC"]  = 300;
    values["IV"]   = 4; values["XL"]   = 40; values["CD"]   = 400;
    values["V"]    = 5; values["L"]    = 50; values["D"]    = 500;
    values["VI"]   = 6; values["LX"]   = 60; values["DC"]   = 600;
    values["VII"]  = 7; values["LXX"]  = 70; values["DCC"]  = 700;
    values["VIII"] = 8; values["LXXX"] = 80; values["DCCC"] = 800;
    values["IX"]   = 9; values["XC"]   = 90; values["CM"]   = 900;
                                             values["M"]    = 1000;
    int result = 0;
    std::string key = "";
    typedef std::map<std::string, int>::const_iterator map_const_iterator;

    map_const_iterator isKeyFound = values.find(romanNumber);

    if (isKeyFound != values.end()) {
        return values[romanNumber];
    }

    for (std::string::const_iterator it = romanNumber.begin(); it != romanNumber.end(); ++it) {

        if (*it != 'M') {

            key += *it;
            map_const_iterator keyIsFound = values.find(key);

            if (keyIsFound == values.end()) {

                key.pop_back();
                result += values[key];
                key.clear();
                --it;

            } //else if (*it == '\0') {
                //--it;
            //}


        } else {
            
            do {
                key += *it;
                ++it;
            } while (*it == 'M' && key != "CM");
            result += (key[0] == 'C') ? values[key] : count(key.begin(), key.end(), 'M') * 1000;
            key.clear();
            --it; /// get back beacause of for loop incr
        }

    }
    result += values[key];
    return result;
}

bool
RomanNumber::isValidRomNumber(const std::string& romanNumber)
{
    for (std::string::const_iterator it = romanNumber.begin(); it != romanNumber.end(); ++it) {

        bool falseLatterExists = false;
        for (int i = 0; i < ROMAN_CHARACTERS_COUNT; ++i) {

            if (*it == symbol[i]) {
                falseLatterExists = true;
            }
        }
        if (!falseLatterExists) return falseLatterExists;
    }
//    for (std::string::const_iterator it = romanNumber.begin(); it != romanNumber.end(); ++it) {
//        std::string temp = "";


    return true;
}


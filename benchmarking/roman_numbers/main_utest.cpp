#include "headers/roman_numbers.hpp"
#include <gtest/gtest.h>

TEST(RomanNumberTest, DefaultConstructor)
{
    RomanNumber num;
    EXPECT_EQ(num.getNumber(), "I");
}

TEST(RomanNumberTest, ShortConstructor)
{
    RomanNumber num1(123);
    EXPECT_EQ(num1.getNumber(), "CXXIII");
    RomanNumber num2(32);
    EXPECT_EQ(num2.getNumber(), "XXXII");
    RomanNumber num3(45);
    EXPECT_EQ(num3.getNumber(), "XLV");
    RomanNumber num4(99);
    EXPECT_EQ(num4.getNumber(), "XCIX");
    RomanNumber num5(4);
    EXPECT_EQ(num5.getNumber(), "IV");
}

TEST(RomanNumberTest, SetNumber)
{
    RomanNumber num;
    num.setNumber(11220);
    EXPECT_EQ(num.getNumber(), "MMMMMMMMMMMCCXX");
}

TEST(RomanNumberTest, CrashTest)
{
    RomanNumber num(-23);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


#include "headers/roman_numbers.hpp"
#include <benchmark/benchmark.h>

static void
BM_example(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    while (state.KeepRunning()) {
        std::cout << "Hello World" << std::endl;
    }
    std::cout.clear();
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_example);

BENCHMARK_MAIN();


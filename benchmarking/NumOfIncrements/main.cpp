#include "headers/NumberOfIncrements.hpp"
#include <iostream>

int
main()
{
    const size_t SIZE = 3;
    int intArray[SIZE] = { 2, 1, 7 };

    const int count = NumberOfIncrements(intArray, SIZE);
    std::cout << "Number of incrments " << count << std::endl;
    return 0;
}


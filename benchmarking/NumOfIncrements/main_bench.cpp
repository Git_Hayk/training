#include "headers/NumberOfIncrements.hpp"
#include <benchmark/benchmark.h>

const size_t SIZE = 2<<10;

static void
BM_num_of_incrs(benchmark::State& state)
{
    int intArray[SIZE];
    int init = static_cast<int>(SIZE);
    for (size_t i = 0; i < SIZE; ++i, --init) {
        intArray[i] = init;
    }
    while (state.KeepRunning()) {
        NumberOfIncrements(intArray, SIZE);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_num_of_incrs);

BENCHMARK_MAIN();


#include "headers/NumberOfIncrements.hpp"
#include <iostream>


template <typename T>
int
NumberOfIncrements(T* arr, const size_t size)
{
    int numOfIncrs = 0;
    for (size_t i = 1; i < size; ++i) {
        if (arr[i] <= arr[i - 1]) {
            numOfIncrs += arr[i - 1] - arr[i] + 1;
            arr[i] += arr[i - 1] - arr[i] + 1;
        }
    }
    return numOfIncrs;
}


#ifndef __NUMOFINCRS_HPP__
#define __NUMOFINCRS_HPP__

#include <cstddef>

template <typename T>
int NumberOfIncrements(T* arr, const size_t size);

#include "sources/NumberOfIncrements.cpp"

#endif ///__NUMOFINCRS_HPP__


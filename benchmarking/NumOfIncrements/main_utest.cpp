#include "headers/NumberOfIncrements.hpp"
#include <gtest/gtest.h>

TEST(Algorithm, Increments)
{
    const size_t SIZE = 10;
    int intArray[SIZE] = { 5, 4, 1, 10, 7 , 3, 8, 9, 6, 2 };
    int count = NumberOfIncrements(intArray, SIZE);
    EXPECT_EQ(count, 54);
}

int
main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


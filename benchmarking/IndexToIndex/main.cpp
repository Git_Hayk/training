#include "headers/IndexToIndex.hpp"
#include <iostream>

template <typename T>
void printArray(const T* arr, const size_t size);

int
main()
{
    const size_t SIZE1 = 4;
    int input1[SIZE1] = { 2, 3, 1, 0 };
    printArray(input1, SIZE1);
    IndexToIndex(input1, SIZE1);
    printArray(input1, SIZE1);
    IndexToIndex(input1, SIZE1);
    printArray(input1, SIZE1);

    const size_t SIZE2 = 6;
    int input2[SIZE2] = { 1, 5, 3, 0, 2, 4 };
    printArray(input2, SIZE2);
    IndexToIndex(input2, SIZE2);
    printArray(input2, SIZE2);
    IndexToIndex(input2, SIZE2);
    printArray(input2, SIZE2);
    return 0;
}

template <typename T>
void
printArray(const T* arr, const size_t size)
{
    std::cout << "{ ";
    for (size_t i = 0; i < size; ++i) {
        std::cout << arr[i] << (i == size - 1 ? " }" : ", ");
    }
    std::cout << '\n';
}


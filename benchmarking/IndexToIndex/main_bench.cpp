#include "headers/IndexToIndex.hpp"
#include <benchmark/benchmark.h>

const size_t SIZE = 3;

static void
BM_example(benchmark::State& state)
{
    std::cout.setstate(std::ios_base::failbit);
    int input[SIZE] = { 2, 0, 1 };
    while (state.KeepRunning()) {
        IndexToIndex(input, SIZE);
    }
    std::cout.clear();
    state.SetBytesProcessed(SIZE * state.iterations());
}
BENCHMARK(BM_example);

BENCHMARK_MAIN();


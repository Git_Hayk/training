#include "headers/IndexToIndex.hpp"

void
IndexToIndex(int* arr, const size_t size)
{
    for (size_t i = 0; i < size; ++i) {
        assert(arr[i] < static_cast<int>(size));
    }
    int temp[size];
    for (size_t index = 0; index < size; ++index) {
        temp[index] = arr[index];
    }
    for (size_t j = 0; j < size; ++j) {
        arr[j] = temp[temp[j]];
    }
}


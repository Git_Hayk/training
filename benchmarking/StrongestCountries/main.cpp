#include "headers/StrongestCountries.hpp"
#include <iostream>

void printArray(const std::string* arr, const size_t size);

int
main()
{
    const size_t COUNTRIES = 5;
    const size_t RESIDENTS = 8;

    Resident array[COUNTRIES][RESIDENTS] = { { T, T, T, F, F, T, T, F },
                                            { T, T, T, T, T, T, T, F },
                                            { T, F, F, F, F, T, T, F },
                                            { T, T, F, F, F, T, F, F },
                                            { T, T, T, F, F, F, F, F }, };

    std::string countries[COUNTRIES] = { "Russia", "China", "Armenia", "Italia", "Egypt" };
    
    printArray(countries, COUNTRIES);
    sortCountries(array, countries);
    printArray(countries, COUNTRIES);

    return 0;
}

void
printArray(const std::string* arr, const size_t size)
{
    for (size_t i = 0; i < size; ++i) {
        std::cout << arr[i] << (i == size - 1 ? "" : ", ");
    }
    std::cout << '\n';
}


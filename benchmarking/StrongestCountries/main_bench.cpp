#include "headers/StrongestCountries.hpp"
#include <benchmark/benchmark.h>
#include <iostream>

const size_t COUNTRIES = 5;
const size_t RESIDENTS = 8;

static void
BM_sort_countries(benchmark::State& state)
{
    Resident array[COUNTRIES][RESIDENTS] = { { T, T, T, F, F, T, T, F },
                                            { T, T, T, T, T, T, T, F },
                                            { T, F, F, F, F, T, T, F },
                                            { T, T, F, F, F, T, F, F },
                                            { T, T, T, F, F, F, F, F }, };
    std::string countries[COUNTRIES] = { "Russia", "China", "Armenia", "Italia", "Egypt" };
    while (state.KeepRunning()) {
        sortCountries(array, countries);
    }
    state.SetBytesProcessed(state.iterations());
}
BENCHMARK(BM_sort_countries);

BENCHMARK_MAIN();


#include "headers/StrongestCountries.hpp"
#include <iostream>
#include <limits.h>

template <size_t countryCount, size_t residentCount>
void
sortCountries(const Resident (&arr)[countryCount][residentCount], std::string* countries)
{
    size_t temp[countryCount] = {0};
    for (size_t row = 0; row < countryCount; ++row) {
        for (size_t column = 0; column < residentCount; ++column) {
            if (F == arr[row][column]) {
                ++temp[row];
            }
        }
    }
    bubbleSort(temp, countries, countryCount);
}

void
bubbleSort(size_t* arr, std::string* countries, const size_t size)
{
    for (size_t i = 1; i < size + 1; ++i) {
        bool swapped = false;
        for (size_t j = 0; j < size - i; ++j) {
            if (arr[j] < arr[j + 1]) {
                std::swap(arr[j + 1], arr[j]);
                std::swap(countries[j + 1], countries[j]);
                swapped = true;
            }
        }
        if (!swapped) break;
    }
}


#ifndef __STRONGESTCOUNTRIES_HPP__
#define __STRONGESTCOUNTRIES_HPP__

#include <cstddef>
#include <string>

enum Resident { F, T };

template <size_t countryCount, size_t residentCount>
void sortCountries(const Resident (&arr)[countryCount][residentCount], std::string* countries); /// , const size_t residentCount, const size_t countryCount);
void bubbleSort(size_t* arr, std::string* countries, const size_t size);

#include "sources/StrongestCountries.cpp"

#endif ///__STRONGESTCOUNTRIES_HPP__

